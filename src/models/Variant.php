<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetImage;
use dzlab\commerce\models\_base\Variant as BaseVariant;
use dzlab\commerce\models\LineItem;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\ProductOption;
use dzlab\commerce\models\ProductPrice;
use user\models\User;
use Yii;

/**
 * Variant model class for "commerce_variant" database table
 *
 * Columns in table "commerce_variant" available as properties of the model,
 * followed by relations of table "commerce_variant" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $variant_id
 * @property integer $product_id
 * @property integer $default_option_id
 * @property string $sku
 * @property string $ean_code
 * @property integer $is_default
 * @property double $base_price
 * @property integer $total_stock
 * @property integer $is_unlimited_stock
 * @property integer $image_id
 * @property integer $weight
 * @property integer $available_from_date
 * @property integer $available_to_date
 * @property integer $is_available
 * @property integer $is_disabled
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $lineItems
 * @property mixed $products
 * @property mixed $createdUser
 * @property mixed $defaultOption
 * @property mixed $disableUser
 * @property mixed $image
 * @property mixed $product
 * @property mixed $updatedUser
 */
class Variant extends BaseVariant
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['product_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['product_id, default_option_id, is_default, total_stock, is_unlimited_stock, image_id, weight, available_from_date, available_to_date, is_available, is_disabled, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['base_price', 'numerical'],
			['sku', 'length', 'max'=> 128],
			['ean_code', 'length', 'max'=> 16],
			['uuid', 'length', 'max'=> 36],
			['default_option_id, sku, ean_code, is_default, base_price, total_stock, is_unlimited_stock, image_id, weight, available_from_date, available_to_date, is_available, is_disabled, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['variant_id, product_id, default_option_id, sku, ean_code, is_default, base_price, total_stock, is_unlimited_stock, image_id, weight, available_from_date, available_to_date, is_available, is_disabled, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'product' => [self::BELONGS_TO, Product::class, 'product_id'],
            'defaultOption' => [self::BELONGS_TO, ProductOption::class, 'default_option_id'],
            'image' => [self::BELONGS_TO, AssetImage::class, 'image_id'],
			'lineItems' => [self::HAS_MANY, LineItem::class, 'variant_id'],
            'prices' => [self::HAS_MANY, ProductPrice::class, 'variant_id'],
            'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
            // 'products' => [self::HAS_MANY, Product::class, 'default_variant_id'],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'available_from_date' => 'd/m/Y - H:i',
                    'available_to_date' => 'd/m/Y - H:i',
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

            // Currency format
            'CurrencyBehavior' => [
                'class' => '\dz\behaviors\CurrencyBehavior',
                'columns' => [
                    'base_price',
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'variant_id' => Yii::t('app', 'Variant'),
			'product_id' => null,
			'default_option_id' => null,
			'sku' => Yii::t('app', 'SKU'),
			'ean_code' => Yii::t('app', 'EAN code'),
			'is_default' => Yii::t('app', 'Is Default'),
			'base_price' => Yii::t('app', 'Base Price'),
			'total_stock' => Yii::t('app', 'Total Stock'),
			'is_unlimited_stock' => Yii::t('app', 'Is Unlimited Stock'),
            'weight' => Yii::t('app', 'Search Weight'),
			'image_id' => Yii::t('app', 'Image'),
			'available_from_date' => Yii::t('app', 'Available From Date'),
			'available_to_date' => Yii::t('app', 'Available To Date'),
			'is_available' => Yii::t('app', 'Is Available'),
			'is_disabled' => Yii::t('app', 'Is Disabled'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'lineItems' => null,
			'products' => null,
			'createdUser' => null,
			'defaultOption' => null,
			'disableUser' => null,
			'image' => null,
			'product' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.variant_id', $this->variant_id);
        $criteria->compare('t.sku', $this->sku, true);
        $criteria->compare('t.ean_code', $this->ean_code, true);
        $criteria->compare('t.is_default', $this->is_default);
        $criteria->compare('t.base_price', $this->base_price);
        $criteria->compare('t.total_stock', $this->total_stock);
        $criteria->compare('t.is_unlimited_stock', $this->is_unlimited_stock);
        $criteria->compare('t.available_from_date', $this->available_from_date);
        $criteria->compare('t.available_to_date', $this->available_to_date);
        $criteria->compare('t.is_available', $this->is_available);
        $criteria->compare('t.is_disabled', $this->is_disabled);
        $criteria->compare('t.disable_date', $this->disable_date);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['variant_id' => true]]
        ]);
    }


    /**
     * Variant models list
     * 
     * @return array
     */
    public function variant_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['variant_id', 'uuid'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Variant::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('variant_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Find a Variant model by SKU
     */
    public function findBySKU($sku)
    {
        return $this->findByAttributes([
            'sku'  => $sku
        ]);
    }


    /**
     * Check if it's the default variant of the product
     */
    public function is_default()
    {
        return ( $this->product && $this->product->default_variant_id == $this->variant_id );
    }


    /**
     * Add an AssetImage model to this product
     */
    public function add_image($image_model)
    {
        // Upload image
        $image_destination_path = $image_model->getImagesPath() . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR;
        $this->upload_image('image_id', $image_destination_path);

        // Create/update ProductImage model
        if ( !empty($this->image_id) )
        {
            $product_image_model = ProductImage::get()->where([
                'product_id' => $this->product_id,
                'image_id'  => $this->image_id
            ])->one();

            if ( ! $product_image_model )
            {
                $product_image_model = Yii::createObject(ProductImage::class);
                $product_image_model->setAttributes([
                    'product_id'    => $this->product_id,
                    'image_id'      => $this->image_id,
                    'is_default'    => 1,
                ]);
            }
            $product_image_model->variant_id = $this->variant_id;
            if ( ! $product_image_model->save() )
            {
                Log::save_model_error($product_image_model);
            }
        }

        return true;
    }


    /**
     * Get prices models indexed by alias
     */
    public function get_price_models()
    {
        $vec_price_models = [];
        if ( $this->prices )
        {
            foreach ( $this->prices as $product_price_model )
            {
                if ( $product_price_model->price )
                {
                    $vec_price_models[$product_price_model->price->alias] = $product_price_model;
                }
            }
        }

        return $vec_price_models;
    }


    /**
     * Return a ProductPrice model by alias
     */
    public function get_price_model($price_alias)
    {
        $vec_price_models = $this->get_price_models();
        if ( !empty($vec_price_models) && isset($vec_price_models[$price_alias]) )
        {
            return $vec_price_models[$price_alias];
        }

        return null;
    }


    /**
     * Get current selling price
     */
    public function get_price($is_raw = false)
    {
        $vec_price_models = $this->get_price_models();
        if ( $this->is_on_offer == 1 && isset($vec_price_models['reduced_price']) && $vec_price_models['reduced_price']->raw_attributes['amount'] > 0 )
        {
            if ( ! $is_raw )
            {
                return $vec_price_models['reduced_price']->amount;
            }
            return (float)$vec_price_models['reduced_price']->raw_attributes['amount'];
        }
        else if ( isset($vec_price_models['base_price']) && $vec_price_models['base_price']->raw_attributes['amount'] > 0 )
        {
            if ( ! $is_raw )
            {
                return $vec_price_models['base_price']->amount;
            }
            return (float)$vec_price_models['base_price']->raw_attributes['amount'];
        }

        return 0;
    }


    /**
     * Save a ProductPrice model
     */
    public function save_price($product_price_model)
    {
        $product_price_model->product_id = $this->product_id;
        $product_price_model->variant_id = $this->variant_id;

        // Amount cannot be NULL or EMPTY. Force to be a 0
        if ( empty($product_price_model->amount) )
        {
            $product_price_model->amount = 0;
        }

        if ( ! $product_price_model->save() )
        {
            Log::save_model_error($product_price_model);
            return false;
        }

        // Base price?
        if ( $product_price_model->price_id == 1 )
        {
            $product_price_model = ProductPrice::model()->cache(0)->findByAttributes([
                'price_id'      => $product_price_model->price_id,
                'variant_id'    => $this->variant_id,
            ]);
            $this->base_price = $product_price_model->amount;
            $this->saveAttributes(['base_price' => (float)$product_price_model->raw_attributes['amount']]);
        }

        return true;
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->sku;
    }
}