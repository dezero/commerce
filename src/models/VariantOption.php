<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\VariantOption as BaseVariantOption;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\ProductOption;
use dzlab\commerce\models\Variant;
use user\models\User;
use Yii;

/**
 * VariantOption model class for "commerce_variant_option" database table
 *
 * Columns in table "commerce_variant_option" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $variant_id
 * @property integer $option_id
 * @property integer $product_id
 * @property integer $created_date
 * @property integer $created_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class VariantOption extends BaseVariantOption
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['variant_id, option_id, product_id, created_date, created_uid', 'required'],
			['variant_id, option_id, product_id, created_date, created_uid', 'numerical', 'integerOnly' => true],
			['variant_id, option_id, product_id, created_date, created_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'variant' => [self::BELONGS_TO, Variant::class, 'variant_id'],
            'option' => [self::BELONGS_TO, ProductOption::class, 'option_id'],
            'product' => [self::BELONGS_TO, Product::class, 'product_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'variant_id' => null,
			'option_id' => null,
			'product_id' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.created_date', $this->created_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['variant_id' => true]]
        ]);
    }


    /**
     * VariantOption models list
     * 
     * @return array
     */
    public function variantoption_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['variant_id', 'variant_id', 'option_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = VariantOption::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('variant_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}