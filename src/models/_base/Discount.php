<?php
/**
 * @package dzlab\commerce\models\_base
 */

namespace dzlab\commerce\models\_base;

use dz\db\ActiveRecord;
use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use user\models\User;
use Yii;

/**
 * DO NOT MODIFY THIS FILE! It is automatically generated by Gii.
 * If any changes are necessary, you must set or override the required
 * property or method in class "dzlab\commerce\src\models\Discount".
 *
 * This is the model base class for the table "commerce_discount".
 * Columns in table "commerce_discount" available as properties of the model,
 * followed by relations of table "commerce_discount" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $discount_id
 * @property string $name
 * @property string $code
 * @property string $discount_type
 * @property integer $is_active
 * @property integer $limit_uses
 * @property integer $is_per_user_limit
 * @property integer $total_uses
 * @property integer $start_date
 * @property integer $expiration_date
 * @property double $amount
 * @property integer $is_fixed_amount
 * @property string $application_type
 * @property integer $is_free_shipping
 * @property double $minimum_amount
 * @property integer $is_category_restriction
 * @property string $category_restriction_type
 * @property integer $is_product_restriction
 * @property string $product_restriction_type
 * @property integer $is_user_restriction
 * @property string $user_restriction_type
 * @property string $user_restriction_values
 * @property integer $is_after_order_restriction
 * @property integer $after_order_num_days
 * @property string $internal_comments
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $disableUser
 * @property mixed $updatedUser
 * @property mixed $discountUses
 * @property mixed $lineItems
 * @property mixed $orders
 */
abstract class Discount extends ActiveRecord
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * Returns the name of the associated database table
	 */
	public function tableName()
	{
		return 'commerce_discount';
	}


	/**
	 * Label with translation support (from GIIX)
	 */
	public static function label($n = 1)
	{
		return Yii::t('app', 'Discount|Discounts', $n);
	}


    /**
     * Returns the validation rules for attributes
     */
    public function rules()
    {
        return [
            ['name, created_date, created_uid, updated_date, updated_uid', 'required'],
            ['is_active, limit_uses, is_per_user_limit, total_uses, start_date, expiration_date, is_fixed_amount, is_free_shipping, is_category_restriction, is_product_restriction, is_user_restriction, is_after_order_restriction, after_order_num_days, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
            ['amount, minimum_amount', 'numerical'],
            ['name, code, user_restriction_values', 'length', 'max'=> 255],
            ['uuid', 'length', 'max'=> 36],
            ['discount_type', 'in', 'range' => ['code', 'auto']],
            ['application_type', 'in', 'range' => ['total', 'items', 'shipping']],
            ['category_restriction_type', 'in', 'range' => ['include', 'exclude']],
            ['product_restriction_type', 'in', 'range' => ['include', 'exclude']],
            ['user_restriction_type', 'in', 'range' => ['users', 'roles']],
            ['code, discount_type, is_active, limit_uses, is_per_user_limit, total_uses, start_date, expiration_date, amount, is_fixed_amount, application_type, is_free_shipping, minimum_amount, is_category_restriction, category_restriction_type, is_product_restriction, product_restriction_type, is_user_restriction, user_restriction_type, user_restriction_values, is_after_order_restriction, after_order_num_days, internal_comments, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
            ['internal_comments', 'safe'],
            ['discount_id, name, code, discount_type, is_active, limit_uses, is_per_user_limit, total_uses, start_date, expiration_date, amount, is_fixed_amount, application_type, is_free_shipping, minimum_amount, is_category_restriction, category_restriction_type, is_product_restriction, product_restriction_type, is_user_restriction, user_restriction_type, user_restriction_values, is_after_order_restriction, after_order_num_days, internal_comments, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
        ];
    }


    /**
     * Define relations with other objects
     *
     * There are four types of relations that may exist between two active record objects:
     *   - BELONGS_TO: e.g. a member belongs to a team;
     *   - HAS_ONE: e.g. a member has at most one profile;
     *   - HAS_MANY: e.g. a team has many members;
     *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
     */
    public function relations()
    {
        return [
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
            'discountUses' => [self::HAS_MANY, DiscountUse::class, 'discount_id'],
            'lineItems' => [self::HAS_MANY, LineItem::class, 'discount_id'],
            'orders' => [self::HAS_MANY, Order::class, 'discount_id'],
        ];
    }


    /**
     * Returns the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'discount_id' => Yii::t('app', 'Discount'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'discount_type' => Yii::t('app', 'Discount Type'),
            'is_active' => Yii::t('app', 'Is Active'),
            'limit_uses' => Yii::t('app', 'Limit Uses'),
            'is_per_user_limit' => Yii::t('app', 'Is Per User Limit'),
            'total_uses' => Yii::t('app', 'Total Uses'),
            'start_date' => Yii::t('app', 'Start Date'),
            'expiration_date' => Yii::t('app', 'Expiration Date'),
            'amount' => Yii::t('app', 'Amount'),
            'is_fixed_amount' => Yii::t('app', 'Is Fixed Amount'),
            'application_type' => Yii::t('app', 'Application Type'),
            'is_free_shipping' => Yii::t('app', 'Is Free Shipping'),
            'minimum_amount' => Yii::t('app', 'Minimum Amount'),
            'is_category_restriction' => Yii::t('app', 'Is Category Restriction'),
            'category_restriction_type' => Yii::t('app', 'Category Restriction Type'),
            'is_product_restriction' => Yii::t('app', 'Is Product Restriction'),
            'product_restriction_type' => Yii::t('app', 'Product Restriction Type'),
            'is_user_restriction' => Yii::t('app', 'Is User Restriction'),
            'user_restriction_type' => Yii::t('app', 'User Restriction Type'),
            'user_restriction_values' => Yii::t('app', 'User Restriction Values'),
            'is_after_order_restriction' => Yii::t('app', 'Is After Order Restriction'),
            'after_order_num_days' => Yii::t('app', 'After Order Num Days'),
            'internal_comments' => Yii::t('app', 'Internal Comments'),
            'disable_date' => Yii::t('app', 'Disable Date'),
            'disable_uid' => null,
            'created_date' => Yii::t('app', 'Created Date'),
            'created_uid' => null,
            'updated_date' => Yii::t('app', 'Updated Date'),
            'updated_uid' => null,
            'uuid' => Yii::t('app', 'Uuid'),
            'createdUser' => null,
            'disableUser' => null,
            'updatedUser' => null,
            'discountUses' => null,
            'lineItems' => null,
            'orders' => null,
        ];
    }


    /**
     * Get "discount_type" labels
     */
    public function discount_type_labels()
    {
        return [
            'code' => Yii::t('app', 'code'),
            'auto' => Yii::t('app', 'auto'),
        ];
    }


    /**
     * Get "discount_type" specific label
     */
    public function discount_type_label($discount_type = null)
    {
        if ( $discount_type === null )
        {
            $discount_type = $this->discount_type;
        }
        $vec_labels = $this->discount_type_labels();
        return isset($vec_labels[$discount_type]) ? $vec_labels[$discount_type] : '';
    }
    /**
     * Get "application_type" labels
     */
    public function application_type_labels()
    {
        return [
            'total' => Yii::t('app', 'total'),
            'items' => Yii::t('app', 'items'),
            'shipping' => Yii::t('app', 'shipping'),
        ];
    }


    /**
     * Get "application_type" specific label
     */
    public function application_type_label($application_type = null)
    {
        if ( $application_type === null )
        {
            $application_type = $this->application_type;
        }
        $vec_labels = $this->application_type_labels();
        return isset($vec_labels[$application_type]) ? $vec_labels[$application_type] : '';
    }
    /**
     * Get "category_restriction_type" labels
     */
    public function category_restriction_type_labels()
    {
        return [
            'include' => Yii::t('app', 'include'),
            'exclude' => Yii::t('app', 'exclude'),
        ];
    }


    /**
     * Get "category_restriction_type" specific label
     */
    public function category_restriction_type_label($category_restriction_type = null)
    {
        if ( $category_restriction_type === null )
        {
            $category_restriction_type = $this->category_restriction_type;
        }
        $vec_labels = $this->category_restriction_type_labels();
        return isset($vec_labels[$category_restriction_type]) ? $vec_labels[$category_restriction_type] : '';
    }
    /**
     * Get "product_restriction_type" labels
     */
    public function product_restriction_type_labels()
    {
        return [
            'include' => Yii::t('app', 'include'),
            'exclude' => Yii::t('app', 'exclude'),
        ];
    }


    /**
     * Get "product_restriction_type" specific label
     */
    public function product_restriction_type_label($product_restriction_type = null)
    {
        if ( $product_restriction_type === null )
        {
            $product_restriction_type = $this->product_restriction_type;
        }
        $vec_labels = $this->product_restriction_type_labels();
        return isset($vec_labels[$product_restriction_type]) ? $vec_labels[$product_restriction_type] : '';
    }
    /**
     * Get "user_restriction_type" labels
     */
    public function user_restriction_type_labels()
    {
        return [
            'users' => Yii::t('app', 'users'),
            'roles' => Yii::t('app', 'roles'),
        ];
    }


    /**
     * Get "user_restriction_type" specific label
     */
    public function user_restriction_type_label($user_restriction_type = null)
    {
        if ( $user_restriction_type === null )
        {
            $user_restriction_type = $this->user_restriction_type;
        }
        $vec_labels = $this->user_restriction_type_labels();
        return isset($vec_labels[$user_restriction_type]) ? $vec_labels[$user_restriction_type] : '';
    }


    /**
     * Discount models list
     * 
     * @return array
     */
    public function discount_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['discount_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Discount::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('discount_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


	/**
	 * Generate an ActiveDataProvider for search form of this model
	 *
	 * Used in CGridView
	 */
	public function search()
	{
		$criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

		$criteria->compare('t.discount_id', $this->discount_id);
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.code', $this->code, true);
		$criteria->compare('t.discount_type', $this->discount_type, true);
		$criteria->compare('t.is_active', $this->is_active);
		$criteria->compare('t.limit_uses', $this->limit_uses);
		$criteria->compare('t.is_per_user_limit', $this->is_per_user_limit);
		$criteria->compare('t.total_uses', $this->total_uses);
		$criteria->compare('t.start_date', $this->start_date);
		$criteria->compare('t.expiration_date', $this->expiration_date);
		$criteria->compare('t.amount', $this->amount);
		$criteria->compare('t.is_fixed_amount', $this->is_fixed_amount);
		$criteria->compare('t.application_type', $this->application_type, true);
		$criteria->compare('t.is_free_shipping', $this->is_free_shipping);
		$criteria->compare('t.minimum_amount', $this->minimum_amount);
		$criteria->compare('t.is_category_restriction', $this->is_category_restriction);
		$criteria->compare('t.category_restriction_type', $this->category_restriction_type, true);
		$criteria->compare('t.is_product_restriction', $this->is_product_restriction);
		$criteria->compare('t.product_restriction_type', $this->product_restriction_type, true);
		$criteria->compare('t.is_user_restriction', $this->is_user_restriction);
		$criteria->compare('t.user_restriction_type', $this->user_restriction_type, true);
		$criteria->compare('t.user_restriction_values', $this->user_restriction_values, true);
		$criteria->compare('t.is_after_order_restriction', $this->is_after_order_restriction);
		$criteria->compare('t.after_order_num_days', $this->after_order_num_days);
		$criteria->compare('t.internal_comments', $this->internal_comments, true);
		$criteria->compare('t.disable_date', $this->disable_date);
		$criteria->compare('t.created_date', $this->created_date);
		$criteria->compare('t.updated_date', $this->updated_date);
		$criteria->compare('t.uuid', $this->uuid, true);

		return new \CActiveDataProvider($this, [
			'criteria' => $criteria,
			'pagination' => ['pageSize' => 30],
			'sort' => ['defaultOrder' => ['discount_id' => true]]
		]);
	}
	
	
	/**
	 * Returns fields not showed in create/update forms
	 */
	public function excludedFormFields()
	{
		return [
			'created_date' => true, 
			'created_uid' => true, 
			'updated_date' => true, 
			'updated_uid' => true, 
		];
	}


    /**
     * Automatic string representation for a model (from GIIX)
     */
    public static function representingColumn()
    {
        return 'name';
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->name;
    }
}
