<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dz\modules\settings\models\MailHistory;
use dzlab\commerce\models\_base\Customer as BaseCustomer;
use dzlab\commerce\models\CustomerAddress;
use dzlab\commerce\models\Order;
use user\models\User;
use Yii;

/**
 * Customer model class for "commerce_customer" database table
 *
 * Columns in table "commerce_customer" available as properties of the model,
 * followed by relations of table "commerce_customer" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $user_id
 * @property string $email
 * @property integer $billing_address_id
 * @property integer $shipping_address_id
 * @property integer $last_paid_order_id
 * @property integer $is_newsletter
 * @property integer $is_receive_invoice
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $billingAddress
 * @property mixed $createdUser
 * @property mixed $shippingAddress
 * @property mixed $updatedUser
 * @property mixed $user
 */
class Customer extends BaseCustomer
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';


    /**
     * Search filter by firstname or lastname
     */
    public $name_filter;    


    /**
     * Search filter by phone
     */
    public $phone_filter;


    /**
     * Search filter by status
     */
    public $status_filter;


    /**
     * Search filter by VAT code filter
     */
    public $vat_filter;


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['user_id, email, created_date, created_uid, updated_date, updated_uid', 'required'],
			['user_id, billing_address_id, shipping_address_id, last_paid_order_id, is_newsletter, is_receive_invoice, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
            ['email', 'length', 'max'=> 255],
            ['email', 'email'],
			['uuid', 'length', 'max'=> 36],
			['billing_address_id, shipping_address_id, last_paid_order_id, is_newsletter, is_receive_invoice, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['user_id, email, billing_address_id, shipping_address_id, last_paid_order_id, is_newsletter, is_receive_invoice, created_date, created_uid, updated_date, updated_uid, uuid, name_filter, phone_filter, status_filter, vat_filter, global_search_filter', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'user' => [self::BELONGS_TO, User::class, 'user_id'],
			'billingAddress' => [self::BELONGS_TO, CustomerAddress::class, 'billing_address_id'],
			'shippingAddress' => [self::BELONGS_TO, CustomerAddress::class, 'shipping_address_id'],
            'lastOrder' => [self::BELONGS_TO, Order::class, ['last_paid_order_id' => 'order_id']],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
            'cartCompletedOrders' => [self::HAS_MANY, Order::class, ['user_id' => 'user_id'], 'condition' => 'cartCompletedOrders.is_cart_completed = 1', 'order' => 'cartCompletedOrders.paid_date DESC'],
            'orders' => [self::HAS_MANY, Order::class, ['user_id' => 'user_id'], 'order' => 'orders.paid_date DESC'],
            'mailHistory' => [self::HAS_MANY, MailHistory::class, ['recipient_uid' => 'user_id'], 'order' => 'mailHistory.history_id DESC'],
            // 'lastOrder' => [self::BELONGS_TO, Order::class, ['user_id' => 'user_id'], 'condition' => 'lastOrder.is_cart_completed = 1', 'order' => 'lastOrder.paid_date DESC'],
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'user_id' => null,
            'email' => Yii::t('app', 'Email'),
			'billing_address_id' => Yii::t('app', 'Billing Address'),
			'shipping_address_id' => Yii::t('app', 'Shipping Address'),
            'last_paid_order_id' => Yii::t('app', 'Last Paid Order'),
			'is_newsletter' => Yii::t('app', 'Newsletter?'),
            'is_receive_invoice' => Yii::t('app', 'Is Receive Invoice?'),
			'created_date' => Yii::t('app', 'Register Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'billingAddress' => null,
			'createdUser' => null,
			'shippingAddress' => null,
			'updatedUser' => null,
			'user' => null,

            // Search filters
            'name_filter' => Yii::t('app', 'Name'),
            'phone_filter' => Yii::t('app', 'Phone'),
            'status_filter' => Yii::t('app', 'Status'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search($search_id = null)
    {
        $criteria = new DbCriteria;

        // $criteria->together = true;
        $criteria->with = ['user'];
        $criteria->addCondition('user.firstname IS NOT NULL');

        // Filter by email
        $criteria->compare('t.email', $this->email, true);

        // Filter by name (user.firstname OR user.lastname)
        if ( $this->name_filter )
        {
            $criteria->addCondition('user.firstname LIKE CONCAT(:name_filter, "%") OR user.lastname LIKE CONCAT(:name_filter, "%") OR CONCAT(user.firstname, " ", user.lastname) LIKE CONCAT(:name_filter, "%")');
            $criteria->addParams([':name_filter' => $this->name_filter]);
        }

        // Filter by phone (customer_address.phone)
        if ( $this->phone_filter )
        {
            $criteria->with['billingAddress'] = 'billingAddress';
            $criteria->with['shippingAddress'] = 'shippingAddress';
            $criteria->addCondition('billingAddress.phone LIKE CONCAT(:phone_filter, "%") OR shippingAddress.phone LIKE CONCAT(:phone_filter, "%")');
            $criteria->addParams([':phone_filter' => $this->phone_filter]);
        }

        // Filter by status (user.status_type)
        if ( $this->status_filter )
        {
            $criteria->compare('user.status_type', $this->status_filter);
        }

        // Filter by VAT code (customer_address.vat_code)
        if ( $this->vat_filter )
        {
            $criteria->with['billingAddress'] = 'billingAddress';
            $criteria->with['shippingAddress'] = 'shippingAddress';
            $criteria->addCondition('billingAddress.vat_code LIKE CONCAT(:vat_filter, "%") OR shippingAddress.vat_code LIKE CONCAT(:vat_filter, "%")');
            $criteria->addParams([':vat_filter' => $this->vat_filter]);
        }

        // Filter by "is_newsletter"
        $criteria->compare('t.is_newsletter', $this->is_newsletter);

        // Filter by "is_receive_invoice"
        $criteria->compare('t.is_receive_invoice', $this->is_receive_invoice);

        // Filter by "last_paid_order_id"
        $criteria->compare('t.last_paid_order_id', $this->last_paid_order_id);

        // Global search filter
        if ( $this->global_search_filter )
        {
            // Search on relationed tables
            $criteria->with['user'] = 'user';
            $criteria->with['billingAddress'] = 'billingAddress';
            $criteria->with['shippingAddress'] = 'shippingAddress';
            // $criteria->together = true;
            $criteria->distinct = true;

            // Search on text columns
            $vec_textfield = [
                't.email',
                'user.firstname', 'user.lastname',
                'billingAddress.firstname', 'shippingAddress.lastname', 'billingAddress.vat_code', 'billingAddress.address_line', 'billingAddress.city', 'billingAddress.postal_code', 'billingAddress.phone',
                'shippingAddress.firstname', 'billingAddress.lastname', 'shippingAddress.vat_code', 'shippingAddress.address_line', 'shippingAddress.city', 'shippingAddress.postal_code', 'shippingAddress.phone',
            ];

            // Build SQL condition
            $sql_condition = '';
            foreach ( $vec_textfield as $que_textfield )
            {
                if ( !empty($sql_condition) )
                {
                    $sql_condition .= " OR ";
                }
                $sql_condition .= $que_textfield ." LIKE CONCAT('%', :global_search , '%')";
            }
            $criteria->addCondition($sql_condition);
            $criteria->params[':global_search'] = $this->global_search_filter;
        }

        // Limit 30 rows per page by default
        $page_size = 30;

        // "Unlimited" paging for export excel action
        if ( $search_id == 'excel' )
        {
            $page_size = 100000;

            // Force "ORDER BY"
            $criteria->order = 't.user_id DESC';
        }

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => $page_size],
            'sort' => ['defaultOrder' => ['user_id' => true]]
        ]);
    }


    /**
     * Customer models list
     * 
     * @return array
     */
    public function customer_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['user_id', 'email'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Customer::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('user_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_DISABLED => Yii::t('app', 'Disabled'),
        ];
    }


    /**
     * Called after the order successfully completes
     * 
     * Sets the last used addresses on the customer on order completion.
     * Duplicates the address records used for the order so they are independent to the
     * customers address book.
     */
    public function after_complete_order($order_model)
    {
        $user_firstname = '';
        $user_lastname = '';

        // Shipping address
        if ( $order_model->shippingAddress )
        {
            // Clone shipping address
            $snapshot_address_model = $this->_copy_address($order_model->shippingAddress);
            if ( $snapshot_address_model )
            {
                // Use new cloned BILLING address as default for the costumer
                $this->shipping_address_id = $snapshot_address_model->address_id;
            }

            // Set "order_id" to the shipping address -> Mark as INDEPENDENT to the customer address book
            $order_model->shippingAddress->order_id = $order_model->order_id;
            $order_model->shippingAddress->saveAttributes([
                'order_id' => $order_model->order_id
            ]);

            // Set user firstname and lastname
            $user_firstname = $order_model->shippingAddress->firstname;
            $user_lastname = $order_model->shippingAddress->lastname;
        }

        // Billing address
        if ( $order_model->billingAddress && $order_model->shipping_address_id !== $order_model->billing_address_id )
        {
            // Clone billing address
            $snapshot_address_model = $this->_copy_address($order_model->billingAddress);
            if ( $snapshot_address_model )
            {
                // Use new cloned BILLING address as default for the costumer
                $this->billing_address_id = $snapshot_address_model->address_id;
            }

            // Set "order_id" to the billing address -> Mark as INDEPENDENT to the customer address book
            $order_model->billingAddress->order_id = $order_model->order_id;
            $order_model->billingAddress->saveAttributes([
                'order_id' => $order_model->order_id
            ]);

            // Set user firstname and lastname
            $user_firstname = $order_model->billingAddress->firstname;
            $user_lastname = $order_model->billingAddress->lastname;
        }

        // Set last paid order
        $this->last_paid_order_id = $order_model->order_id;

        // Save Customer model with the last used addresses as the default ones
        if ( ! $this->save() )
        {
            Log::save_model_error($this);

            return false;
        }

        // Save firstname and lastname of User model
        $this->save_fullname($user_firstname, $user_lastname);

        return true;
    }


    /**
     * Save customer fullname
     */
    public function save_fullname($firstname = '', $lastname = '')
    {
        if ( !empty($firstname) && $this->user )
        {
            $this->user->setAttributes([
                'firstname' => $firstname,
                'lastname'  => !empty($lastname) ? $lastname : $this->user->lastname
            ]);

            if ( ! $this->user()->save() )
            {
                Log::save_model_error($this);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Get full name: first name + last name
     */
    public function fullname()
    {
        if ( $this->user )
        {
            return $this->user->fullname();
        }

        return '';
    }


    /**
     * Send email using a mailing template
     */
    public function send_email($template_mail, $recipient_email = null)
    {
        if ( $recipient_email === null )
        {
            $recipient_email = $this->email;
        }

        $sending_result = Yii::app()->mail
            ->setTo($recipient_email)
            ->addModel($this->user, 'user')
            ->addModel($this, 'customer')
            ->setEntityInfo($this->user_id, 'Customer')
            ->compose($template_mail)
            ->send();

        // Save error into LOG
        if ( ! $sending_result )
        {
            Yii::app()->mail->logError();
        }

        return $sending_result;
    }


    /**
     * Returns the mail tokens
     */
    public function mailTokens()
    {
        return [
            '{customer.firstname}'      => $this->user->firstname,
            '{customer.lastname}'       => $this->user->lastname,
            '{customer.fullname}'       => $this->fullname(),
            '{customer.email}'          => $this->email,
            '{customer.account_url}'    => Url::to('/profile'),
            '{customer.orders_url}'     => Url::to('/profile/orders')
        ];
    }


    /**
     * Help text for the mail tokens
     */
    public function mailHelp()
    {
        return [
            '{customer.firstname}'      => 'Customer first name',
            '{customer.lastname}'       => 'Customer last name',
            '{customer.fullname}'       => 'Customer full name',
            '{customer.email}'          => 'Customer email',
            '{customer.account_url}'    => 'Customer account URL ('. Url::to('/profile') .')',
            '{customer.orders_url}'     => 'Customer orders URL ('. Url::to('/profile/orders') .')',
        ];
    }


    /**
     * Clone or copy an address model
     */
    private function _copy_address($address_model)
    {
        // Create a new "cloned" address
        $snapshot_address_model = Yii::createObject(CustomerAddress::class);
        
        // Copy attributes
        $vec_address_attributes = $address_model->getAttributes();
        unset($vec_address_attributes['address_id']);
        unset($vec_address_attributes['order_id']);
        unset($vec_address_attributes['created_date']);
        unset($vec_address_attributes['created_uid']);
        unset($vec_address_attributes['updated_date']);
        unset($vec_address_attributes['updated_uid']);
        unset($vec_address_attributes['uuid']);
        $snapshot_address_model->setAttributes($vec_address_attributes);
        
        if ( ! $snapshot_address_model->save() )
        {
            Log::save_model_error($snapshot_address_model);
            return false;
        }
        
        return $snapshot_address_model;
    }
}
