<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\category\models\Category;
use dzlab\commerce\models\_base\SearchProduct as BaseSearchProduct;
use dzlab\commerce\models\Product;
use dz\modules\settings\models\Language;
use user\models\User;
use Yii;

/**
 * SearchProduct model class for "search_product" database table
 *
 * Columns in table "search_product" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $product_id
 * @property string $language_id
 * @property string $product_type
 * @property string $default_sku
 * @property integer $is_disabled
 * @property string $name
 * @property string $content
 * @property integer $main_category_id
 * @property integer $brand_category_id
 * @property integer $weight
 * @property integer $updated_date
 * @property integer $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class SearchProduct extends BaseSearchProduct
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['product_id, name, updated_date, updated_uid', 'required'],
			['product_id, is_disabled, main_category_id, brand_category_id, weight, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['language_id', 'length', 'max'=> 4],
            ['product_type', 'length', 'max'=> 32],
			['name', 'length', 'max'=> 255],
            ['default_sku', 'length', 'max'=> 128],
			['language_id, product_type, default_sku, is_disabled, main_category_id, brand_category_id, weight, content', 'default', 'setOnEmpty' => true, 'value' => null],
			['content', 'safe'],
			['product_id, language_id, product_type, default_sku, is_disabled, main_category_id, brand_category_id, weight, name, content, updated_date, updated_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'product' => [self::BELONGS_TO, Product::class, 'product_id'],
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
            'mainCategory' => [self::BELONGS_TO, Category::class, 'main_category_id'],
            'brandCategory' => [self::BELONGS_TO, Category::class, 'brand_category_id'],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'product_id' => Yii::t('app', 'Product'),
			'language_id' => Yii::t('app', 'Language'),
            'product_type' => Yii::t('app', 'Product Type'),
            'default_sku' => Yii::t('app', 'Default SKU'),
			'is_disabled' => Yii::t('app', 'Disabled?'),
			'name' => Yii::t('app', 'Name'),
			'content' => Yii::t('app', 'Content'),
            'main_category_id' => Yii::t('app', 'Main Category'),
            'brand_category_id' => Yii::t('app', 'Brand Category'),
            'weight' => Yii::t('app', 'Weight'),
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.product_type', $this->product_type);
        $criteria->compare('t.default_sku', $this->default_sku);
        $criteria->compare('t.is_disabled', $this->is_disabled);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.content', $this->content, true);
        $criteria->compare('t.weight', $this->weight);
        $criteria->compare('t.updated_date', $this->updated_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['product_id' => true]]
        ]);
    }


    /**
     * SearchProduct models list
     * 
     * @return array
     */
    public function searchproduct_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['product_id', 'default_sku', 'language_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = SearchProduct::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('product_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}
