<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\LineItem as BaseLineItem;
use dzlab\commerce\models\Discount;
use dzlab\commerce\models\LineItemOption;
use dzlab\commerce\models\Order;
use dzlab\commerce\models\Price;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\ProductPrice;
use dzlab\commerce\models\Tax;
use dzlab\commerce\models\Variant;
use user\models\User;
use Yii;

/**
 * LineItem model class for "commerce_line_item" database table
 *
 * Columns in table "commerce_line_item" available as properties of the model,
 * followed by relations of table "commerce_line_item" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $line_item_id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $variant_id
 * @property string $sku
 * @property string $title
 * @property string $description
 * @property string $options_json
 * @property double $unit_product_price
 * @property double $unit_options_price
 * @property double $unit_price
 * @property integer $quantity
 * @property double $total_price
 * @property integer $price_id
 * @property string $price_alias
 * @property double $tax_price
 * @property integer $tax_id
 * @property integer $is_taxes_included
 * @property integer $discount_id
 * @property double $product_base_price
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $discount
 * @property mixed $order
 * @property mixed $price
 * @property mixed $product
 * @property mixed $tax
 * @property mixed $updatedUser
 * @property mixed $variant
 */
class LineItem extends BaseLineItem
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['order_id, product_id, variant_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['order_id, product_id, variant_id, quantity, price_id, tax_id, is_taxes_included, discount_id, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['unit_product_price, unit_options_price, unit_price, total_price, tax_price, product_base_price', 'numerical'],
			['sku', 'length', 'max'=> 128],
			['title', 'length', 'max'=> 255],
            ['price_alias', 'length', 'max'=> 32],
			['uuid', 'length', 'max'=> 36],
			['sku, title, description, options_json, unit_product_price, unit_options_price, unit_price, quantity, total_price, price_id, price_alias, tax_price, tax_id, is_taxes_included, discount_id, product_base_price, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['description, options_json', 'safe'],
			['line_item_id, order_id, product_id, variant_id, sku, title, description, options_json, unit_product_price, unit_options_price, unit_price, quantity, total_price, price_id, price_alias, tax_price, tax_id, is_taxes_included, discount_id, product_base_price, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'order' => [self::BELONGS_TO, Order::class, 'order_id'],
            'product' => [self::BELONGS_TO, Product::class, 'product_id'],
            'variant' => [self::BELONGS_TO, Variant::class, 'variant_id'],
            'price' => [self::BELONGS_TO, Price::class, 'price_id'],
            'productPrice' => [self::BELONGS_TO, ProductPrice::class, ['price_id' => 'price_id', 'variant_id' => 'variant_id']],
            'options' => [self::HAS_MANY, LineItemOption::class, 'line_item_id'],
            'discount' => [self::BELONGS_TO, Discount::class, 'discount_id'],
            'tax' => [self::BELONGS_TO, Tax::class, 'tax_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Currency format
            'CurrencyBehavior' => [
                'class' => '\dz\behaviors\CurrencyBehavior',
                'columns' => [
                    'unit_product_price',
                    'unit_options_price',
                    'unit_price',
                    'total_price',
                    'tax_price',
                    'product_base_price'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'line_item_id' => Yii::t('app', 'Line Item'),
			'order_id' => Yii::t('app', 'Order'),
			'product_id' => Yii::t('app', 'Product'),
			'variant_id' => Yii::t('app', 'Variant'),
			'sku' => Yii::t('app', 'Sku'),
			'title' => Yii::t('app', 'Title'),
			'description' => Yii::t('app', 'Description'),
			'options_json' => Yii::t('app', 'Options Json'),
            'unit_product_price' => Yii::t('app', 'Unit Product Price'),
            'unit_options_price' => Yii::t('app', 'Unit Options Price'),
			'unit_price' => Yii::t('app', 'Unit Price'),
			'quantity' => Yii::t('app', 'Quantity'),
			'total_price' => Yii::t('app', 'Total Price'),
            'price_id' => Yii::t('app', 'Price Type'),
            'price_alias' => Yii::t('app', 'Price Type'),
            'tax_price' => Yii::t('app', 'Taxes Price'),
            'tax_id' => Yii::t('app', 'Taxes'),
            'is_taxes_included' => Yii::t('app', 'Taxes Included?'),
            'discount_id' => Yii::t('app', 'Discount'),
            'product_base_price' => Yii::t('app', 'Product Base Price'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
            'discount' => null,
			'order' => null,
            'price' => null,
			'product' => null,
            'tax' => null,
			'updatedUser' => null,
			'variant' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search($search_id = null)
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        // Filter by ORDER
        if ( $this->order_id )
        {
            if ( is_array($this->order_id) )
            {
                $criteria->addInCondition('t.order_id', $this->order_id);
            }
            else
            {
                $criteria->compare('t.order_id', $this->order_id);   
            }
        }

        $criteria->compare('t.line_item_id', $this->line_item_id);
        $criteria->compare('t.sku', $this->sku, true);
        $criteria->compare('t.title', $this->title, true);
        $criteria->compare('t.description', $this->description, true);
        // $criteria->compare('t.options_json', $this->options_json, true);
        // $criteria->compare('t.unit_product_price', $this->unit_product_price);
        // $criteria->compare('t.unit_options_price', $this->unit_options_price);
        // $criteria->compare('t.unit_price', $this->unit_price);
        // $criteria->compare('t.quantity', $this->quantity);
        // $criteria->compare('t.total_price', $this->total_price);
        // $criteria->compare('t.price_id', $this->price_id);
        // $criteria->compare('t.price_alias', $this->price_alias, true);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid, true);

        // Limit 30 rows per page by default
        $page_size = 30;

        // "Unlimited" paging for export excel action
        if ( $search_id == 'excel' )
        {
            $page_size = 100000;

            // Force "ORDER BY"
            $criteria->order = 't.order_id ASC, t.line_item_id ASC';
        }

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => $page_size],
            'sort' => ['defaultOrder' => ['line_item_id' => true]]
        ]);
    }


    /**
     * LineItem models list
     * 
     * @return array
     */
    public function lineitem_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['line_item_id', 'uuid'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = LineItem::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('line_item_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Short reference
     */
    public function short_reference($is_uppercase = true)
    {
        if ( $this->order )
        {
            return $this->order->short_reference($is_uppercase);
        }

        return '';
    }


    /**
     * Get line position in the Order
     */
    public function get_line_position()
    {
        if ( $this->order && $this->order->lineItems )
        {
            foreach ( $this->order->lineItems as $num_line_item => $line_item_model )
            {
                if ( $line_item_model->line_item_id === $this->line_item_id )
                {
                    return $num_line_item + 1;
                }
            }
        }

        return 1;
    }


    /**
     * Add a ProductOption into a LineItem
     */
    public function add_product_option($option_id, $category_id = '', $quantity = 1)
    {
        $product_option_model = ProductOption::findOne($option_id);
        if ( $product_option_model && $product_option_model->is_enabled() )
        {
            $line_item_option_model = Yii::createObject(LineItemOption::class);
            $line_item_option_model->setAttributes([
                'line_item_id'  => $this->line_item_id,
                'option_id'     => $product_option_model->option_id,
                'order_id'      => $this->order_id,
                'option_type'   => $product_option_model->option_type,
                'unit_price'    => Yii::app()->number->formatNumber($product_option_model->raw_attributes['price']),
                'quantity'      => $quantity,
                'total_price'   => Yii::app()->number->formatNumber($product_option_model->raw_attributes['price'] * $quantity)
            ]);
            if ( ! $line_item_option_model->save() )
            {
                Log::save_model_error($product_option_model);
                return false;
            }

            return $line_item_option_model;
        }

        return false;
    }


    /**
     * Clean all product options
     */
    public function delete_product_options()
    {
        if ( $this->options )
        {
            foreach ( $this->options as $line_item_option_model )
            {
                $line_item_option_model->delete();
            }
        }

        return true;
    }


    /**
     * Add one or more quantity items
     */ 
    public function add_quantity($quantity = 1)
    {
        $new_quantity = $this->quantity + $quantity;
        return $this->change_quantity($new_quantity);
    }


    /**
     * Change quantity and update price
     */
    public function change_quantity($new_quantity, $is_save = false)
    {
        $vec_attributes = [
            'quantity'      => $new_quantity,
            'total_price'   => $this->raw_attributes['unit_price'] * $new_quantity
        ];
        $this->setAttributes($vec_attributes);
        
        if ( $is_save )
        {
            $this->saveAttributes($vec_attributes);
        }
        
        return true;
    }


    /**
     * Get total product price
     */
    public function get_total_product_price($quantity = 0, $is_raw = true)
    {
        if ( $quantity === 0 )
        {
            $quantity = $this->quantity;
        }

        $raw_total_price = $this->raw_attributes['unit_product_price'] * $quantity;
        
        return $is_raw ? $raw_total_price : Yii::app()->number->formatNumber($raw_total_price);
    }



    /**
     * Get total options price
     */
    public function get_total_options_price($quantity = 0, $is_raw = true)
    {
        if ( $quantity === 0 )
        {
            $quantity = $this->quantity;
        }

        $raw_total_price = $this->raw_attributes['unit_options_price'] * $quantity;
        
        return $is_raw ? $raw_total_price : Yii::app()->number->formatNumber($raw_total_price);
    }


    /**
     * Update line item prices
     */
    public function update_prices($is_save = true)
    {
        // ProductOption price
        $options_unit_price = 0;
        if ( $this->options )
        {
            foreach ( $this->options as $line_item_option_model )
            {
                $options_unit_price += $line_item_option_model->raw_attributes['unit_price'];
            }
        }
        $this->raw_attributes['unit_options_price'] = $options_unit_price;
        $this->unit_options_price = Yii::app()->number->formatNumber($options_unit_price);

        // Unit price
        $this->raw_attributes['unit_price'] = $this->raw_attributes['unit_options_price'] + $this->raw_attributes['unit_product_price'];
        $this->unit_price = Yii::app()->number->formatNumber($this->raw_attributes['unit_price']);

        // Total price
        $this->raw_attributes['total_price'] = $this->raw_attributes['unit_price'] * $this->quantity;
        $this->total_price = Yii::app()->number->formatNumber($this->raw_attributes['total_price']);

        // Tax price
        $this->raw_attributes['tax_price'] = $this->calculate_tax_price();
        $this->tax_price = Yii::app()->number->formatNumber($this->raw_attributes['tax_price']);

        if ( $is_save && ! $this->save() )
        {
            Log::save_model_error($this);
            return false;
        }

        return true;
    }


    /**
     * Calculate tax price
     */
    public function calculate_tax_price($raw_total_price = null)
    {
        if ( $raw_total_price === null )
        {
            $raw_total_price = $this->raw_attributes['total_price'];
        }

        if ( $this->tax && !empty($this->tax->percentage) )
        {
            return $raw_total_price - ( $raw_total_price / (1 + ($this->tax->percentage / 100)) );
        }

        return 0;
    }


    /**
     * Update tax price
     */
    public function update_tax_price($is_save = true)
    {
        // Tax price
        $this->raw_attributes['tax_price'] = $this->calculate_tax_price();
        $this->tax_price = Yii::app()->number->formatNumber($this->raw_attributes['tax_price']);

        if ( $is_save && ! $this->save() )
        {
            Log::save_model_error($this);
            return false;
        }

        return true;
    }


    /**
     * Add a discount to the application
     */
    public function add_discount($discount_model)
    {
        if ( $discount_model && $discount_model->is_valid($this->order) )
        {
            $this->discount_id = $discount_model->discount_id;
            $this->saveAttributes(['discount_id' => $this->discount_id]);

            return true;
        }

        return false;
    }


    /**
     * Remove any discount
     */
    public function remove_discount($is_save = true)
    {
        $this->discount = null;
        $this->discount_id = null;
        
        if ( $is_save )
        {
            $this->saveAttributes(['discount_id' => null]);
        }

        return true;
    }



    /**
     * Return the line item price with applied discount
     */
    public function get_price_with_discount($is_raw = false, $price_type = 'unit_price')
    {
        if ( $this->discount )
        {
            $discount_price_raw = $this->discount->apply_discount_price($this->raw_attributes[$price_type]);
            $final_price_raw = $this->raw_attributes[$price_type] - $discount_price_raw;
            if ( $final_price_raw > 0 )
            {
                return $is_raw ? $final_price_raw : Yii::app()->number->formatNumber($final_price_raw);
            }
        }

        return $is_raw ? $this->raw_attributes[$price_type] : $this->getAttribute($price_type);
    }


    /**
     * Return the line item UNIT PRICE with applied discount
     */
    public function get_unit_price_with_discount($is_raw = false)
    {
        return $this->get_price_with_discount($is_raw, 'unit_price');
    }


    /**
     * Return the line item TOTAL PRICE with applied discount
     */
    public function get_total_price_with_discount($is_raw = false)
    {
        return $this->get_price_with_discount($is_raw, 'total_price');
    }


    /**
     * Check if the Product or Variant related with the LineItem has been changed
     * from it was inserted into the order as line item
     *
     * @since 21/07/2022
     */
    public function is_product_or_variant_updated()
    {
        return (
            ( $this->variant && $this->variant->raw_attributes['updated_date'] > $this->raw_attributes['created_date'] ) ||
            ( $this->product && $this->product->raw_attributes['updated_date'] > $this->raw_attributes['created_date'] )
        );
    }



    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->title;
    }
}
