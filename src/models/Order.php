<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dzlab\commerce\models\_base\Order as BaseOrder;
use dzlab\commerce\models\Customer;
use dzlab\commerce\models\CustomerAddress;
use dzlab\commerce\models\Discount;
use dzlab\commerce\models\Gateway;
use dzlab\commerce\models\LineItem;
use dzlab\commerce\models\OrderHistory;
use dzlab\commerce\models\OrderJson;
use dzlab\commerce\models\Transaction;
use dzlab\commerce\models\ProductOption;
use dzlab\commerce\models\ShippingMethod;
use dz\modules\settings\models\Currency;
use dz\modules\settings\models\Language;
use dz\modules\settings\models\MailHistory;
use user\models\User;
use Yii;

/**
 * Order model class for "commerce_order" database table
 *
 * Columns in table "commerce_order" available as properties of the model,
 * followed by relations of table "commerce_order" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $order_id
 * @property string $order_reference
 * @property string $order_number
 * @property string $status_type
 * @property integer $user_id
 * @property string $email
 * @property integer $billing_address_id
 * @property integer $shipping_address_id
 * @property integer $is_billing_different
 * @property integer $is_cart_completed
 * @property integer $is_new_account
 * @property string $language_id
 * @property double $items_price
 * @property double $shipping_price
 * @property double $tax_price
 * @property double $discount_price
 * @property double $paid_price
 * @property double $total_price
 * @property string $currency_id
 * @property string $coupon_code
 * @property integer $discount_id
 * @property integer $gateway_id
 * @property string $gateway_alias
 * @property integer $transaction_id
 * @property integer $shipping_method_id
 * @property string $shipping_method_alias
 * @property integer $ordered_date
 * @property integer $paid_date
 * @property integer $is_receive_invoice
 * @property string $source_type
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $customerAddresses
 * @property mixed $lineItems
 * @property mixed $billingAddress
 * @property mixed $createdUser
 * @property mixed $currency
 * @property mixed $gateway
 * @property mixed $language
 * @property mixed $transaction
 * @property mixed $shippingAddress
 * @property mixed $shippingMethod
 * @property mixed $updatedUser
 * @property mixed $user
 * @property mixed $orderHistories
 * @property mixed $transactions
 */
class Order extends BaseOrder
{
    /**
     * Product filter
     */
    public $product_filter;


    /**
     * Send mail?
     */
    public $is_sending_mail = false;


    /**
     * Order date range filter - Date from
     */
    public $ordered_from_date;


    /**
     * Order date range filter - Date to
     */
    public $ordered_to_date;


    /**
     * Discount error message?
     */
    public $discount_error = '';


    /**
     * @var array Cached order status
     */
    private $_vec_statuses;


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['user_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['user_id, billing_address_id, shipping_address_id, is_billing_different, is_cart_completed, is_new_account, discount_id, gateway_id, transaction_id, shipping_method_id, ordered_date, paid_date, is_receive_invoice, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['items_price, shipping_price, tax_price, discount_price, paid_price, total_price', 'numerical'],
            ['order_reference, status_type, gateway_alias, shipping_method_alias', 'length', 'max'=> 32],
            ['order_number', 'length', 'max'=> 64],
            ['email', 'length', 'max'=> 255],
            ['email', 'email'],
			['language_id, currency_id', 'length', 'max'=> 4],
            ['coupon_code', 'length', 'max'=> 128],
			['uuid', 'length', 'max'=> 36],
			['source_type', 'in', 'range' => ['web', 'admin', 'external']],
			['order_reference, order_number, email, billing_address_id, shipping_address_id, is_billing_different, is_cart_completed, is_new_account, language_id, items_price, shipping_price, tax_price, discount_price, paid_price, total_price, currency_id, coupon_code, discount_id, gateway_id, gateway_alias, transaction_id, shipping_method_id, shipping_method_alias, ordered_date, paid_date, is_receive_invoice, source_type, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['order_id, order_reference, order_number, status_type, user_id, email, billing_address_id, shipping_address_id, is_billing_different, is_cart_completed, is_new_account, language_id, items_price, shipping_price, tax_price, discount_price, paid_price, total_price, currency_id, coupon_code, discount_id, gateway_id, gateway_alias, transaction_id, shipping_method_id, shipping_method_alias, ordered_date, paid_date, is_receive_invoice, source_type, created_date, created_uid, updated_date, updated_uid, uuid, product_filter, ordered_from_date, ordered_to_date, global_search_filter', 'safe', 'on' => 'search'],

            // Custom validation for CHECKOUT process
            ['billing_address_id, shipping_address_id, gateway_id', 'required', 'on' => 'checkout'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            // BELONGS relationships
            'user' => [self::BELONGS_TO, User::class, ['user_id' => 'id']],
            'customer' => [self::BELONGS_TO, Customer::class, ['user_id' => 'user_id']],
            'billingAddress' => [self::BELONGS_TO, CustomerAddress::class, 'billing_address_id'],
            'shippingAddress' => [self::BELONGS_TO, CustomerAddress::class, 'shipping_address_id'],
            'currency' => [self::BELONGS_TO, Currency::class, 'currency_id'],
            'discount' => [self::BELONGS_TO, Discount::class, 'discount_id'],
            'gateway' => [self::BELONGS_TO, Gateway::class, 'gateway_id'],
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
            'transaction' => [self::BELONGS_TO, Transaction::class, 'transaction_id'],
            'shippingMethod' => [self::BELONGS_TO, ShippingMethod::class, 'shipping_method_id'],
            'orderJson' => [self::BELONGS_TO, OrderJson::class, 'order_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // HAS_MANY relationships
			'customerAddresses' => [self::HAS_MANY, CustomerAddress::class, 'order_id'],
			'lineItems' => [self::HAS_MANY, LineItem::class, 'order_id', 'order' => 'lineItems.created_date ASC, lineItems.line_item_id ASC'],
			'transactions' => [self::HAS_MANY, Transaction::class, 'order_id', 'order' => 'transactions.updated_date DESC'],
            'statusHistory' => [self::HAS_MANY, OrderHistory::class, 'order_id', 'order' => 'statusHistory.order_history_id DESC'],
            'mailHistory' => [self::HAS_MANY, MailHistory::class, ['recipient_uid' => 'user_id', 'entity_id' => 'order_id'], 'condition' => 'mailHistory.entity_type = "Order"',  'order' => 'mailHistory.history_id DESC'],

            // Custom relations
            'itemsCount' => [self::STAT, LineItem::class, 'order_id'],
            'transactionsTotal' => [self::STAT, Transaction::class, 'order_id'],
            'finalTransaction' => [self::BELONGS_TO, Transaction::class, 'transaction_id'],
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'ordered_date' => 'd/m/Y - H:i',
                    'paid_date' => 'd/m/Y - H:i'
                ],
            ],

            // Currency format
            'CurrencyBehavior' => [
                'class' => '\dz\behaviors\CurrencyBehavior',
                'columns' => [
                    'items_price',
                    'shipping_price',
                    'tax_price',
                    'discount_price',
                    'paid_price',
                    'total_price'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'order_id' => Yii::t('app', 'Order'),
			'order_reference' => Yii::t('app', 'Order Reference'),
            'order_number' => Yii::t('app', 'Order Number'),
			'status_type' => Yii::t('app', 'Order Status'),
			'user_id' => Yii::t('app', 'Customer'),
            'email' => Yii::t('app', 'Email'),
			'billing_address_id' => Yii::t('app', 'Billing Address'),
			'shipping_address_id' => Yii::t('app', 'Shipping Address'),
			'is_billing_different' => Yii::t('app', 'Is Billing Different'),
			'is_cart_completed' => Yii::t('app', 'Is Cart Completed'),
			'is_new_account' => Yii::t('app', 'Is New Account'),
			'language_id' => Yii::t('app', 'Language'),
			'items_price' => Yii::t('app', 'Items Price'),
			'shipping_price' => Yii::t('app', 'Shipping Price'),
			'tax_price' => Yii::t('app', 'Tax Price'),
			'discount_price' => Yii::t('app', 'Discount Price'),
			'paid_price' => Yii::t('app', 'Paid Price'),
			'total_price' => Yii::t('app', 'Total Price'),
			'currency_id' => Yii::t('app', 'Currency'),
			'coupon_code' => Yii::t('app', 'Coupon Code'),
			'discount_id' => Yii::t('app', 'Discount'),
            'gateway_id' => Yii::t('app', 'Gateway'),
            'gateway_alias' => Yii::t('app', 'Gateway Alias'),
            'transaction_id' => Yii::t('app', 'Transaction'),
            'shipping_method_id' => Yii::t('app', 'Shipping Method'),
            'shipping_method_alias' => Yii::t('app', 'Shipping Method Alias'),
			'ordered_date' => Yii::t('app', 'Ordered Date'),
			'paid_date' => Yii::t('app', 'Paid Date'),
            'is_receive_invoice' => Yii::t('app', 'Is Receive Invoice?'),
			'source_type' => Yii::t('app', 'Source Type'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'customerAddresses' => null,
			'lineItems' => null,
			'billingAddress' => null,
			'createdUser' => null,
			'currency' => null,
			'language' => Yii::t('app', 'Language'),
			'transaction' => null,
			'shippingAddress' => null,
			'shippingMethod' => null,
			'updatedUser' => null,
			'user' => Yii::t('app', 'User'),
            'customer' => Yii::t('app', 'Customer'),
			'orderHistories' => null,
			'transactions' => null,
            'product_filter' => Yii::t('app', 'Products')
		];
	}


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        if ( empty($this->_vec_statuses) )
        {
            // First of all, try to get order statuses from commerce.php config file
            $vec_commerce_config = Yii::app()->config->get('components.commerce');
            if ( !empty($vec_commerce_config) && isset($vec_commerce_config['order_status']) )
            {
                $this->_vec_statuses = $vec_commerce_config['order_status'];
            }

            // Values by default
            else
            {
                $this->_vec_statuses = [
                    'cart' => Yii::t('app', 'Cart'),
                    'payment_received' => Yii::t('app', 'Payment Received'),
                    'payment_failed' => Yii::t('app', 'Payment Failed'),
                    'in_progress' => Yii::t('app', 'In progress'),
                    'shipped' => Yii::t('app', 'Shipped'),
                    'completed' => Yii::t('app', 'Completed'),
                    'canceled' => Yii::t('app', 'Canceled'),
                    'on_hold' => Yii::t('app', 'On Hold'),
                ];
            }
        }

        return $this->_vec_statuses;
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type)
    {
        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }
    /**
     * Get "source_type" labels
     */
    public function source_type_labels()
    {
        return [
            'web' => Yii::t('app', 'web'),
            'admin' => Yii::t('app', 'admin'),
            'external' => Yii::t('app', 'external'),
        ];
    }


    /**
     * Get "source_type" specific label
     */
    public function source_type_label($source_type)
    {
        $vec_labels = $this->source_type_labels();
        return isset($vec_labels[$source_type]) ? $vec_labels[$source_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search($search_id = null)
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        // Search by number or reference
        if ( $this->order_number )
        {
            $criteria->addCondition('t.order_reference LIKE :order_reference OR t.order_number = :order_number');
            $criteria->addParams([
                ':order_reference' => '%'. $this->order_number .'%',
                ':order_number' => $this->order_number
            ]);
        }

        // Status
        if ( $this->status_type )
        {
            if ( $this->status_type === 'paid' )
            {
                $criteria->addCondition('t.status_type <> "cart" AND t.status_type <> "payment_failed"');
            }
            else if ( $this->status_type === 'unpaid' )
            {
                $criteria->addCondition('t.status_type = "cart" OR t.status_type = "payment_failed"');
            }
            else if ( $this->status_type !== 'all' )
            {
                $criteria->compare('t.status_type', $this->status_type);
            }
        }

        // Dates filter
        if ( $this->ordered_from_date )
        {
            $criteria->addCondition('t.ordered_date >= '. DateHelper::date_format_to_unix($this->ordered_from_date));
        }
        if ( $this->ordered_to_date )
        {
            $criteria->addCondition('t.ordered_date <= '. DateHelper::date_format_to_unix($this->ordered_to_date));
        }

        // Customer
        if ( $this->user_id )
        {
            if ( preg_match("/\@/", $this->user_id) )
            {
                $criteria->compare('t.email', $this->user_id);
            }
            else
            {
                $criteria->with[] = 'user';
                $criteria->addCondition('user.firstname LIKE CONCAT(:name_filter, "%") OR user.lastname LIKE CONCAT(:name_filter, "%") OR CONCAT(user.firstname, " ", user.lastname) LIKE CONCAT(:name_filter, "%")');
                $criteria->addParams([':name_filter' => $this->user_id]);
            }
        }

        // Product filter
        if ( $this->product_filter )
        {
            $criteria->with[] = 'lineItems';
            $criteria->together = true;
            $criteria->compare('lineItems.title', $this->product_filter, true);
        }

        // Global search filter
        if ( $this->global_search_filter )
        {
            // Search on relationed tables
            // $criteria->with['customer'] = 'customer';
            $criteria->with['user'] = 'user';
            $criteria->with['billingAddress'] = 'billingAddress';
            $criteria->with['shippingAddress'] = 'shippingAddress';
            $criteria->together = true;
            $criteria->distinct = true;

            // Search on text columns
            $vec_textfield = [
                't.order_reference', 't.email',
                'user.firstname', 'user.lastname',
                'billingAddress.firstname', 'shippingAddress.lastname', 'billingAddress.vat_code', 'billingAddress.address_line', 'billingAddress.city', 'billingAddress.postal_code', 'billingAddress.phone',
                'shippingAddress.firstname', 'billingAddress.lastname', 'shippingAddress.vat_code', 'shippingAddress.address_line', 'shippingAddress.city', 'shippingAddress.postal_code', 'shippingAddress.phone',
            ];

            // Build SQL condition
            $sql_condition = '';
            foreach ( $vec_textfield as $que_textfield )
            {
                if ( !empty($sql_condition) )
                {
                    $sql_condition .= " OR ";
                }
                $sql_condition .= $que_textfield ." LIKE CONCAT('%', :global_search , '%')";
            }
            $criteria->addCondition($sql_condition);
            $criteria->params[':global_search'] = $this->global_search_filter;
        }

        /*
        $criteria->compare('t.order_id', $this->order_id);
        $criteria->compare('t.order_reference', $this->order_reference, true);
        $criteria->compare('t.order_number', $this->order_number, true);
        $criteria->compare('t.status_type', $this->status_type, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.is_billing_different', $this->is_billing_different);
        $criteria->compare('t.is_cart_completed', $this->is_cart_completed);
        $criteria->compare('t.is_new_account', $this->is_new_account);
        $criteria->compare('t.items_price', $this->items_price);
        $criteria->compare('t.shipping_price', $this->shipping_price);
        $criteria->compare('t.tax_price', $this->tax_price);
        $criteria->compare('t.discount_price', $this->discount_price);
        $criteria->compare('t.paid_price', $this->paid_price);
        $criteria->compare('t.total_price', $this->total_price);
        $criteria->compare('t.coupon_code', $this->coupon_code, true);
        $criteria->compare('t.discount_id', $this->discount_id);
        $criteria->compare('t.gateway_id', $this->gateway_id);
        $criteria->compare('t.gateway_alias', $this->gateway_alias, true);
        $criteria->compare('t.shipping_method_id', $this->shipping_method_id);
        $criteria->compare('t.shipping_method_alias', $this->shipping_method_alias, true);
        $criteria->compare('t.ordered_date', $this->ordered_date);
        $criteria->compare('t.paid_date', $this->paid_date);
        $criteria->compare('t.is_receive_invoice ', $this->is_receive_invoice);
        $criteria->compare('t.source_type', $this->source_type, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);
        */

        // Limit 30 rows per page by default
        $page_size = 30;

        // "Unlimited" paging for export excel action
        if ( $search_id == 'excel' )
        {
            $page_size = 100000;

            // Force "ORDER BY"
            $criteria->order = 't.order_id ASC';
        }

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => $page_size],
            'sort' => ['defaultOrder' => ['order_id' => true]]
        ]);
    }


    /**
     * Order models list
     * 
     * @return array
     */
    public function order_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['order_id', 'status_type'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Order::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('order_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     *
     * @return bool
     */
    public function beforeValidate()
    {
        switch ( $this->scenario )
        {
            case 'checkout':
                // Get "gateway_id" value from the gateway alias
                if ( !empty($this->gateway_alias) )
                {
                    $this->gateway_id = Gateway::get()->select('gateway_id')->where(['alias' => $this->gateway_alias])->scalar();
                }

                // Get "shipping_method_id" from the alias
                if ( !empty($this->shipping_method_alias) )
                {
                    $this->shipping_method_id = ShippingMethod::get()->select('shipping_method_id')->where(['alias' => $this->shipping_method_alias])->scalar();
                }
            break;
        }

        return parent::beforeValidate();
    }


    /**
     * Find an Order model by reference
     */
    public function findByReference($order_reference)
    {
        $order_model = self::get()->where(['order_reference' => $order_reference])->one();
        if ( ! $order_model )
        {
            $order_model = self::findByShortReference($order_reference);
        }

        return $order_model;
    }


    /**
     * Find an Order model by reference
     */
    public function findByShortReference($short_reference)
    {
        // Short reference must have a 7 character length
        if ( strlen($short_reference) == 7 )
        {
            return self::get()->where('t.order_reference LIKE "'. $short_reference .'%"')->one();
        }

        return null;
    }


    /**
     * Change status
     */
    public function change_status($new_status, $comments = null, $is_sending_mail = false)
    {
        $this->scenario = 'change_status';

        // Save old status
        $old_status = $this->status_type;

        // Set new status
        $this->status_type = $new_status;

        switch ( $new_status )
        {
            // Cart (new) from ordered <- RESTORE
            case 'cart':
                if ( $old_status === 'in_progress' )
                {
                    // Reset "ordered" values
                    $this->is_cart_completed = 0;
                    $this->ordered_date = null;
                }
            break;

            // Payment received
            case 'payment_received':
                if ( $old_status != 'payment_received' )
                {
                    // Register current date as "paid date"
                    $this->paid_date = date('d/m/Y - H:i');

                    // Update total paid amount
                    $this->paid_price = $this->get_total_paid();
                    if ( !empty($this->paid_price) )
                    {
                        $this->paid_price = Yii::app()->number->formatNumber($this->paid_price);
                    }
                
                    // Generate order reference
                    if ( empty($this->order_reference) )
                    {
                        $this->order_reference = $this->generate_order_reference();
                    }
                }
            break;

            // In progress
            case 'in_progress':
                // Mark cart as completed
                $this->is_cart_completed = 1;

                // Register current date as "ordered date"
                if ( empty($this->ordered_date) )
                {
                    $this->ordered_date = date('d/m/Y - H:i');
                }

                // Generate order reference
                if ( empty($this->order_reference) )
                {
                    $this->order_reference = $this->generate_order_reference();
                }
            break;
        }

        // Save only if status has changed
        if ( $old_status !== $this->status_type )
        {
            if ( ! $this->save() )
            {
                Log::save_model_error($this);
                return false;
            }

            // Save order status history
            $this->save_status_history($new_status, $comments);
        }

        return true;
    }


    /**
     * Save new status into order history
     */
    public function save_status_history($new_status, $comments)
    {
        // Get last OrderHistory model
        $last_order_history_model = OrderHistory::get()
            ->where(['order_id' => $this->order_id])
            ->order('order_history_id DESC')
            ->one();
        if ( ! $last_order_history_model || $last_order_history_model->status_type !== $this->status_type )
        {
            $order_history_model = Yii::createObject(OrderHistory::class);
            $order_history_model->setAttributes([
                'order_id'      => $this->order_id,
                'status_type'   => $new_status,
                'comments'      => $comments
            ]);

            if ( ! $order_history_model->save() )
            {
                Log::save_model_error($order_history_model);
                return false;
            }
        }

        return true;
    }


    /**
     * Return last OrderHistory model. It can be filtered by status_type
     */
    public function get_last_history_model($status_type = null)
    {
        $vec_conditions = ['order_id' => $this->order_id];

        // Filter by status?
        if ( $status_type !== null )
        {
            $vec_conditions['status_type'] = $status_type;
        }

        return OrderHistory::get()
            ->where($vec_conditions)
            ->orderBy('order_history_id DESC')
            ->limit(1)
            ->one();
    }


    /**
     * Generate order reference code
     */
    /*
    public function generate_order_reference($length = 6, $is_check_unique = true)
    {
        $valid_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $order_reference = StringHelper::random_string_with_chars($valid_chars, $length);

        // Check if it already exists an Order with same reference
        if ( $is_check_unique )
        {
            $order_model = $this->findByReference($order_reference);
            if ( $order_model )
            {
                return $this->generate_order_reference($length, $is_check_unique);
            }
        }

        return $order_reference;
    }
    */


    /**
     * Generate order reference code
     */
    public function generate_order_reference()
    {
        return md5(uniqid(mt_rand(), true));
    }


    /**
     * Short reference
     */
    public function short_reference($is_uppercase = true)
    {
        if ( $is_uppercase )
        {
            return StringHelper::strtoupper(substr($this->order_reference, 0, 7));
        }

        return substr($this->order_reference, 0, 7);
    }



    /**
     * Assign a Customer to an ANONYMOUS Order
     */
    public function assign_customer($user_id)
    {
        // Check if given user is a Customer
        $customer_model = Customer::findOne($user_id);

        // Current Order must be from an Anonymous user
        if ( $this->user_id == 0 && $customer_model )
        {
            $vec_attributes = [
                'user_id'       => $user_id,
                'created_uid'   => $user_id,
                'updated_uid'   => $user_id,
                'updated_date'  => time()
            ];
            $this->setAttributes($vec_attributes);
            $this->saveAttributes($vec_attributes);

            // Save user in LineItem models too
            if ( $this->lineItems )
            {
                unset($vec_attributes['user_id']);
                foreach ( $this->lineItems as $cart_line_item_model )
                {
                    $cart_line_item_model->setAttributes($vec_attributes);
                    $cart_line_item_model->saveAttributes($vec_attributes);
                }
            }

            return true;
        }

        return false;
    }


    /**
     * Returns an LineItem model by variant_id
     */
    public function get_line_item($variant_id)
    {
        return LineItem::get()->where([
            'order_id'      => $this->order_id,
            'variant_id'    => $variant_id
        ])->one();
    }


    /**
     * Add a Variant product to the cart
     * 
     * @return LineItem
     */
    public function add_line_item($variant_id, $quantity = 1, $price_alias = null, $vec_product_options = [])
    {
        // Check if Variant model exists and is enabled?
        $variant_model = Variant::findOne($variant_id);
        if ( $variant_model && $variant_model->is_enabled() && $variant_model->product && $variant_model->product->is_enabled() )
        {
            $line_item_model = $this->get_line_item($variant_id);
            if ( ! $line_item_model )
            {
                $line_item_model = Yii::createObject(LineItem::class);
                $line_item_model->setAttributes([
                    'order_id'              => $this->order_id,
                    'variant_id'            => $variant_model->variant_id,
                    'product_id'            => $variant_model->product_id,
                    'sku'                   => $variant_model->sku,
                    'title'                 => $variant_model->product->name,
                    'description'           => $variant_model->product->description,
                    'unit_options_price'    => 0,
                    'quantity'              => $quantity,
                    'product_base_price'    => Yii::app()->number->formatNumber($variant_model->raw_attributes['base_price']),
                ]);

                // Specific price rate (not base price)
                if ( $price_alias !== null && $price_alias !== 'base_price' )
                {
                    $product_price_model = $variant_model->get_price_model($price_alias);
                    if ( $product_price_model )
                    {
                        $raw_total_price = $product_price_model->raw_attributes['amount'] * $quantity;
                        $line_item_model->setAttributes([
                            'unit_product_price'    => Yii::app()->number->formatNumber($product_price_model->raw_attributes['amount']),
                            'unit_price'            => Yii::app()->number->formatNumber($product_price_model->raw_attributes['amount']),
                            'total_price'           => Yii::app()->number->formatNumber($raw_total_price),
                            'price_id'              => $product_price_model->price_id,
                            'price_alias'           => $price_alias,
                            'tax_price'             => Yii::app()->number->formatNumber($line_item_model->calculate_tax_price($raw_total_price))
                        ]);
                    }
                }

                // Default price rate -> base price
                else
                {
                    $raw_total_price = $variant_model->raw_attributes['base_price'] * $quantity;
                    $line_item_model->setAttributes([
                        'unit_product_price'    => Yii::app()->number->formatNumber($variant_model->raw_attributes['base_price']),
                        'unit_price'            => Yii::app()->number->formatNumber($variant_model->raw_attributes['base_price']),
                        'total_price'           => Yii::app()->number->formatNumber($raw_total_price),
                        'price_id'              => 1,
                        'price_alias'           => 'base_price',
                        'tax_price'             => Yii::app()->number->formatNumber($line_item_model->calculate_tax_price($raw_total_price))
                    ]);
                }
            }

            // Add more units to current product (update quantity field)
            else
            {
                $line_item_model->add_quantity($quantity);
            }

            if ( ! $line_item_model->save() )
            {
                Log::save_model_error($line_item_model);
                return false;
            }

            // Product options?
            $line_item_model->delete_product_options();
            if ( !empty($vec_product_options) )
            {
                // Add each ProductOption model
                foreach ( $vec_product_options as $category_id => $option_id )
                {
                    $line_item_model->add_product_option($option_id, $category_id, $quantity);
                }

                // Update line item prices
                $line_item_model = LineItem::model()->cache(0)->findByPk($line_item_model->line_item_id);
                $line_item_model->update_prices();
            }

            // Update total prices
            $this->update_prices();

            // Return LineItem model
            return $line_item_model;
        }

        return false;
    }


    /**
     * Update a LineItem
     * 
     * @return LineItem
     */
    public function update_line_item($line_item_id, $quantity = 1, $price_alias = null, $vec_product_options = [])
    {
        // Check if LineItem model exists and it belongs to current order
        $line_item_model = LineItem::findOne($line_item_id);
        if ( $line_item_model && $line_item_model->order_id == $this->order_id && $line_item_model->variant && $line_item_model->product )
        {
            $line_item_model->setAttributes([
                'sku'                   => $line_item_model->variant->sku,
                'title'                 => $line_item_model->product->name,
                'description'           => $line_item_model->product->description,
                'unit_options_price'    => 0,
                'quantity'              => $quantity
            ]);

            // Keep price_alias value 
            if ( $price_alias === null && $line_item_model->price_alias !== null )
            {
                $price_alias = $line_item_model->price_alias;
            }

            // Specific price rate (not base price)
            if ( $price_alias !== null && $price_alias !== 'base_price' && $line_item_model->variant )
            {
                $product_price_model = $line_item_model->variant->get_price_model($price_alias);
                if ( $product_price_model )
                {
                    $raw_total_price = $product_price_model->raw_attributes['amount'] * $quantity;
                    $line_item_model->setAttributes([
                        'unit_product_price'    => Yii::app()->number->formatNumber($product_price_model->raw_attributes['amount']),
                        'unit_price'            => Yii::app()->number->formatNumber($product_price_model->raw_attributes['amount']),
                        'total_price'           => Yii::app()->number->formatNumber($raw_total_price),
                        'price_id'              => $product_price_model->price_id,
                        'price_alias'           => $price_alias,
                        'tax_price'             => Yii::app()->number->formatNumber($line_item_model->calculate_tax_price($raw_total_price))
                    ]);
                }
            }

            // Default price rate -> base price
            else
            {
                $base_price = ( $line_item_model->variant && !empty($line_item_model->variant->base_price) ) ? $line_item_model->variant->base_price : 0;
                $raw_total_price = $line_item_model->variant->raw_attributes['base_price'] * $quantity;
                $line_item_model->setAttributes([
                    'unit_product_price'    => Yii::app()->number->formatNumber($line_item_model->variant->raw_attributes['base_price']),
                    'unit_price'            => Yii::app()->number->formatNumber($line_item_model->variant->raw_attributes['base_price']),
                    'total_price'           => Yii::app()->number->formatNumber($raw_total_price),
                    'price_id'              => 1,
                    'price_alias'           => 'base_price',
                    'tax_price'             => Yii::app()->number->formatNumber($line_item_model->calculate_tax_price($raw_total_price))
                ]);
            }

            if ( ! $line_item_model->save() )
            {
                Log::save_model_error($line_item_model);
                return false;
            }

            // Product options?
            $line_item_model->delete_product_options();
            if ( !empty($vec_product_options) )
            {
                // Add each ProductOption model
                foreach ( $vec_product_options as $category_id => $option_id )
                {
                    $line_item_model->add_product_option($option_id, $category_id, $quantity);
                }

                // Update line item prices
                $line_item_model = LineItem::model()->cache(0)->findByPk($line_item_model->line_item_id);
                $line_item_model->update_prices();
            }

            // Update total prices
            $this->update_prices();

            // Return LineItem model
            return $line_item_model;
        }

        return false;
    }



    /**
     * Remove a line item from the order
     */
    public function remove_line_item($line_item_id)
    {
        $line_item_model = LineItem::findOne($line_item_id);
        if ( $line_item_model && $line_item_model->delete() )
        {
            // Update total final_price after deleting an item
            $order_model = Order::model()->cache(0)->findByPk($this->order_id);
            $order_model->update_prices();

            return true;
        }

        return false;
    }


    /**
     * Remove an item from the order
     */
    public function remove_line_by_variant($variant_id)
    {
        $line_item_model = $this->get_line_item($variant_id);
        if ( $line_item_model && $line_item_model->delete() )
        {
            // Update total final_price after deleting an item
            $order_model = Order::model()->cache(0)->findByPk($this->order_id);
            $order_model->update_prices();

            return true;
        }

        return false;
    }


    /**
     * Update total prices
     */
    public function update_prices($is_save = true)
    {
        // Product prices (subtotal)
        $items_price = 0;
        if ( $this->lineItems )
        {
            foreach ( $this->lineItems as $line_item_model )
            {
                $items_price += $line_item_model->raw_attributes['total_price'];
            }
        }
        $this->raw_attributes['items_price'] = $items_price;
        $this->items_price = Yii::app()->number->formatNumber($items_price);

        // Shipping price
        $this->raw_attributes['shipping_price'] = $this->calculate_shipping_price();
        $this->shipping_price = Yii::app()->number->formatNumber($this->raw_attributes['shipping_price']);

        // Discount price
        if ( !empty($this->discount_id) )
        {
            $discount_model = Discount::findOne($this->discount_id);
            if ( $discount_model )
            {
                // Apply discount
                $this->raw_attributes['discount_price'] = $discount_model->apply_order_discount($this);

                // Save discount attributes
                $this->discount_price = Yii::app()->number->formatNumber($this->raw_attributes['discount_price']);
                if ( !empty($discount_model->code) )
                {
                    $this->coupon_code = $discount_model->code;
                }
            }
            else
            {
                $this->remove_discount(false);
            }
        }
        else if ( !empty($this->discount_price) || !empty($this->coupon_code) )
        {
            $this->remove_discount(false);
        }

        // Total price
        $this->raw_attributes['total_price'] = $this->raw_attributes['items_price'] + $this->raw_attributes['shipping_price'];
        if ( $this->raw_attributes['total_price'] > 0 && isset($this->raw_attributes['discount_price']) && $this->raw_attributes['discount_price'] > 0 )
        {
            $this->raw_attributes['total_price'] = $this->raw_attributes['total_price'] - $this->raw_attributes['discount_price'];
        }
        $this->total_price = Yii::app()->number->formatNumber($this->raw_attributes['total_price']);
        if ( $this->raw_attributes['total_price'] <= 0 )
        {
            $this->total_price = 0;
            $this->raw_attributes['total_price'] = 0;
            Log::warning('Order '. $this->order_id .' - added with PRICE 0,00 €'. print_r($this->raw_attributes, true));
        }

        // Tax price
        $this->raw_attributes['tax_price'] = $this->calculate_tax_price();
        $this->tax_price = Yii::app()->number->formatNumber($this->raw_attributes['tax_price']);

        if ( $is_save && ! $this->save() )
        {
            Log::save_model_error($this);
            return false;
        }

        return true;
    }


    /**
     * Calculate tax price
     */
    public function calculate_tax_price($raw_total_price = null)
    {
        $total_tax_price = 0;

        // Line Items
        if ( $this->lineItems )
        {
            foreach ( $this->lineItems as $line_item_model )
            {
                $line_item_tax_price = $line_item_model->raw_attributes['tax_price'];

                // If not tax_price has been saved, try to re-calculate tax price
                if ( empty($line_item_tax_price) && $line_item_model->tax )
                {
                    $line_item_model->update_tax_price();
                    $line_item_tax_price = $line_item_model->raw_attributes['tax_price'];
                }

                $total_tax_price += $line_item_tax_price;
            }
        }

        return $total_tax_price;

        /*
        if ( $raw_total_price === null )
        {
            $raw_total_price = $this->raw_attributes['total_price'];
        }

        if ( $this->tax && !empty($this->tax->percentage) )
        {
            return $raw_total_price - ( $raw_total_price / (1 + ($this->tax->percentage / 100)) );
        }
        */

        return 0;
    }


    /**
     * Update shipping price
     */
    public function calculate_shipping_price()
    {
        // Get amount of default shipping method
        $default_shipping_method_model = ShippingMethod::model()->findByAlias('default');
        if ( $default_shipping_method_model )
        {
            $this->shipping_method_id = $default_shipping_method_model->shipping_method_id;
            $this->shipping_method_alias = $default_shipping_method_model->alias;
            if ( !empty($default_shipping_method_model->raw_attributes['amount']) )
            {
                return $default_shipping_method_model->raw_attributes['amount'];
            }
        }

        return 0;
    }



    /**
     * Check if FREE shipping method can be applied
     */
    public function is_free_shipping($free_shipping_alias = 'free')
    {
        return ( $this->shippingMethod && $this->shippingMethod->alias === $free_shipping_alias );
    }


    /**
     * Add a discount to the application
     */
    public function add_discount($discount_model)
    {
        if ( $discount_model )
        {
            // Check if discount is valid
            if ( $discount_model->is_valid($this) )
            {
                // Add new discount model
                $this->discount_id = $discount_model->discount_id;
                return $this->update_prices();
            }
            
            // Discount is invalid
            else
            {     
                $this->remove_discount();
                if ( !empty($discount_model->error_message) )
                {
                    $this->discount_error = $discount_model->error_message;
                }
                else
                {
                    $this->discount_error = Yii::t('app', 'Discount cannot be applied');
                }
            }
        }
        else
        {
            $this->discount_error = Yii::t('app', 'Discount incorrect');
        }

        return false;
    }


    /**
     * Add a discount code to the application
     */
    public function add_discount_code($discount_code)
    {
        $discount_model = Discount::model()->findByCode($discount_code);
        if ( $discount_model )
        {
            return $this->add_discount($discount_model);
        }
        else
        {
            $this->discount_error = Yii::t('app', 'Discount incorrect');
        }

        return false;
    }


    /**
     * Remove any discount
     */
    public function remove_discount($is_save = true)
    {
        $this->discount = null;
        $this->discount_id = null;
        $this->discount_price = 0;
        $this->raw_attributes['discount_price'] = 0;
        $this->coupon_code = null;
        
        // Remove discounts to LineItem models
        if ( $this->lineItems )
        {
            foreach ( $this->lineItems as $line_item_model )
            {
                if ( !empty($line_item_model->discount_id) )
                {
                    $line_item_model->remove_discount($is_save);
                }
            }
        }

        if ( $is_save )
        {
            return $this->update_prices();
        }

        return true;
    }


    /**
     * Register new use for this discount
     * 
     * @return DiscountUse model or NULL if error
     */
    public function create_discount_use()
    {
        if ( $this->discount )
        {
            return $this->discount->create_discount_use($this);
        }

        return null;
    }


    /**
     * Get last pending Transaction model (if it exists)
     */
    public function get_pending_transaction()
    {
        return Transaction::get()
            ->where([
                'order_id'      => $this->order_id,
                'status_type'   => 'pending'
            ])
            ->order('created_date DESC')
            ->one();
    }


    /**
     * Mark the order as COMPLETED
     */
    public function mark_as_complete($transaction_model = null, $is_sending_mail = true)
    {
        // Set status to PAID
        if ( $transaction_model )
        {
            $this->transaction_id = $transaction_model->transaction_id;
            $this->setAttributes([
                'transaction_id'    => $this->transaction_id,
            ]);

            // Change status to PAYMENT RECEIVED (save model)
            if ( $transaction_model->transaction_type == 'purchase' )
            {
                if ( ! $this->change_status('payment_received') )
                {
                    Log::save_model_error($this);

                    return false;
                }
            }

            // Change status to IN PROGRESS (save model)
            else
            {
                if ( ! $this->change_status('in_progress') )
                {
                    Log::save_model_error($this);

                    return false;
                }
            }
        }

        // Call custom event "after_complete_order"
        return $this->after_complete_order($transaction_model, $is_sending_mail);
    }


    /**
     * Called after the order successfully completes
     */
    public function after_complete_order($transaction_model = null, $is_sending_mail = true)
    {
        // Reload Order model
        $order_model = Order::model()->cache(0)->findByPk($this->order_id);

        // Delete all PENDING payment transactions
        if ( $this->transactions )
        {
            foreach ( $this->transactions as $transaction_model )
            {
                if ( $transaction_model->status_type == 'pending' )
                {
                    $transaction_model->delete();
                }
            }
        }

        // Run order complete events directly
        if ( $this->customer )
        {
            // Customer -> Clone addresses
            $this->customer->after_complete_order($order_model);
        }

        // Send "order_summary" email to the user
        if ( $is_sending_mail )
        {
            $order_model->send_email('new_order');
        }

        // Discounts - Mark as used -> via "after_complete_order" custom event
        $this->create_discount_use();
        
        return true;
    }


    /**
     * Get ITEMS price of the order
     * 
     * This funcion is considering "reduced_price" line items
     */
    public function recalculate_items_price($is_formatted = false)
    {
        $items_price_raw = 0;
        if ( $this->lineItems )
        {
            foreach ( $this->lineItems as $line_item_model )
            {
                if ( $line_item_model->price_alias === 'reduced_price' )
                {
                    $items_price_raw += $line_item_model->raw_attributes['product_base_price'] * $line_item_model->quantity;
                }
                else
                {
                    $items_price_raw += $line_item_model->raw_attributes['total_price'];
                }
            }
        }

        return ( ! $is_formatted ) ? $items_price_raw : Yii::app()->number->formatNumber($items_price_raw);
    }


    /**
     * Get DISCOUNT price of the order
     * 
     * This funcion is considering "reduced_price" line items
     */
    public function recalculate_discount_price($is_formatted = false)
    {
        $discount_price_raw = $this->raw_attributes['discount_price'];

        // Add discount price for "reduced_price"
        if ( $this->lineItems )
        {
            foreach ( $this->lineItems as $line_item_model )
            {
                if ( $line_item_model->price_alias === 'reduced_price' )
                {
                    $reduced_discount_price_raw = $line_item_model->raw_attributes['product_base_price'] - $line_item_model->raw_attributes['unit_price'];
                    if ( $reduced_discount_price_raw > 0 )
                    {
                        $discount_price_raw += $reduced_discount_price_raw * $line_item_model->quantity;
                    }
                }
            }
        }

        return ( ! $is_formatted ) ? $discount_price_raw : Yii::app()->number->formatNumber($discount_price_raw);
    }


    /**
     * Get TOTAL price of the order
     */
    public function get_total_price()
    {
        return $this->raw_attributes['total_price'];
    }


    /**
     * Get TOTAL price to be paid
     */
    public function get_payment_amount()
    {
        return $this->raw_attributes['total_price'];
    }


    /**
     * Returns the total "purchase" transactions belonging to this order
     */
    public function get_total_paid($is_save = false)
    {
        if ( $this->gateway_alias !== 'dummy' )
        {
            $total_amount = 0;

            if ( $this->transactions )
            {
                foreach ( $this->transactions as $transaction_model )
                {
                    if ( $transaction_model->status_type == 'accepted' )
                    {
                        $total_amount += $transaction_model->raw_attributes['amount'];
                    }
                }
            }

            if ( $is_save )
            {
                $this->raw_attributes['paid_price'] = $total_amount;
                $this->paid_price = Yii::app()->number->formatNumber($total_amount);
                $this->saveAttributes([
                    'paid_price'    => $this->raw_attributes['paid_price'],
                ]);
            }

            return $total_amount;
        }
        else
        {
            return $this->raw_attributes['paid_price'];
        }
    }


    /**
     * Get total amount pending to pay.
     * Difference between total order amount and paid amount
     */
    public function get_outstanding_balance()
    {
        return $this->get_total_price() - $this->get_total_paid();
    }


    /**
     * Check if an Order has been marked as paid? At least, one payment
     */
    public function is_paid()
    {
        // return $this->status_type !== 'cart' && $this->status_type !== 'payment_failed';
        return $this->is_cart_completed == 1 && !empty($this->paid_date);
    }


    /**
     * Check if an Order has been marked as paid?
     */
    public function is_total_paid()
    {
        return ( $this->get_outstanding_balance() <= 0 );
    }


    /**
     * Send email using a mailing template
     */
    public function send_email($template_mail, $recipient_email = null)
    {
        if ( $recipient_email === null )
        {
            $recipient_email = $this->email;
        }

        $sending_result = Yii::app()->mail
            ->setTo($recipient_email)
            ->addModel($this, 'order')
            ->addModel($this->customer, 'customer')
            ->addModel($this->user, 'user')
            ->setEntityInfo($this->order_id, 'Order')
            ->compose($template_mail)
            ->send();

        // Save error into LOG
        if ( ! $sending_result )
        {
            Yii::app()->mail->logError();
        }

        return $sending_result;
    }


    /**
     * Returns the mail tokens
     */
    public function mailTokens()
    {
        return [
            '{order.id}'                    => $this->order_id,
            '{order.reference}'             => $this->title(),
            '{order.ordered_date}'          => $this->ordered_date,
            '{order.email}'                 => $this->email,
            '{order.total_price}'           => $this->total_price,
            '{order.paid_price}'            => $this->paid_price,
            '{order.customer}'              => $this->user->firstname,
            '{order.is_receive_invoice}'    => $this->is_receive_invoice == 1 ? Yii::t('app', 'Yes') : Yii::t('app', 'No'),
            '{order.language}'              => $this->language ? $this->language->title() : Yii::app()->i18n->get_default_language(),
            '{order.admin_url}'             => Url::to('/commerce/order/view', ['id' => $this->order_id])
        ];
    }


    /**
     * Help text for the mail tokens
     */
    public function mailHelp()
    {
        return [
            '{order.id}'                    => 'Order Number (internal)',
            '{order.reference}'             => 'Order Reference (alpha-numerical value)',
            '{order.ordered_date}'          => 'Ordered date',
            '{order.email}'                 => 'Customer email',
            '{order.total_price}'           => 'Order total price',
            '{order.paid_price}'            => 'Order paid price',
            '{order.customer}'              => 'Order customer first name',
            '{order.is_receive_invoice}'    => 'Does the customer want to receive an invoice?',
            '{order.language}'              => 'Customer language',
            '{order.admin_url}'             => 'Backend order URL ('. Url::to('/commerce/order/view', ['id' => $this->order_id]) .')',
        ];
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return !empty($this->order_number) ? $this->order_number : $this->short_reference();
    }
}
