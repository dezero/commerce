<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\ProductAssociation as BaseProductAssociation;
use dzlab\commerce\models\Product;
use user\models\User;
use Yii;

/**
 * ProductAssociation model class for "commerce_product_association" database table
 *
 * Columns in table "commerce_product_association" available as properties of the model,
 * followed by relations of table "commerce_product_association" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $association_id
 * @property integer $product_id
 * @property integer $product_associated_id
 * @property string $association_type
 * @property integer $weight
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $productAssociated
 * @property mixed $product
 * @property mixed $updatedUser
 */
class ProductAssociation extends BaseProductAssociation
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['product_id, product_associated_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['product_id, product_associated_id, weight, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['association_type', 'in', 'range' => ['related', 'cross-sell', 'upsell', 'reconditioned', 'substitution', 'pack']],
			['association_type, weight', 'default', 'setOnEmpty' => true, 'value' => null],
			['association_id, product_id, product_associated_id, association_type, weight, created_date, created_uid, updated_date, updated_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'product' => [self::BELONGS_TO, Product::class, 'product_id'],
            'productAssociated' => [self::BELONGS_TO, Product::class, 'product_associated_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'association_id' => Yii::t('app', 'Association'),
			'product_id' => null,
			'product_associated_id' => null,
			'association_type' => Yii::t('app', 'Association Type'),
			'weight' => Yii::t('app', 'Weight'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'createdUser' => null,
			'productAssociated' => null,
			'product' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Get "association_type" labels
     */
    public function association_type_labels()
    {
        return [
            'related' => Yii::t('app', 'Related'),
            'cross-sell' => Yii::t('app', 'Cross sell'),
            'upsell' => Yii::t('app', 'Upsell'),
            'reconditioned' => Yii::t('app', 'Reconditioned'),
            'substitution' => Yii::t('app', 'Substitution'),
            'pack' => Yii::t('app', 'Pack'),
        ];
    }


    /**
     * Get "association_type" specific label
     */
    public function association_type_label($association_type)
    {
        $vec_labels = $this->association_type_labels();
        return isset($vec_labels[$association_type]) ? $vec_labels[$association_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.association_id', $this->association_id);
        $criteria->compare('t.association_type', $this->association_type, true);
        $criteria->compare('t.weight', $this->weight);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['association_id' => true]]
        ]);
    }


    /**
     * ProductAssociation models list
     * 
     * @return array
     */
    public function productassociation_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['association_id', 'association_type'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = ProductAssociation::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('association_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}