<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\DiscountProduct as BaseDiscountProduct;
use dzlab\commerce\models\Discount;
use dzlab\commerce\models\Product;
use user\models\User;
use Yii;

/**
 * DiscountProduct model class for "commerce_discount_product" database table
 *
 * Columns in table "commerce_discount_product" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $discount_id
 * @property integer $product_id
 * @property integer $created_date
 * @property integer $created_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class DiscountProduct extends BaseDiscountProduct
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['discount_id, product_id, created_date, created_uid', 'required'],
			['discount_id, product_id, created_date, created_uid', 'numerical', 'integerOnly' => true],
			['discount_id, product_id, created_date, created_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'discount' => [self::BELONGS_TO, Discount::class, 'discount_id'],
            'product' => [self::BELONGS_TO, Product::class, 'product_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'discount_id' => null,
			'product_id' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.discount_id', $this->discount_id);
        $criteria->compare('t.product_id', $this->product_id);
        $criteria->compare('t.created_date', $this->created_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['discount_id' => true]]
        ]);
    }


    /**
     * DiscountProduct models list
     * 
     * @return array
     */
    public function discountproduct_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['discount_id', 'product_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = DiscountProduct::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('discount_id') .'-'. $que_model->getAttribute('product_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}