<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\LineItemOption as BaseLineItemOption;
use dzlab\commerce\models\LineItem;
use dzlab\commerce\models\Order;
use dzlab\commerce\models\ProductOption;
use user\models\User;
use Yii;

/**
 * LineItemOption model class for "commerce_line_item_option" database table
 *
 * Columns in table "commerce_line_item_option" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $line_item_id
 * @property integer $option_id
 * @property integer $order_id
 * @property string $option_type
 * @property double $unit_price
 * @property integer $quantity
 * @property double $total_price
 * @property integer $created_date
 * @property integer $created_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class LineItemOption extends BaseLineItemOption
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['line_item_id, option_id, order_id, option_type, created_date, created_uid', 'required'],
			['line_item_id, option_id, order_id, quantity, created_date, created_uid', 'numerical', 'integerOnly' => true],
			['unit_price, total_price', 'numerical'],
			['option_type', 'length', 'max'=> 32],
			['unit_price, quantity, total_price', 'default', 'setOnEmpty' => true, 'value' => null],
			['line_item_id, option_id, order_id, option_type, unit_price, quantity, total_price, created_date, created_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'lineItem' => [self::BELONGS_TO, LineItem::class, 'line_item_id'],
            'option' => [self::BELONGS_TO, ProductOption::class, 'option_id'],
            'order' => [self::BELONGS_TO, Order::class, 'order_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Currency format
            'CurrencyBehavior' => [
                'class' => '\dz\behaviors\CurrencyBehavior',
                'columns' => [
                    'unit_price',
                    'total_price'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'line_item_id' => null,
			'option_id' => null,
			'order_id' => null,
			'option_type' => Yii::t('app', 'Option Type'),
			'unit_price' => Yii::t('app', 'Unit Price'),
            'quantity' => Yii::t('app', 'Quantity'),
            'total_price' => Yii::t('app', 'Total Price'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.option_type', $this->option_type, true);
        $criteria->compare('t.unit_price', $this->unit_price);
        $criteria->compare('t.quantity', $this->quantity);
        $criteria->compare('t.total_price', $this->total_price);
        $criteria->compare('t.created_date', $this->created_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['line_item_id' => true]]
        ]);
    }


    /**
     * LineItemOption models list
     * 
     * @return array
     */
    public function lineitemoption_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['line_item_id', 'option_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = LineItemOption::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('line_item_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}