<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\ProductPrice as BaseProductPrice;
use dzlab\commerce\models\Price;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\Tax;
use dzlab\commerce\models\Variant;
use dzlab\commerce\models\Warehouse;
use dz\modules\settings\models\Currency;
use user\models\User;
use Yii;

/**
 * ProductPrice model class for "commerce_product_price" database table
 *
 * Columns in table "commerce_product_price" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $price_id
 * @property string $price_alias
 * @property integer $variant_id
 * @property integer $product_id
 * @property integer $warehouse_id
 * @property double $amount
 * @property string $currency_id
 * @property integer $tax_id
 * @property integer $is_taxes_included
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class ProductPrice extends BaseProductPrice
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['price_id, variant_id, product_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['price_id, variant_id, product_id, warehouse_id, tax_id, is_taxes_included, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['amount', 'numerical'],
            ['price_alias', 'length', 'max'=> 32],
			['currency_id', 'length', 'max'=> 4],
			['price_alias, warehouse_id, amount, currency_id, tax_id, is_taxes_included', 'default', 'setOnEmpty' => true, 'value' => null],
			['price_id, price_alias, variant_id, product_id, warehouse_id, amount, currency_id, tax_id, is_taxes_included, created_date, created_uid, updated_date, updated_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'price' => [self::BELONGS_TO, Price::class, 'price_id'],
            'variant' => [self::BELONGS_TO, Variant::class, 'variant_id'],
            'product' => [self::BELONGS_TO, Product::class, 'product_id'],
            'warehouse' => [self::BELONGS_TO, Warehouse::class, 'warehouse_id'],
            'currency' => [self::BELONGS_TO, Currency::class, 'currency_id'],
            'tax' => [self::BELONGS_TO, Tax::class, 'tax_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Currency format
            'CurrencyBehavior' => [
                'class' => '\dz\behaviors\CurrencyBehavior',
                'columns' => [
                    'amount',
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'price_id' => Yii::t('app', 'Price Id'),
            'price_alias' => Yii::t('app', 'Price Type'),
			'variant_id' => Yii::t('app', 'Variant'),
			'product_id' => Yii::t('app', 'Product'),
			'warehouse_id' => Yii::t('app', 'Warehouse'),
			'amount' => Yii::t('app', 'Amount'),
			'currency_id' => Yii::t('app', 'Currency'),
			'tax_id' => Yii::t('app', 'Tax'),
			'is_taxes_included' => Yii::t('app', 'Is Taxes Included'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.price_id', $this->price_id);
        $criteria->compare('t.price_alias', $this->price_alias, true);
        $criteria->compare('t.variant_id', $this->variant_id);
        $criteria->compare('t.product_id', $this->product_id);
        $criteria->compare('t.warehouse_id', $this->warehouse_id);
        $criteria->compare('t.amount', $this->amount);
        $criteria->compare('t.currency_id', $this->currency_id);
        $criteria->compare('t.tax_id', $this->tax_id);
        $criteria->compare('t.is_taxes_included', $this->is_taxes_included);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['price_id' => true]]
        ]);
    }


    /**
     * ProductPrice models list
     * 
     * @return array
     */
    public function productprice_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['price_id', 'price_id', 'variant_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = ProductPrice::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('price_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}