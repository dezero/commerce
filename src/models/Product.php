<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use Dz;
use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Transliteration;
use dz\helpers\Url;
use dz\modules\asset\models\AssetImage;
use dz\modules\category\models\Category;
use dzlab\commerce\models\_base\Product as BaseProduct;
use dzlab\commerce\models\LineItem;
use dzlab\commerce\models\ProductAssociation;
use dzlab\commerce\models\ProductCategory;
use dzlab\commerce\models\ProductImage;
use dzlab\commerce\models\ProductLink;
use dzlab\commerce\models\ProductPrice;
use dzlab\commerce\models\SearchProduct;
use dzlab\commerce\models\Stock;
use dzlab\commerce\models\TranslatedProduct;
use dzlab\commerce\models\Variant;
use dz\modules\web\models\Seo;
use user\models\User;
use Yii;

/**
 * Product model class for "commerce_product" database table
 *
 * Columns in table "commerce_product" available as properties of the model,
 * followed by relations of table "commerce_product" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $product_id
 * @property string $product_type
 * @property string $name
 * @property string $description
 * @property integer $main_category_id
 * @property integer $brand_category_id
 * @property string $seo_url
 * @property string $default_ean_code
 * @property string $default_sku
 * @property integer $default_image_id
 * @property integer $default_variant_id
 * @property double $default_base_price
 * @property integer $weight
 * @property integer $is_on_offer
 * @property integer $is_disabled
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $lineItems
 * @property mixed $createdUser
 * @property mixed $defaultImage
 * @property mixed $defaultVariant
 * @property mixed $disableUser
 * @property mixed $mainCategory
 * @property mixed $updatedUser
 * @property mixed $variants
 */
class Product extends BaseProduct
{
    /**
     * Products type configuration
     */
    protected $vec_config = [];


    /**
     * Special search filter by category
     */
    public $category_filter;


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['product_type, name, created_date, created_uid, updated_date, updated_uid', 'required'],
			['main_category_id, brand_category_id, default_image_id, default_variant_id, weight, is_on_offer, is_disabled, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['default_base_price', 'numerical'],
			['product_type', 'length', 'max'=> 32],
			['name, seo_url', 'length', 'max'=> 255],
			['default_ean_code', 'length', 'max'=> 16],
			['default_sku', 'length', 'max'=> 128],
			['uuid', 'length', 'max'=> 36],
			['description, main_category_id, brand_category_id, seo_url, default_ean_code, default_sku, default_image_id, default_variant_id, default_base_price, weight, is_on_offer, is_disabled, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['description', 'safe'],
			['product_id, product_type, name, description, main_category_id, brand_category_id, seo_url, default_ean_code, default_sku, default_image_id, default_variant_id, default_base_price, weight, is_on_offer, is_disabled, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid, category_filter, global_search_filter', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'variants' => [self::HAS_MANY, Variant::class, 'product_id', 'order' => 'variants.variant_id ASC'],
            'mainCategory' => [self::BELONGS_TO, Category::class, 'main_category_id'],
            'brandCategory' => [self::BELONGS_TO, Category::class, 'brand_category_id'],
            'defaultImage' => [self::BELONGS_TO, AssetImage::class, 'default_image_id'],
			'defaultVariant' => [self::BELONGS_TO, Variant::class, 'default_variant_id'],
            'lineItems' => [self::HAS_MANY, LineItem::class, 'product_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
            'productCategories' => [self::HAS_MANY, ProductCategory::class, 'product_id'],
            'otherCategories' => [self::HAS_MANY, ProductCategory::class, 'product_id', 'condition' => 'otherCategories.category_id <> product.main_category_id', 'with' => 'product', 'together' => true],
            'translations' => [self::HAS_MANY, TranslatedProduct::class, 'product_id'],
            'prices' => [self::HAS_MANY, ProductPrice::class, 'product_id'],
            'seo' => [self::HAS_MANY, Seo::class, ['entity_id' => 'product_id'], 'condition' => 'seo.entity_type = "product"', 'order' => 'seo.updated_date DESC'],
            'searchProduct' => [self::BELONGS_TO, SearchProduct::class, 'product_id'],

            // Images, links and videos
            'images' => [self::HAS_MANY, ProductImage::class, 'product_id', 'order' => 'images.weight ASC'],
            'countImages' => [self::STAT, ProductImage::class, 'product_id'],
            'links' => [self::HAS_MANY, ProductLink::class, 'product_id', 'condition' => 'links.is_video = 0', 'order' => 'links.weight ASC'],
            'videos' => [self::HAS_MANY, ProductLink::class, 'product_id', 'condition' => 'videos.is_video = 1', 'order' => 'videos.weight ASC'],

            // Stock
            'stock' => [self::STAT, Stock::class, 'product_id', 'select' => 'SUM(stock)'],
            'warehouses' => [self::HAS_MANY, Stock::class, 'product_id'],

            // ProductAssociation
            'relatedProducts' => [self::HAS_MANY, ProductAssociation::class, 'product_id', 'condition' => 'relatedProducts.association_type = "related"', 'order' => 'relatedProducts.weight DESC'],
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

            // Currency format
            'CurrencyBehavior' => [
                'class' => '\dz\behaviors\CurrencyBehavior',
                'columns' => [
                    'default_base_price',
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'product_id' => Yii::t('app', 'Product'),
			'product_type' => Yii::t('app', 'Product Type'),
			'name' => Yii::t('app', 'Product Name'),
			'description' => Yii::t('app', 'Description'),
			'main_category_id' => Yii::t('app', 'Category'),
            'brand_category_id' => Yii::t('app', 'Brand'),
			'seo_url' => Yii::t('app', 'SEO URL'),
			'default_ean_code' => Yii::t('app', 'EAN code'),
			'default_sku' => Yii::t('app', 'SKU'),
			'default_image_id' => Yii::t('app', 'Image'),
			'default_variant_id' => null,
			'default_base_price' => Yii::t('app', 'Base Price'),
            'weight' => Yii::t('app', 'Search Weight'),
			'is_on_offer' => Yii::t('app', 'On Offer?'),
			'is_disabled' => Yii::t('app', 'Disabled?'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'lineItems' => null,
			'createdUser' => null,
			'defaultImage' => null,
			'defaultVariant' => null,
			'disableUser' => null,
			'mainCategory' => null,
			'updatedUser' => null,
			'variants' => null,

            // Custom labels
            'productCategories' => Yii::t('app', 'Other Categories'),
            'category_filter' => Yii::t('app', 'Categories'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search($search_id = null)
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.product_id', $this->product_id);
        $criteria->compare('t.main_category_id', $this->main_category_id);
        $criteria->compare('t.brand_category_id', $this->brand_category_id);
        $criteria->compare('t.product_type', $this->product_type);
        
        // Filter by product name
        if ( $this->name )
        {
            $criteria->addCondition('t.name LIKE :name OR t.default_sku LIKE :name OR t.default_ean_code LIKE :name');
            $criteria->addParams([
                ':name' => '%'. $this->name .'%'
            ]);
        }

        // Filter by category
        if ( $this->category_filter )
        {
            $criteria->with[] = 'productCategories';
            $criteria->together = true;
            $criteria->compare('productCategories.category_id', $this->category_filter);
        }

        // Global search filter
        if ( $this->global_search_filter )
        {
            // Search on relationed tables
            $criteria->with['searchProduct'] = 'searchProduct';
            // $criteria->together = true;
            $criteria->distinct = true;

            // Search on text columns
            $vec_textfield = [
                't.name', 't.description', 't.default_sku', 't.default_ean_code',
                'searchProduct.content'
            ];

            // Build SQL condition
            $sql_condition = '';
            foreach ( $vec_textfield as $que_textfield )
            {
                if ( !empty($sql_condition) )
                {
                    $sql_condition .= " OR ";
                }
                $sql_condition .= $que_textfield ." LIKE CONCAT('%', :global_search , '%')";
            }
            $criteria->addCondition($sql_condition);
            $criteria->params[':global_search'] = $this->global_search_filter;
        }

        // $criteria->compare('t.description', $this->description, true);
        // $criteria->compare('t.seo_url', $this->seo_url, true);
        // $criteria->compare('t.default_ean_code', $this->default_ean_code, true);
        // $criteria->compare('t.default_sku', $this->default_sku, true);
        // $criteria->compare('t.default_base_price', $this->default_base_price);
        // $criteria->compare('t.is_on_offer', $this->is_on_offer);
        // $criteria->compare('t.is_disabled', $this->is_disabled);
        // $criteria->compare('t.disable_date', $this->disable_date);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid, true);

        // Limit 30 rows per page by default
        $page_size = 30;

        // "Unlimited" paging for export excel action
        if ( $search_id == 'excel' )
        {
            $page_size = 100000;

            // Force "ORDER BY"
            $criteria->order = 't.product_id ASC';
        }

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => $page_size],
            'sort' => ['defaultOrder' => ['weight' => true]]
        ]);
    }


    /**
     * Disables the product
     */
    public function disable()
    {
        if ( parent::disable() )
        {
            if ( $this->searchProduct )
            {
                $this->searchProduct->saveAttributes([
                    'is_disabled' => 1
                ]);
            }

            return true;
        }

        return $this->save();
    }


    /**
     * Enables the product
     */
    public function enable()
    {
        if ( parent::enable() )
        {
            if ( $this->searchProduct )
            {
                $this->searchProduct->saveAttributes([
                    'is_disabled' => 0
                ]);
            }

            return true;
        }

        return false;
    }


    /**
     * Deletes the product
     */
    public function delete()
    {
        // It is not allowed to DELETE a product if it has been registered on an order
        if ( $this->lineItems )
        {
            return false;
        }

        // Product images -> Delete image files
        if ( $this->images )
        {
            foreach ( $this->images as $product_image_model )
            {
                $product_image_model->delete();
            }
        }

        // Default image -> Delete image file
        if ( $this->defaultImage )
        {
            $this->defaultImage->delete();
        }

        // Trying to delete product images directory
        $images_path = Yii::app()->productManager->get_images_path($this->product_id);
        $product_image_dir = Yii::app()->file->set($images_path);
        if (  $product_image_dir->isDir )
        {
            $product_image_dir->delete();
        }

        // Delete SEO models
        if ( $this->seo )
        {
            foreach ( $this->seo as $seo_model )
            {
                $seo_model->delete();
            }
        }

        // Delele translations
        if ( $this->translations )
        {
            foreach ( $this->translations as $translated_product_model )
            {
                $translated_product_model->delete();
            }
        }

        return parent::delete();
    }


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     *
     * @return bool
     */
    public function beforeValidate()
    {
        if ( $this->scenario !== 'insert' && $this->defaultVariant )
        {
            // Sync data from default Variant model
            $this->default_sku = !empty($this->defaultVariant->sku) ? $this->defaultVariant->sku : null;
            $this->default_ean_code = !empty($this->defaultVariant->ean_code) ? $this->defaultVariant->ean_code : null;
            $this->default_base_price = !empty($this->defaultVariant->base_price) ? $this->defaultVariant->base_price : $this->default_base_price;
            // $this->default_image_id = !empty($this->defaultVariant->image_id) ? $this->defaultVariant->image_id : $this->default_image_id;
        }

        return parent::beforeValidate();
    }


    /**
     * Event "afterSave"
     *
     * This method is invoked after saving a record successfully
     */
    public function afterSave()
    {
        switch ( $this->scenario )
        {
            case 'insert':
            case 'update':
                if ( $this->is_multilanguage() && Yii::isMultilanguage() )
                {
                    // Copy translatable attributes from "commerce_product" table to "commerce_translated_product" table
                    $this->copy_translation();
                }
            break;
        }

        return parent::afterSave();
    }


    /**
     * Product models list
     * 
     * @return array
     */
    public function product_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['product_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Product::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('product_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Variant models list
     * 
     * @return array
     */
    public function variant_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['t.variant_id', 't.sku'];
        $criteria->order = 't.sku ASC';
        $criteria->compare('t.product_id', $this->product_id);
        
        $vec_models = Variant::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('variant_id')] = $que_model->title();
            }
        }

        return $vec_output;
    } 


    /**
     * Find a Product model by default SKU
     */
    public function findBySKU($sku)
    {
        return $this->findByAttributes([
            'default_sku'  => $sku
        ]);
    }


    /**
     * Load and returns schema configuration of product types
     */
    public function get_config($product_type = null)
    {
        if ( empty($this->vec_config) )
        {
            $this->vec_config = Yii::app()->config->get('components.products');
        }

        if ( !empty($this->vec_config) )
        {
            // Return full configuration options
            if ( $product_type == 'all' )
            {
                return $this->vec_config;
            }

            // Current category type
            if ( $product_type == null )
            {
                $product_type = $this->product_type;
            }

            // Return configuration given input params
            if ( !empty($product_type) && isset($this->vec_config[$product_type]) )
            {
                return $this->vec_config[$product_type];
            }
        }

        // Default values
        return [
            'name'                  => Yii::t('app', 'Product'),

            // Product information tab
            'main_category_type'    => 'category',
            'is_multilanguage'      => true,
            'is_description'        => true,
            'is_seo_fields'         => true,

            // Images & videos tab
            'is_image'              => true,
            'is_video'              => false,
            'is_link'               => false,

            // Related products tab
            'is_related_products'   => false,

            // Actions allowed
            'is_disable_allowed'    => true,
            'is_delete_allowed'     => true,

            // View files path
            'views' => [
                'index'         => '//commerce/product/_base/index',
                'create'        => '//commerce/product/_base/create',    
                'update'        => '//commerce/product/_base/update',
                '_form'         => '//commerce/product/_base/_form',
                '_form_seo'     => '//commerce/product/_base/_form_seo',
                '_grid_column'  => '//commerce/product/_base/_grid_column',
                '_header_grid'  => '//commerce/product/_base/_header_grid',
                '_header_menu'  => '//commerce/product/_base/_header_menu',
                '_header_title' => '//commerce/product/_base/_header_title',
                '_sidebar'      => '//commerce/product/_base/_sidebar',
                '_tree'         => '//commerce/product/_base/_tree',
            ],

            // Texts
            'texts' => [
                'entity_label'      => 'Product',
                'entities_label'    => 'Products',

                'index_title'       => 'Manage products',
                'add_button'        => 'Add product',
                'create_title'      => 'Create product',

                // Success messages
                'created_success'   => 'New product created successfully',
                'updated_success'   => 'Product updated successfully',

                // Disable
                'disable_success'   => 'Product DISABLED successfully',
                'disable_error'     => 'Product could not be DISABLED',
                'disable_confirm'   => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this product?</h3><p><u class=\'text-danger\'>WARNING:</u> Products will not be available for purchase.</p>',

                // Enable
                'enable_success'    => 'Product ENABLED successfully',
                'enable_error'      => 'Product could not be ENABLED',
                'enable_confirm'    => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this product?</h3><p>Products will be available for purchase.</p>',

                // Delete
                'delete_success'    => 'Product DELETED successfully',
                'delete_error'      => 'Product could not be DELETED',
                'delete_confirm'    => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this product?</h3><p><strong>WARNING:</strong> All the product data will be permanently removed. Consider disabling it.</p>',

                // Other
                'empty_text'        => 'No products found',
            ]
        ];
    }


    /**
     * Returns the view file path
     */
    public function get_view_path($view_name)
    {
        // Default view paths
        $vec_views = [
            'index'         => '//commerce/product/_base/index',
            'create'        => '//commerce/product/_base/create',    
            'update'        => '//commerce/product/_base/update',
            '_form'         => '//commerce/product/_base/_form',
            '_form_seo'     => '//commerce/product/_base/_form_seo',
            '_grid_column'  => '//commerce/product/_base/_grid_column',
            '_header_grid'  => '//commerce/product/_base/_header_grid',
            '_header_menu'  => '//commerce/product/_base/_header_menu',
            '_header_title' => '//commerce/product/_base/_header_title',
            '_sidebar'      => '//commerce/product/_base/_sidebar',
            '_tree'         => '//commerce/product/_base/_tree',
        ];

        // Return view path from configuration file
        if ( !empty($this->product_type) )
        {
            $vec_config = $this->get_config($this->product_type);
            if ( !empty($vec_config) && isset($vec_config['views']) && isset($vec_config['views'][$view_name]) )
            {
                return $vec_config['views'][$view_name];
            }
        }

        // Return view path from default values
        if ( isset($vec_views[$view_name]) )
        {
            return $vec_views[$view_name];
        }

        // No view has been found
        return $view_name;
    }


    /**
     * Return the corresponding text
     */
    public function text($text_key, $product_type = null)
    {
        // Current product type
        if ( $product_type === null )
        {
            $product_type = $this->product_type;
        }

        // Load configuration and return text
        $vec_config = $this->get_config($product_type);
        if ( !empty($vec_config) && isset($vec_config['texts']) && isset($vec_config['texts'][$text_key]) )
        {
            return Yii::t('app', $vec_config['texts'][$text_key]);
        }

        return '';
    }


    /**
     * Create the DEFAULT Variant model
     */
    public function add_default_variant($variant_model)
    {
        if ( $variant_model->is_default == 1 )
        {
            $this->default_variant_id = $variant_model->variant_id;
            $this->saveAttributes(['default_variant_id' => $variant_model->variant_id]);
            return true;
        }

        return false;
    }


    /**
     * Add a Variant model
     */
    public function save_variant($variant_model, $is_save_product = true)
    {
        if ( $variant_model->product_id == $this->product_id )
        {
            if ( ! $variant_model->save() )
            {
                Log::save_model_error($variant_model);
                return false;
            }

            // Default variant? Save product data
            if ( $variant_model->is_default == 1 && $is_save_product && ! $this->save()  )
            {
                Log::save_model_error($this);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Get default images path for this product
     */
    public function get_images_path($is_check_exists = false)
    {
        // $product_images_path = AssertFile::model()->getImagesPath() . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR . $this->product_id . DIRECTORY_SEPARATOR;
        $product_images_path = Yii::app()->productManager->get_images_path($this->product_id);

        // Check if directory exists, unless create it
        if ( $is_check_exists )
        {
            $product_image_dir = Yii::app()->file->set($product_images_path);
            if (  ! $product_image_dir->isDir )
            {
                // Default UNIX file permissions = 755
                $default_permissions = AssetImage::model()->get_default_permissions();

                $product_image_dir->createDir($default_permissions, $product_images_path);
                // $product_image_dir->setPermissions($default_permissions);
            }
        }

        return $product_images_path;
    }


    /**
     * Upload and add a ProductImage model
     */
    public function upload_image_file($file, $is_default = false, $file_options = 'krajee')
    {
        $image_model = Yii::createObject(AssetImage::class);
        $image_model->setAttributes([
            'file_name'     => Transliteration::file($file->basename),
            'file_mime'     => $file->mimeType,
            'file_size'     => $file->getSize(false),
            'file_path'     => $this->get_images_path(true),
            'file_options'  => $file_options,
            'entity_type'   => 'Product',
            'entity_id'     => $this->product_id
        ]);
        $image_model->file = $file;

        // Change save scenario
        $image_model->scenario = 'upload_image';

        if ( ! $image_model->save() )
        {
            Log::save_model_error($image_model);
            return false;
        }

        // Finally, create the ProductImage model
        if ( $this->add_product_image($image_model) && $is_default )
        {
            $this->default_image_id = $image_model->file_id;
            $this->saveAttributes([
                'default_image_id'  => $this->default_image_id
            ]);
        }

        return true;
    }


    /**
     * Returns a ProductImage model
     */
    public function get_product_image($image_id)
    {
        return ProductImage::get()->where([
            'product_id'    => $this->product_id,
            'image_id'      => $image_id
        ])->one();
    }


    /**
     * Save default image
     */
    public function save_default_image($image_id)
    {
        $this->default_image_id = $image_id;
        $this->saveAttributes([
            'default_image_id'  => $this->default_image_id
        ]);

        return true;
    }


    /**
     * Add an AssetImage model to this product as DEFAULT
     */
    public function add_default_image($image_model)
    {
        if ( ! Dz::is_console() )
        {
            // Get image path
            // $image_destination_path = $image_model->getImagesPath() . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR . $this->product_id . DIRECTORY_SEPARATOR;
            $image_destination_path = $this->get_images_path(true);

            // Try to save image file
            if ( ! $this->upload_image('default_image_id', $image_destination_path) )
            {
                Yii::app()->controller->showErrors($this->get_file_errors());
            }

            // Create/update ProductImage model
            $file_model = $this->get_file_model();
            if ( $file_model && !empty($this->default_image_id) )
            {
                $this->add_product_image($file_model);
            }

            // Check if image has been deleted
            else
            {
                $default_image_model = AssetImage::get()->where(['file_id' => $this->default_image_id])->one();
                if ( ! $default_image_model )
                {
                    $this->default_image_id = null;
                }
            }

            return true;

            /*
            if ( $this->defaultVariant )
            {
                return $this->defaultVariant->add_default_image($image_model);
            }
            */
        }

        return false;
    }


    /**
     * Add a ProductImage model
     */
    public function add_product_image($image_model)
    {
        $product_image_model = $this->get_product_image($image_model->file_id);

        if ( ! $product_image_model )
        {
            $product_image_model = Yii::createObject(ProductImage::class);
            $product_image_model->setAttributes([
                'product_id'    => $this->product_id,
                'image_id'      => $image_model->file_id
            ]);

            if ( ! $product_image_model->save() )
            {
                Log::save_model_error($product_image_model);
                return false;
            }
        }

        return $product_image_model;
    }


    /**
     * Remove an image from this Product model
     */
    public function remove_product_image($image_id)
    {
        $product_image_model = $this->get_product_image($image_id);
        
        if ( $product_image_model )
        {
            return $product_image_model->delete();
        }

        return false;
    }


    /**
     * Add a ProductLink model
     */
    public function add_product_link($url, $title = null, $source = null, $is_video = false)
    {
        $product_link_model = Yii::createObject(ProductLink::class);
        $product_link_model->setAttributes([
            'product_id'    => $this->product_id,
            'url'           => $url,
            'is_video'      => $is_video ? 1 : 0,
            'title'         => $title !== null ? $title : null,
            'source'        => $source !== null ? $source : null,
        ]);

        if ( ! $product_link_model->save() )
        {
            Log::save_model_error($product_link_model);
            return false;
        }

        return $product_link_model;
    }


    /**
     * Remove an image from this Product model
     */
    public function remove_product_link($link_id)
    {
        $product_link_model = ProductLink::findOne($link_id);
        
        if ( $product_link_model && $product_link_model->product_id === $this->product_id )
        {
            return $product_link_model->delete();
        }

        return false;
    }


    /**
     * Add a video (ProductLink model)
     */
    public function add_product_video($url, $title = null, $source = null)
    {
        return $this->add_product_link($url, $title, $source, true);
    }


    /**
     * Add a video (ProductLink model)
     */
    public function remove_product_video($link_id)
    {
        return $this->remove_product_link($link_id);
    }


    /**
     * Save a ProductPrice model
     */
    public function save_price($product_price_model)
    {
        if ( $this->defaultVariant )
        {
            return $this->defaultVariant->save_price($product_price_model);
        }
    }


    /**
     * Save categories of a Product model
     */
    public function save_categories($vec_product_categories = [], $is_delete_previous_categories = true)
    {
        // Delete previous relations
        if ( $is_delete_previous_categories )
        {
            Yii::app()->db->createCommand()->delete('commerce_product_category', 'product_id = :product_id', ['product_id' => $this->product_id]);
        }

        if ( !empty($vec_product_categories) )
        {
            foreach ( $vec_product_categories as $vec_category_values )
            {
                if ( !is_array($vec_category_values) )
                {
                    $vec_category_values = [$vec_category_values];
                }

                foreach ( $vec_category_values as $category_id )
                {
                    if ( !empty($category_id) )
                    {
                        $product_category_model = ProductCategory::get()->where([
                            'product_id'    => $this->product_id,
                            'category_id'   => $category_id,
                        ])->one();

                        if ( empty($product_category_model) )
                        {
                            $product_category_model = Yii::createObject(ProductCategory::class);
                            $product_category_model->setAttributes([
                                'product_id'    => $this->product_id,
                                'category_id'   => $category_id,
                                'category_type' => Yii::app()->categoryManager->get_category_type($category_id)
                            ]);
                            if ( ! $product_category_model->save() )
                            {
                                Log::save_model_error($product_category_model);
                            }
                        }
                    }
                }
            }
        }

        return true;
    }


    /**
     * Get TranslatedProduct model given a language
     */
    public function get_translation($language_id)
    {
        return TranslatedProduct::get()
            ->where([
                'product_id'    => $this->product_id,
                'language_id'   => $language_id
            ])
            ->one();
    }


    /**
     * Save a TranslatedProduct model
     */
    public function save_translation($translated_product_model)
    {
        if ( empty($translated_product_model->product_id) || $translated_product_model->product_id == $this->product_id )
        {
            $translated_product_model->product_id = $this->product_id;

            if ( ! $translated_product_model->save() )
            {
                Log::save_model_error($translated_product_model);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Copy current translatable fields to TranslatedProduct model
     * 
     * Copy from Product model to TranslatedProduct model
     */
    public function copy_translation($language_id = '')
    {
        // Copy translation with default language
        if ( empty($language_id) )
        {
            $language_id = Yii::defaultLanguage();
        }

        if ( !empty($language_id) )
        {
            $translated_product_model = $this->get_translation($language_id);
            if ( ! $translated_product_model )
            {
                $translated_product_model = Yii::createObject(TranslatedProduct::class);
                $translated_product_model->setAttributes([
                    'product_id'    => $this->product_id,
                    'language_id'   => $language_id,
                ]);
            }

            // Translatable attributes
            $translated_product_model->setAttributes([
                'name'          => $this->name,
                'description'   => $this->description,
                'seo_url'       => $this->seo_url,
            ]);

            if ( ! $translated_product_model->save() )
            {
                Log::save_model_error($translated_product_model);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Check if this Product type has multilanguage option enabled
     */
    public function is_multilanguage()
    {
        $vec_config = $this->get_config($this->product_type);
        return ( !empty($vec_config) && isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] );
    }


    /**
     * Get SEO model given a language
     */
    public function get_seo($language_id = null)
    {
        // Default language
        if ( $language_id === null )
        {
            $language_id = Yii::defaultLanguage();
        }
        
        return Seo::get()
            ->where([
                'entity_id'     => $this->product_id,
                'entity_type'   => 'product',
                'language_id'   => $language_id
            ])
            ->one();
    }


    /**
     * Save a Seo model
     */
    public function save_seo($seo_model, $is_save_product = false, $language_id = null)
    {
        if ( empty($seo_model->entity_id) || ($seo_model->entity_id == $this->product_id && $seo_model->entity_type === 'product') )
        {
            if ( empty($seo_model->entity_id) )
            {
                // Default language
                if ( $language_id === null )
                {
                    $language_id = Yii::defaultLanguage();
                }

                $seo_model->entity_id = $this->product_id;
                $seo_model->entity_type = 'product';
                $seo_model->language_id = $language_id;
            }

            if ( ! $seo_model->save() )
            {
                Log::save_model_error($seo_model);
                return false;
            }

            // Save "seo_url" cached attribute
            if ( !empty($seo_model->url_manual) )
            {
                // Non multi-language
                if ( ! $this->is_multilanguage() )
                {
                    $this->seo_url = $seo_model->url_manual;
                    if ( $is_save_product )
                    {
                        $this->saveAttributes(['seo_url' => $this->seo_url]);
                    }
                }
                else
                {
                    // Default language
                    if ( $seo_model->language_id == Yii::defaultLanguage() )
                    {
                        $this->seo_url = $seo_model->url_manual;
                        if ( $is_save_product )
                        {
                            $this->saveAttributes(['seo_url' => $this->seo_url]);
                        }
                    }

                    $translated_model = $this->get_translation($seo_model->language_id);
                    if ( $translated_model )
                    {
                        $translated_model->seo_url = $seo_model->url_manual;
                        $translated_model->saveAttributes([
                            'seo_url' => $seo_model->url_manual
                        ]);
                    }
                }
            }

            return true;
        }

        return false;
    }


    /**
     * Returns the SEO URL for this model
     * 
     * @see \dz\helper\Url::to() method
     */
    public function url($vec_params=[], $ampersand='&')
    {
        // Default route (NO SEO way)
        $route = 'product/'. $this->product_id;

        // Current language
        $language_id = Yii::currentLanguage();
        $is_multilanguage = $this->is_multilanguage();

        if ( ! $is_multilanguage )
        {
            if ( !empty($this->seo_url) )
            {
                $route = $this->seo_url;
            }

            else if ( !empty($this->seo) )
            {
                $route = $this->seo[0]->url_manual;
            }
        }
        else if ( $is_multilanguage )
        {
            if ( $language_id == Yii::defaultLanguage() && !empty($this->seo_url) )
            {
                $route = $this->seo_url;
            }
            else
            {
                $seo_model = $this->get_seo($language_id);
                if ( $seo_model )
                {
                    $route = $seo_model->url_manual;
                }
            }
        }

        return Url::to($route, $vec_params, '', $ampersand);
    }


    /**
     * Get SEO URL generated automatically
     *
     *  1. First of all, use <name> separated by "-"
     *  2. If it exists, use <name>-<product>
     *
     */
    public function get_seo_url_auto($language_id = '', $is_check_url_exists = false)
    {
        $url_auto = Transliteration::url($this->name, true);
        if ( !empty($language_id) && $this->is_multilanguage() )
        {
            $translated_model = $this->get_translation($language_id);
            if ( $translated_model && !empty($translated_model->name) )
            {
                $url_auto = Transliteration::url($translated_model->name, true);
            }
        }

        // Check if URL already exists
        if ( $is_check_url_exists && Yii::app()->seo->check_url_unique($url_auto, $this->product_id, 'product', $language_id) )
        {
            $url_auto .= '-'. $this->product_id;
        }

        return $url_auto;
    }


    /**
     * Generate SEO data
     */
    public function generate_seo($language_id)
    {
        $translated_model = $this->get_translation($language_id);
        return [
            'url_auto'          => $this->get_seo_url_auto($language_id, true),
            'meta_title'        => ($translated_model === null) ? $this->name : $translated_model->name,
            // 'meta_description'  => ($translated_model === null) ? $this->description : $translated_model->description,
        ];
    }


    /**
     * SEO - Return a meta tags array for a Product page
     */
    public function get_seo_metatags()
    {
        $vec_meta_tags = [
            'title'         => $this->title(),
            'description'   => $this->description,
            'image'         => $this->defaultImage,
            'type'          => 'article'
        ];

        return $vec_meta_tags;
    }


    /**
     * Get prices models indexed by alias
     */
    public function get_price_models()
    {
        $vec_price_models = [];
        if ( $this->prices )
        {
            foreach ( $this->prices as $product_price_model )
            {
                if ( $product_price_model->price )
                {
                    $vec_price_models[$product_price_model->price->alias] = $product_price_model;
                }
            }
        }

        return $vec_price_models;
    }


    /**
     * Return a ProductPrice model by alias
     */
    public function get_price_model($price_alias)
    {
        $vec_price_models = $this->get_price_models();
        if ( !empty($vec_price_models) && isset($vec_price_models[$price_alias]) )
        {
            return $vec_price_models[$price_alias];
        }

        return null;
    }


    /**
     * Get current selling price
     */
    public function get_price($is_raw = false)
    {
        $vec_price_models = $this->get_price_models();
        if ( $this->is_on_offer == 1 && isset($vec_price_models['reduced_price']) && $vec_price_models['reduced_price']->raw_attributes['amount'] > 0 )
        {
            if ( ! $is_raw )
            {
                return $vec_price_models['reduced_price']->amount;
            }
            return (float)$vec_price_models['reduced_price']->raw_attributes['amount'];
        }
        else if ( isset($vec_price_models['base_price']) && $vec_price_models['base_price']->raw_attributes['amount'] > 0 )
        {
            if ( ! $is_raw )
            {
                return $vec_price_models['base_price']->amount;
            }
            return (float)$vec_price_models['base_price']->raw_attributes['amount'];
        }

        return 0;
    }


    /**
     * Get a specific "product_price"
     */
    public function get_product_price($price_alias, $is_raw = false)
    {
        $vec_price_models = $this->get_price_models();
        if ( isset($vec_price_models[$price_alias]) && $vec_price_models[$price_alias]->raw_attributes['amount'] > 0 )
        {
            if ( ! $is_raw )
            {
                return $vec_price_models[$price_alias]->amount;
            }
            return (float)$vec_price_models[$price_alias]->raw_attributes['amount'];
        }

        return 0;
    }


    /**
     * Get base price
     */
    public function get_base_price($is_raw = false)
    {
        return $this->get_product_price('base_price', $is_raw);
    }


    /**
     * Get reduced price
     */
    public function get_reduced_price($is_raw = false)
    {
        return $this->get_product_price('reduced_price', $is_raw);
    }


    /**
     * Check if reduced price is enabled
     */
    public function is_reduced_price()
    {
        return $this->is_on_offer == 1 && $this->get_reduced_price(true) > 0;
    }


    /**
     * Update search product table
     */
    public function update_search_product()
    {
        $search_product_model = $this->searchProduct;
        if ( ! $search_product_model )
        {
            $search_product_model = Yii::createObject(SearchProduct::class);
            $search_product_model->product_id = $this->product_id;
        }

        $search_product_model->setAttributes([
            'product_type'      => $this->product_type,
            'default_sku'       => $this->default_sku,
            'is_disabled'       => $this->is_enabled() ? 0 : 1,
            'name'              => $this->name,
            'content'           => StringHelper::clean_html($this->description),
            'main_category_id'  => $this->main_category_id,
            'brand_category_id' => $this->brand_category_id,
            'weight'            => $this->weight
        ]);

        // Category names
        if ( $this->productCategories )
        {
            foreach ( $this->productCategories as $product_category_model )
            {
                if ( $product_category_model->category )
                {
                    if ( empty($search_product_model->main_category_id) )
                    {
                        $search_product_model->main_category_id = $product_category_model->category_id;
                    }
                    $search_product_model->content .= ' '. $product_category_model->category->title();
                }
            }
        }

        if ( ! $search_product_model->save() )
        {
            Log::save_model_error($search_product_model);
            return false;
        }

        return true;
    }


    /**
     * Returns a ProductAssociation model
     */
    public function get_product_association($product_id, $type)
    {
        return ProductAssociation::get()->where([
            'product_id'            => $this->product_id,
            'product_associated_id' => $product_id,
            'association_type'      => $type
        ])->one();
    }


    /**
     * Add a ProductAssociation model (association between prodcuts)
     * 
     * @return ProductAssociation
     */
    public function add_product_association($product_id, $type)
    {
        // Check if Product model exists
        $product_model = Product::findOne($product_id);
        if ( $product_model && $product_id != $this->product_id )
        {
            $product_association_model = $this->get_product_association($product_id, $type);
            if ( ! $product_association_model )
            {
                $product_association_model = Yii::createObject(ProductAssociation::class);
                $product_association_model->setAttributes([
                    'product_id'            => $this->product_id,
                    'product_associated_id' => $product_model->product_id,
                    'association_type'      => $type,
                    'weight'                => $product_model->weight
                ]);

                if ( ! $product_association_model->save() )
                {
                    Log::save_model_error($product_association_model);
                    return false;
                }

                return $product_association_model;
            }
        }

        return false;
    }


    /**
     * Remove a ProductAssociation from this product
     */
    public function remove_product_association($association_id)
    {
        $product_association_model = ProductAssociation::findOne($association_id);
        
        // Ensure this ProductAssociation model belongs to current product
        if ( $product_association_model->product_id == $this->product_id )
        {
            return $product_association_model && $product_association_model->delete();
        }

        return false;
    }


    /**
     * Returns the mail tokens
     */
    public function mailTokens()
    {
        return [
            '{product.id}'      => $this->product_id,
            '{product.name}'    => $this->name,
        ];
    }


    /**
     * Help text for the mail tokens
     */
    public function mailHelp()
    {
        return [
            '{product.id}'      => 'Product identificator (internal)',
            '{product.name}'    => 'Product name',
        ];
    }


    /**
     * Get title
     */
    public function title()
    {
        return $this->name;
    }
}
