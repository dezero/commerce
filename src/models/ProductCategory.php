<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\category\models\Category;
use dzlab\commerce\models\_base\ProductCategory as BaseProductCategory;
use dzlab\commerce\models\Product;
use user\models\User;
use Yii;

/**
 * ProductCategory model class for "commerce_product_category" database table
 *
 * Columns in table "commerce_product_category" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $product_id
 * @property integer $category_id
 * @property string $category_type
 * @property integer $created_date
 * @property integer $created_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class ProductCategory extends BaseProductCategory
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['product_id, category_id, category_type, created_date, created_uid', 'required'],
			['product_id, category_id, created_date, created_uid', 'numerical', 'integerOnly' => true],
			['category_type', 'length', 'max'=> 32],
			['product_id, category_id, category_type, created_date, created_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'product' => [self::BELONGS_TO, Product::class, 'product_id'],
            'category' => [self::BELONGS_TO, Category::class, 'category_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'product_id' => null,
			'category_id' => null,
			'category_type' => Yii::t('app', 'Category Type'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.category_type', $this->category_type, true);
        $criteria->compare('t.created_date', $this->created_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['product_id' => true]]
        ]);
    }


    /**
     * ProductCategory models list
     * 
     * @return array
     */
    public function productcategory_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['product_id', 'product_id', 'category_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = ProductCategory::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('product_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Return an array with category_id values
     */
    public function get_category_values( $vec_product_category_models = [] )
    {
        $vec_category_values = [];
        if ( !empty($vec_product_category_models) )
        {
            foreach ( $vec_product_category_models as $product_category_model )
            {
                $vec_category_values[$product_category_model->category_id] = $product_category_model->category_id;
            }
        }

        return $vec_category_values;
    }
}