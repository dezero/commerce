<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\TranslatedProductOption as BaseTranslatedProductOption;
use dzlab\commerce\models\ProductOption;
use dz\modules\settings\models\Language;
use user\models\User;
use Yii;

/**
 * TranslatedProductOption model class for "commerce_translated_product_option" database table
 *
 * Columns in table "commerce_translated_product_option" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $option_id
 * @property string $language_id
 * @property string $name
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class TranslatedProductOption extends BaseTranslatedProductOption
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['option_id, name, updated_date, updated_uid', 'required'],
			['option_id, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['language_id', 'length', 'max'=> 4],
			['name', 'length', 'max'=> 255],
			['uuid', 'length', 'max'=> 36],
			['language_id, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['option_id, language_id, name, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'option' => [self::BELONGS_TO, ProductOption::class, 'option_id'],
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'option_id' => null,
			'language_id' => null,
			'name' => Yii::t('app', 'Name'),
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['option_id' => true]]
        ]);
    }


    /**
     * TranslatedProductOption models list
     * 
     * @return array
     */
    public function translatedproductoption_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['option_id', 'option_id', 'language_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = TranslatedProductOption::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('option_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->name;
    } 
}