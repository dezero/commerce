<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use Dz;
use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\Gateway as BaseGateway;
use dzlab\commerce\models\Transaction;
use user\models\User;
use Yii;

/**
 * Gateway model class for "commerce_gateway" database table
 *
 * Columns in table "commerce_gateway" available as properties of the model,
 * followed by relations of table "commerce_gateway" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $gateway_id
 * @property string $alias
 * @property string $name
 * @property string $description
 * @property string $settings_json
 * @property integer $is_disabled
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $updatedUser
 * @property mixed $transactions
 */
class Gateway extends BaseGateway
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['alias, name, created_date, created_uid, updated_date, updated_uid', 'required'],
			['is_disabled, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['alias', 'length', 'max'=> 32],
			['name', 'length', 'max'=> 128],
			['description', 'length', 'max'=> 255],
			['uuid', 'length', 'max'=> 36],
			['description, settings_json, is_disabled, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['settings_json', 'safe'],
			['gateway_id, alias, name, description, settings_json, is_disabled, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
			'transactions' => [self::HAS_MANY, Transaction::class, 'gateway_id'],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'gateway_id' => Yii::t('app', 'Gateway'),
			'alias' => Yii::t('app', 'Alias'),
			'name' => Yii::t('app', 'Name'),
			'description' => Yii::t('app', 'Description'),
			'settings_json' => Yii::t('app', 'Settings Json'),
			'is_disabled' => Yii::t('app', 'Is Disabled'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => Yii::t('app', 'Disable Uid'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'updatedUser' => null,
			'transactions' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.gateway_id', $this->gateway_id);
        $criteria->compare('t.alias', $this->alias, true);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.settings_json', $this->settings_json, true);
        $criteria->compare('t.is_disabled', $this->is_disabled);
        $criteria->compare('t.disable_date', $this->disable_date);
        $criteria->compare('t.disable_uid', $this->disable_uid);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['gateway_id' => true]]
        ]);
    }


    /**
     * Gateway models list
     * 
     * @return array
     */
    public function gateway_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['gateway_id', 'alias', 'name'];
        $criteria->addCondition('is_disabled = 0 AND disable_date IS NULL');
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';

        // Exclude DUMMY payment for PRODUCTION environment
        if ( $list_id !== 'full' && Dz::is_production() )
        {
            $criteria->addCondition('alias <> "dummy"');
        }
        
        $vec_models = Gateway::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->gateway_id] = [
                    'title' => $que_model->title(),
                    'alias' => $que_model->alias
                ];
            }
        }

        return $vec_output;
    }


    /**
     * Create a new "purchase" Transaction
     */
    public function purchase($order_model, $payment_amount, $vec_input_data = [])
    {
        return [
            'error_msg'         => '',
            'error_code'        => 0,
            'transaction_id'    => 0
        ];
    }


    /**
     * Process the response from the Gateway
     */
    public function response($order_model, $transaction_type = 'purchase', $payment_amount = null, $vec_input_data = [])
    {
        return false;
    }


    /**
     * Get order reference for this Gateway
     */
    public function get_order_reference($order_id, $transaction_num = 0)
    {
        return $this->alias .'-'. $order_id .'-'. $transaction_num;
    }


    /**
     * Process an array from the request data of a Transaction model
     */
    public function view_request_data($vec_data, $transaction_model = null)
    {
        return $vec_data;
    }


    /**
     * Process an array from the response data of a Transaction model
     */
    public function view_response_data($vec_data, $transaction_model = null)
    {
        return $vec_data;
    }

    /**
     * Find a Gateway by alias
     */
    public function findByAlias($alias, $language_id = null)
    {
        if ( ! $language_id )
        {
            return Yii::app()->checkoutManager->get_gateway($alias);
        }

        return parent::findByAlias($alias, $language_id);
    }


    /**
     * Short alias of ActiverRecord::model()->findByPk()
     */
    public static function findOne($pk, $condition = '', $params = [])
    {
        $class_name = Yii::resolveClass(get_called_class());
        $gateway_model = $class_name::model()->findByPk($pk, $condition, $params);

        // Try to find a specific subclass for this Gateway model (defined on CheckoutManager)
        if ( $gateway_model )
        {
            $gateway_submodel = Yii::app()->checkoutManager->get_gateway($gateway_model->alias);
            if ( $gateway_submodel )
            {
                return $gateway_submodel;
            }
        }

        return $gateway_model;
    }
}
