<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\Stock as BaseStock;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\Variant;
use dzlab\commerce\models\Warehouse;
use user\models\User;
use Yii;

/**
 * Stock model class for "commerce_stock" database table
 *
 * Columns in table "commerce_stock" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $warehouse_id
 * @property integer $variant_id
 * @property integer $product_id
 * @property integer $stock
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class Stock extends BaseStock
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['variant_id, product_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['warehouse_id, variant_id, product_id, stock, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['warehouse_id, stock', 'default', 'setOnEmpty' => true, 'value' => null],
			['warehouse_id, variant_id, product_id, stock, created_date, created_uid, updated_date, updated_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'warehouse' => [self::BELONGS_TO, Warehouse::class, 'warehouse_id'],
            'variant' => [self::BELONGS_TO, Variant::class, 'variant_id'],
            'product' => [self::BELONGS_TO, Product::class, 'product_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'warehouse_id' => null,
			'variant_id' => null,
			'product_id' => null,
			'stock' => Yii::t('app', 'Stock'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.stock', $this->stock);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['warehouse_id' => true]]
        ]);
    }


    /**
     * Stock models list
     * 
     * @return array
     */
    public function stock_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['warehouse_id', 'warehouse_id', 'variant_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Stock::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('warehouse_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}