<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetImage;
use dzlab\commerce\models\_base\ProductImage as BaseProductImage;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\Variant;
use user\models\User;
use Yii;

/**
 * ProductImage model class for "commerce_product_image" database table
 *
 * Columns in table "commerce_product_image" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $product_id
 * @property integer $image_id
 * @property integer $variant_id
 * @property integer $is_default
 * @property integer $weight
 * @property integer $created_date
 * @property integer $created_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class ProductImage extends BaseProductImage
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['product_id, image_id, created_date, created_uid', 'required'],
			['product_id, image_id, variant_id, is_default, weight, created_date, created_uid', 'numerical', 'integerOnly' => true],
			['variant_id, is_default, weight', 'default', 'setOnEmpty' => true, 'value' => null],
			['product_id, image_id, variant_id, is_default, weight, created_date, created_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'product' => [self::BELONGS_TO, Product::class, 'product_id'],
            'image' => [self::BELONGS_TO, AssetImage::class, 'image_id'],
            'variant' => [self::BELONGS_TO, Variant::class, 'variant_id'],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'product_id' => null,
			'image_id' => null,
			'variant_id' => null,
			'is_default' => Yii::t('app', 'Is Default'),
			'weight' => Yii::t('app', 'Weight'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.is_default', $this->is_default);
        $criteria->compare('t.weight', $this->weight);
        $criteria->compare('t.created_date', $this->created_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['product_id' => true]]
        ]);
    }


    /**
     * ProductImage models list
     * 
     * @return array
     */
    public function productimage_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['product_id', 'product_id', 'image_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = ProductImage::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('product_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     *
     * @return bool
     */
    public function beforeValidate()
    {
        // Create new ProductImage model
        switch ( $this->scenario )
        {
            // Create new ProductImage
            case 'insert':
            case 'upload_import':
                // Manual weight has been set
                if ( $this->scenario == 'upload_import' && $this->weight > 0 )
                {
                    $this->weight = $this->weight - 1;
                }

                // Get last weight value automatically
                else
                {
                    $criteria = new DbCriteria();
                    $criteria->compare('product_id', $this->product_id);
                    $criteria->order = 'weight DESC';
                    $criteria->limit = 1;
                    $last_product_image_model = ProductImage::model()->find($criteria);
                    if ( $last_product_image_model )
                    {
                        $this->weight = $last_product_image_model->weight + 1;
                        if ( $this->weight > 255 )
                        {
                            $this->weight = 255;
                        }
                    }
                }

                // Default image?
                if ( $this->weight == 1 )
                {
                    $this->is_default = 1;
                    $product_model = Product::findOne($this->product_id);
                    if ( $product_model )
                    {
                        $product_model->save_default_image($this->image_id);
                    }
                }
            break;
        }

        return parent::beforeValidate();
    }


    /**
     * Action "delete"
     *
     * Deletes the row corresponding to this active record
     */
    public function delete()
    {
        // AssetImage::delete() methods is the responsible to DELETE this model
        if ( $this->image )
        {
            return $this->image->delete();
        }

        // Going on with "delete" action
        return parent::delete();
    }


    /**
     * Check if this ProductImage is the default image of the product
     */
    public function is_default_image()
    {
        return $this->product && $this->product->default_image_id === $this->image_id;
    }
}