<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\DiscountUse as BaseDiscountUse;
use dzlab\commerce\models\Customer;
use dzlab\commerce\models\Discount;
use dzlab\commerce\models\Order;
use user\models\User;
use Yii;

/**
 * DiscountUse model class for "commerce_discount_use" database table
 *
 * Columns in table "commerce_discount_use" available as properties of the model,
 * followed by relations of table "commerce_discount_use" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $usage_id
 * @property integer $discount_id
 * @property integer $user_id
 * @property integer $order_id
 * @property string $discount_json
 * @property integer $created_date
 * @property integer $created_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $discount
 * @property mixed $order
 * @property mixed $user
 */
class DiscountUse extends BaseDiscountUse
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['discount_id, user_id, created_date, created_uid', 'required'],
			['discount_id, user_id, order_id, created_date, created_uid', 'numerical', 'integerOnly' => true],
			['order_id, discount_json', 'default', 'setOnEmpty' => true, 'value' => null],
			['discount_json', 'safe'],
			['usage_id, discount_id, user_id, order_id, discount_json, created_date, created_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'discount' => [self::BELONGS_TO, Discount::class, 'discount_id'],
			'order' => [self::BELONGS_TO, Order::class, 'order_id'],
			'customer' => [self::BELONGS_TO, Customer::class, 'user_id'],
            'user' => [self::BELONGS_TO, User::class, ['user_id' => 'id']],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'usage_id' => Yii::t('app', 'Usage'),
			'discount_id' => null,
			'user_id' => null,
			'order_id' => null,
			'discount_json' => Yii::t('app', 'Discount Json'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'createdUser' => null,
			'discount' => null,
			'order' => null,
			'user' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.usage_id', $this->usage_id);
        $criteria->compare('t.discount_id', $this->discount_id);
        $criteria->compare('t.discount_json', $this->discount_json, true);
        $criteria->compare('t.created_date', $this->created_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['usage_id' => true]]
        ]);
    }


    /**
     * DiscountUse models list
     * 
     * @return array
     */
    public function discountuse_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['usage_id', 'discount_json'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = DiscountUse::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('usage_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}
