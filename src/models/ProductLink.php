<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\ProductLink as BaseProductLink;
use dzlab\commerce\models\Product;
use user\models\User;
use Yii;

/**
 * ProductLink model class for "commerce_product_link" database table
 *
 * Columns in table "commerce_product_link" available as properties of the model,
 * followed by relations of table "commerce_product_link" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $link_id
 * @property integer $product_id
 * @property string $url
 * @property string $title
 * @property integer $is_video
 * @property integer $weight
 * @property string $source
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $product
 * @property mixed $updatedUser
 */
class ProductLink extends BaseProductLink
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['product_id, url, created_date, created_uid, updated_date, updated_uid', 'required'],
			['product_id, is_video, weight, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['url, title, source', 'length', 'max'=> 255],
			['uuid', 'length', 'max'=> 36],
			['title, is_video, weight, source, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['link_id, product_id, url, title, is_video, weight, source, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'product' => [self::BELONGS_TO, Product::class, 'product_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'link_id' => Yii::t('app', 'Link'),
			'product_id' => Yii::t('app', 'Product'),
			'url' => Yii::t('app', 'URL'),
			'title' => Yii::t('app', 'Title'),
			'is_video' => Yii::t('app', 'Is Video'),
			'weight' => Yii::t('app', 'Weight'),
			'source' => Yii::t('app', 'Source'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'product' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.link_id', $this->link_id);
        $criteria->compare('t.url', $this->url, true);
        $criteria->compare('t.title', $this->title, true);
        $criteria->compare('t.is_video', $this->is_video);
        $criteria->compare('t.weight', $this->weight);
        $criteria->compare('t.source', $this->source, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['link_id' => true]]
        ]);
    }


    /**
     * ProductLink models list
     * 
     * @return array
     */
    public function productlink_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['link_id', 'url'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = ProductLink::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('link_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Try to get EMBEDED URL for a Youtube video
     * 
     * @see https://gist.github.com/ghalusa/6c7f3a00fd2383e5ef33
     */
    public function get_youtube_url()
    {
        if ( !empty($this->url) )
        {
            /**
             * YOUTUBE -> Try to get video ID
             * 
             * http://youtu.be/dQw4w9WgXcQ
             * http://www.youtube.com/embed/dQw4w9WgXcQ
             * http://www.youtube.com/watch?v=dQw4w9WgXcQ
             * http://www.youtube.com/?v=dQw4w9WgXcQ
             * http://www.youtube.com/v/dQw4w9WgXcQ
             * http://www.youtube.com/e/dQw4w9WgXcQ
             * http://www.youtube.com/user/username#p/u/11/dQw4w9WgXcQ
             * http://www.youtube.com/sandalsResorts#p/c/54B8C800269D7C1B/0/dQw4w9WgXcQ
             * http://www.youtube.com/watch?feature=player_embedded&v=dQw4w9WgXcQ
             * http://www.youtube.com/?feature=player_embedded&v=dQw4w9WgXcQ
             */
            $youtube_id = '';
            $match = [];

            if ( preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $this->url, $match) )
            {
                $youtube_id = $match[1];
            }

            if ( !empty($youtube_id) )
            {
                return 'https://www.youtube.com/embed/'. $youtube_id .'?feature=oembed';
            }
        }

        return '';
    }


    /**
     * Try to get EMBEDED URL for a Vimeo video
     * 
     * @see https://gist.github.com/anjan011/1fcecdc236594e6d700f
     */
    public function get_vimeo_url()
    {
        if ( !empty($this->url) )
        {
            /**
             * VIMEO -> Try to get video ID

             * https://vimeo.com/11111111
             * http://vimeo.com/11111111
             * https://www.vimeo.com/11111111
             * http://www.vimeo.com/11111111
             * https://vimeo.com/channels/11111111
             * http://vimeo.com/channels/11111111
             * https://vimeo.com/groups/name/videos/11111111
             * http://vimeo.com/groups/name/videos/11111111
             * https://vimeo.com/album/2222222/video/11111111
             * http://vimeo.com/album/2222222/video/11111111
             * https://vimeo.com/11111111?param=test
             * http://vimeo.com/11111111?param=test
             */
            $vimeo_id = '';
            $match = [];

            if ( preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $this->url, $match) ) {
                $vimeo_id = $match[3];
            }

            if ( !empty($vimeo_id) )
            {
                return 'https://player.vimeo.com/video/'. $vimeo_id .'?title=0&byline=0';
            }
        }

        return '';
    }
}