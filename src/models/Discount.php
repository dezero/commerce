<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\Discount as BaseDiscount;
use dzlab\commerce\models\DiscountCategory;
use dzlab\commerce\models\DiscountProduct;
use dzlab\commerce\models\DiscountUse;
use dzlab\commerce\models\LineItem;
use dzlab\commerce\models\Order;
use dzlab\commerce\models\Product;
use user\models\User;
use Yii;

/**
 * Discount model class for "commerce_discount" database table
 *
 * Columns in table "commerce_discount" available as properties of the model,
 * followed by relations of table "commerce_discount" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $discount_id
 * @property string $name
 * @property string $code
 * @property string $discount_type
 * @property integer $is_active
 * @property integer $limit_uses
 * @property integer $is_per_user_limit
 * @property integer $total_uses
 * @property integer $start_date
 * @property integer $expiration_date
 * @property double $amount
 * @property integer $is_fixed_amount
 * @property string $application_type
 * @property integer $is_free_shipping
 * @property double $minimum_amount
 * @property integer $is_category_restriction
 * @property string $category_restriction_type
 * @property integer $is_product_restriction
 * @property string $product_restriction_type
 * @property integer $is_user_restriction
 * @property string $user_restriction_type
 * @property string $user_restriction_values
 * @property integer $is_after_order_restriction
 * @property integer $after_order_num_days
 * @property string $internal_comments
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $disableUser
 * @property mixed $updatedUser
 * @property mixed $discountUses
 * @property mixed $lineItems
 * @property mixed $orders
 */
class Discount extends BaseDiscount
{
    /**
     * Alias for amount (when is_fixed_amount = 0)
     */ 
    public $perc_amount = 0;


    /**
     * Discount error code when applying the discount
     */
    public $error_message;


    /**
     * Cache for array with product ID's of discountProducts models
     */
    private $_vec_product_ids = [];


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['name, code, created_date, created_uid, updated_date, updated_uid', 'required'],
			['is_active, limit_uses, is_per_user_limit, total_uses, start_date, expiration_date, is_fixed_amount, is_free_shipping, is_category_restriction, is_product_restriction, is_user_restriction, is_after_order_restriction, after_order_num_days, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['amount, perc_amount, minimum_amount', 'numerical'],
			['name, code, user_restriction_values', 'length', 'max'=> 255],
			['uuid', 'length', 'max'=> 36],
            ['discount_type', 'in', 'range' => ['code', 'auto']],
			['application_type', 'in', 'range' => ['total', 'items', 'shipping']],
            ['category_restriction_type', 'in', 'range' => ['include', 'exclude']],
            ['product_restriction_type', 'in', 'range' => ['include', 'exclude']],
            ['user_restriction_type', 'in', 'range' => ['users', 'roles']],
			['code, discount_type, is_active, limit_uses, is_per_user_limit, total_uses, start_date, expiration_date, amount, perc_amount, is_fixed_amount, application_type, is_free_shipping, minimum_amount, is_category_restriction, category_restriction_type, is_product_restriction, product_restriction_type, is_user_restriction, user_restriction_type, user_restriction_values, is_after_order_restriction, after_order_num_days, internal_comments, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['internal_comments', 'safe'],
			['discount_id, name, code, discount_type, is_active, limit_uses, is_per_user_limit, total_uses, start_date, expiration_date, amount, perc_amount, is_fixed_amount, application_type, is_free_shipping, minimum_amount, is_category_restriction, category_restriction_type, is_product_restriction, product_restriction_type, is_user_restriction, user_restriction_type, user_restriction_values, is_after_order_restriction, after_order_num_days, internal_comments, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid, disable_filter', 'safe', 'on' => 'search'],

            // Custom validation rules
            ['amount', 'validate_amount', 'on' => 'insert, update'],
            ['start_date', 'validate_dates', 'on' => 'insert, update'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
            'lineItems' => [self::HAS_MANY, LineItem::class, 'discount_id'],
			'discountUses' => [self::HAS_MANY, DiscountUse::class, 'discount_id'],
			'orders' => [self::HAS_MANY, Order::class, 'discount_id'],

            // Custom relations
            'totalUses' => [self::STAT, DiscountUse::class, 'discount_id'],
            'discountCategories' => [self::HAS_MANY, DiscountCategory::class, 'discount_id'],
            'discountProducts' => [self::HAS_MANY, DiscountProduct::class, 'discount_id'],
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'start_date' => 'd/m/Y',
                    'expiration_date' => 'd/m/Y',
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

            // Currency format
            'CurrencyBehavior' => [
                'class' => '\dz\behaviors\CurrencyBehavior',
                'columns' => [
                    'amount',
                    'minimum_amount'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'discount_id' => Yii::t('app', 'Discount'),
			'name' => Yii::t('app', 'Name'),
			'code' => Yii::t('app', 'Coupon Code'),
            'discount_type' => Yii::t('app', 'Discount Type'),
			'is_active' => Yii::t('app', 'Active?'),
			'limit_uses' => Yii::t('app', 'Limit Uses'),
            'is_per_user_limit' => Yii::t('app', 'Is Per User Limit'),
			'total_uses' => Yii::t('app', 'Total Uses'),
			'start_date' => Yii::t('app', 'Start Date'),
			'expiration_date' => Yii::t('app', 'Expiration Date'),
			'amount' => Yii::t('app', 'Amount'),
            'perc_amount' => Yii::t('app', 'Percentage'),
			'is_fixed_amount' => Yii::t('app', 'Amount Type'),
			'application_type' => Yii::t('app', 'Application Type'),
			'is_free_shipping' => Yii::t('app', 'Free Shipping?'),
            'minimum_amount' => Yii::t('app', 'Minimum Amount'),
            'is_category_restriction' => Yii::t('app', 'Category Restriction?'),
            'category_restriction_type' => Yii::t('app', 'Category Restriction'),
            'is_product_restriction' => Yii::t('app', 'Product Restriction?'),
            'product_restriction_type' => Yii::t('app', 'Product Restriction'),
            'is_user_restriction' => Yii::t('app', 'User Restriction?'),
            'user_restriction_type' => Yii::t('app', 'User Restriction'),
            'user_restriction_values' => Yii::t('app', 'User Restriction Values'),
            'is_after_order_restriction' => Yii::t('app', 'Valid after Order?'),
            'after_order_num_days' => Yii::t('app', ' Number of Days (after order)'),
			'internal_comments' => Yii::t('app', 'Internal Comments'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'createdUser' => null,
			'disableUser' => null,
			'updatedUser' => null,
			'discountUses' => null,
            'lineItems' => null,
			'orders' => null,
		];
	}


    /**
     * Get "discount_type" labels
     */
    public function discount_type_labels()
    {
        return [
            'code' => Yii::t('app', 'Using a COUPON code'),
            'auto' => Yii::t('app', 'Applied AUTOMATICALLY'),
        ];
    }


    /**
     * Get "discount_type" specific label
     */
    public function discount_type_label($discount_type = null)
    {
        if ( $discount_type === null )
        {
            $discount_type = $this->discount_type;
        }

        $vec_labels = $this->discount_type_labels();
        return isset($vec_labels[$discount_type]) ? $vec_labels[$discount_type] : '';
    }


    /**
     * Get "application_type" labels
     */
    public function application_type_labels()
    {
        return [
            'total' => Yii::t('app', 'total'),
            'items' => Yii::t('app', 'items'),
            'shipping' => Yii::t('app', 'shipping'),
        ];
    }


    /**
     * Get "application_type" specific label
     */
    public function application_type_label($application_type = null)
    {
        if ( $application_type === null )
        {
            $application_type = $this->application_type;
        }

        $vec_labels = $this->application_type_labels();
        return isset($vec_labels[$application_type]) ? $vec_labels[$application_type] : '';
    }


    /**
     * Get "category_restriction_type" labels
     */
    public function category_restriction_type_labels()
    {
        return [
            'include' => Yii::t('app', 'Include'),
            'exclude' => Yii::t('app', 'Exclude'),
        ];
    }


    /**
     * Get "category_restriction_type" specific label
     */
    public function category_restriction_type_label($category_restriction_type = null)
    {
        if ( $category_restriction_type === null )
        {
            $category_restriction_type = $this->category_restriction_type;
        }
        $vec_labels = $this->category_restriction_type_labels();
        return isset($vec_labels[$category_restriction_type]) ? $vec_labels[$category_restriction_type] : '';
    }


    /**
     * Get "product_restriction_type" labels
     */
    public function product_restriction_type_labels()
    {
        return [
            'include' => Yii::t('app', 'Include'),
            'exclude' => Yii::t('app', 'Exclude'),
        ];
    }


    /**
     * Get "product_restriction_type" specific label
     */
    public function product_restriction_type_label($product_restriction_type = null)
    {
        if ( $product_restriction_type === null )
        {
            $product_restriction_type = $this->product_restriction_type;
        }
        $vec_labels = $this->product_restriction_type_labels();
        return isset($vec_labels[$product_restriction_type]) ? $vec_labels[$product_restriction_type] : '';
    }


    /**
     * Get "user_restriction_type" labels
     */
    public function user_restriction_type_labels()
    {
        return [
            'users' => Yii::t('app', 'Users'),
            'roles' => Yii::t('app', 'Roles'),
        ];
    }


    /**
     * Get "user_restriction_type" specific label
     */
    public function user_restriction_type_label($user_restriction_type = null)
    {
        if ( $user_restriction_type === null )
        {
            $user_restriction_type = $this->user_restriction_type;
        }
        $vec_labels = $this->user_restriction_type_labels();
        return isset($vec_labels[$user_restriction_type]) ? $vec_labels[$user_restriction_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.discount_id', $this->discount_id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.code', $this->code, true);
        $criteria->compare('t.discount_type', $this->discount_type);
        $criteria->compare('t.is_active', $this->is_active);

        // Disable filter
        if ( $this->disable_filter )
        {
            switch ( $this->disable_filter )
            {
                // Show just ENABLED discount
                case 1:
                    $criteria->addCondition('t.disable_date IS NULL OR t.disable_date = "0"');
                break;
                
                // Show just DISABLED discount
                case 2:
                    $criteria->addCondition('t.disable_date IS NOT NULL OR t.disable_date <> "0"');
                break;
                
                // Show ALL discounts
                case 3:
                break;
            }
        }

        // $criteria->compare('t.limit_uses', $this->limit_uses);
        // $criteria->compare('t.is_per_user_limit', $this->is_per_user_limit);
        // $criteria->compare('t.total_uses', $this->total_uses);
        // $criteria->compare('t.start_date', $this->start_date);
        // $criteria->compare('t.expiration_date', $this->expiration_date);
        // $criteria->compare('t.amount', $this->amount);
        // $criteria->compare('t.is_fixed_amount', $this->is_fixed_amount);
        // $criteria->compare('t.application_type', $this->application_type, true);
        // $criteria->compare('t.is_free_shipping', $this->is_free_shipping);
        // $criteria->compare('t.minimum_amount', $this->minimum_amount);
        // $criteria->compare('t.internal_comments', $this->internal_comments, true);
        // $criteria->compare('t.disable_date', $this->disable_date);
        // $criteria->compare('t.created_date', $this->created_date);
        // $criteria->compare('t.updated_date', $this->updated_date);
        // $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['discount_id' => true]]
        ]);
    }


    /**
     * Discount models list
     * 
     * @return array
     */
    public function discount_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['discount_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Discount::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('discount_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     *
     * @return bool
     */
    public function beforeValidate()
    {
        // Discount type = AUTO? If so, generate an internal discount code
        if ( $this->scenario === 'insert' && $this->discount_type === 'auto' )
        {
            $this->code = 'DISCOUNT_'. time();
        }

        // Percentage amount has been added?
        if ( $this->is_fixed_amount == 0 && !empty($this->perc_amount) )
        {
            $this->amount = $this->perc_amount;
        }

        // Product restriction disabled when ALL value is selected (default)
        if ( $this->product_restriction_type === 'all' )
        {
            $this->is_product_restriction = 0;
            $this->product_restriction_type = 'include';
        }
        // Product restriction enabled when INCLUDE or EXCLUDE values are selected
        else
        {
            $this->is_product_restriction = 1;
        }

        // User restriction disabled when ALL value is selected (default)
        if ( $this->user_restriction_type === 'all' )
        {
            $this->is_user_restriction = 0;
            $this->user_restriction_type = 'users';
        }
        // User restriction enabled when some value is selected
        else
        {
            $this->is_user_restriction = 1;
        }

        return parent::beforeValidate();
    }


    /**
     * Event "afterSave"
     *
     * This method is invoked after saving a record successfully
     */
    public function afterSave()
    {
        $discount_model = Discount::model()->cache(0)->findByPk($this->discount_id);
        if ( $discount_model )
        {
            // Discount type = AUTO? If so, generate an internal discount code with DISCOUNT_ID
            if ( $this->scenario === 'insert' && $this->discount_type === 'auto' )
            {
                $discount_model->code = 'DISCOUNT_'. $discount_model->discount_id;
                $discount_model->saveAttributes(['code' => $discount_model->code]);
            }

            // Check if Discount is valid. It saves "is_active" attribute
            $is_valid = $discount_model->is_valid(null, true);
        }

        return parent::afterSave();
    }


    /**
     * Disables the discount
     */
    public function disable()
    {
        if ( parent::disable() )
        {
            // Mark Discount as NOT ACTIVE
            $this->set_active(false);

            return true;
        }

        return false;
    }


    /**
     * Enables the discount
     */
    public function enable()
    {
        if ( parent::enable() )
        {
            // Mark Discount as ACTIVE
            $this->set_active(true);

            // If Discount is INVALID, mark it as NOT ACTIVE
            if ( ! $this->is_valid(null, true) )
            {
                $this->set_active(false);
            }

            return true;
        }

        return false;
    }


    /**
     * Find a Discount model by code
     */
    public function findByCode($discount_code)
    {
        return $this->findByAttributes([
            'code'  => $discount_code
        ]);
    }


    /**
     * Check if discount conditions are valid to be applied
     */
    public function is_valid($order_model = null, $is_save_active = false)
    {
        // #1 - IS DISABLED?
        if ( ! $this->is_enabled() )
        {
            return false;
        }

        // #2 - DATES ARE VALID?
        if ( !empty($this->start_date) )
        {
            $now = time();

            // Start date hasn't yet arrived
            if ( (int)$this->raw_attributes['start_date'] > $now )
            {
                // Update "is_active" attribute
                if ( $is_save_active )
                {
                    $this->set_active(false);
                }
                return false;
            }

            // Expiration date
            if ( !empty($this->expiration_date) )
            {
                // Add one day to expiration_date because discount must be available until 23:59 hours
                $expiration_date = (int)$this->raw_attributes['expiration_date'];
                $expiration_date += (86400 - 1);
                if ( $expiration_date < $now )
                {
                    // Discount has expired
                    $this->error_message = Yii::t('app', 'Discount has expired');

                    // Update "is_active" attribute
                    if ( $is_save_active )
                    {
                        $this->set_active(false);
                    }

                    return false;
                }
            }
        }

        // #3 - CATEGORY IS ALLOWED?
        /*
        if ( $category_id !== null && $this->categories )
        {
            // Get allowed categories
            $vec_allowed_categories = [];
            foreach ( $this->categories as $discount_category_model )
            {
                $vec_allowed_categories[$discount_category_model->category_id] = $discount_category_model->category_id;
            }

            // This discount is not allowed for this category
            if ( ! isset($vec_allowed_categories[$category_id]) )
            {
                // Discount not available for this examination
                $this->error_message = 'Discount not available for this category';
                return false;
            }
        }
        */

        // #4 - LIMIT USES?
        if ( $this->limit_uses > 0 && $this->totalUses >= $this->limit_uses )
        {
            // Discount already used
            $this->error_message = Yii::t('app', 'Discount already used (limit uses)');

            // Update "is_active" attribute
            if ( $is_save_active )
            {
                $this->set_active(false);
            }

            return false;
        }

        // #5 - It only can exists one APPLIED AUTOMATICALLY discount
        if ( $this->discount_type === 'auto' )
        {
            $active_discount_model = Yii::app()->discountManager->get_active_discount();
            if ( $active_discount_model && $active_discount_model->discount_id !== $this->discount_id )
            {
                // Discount already used
                $this->error_message = Yii::t('app', 'It already exists another AUTO discount');

                // Update "is_active" attribute
                // if ( $is_save_active )
                // {
                //     $this->set_active(false);
                // }

                return false;
            }
        }

        // #6 - IS PER USER LIMIT?
        if ( $this->is_per_user_limit == 1 )
        {
            $user_id = ( $order_model !== null && !empty($order_model->user_id) ) ? $order_model->user_id : Yii::app()->user->id;
            $vec_discount_use_models = $this->get_uses_by_user($user_id);
            if ( !empty($vec_discount_use_models) )
            {
                // Discount already used by this customer
                $this->error_message = Yii::t('app', 'You have already used this discount');

                return false;
            }
        }

        // #7 - MINIMUM AMOUNT?
        if ( $order_model !== null && !empty($this->minimum_amount) )
        {
            $subtotal_price = $order_model->raw_attributes['items_price'] + $order_model->raw_attributes['shipping_price'];
            if ( $subtotal_price < $this->raw_attributes['minimum_amount'] )
            {
                // Discount cannot be applie because Order amount is lower than the defined minimum amount
                $this->error_message = Yii::t('app', 'The minimum amount to apply this discount is {amount} €', ['{amount}' => $this->minimum_amount]);

                return false;
            }
        }

        // #8 - VALID "X" DAYS AFTER ORDER
        if ( $this->is_after_order_restriction == 1 && $this->after_order_num_days > 0 )
        {
            // Discount only available for registered users
            if ( Yii::app()->user->id == 0 )
            {
                $this->error_message = Yii::t('app', 'This discount is only available to registered users');

                return false;
            }

            // Check if the discount can be applied or it has been expired
            if ( $order_model !== null )
            {
                // Check if customer exists
                if ( ! $order_model->customer || ! $order_model->customer->lastOrder )
                {
                    $this->error_message = Yii::t('app', 'Discount cannot be applied');

                    return false;
                }

                // Check if last order has expired
                $last_order_model = $order_model->customer->lastOrder;
                $now = time();

                // Add "after_order_num_days" to last order date and include last day
                // $expiration_date = (int)$last_order_model->raw_attributes['ordered_date'] + ($this->after_order_num_days * 86400);
                $ordered_date = DateHelper::unix_to_date($last_order_model->raw_attributes['ordered_date'], 'Ymd');
                $expiration_date = DateHelper::date_to_unix($ordered_date) + ($this->after_order_num_days * 86400);
                $expiration_date += (86400 - 1);

                if ( $expiration_date < $now )
                {
                    // Discount has expired
                    $this->error_message = Yii::t('app', 'Discount has expired');

                    return false;
                }
            }
        }

        // Update "is_active" attribute
        if ( $is_save_active )
        {
            $this->set_active(true);
        }

        return true;
    }


    /**
     * Save "is_active" attribubte
     */
    public function set_active($is_active)
    {
        $is_active = $is_active ? 1 : 0;

        if ( $this->is_active != $is_active )
        {
            $this->is_active = $is_active ? 1 : 0;
            $this->saveAttributes(['is_active' => $this->is_active]);
        }

        return true;
    }


    /**
     * Apply discount to a given Order model
     */
    public function apply_order_discount($order_model)
    {
        $discount_price = 0;

        if ( $this->is_valid($order_model) )
        {
            $vec_line_item_models = $this->get_applicable_line_items($order_model);
            if ( !empty($vec_line_item_models) )
            {
                $raw_items_price = 0;

                foreach ( $vec_line_item_models as $line_item_model )
                {
                    $raw_items_price += $line_item_model->raw_attributes['total_price'];
                    $line_item_model->add_discount($this);
                }

                // Strange situation. But we prevent it
                if ( $raw_items_price > $order_model->raw_attributes['items_price'] )
                {
                    $raw_items_price = $order_model->raw_attributes['items_price'];
                }

                if ( $raw_items_price > 0 )
                {
                    // Apply discount to total ITEMS price
                    if ( $this->application_type == 'items' )
                    {
                        $discount_price = $this->apply_discount_price($raw_items_price);
                    }

                    // Apply discount to total ORDER price
                    else
                    {
                        $discount_price = $this->apply_discount_price($raw_items_price + $order_model->raw_attributes['shipping_price']);
                    }
                }
            }
        }

        return $discount_price;
    }


    /**
     * Return LineItem models where this Discount is applicable
     */
    public function get_applicable_line_items($order_model)
    {
        $vec_line_item_models = [];
        
        if ( $order_model->lineItems )
        {
            // By default, all order's line items will be applicable
            foreach ( $order_model->lineItems as $line_item_model )
            {
                $vec_line_item_models[$line_item_model->line_item_id] = $line_item_model;
            }

            // Product restriction?
            if ( $this->is_product_restriction == 1 && $this->discountProducts )
            {
                // Get array with product ID's of discountProducts models
                $vec_product_ids = $this->_get_product_ids();
                foreach ( $order_model->lineItems as $line_item_model )
                {
                    // Excluded product?
                    if ( $this->product_restriction_type === 'exclude' && isset($vec_product_ids[$line_item_model->product_id]) )
                    {
                        unset($vec_line_item_models[$line_item_model->line_item_id]);
                    }

                    // Included product?
                    else if ( $this->product_restriction_type === 'include' && !isset($vec_product_ids[$line_item_model->product_id]) )
                    {
                        unset($vec_line_item_models[$line_item_model->line_item_id]);
                    }
                }
            }
        }

        return $vec_line_item_models;
    }


    /**
     * Check if current Discount can be applicable for a given Product model
     */
    public function is_applicable($product_model)
    {
        // Product restriction?
        if ( $this->is_product_restriction == 1 && $this->discountProducts )
        {
            // Get array with product ID's of discountProducts models
            $vec_product_ids = $this->_get_product_ids();

            // Excluded product?
            if ( $this->product_restriction_type === 'exclude' && isset($vec_product_ids[$product_model->product_id]) )
            {
                return false;
            }

            // Included product?
            else if ( $this->product_restriction_type === 'include' && !isset($vec_product_ids[$product_model->product_id]) )
            {
                return false;
            }
        }

        return true;
    }


    /**
     * Apply discount from a given base price and return discount price
     */
    public function apply_discount_price($base_price)
    {
        $discount_price_raw = 0;

        // Fixed amount discount?
        if ( $this->is_fixed_amount == 1 )
        {
            return $this->raw_attributes['amount'];
        }

        // Percentage discount?
        return round($base_price * ( $this->raw_attributes['amount'] / 100), 2);
    }


    /**
     * Validate amount field
     */
    public function validate_amount($attribute, $params)
    {
        if ( $attribute == 'amount' && $this->amount <= 0 )
        {
            $this->addError('amount', 'Amount cannot be zero or empty.');   
        }
    }


    /**
     * Validate start_date and expiration_date fields
     */
    public function validate_dates($attribute, $params)
    {
        if ( !empty($this->start_date) && !empty($this->expiration_date) )
        {
            $raw_start_date = $this->start_date;
            if ( preg_match("/\//", $raw_start_date) )
            {
                $raw_start_date = DateHelper::date_format_to_unix($raw_start_date);
            }

            $raw_expiration_date = $this->expiration_date;
            if ( preg_match("/\//", $raw_expiration_date) )
            {
                $raw_expiration_date = DateHelper::date_format_to_unix($raw_expiration_date);
            }

            if ( $raw_start_date > $raw_expiration_date )
            {
                $this->addError('start_date', 'Available dates - The expiration date cannot be before the start date');
            }
        }
    }


    /**
     * Return amount label ready to show on a view template
     */
    public function amount_label()
    {
        if ( $this->is_fixed_amount == 1 )
        {
            return '-'. $this->amount . ' &euro;';
        }
        else
        {
            return str_replace(".00", "", $this->amount) . '% off';
        }
    }


    /**
     * Register new use for this discount
     * 
     * @return DiscountUse model or NULL if error
     */
    public function create_discount_use($order_model)
    {
        // Ensure we don't use the same discount and order twice
        $discount_use_model = DiscountUse::get()->where([
            'discount_id'   => $this->discount_id,
            'order_id'      => $order_model->order_id
        ])->one();

        if ( ! $discount_use_model )
        {
            $discount_use_model = Yii::createObject(DiscountUse::class);
            $discount_use_model->setAttributes([
                'discount_id'       => $this->discount_id,
                'user_id'           => $order_model->user_id,
                'order_id'          => $order_model->order_id,
                'discount_json'     => Json::encode($this->raw_attributes),
            ]);

            if ( ! $discount_use_model->save() )
            {
                Log::save_model_error($discount_use_model);
                return false;
            }
        }

        return $discount_use_model;
    }


    /**
     * Returns an array of DiscountUse models filtered by user
     */
    public function get_uses_by_user($user_id)
    {
        $vec_conditions = [
            'discount_id'   => $this->discount_id,
        ];

        if ( $user_id !== null )
        {
            $vec_conditions['user_id'] = $user_id;
        }
        
        // Return DiscountUse models
        return DiscountUse::get()->where($vec_conditions)->all();
    }


    /**
     * Returns a DiscountProduct model
     */
    public function get_discount_product($product_id)
    {
        return DiscountProduct::get()
            ->where([
                'discount_id'   => $this->discount_id,
                'product_id'    => $product_id
            ])
            ->one();
    }


    /**
     * Add a DiscountProduct model
     * 
     * @return DiscountProduct
     */
    public function add_product($product_id)
    {
        // Check if Product model exists
        $product_model = Product::findOne($product_id);
        if ( $product_model )
        {
            $discount_product_model = $this->get_discount_product($product_id);
            if ( ! $discount_product_model )
            {
                $discount_product_model = Yii::createObject(DiscountProduct::class);
                $discount_product_model->setAttributes([
                    'discount_id'   => $this->discount_id,
                    'product_id'    => $product_id
                ]);

                if ( ! $discount_product_model->save() )
                {
                    Log::save_model_error($discount_product_model);
                    return false;
                }

                return $discount_product_model;
            }
        }

        return false;
    }


    /**
     * Remove a DiscountProduct from this product
     */
    public function remove_product($product_id)
    {
        $discount_product_model = $this->get_discount_product($product_id);
        
        // Ensure this DiscountProduct model belongs to current discount
        if ( $discount_product_model->discount_id == $this->discount_id )
        {
            return $discount_product_model && $discount_product_model->delete();
        }

        return false;
    }


    /**
     * Remove all the DiscountProduct form this product
     */
    public function remove_all_products()
    {
        if ( $this->discountProducts )
        {
            foreach ( $this->discountProducts as $discount_product_model )
            {
                $discount_product_model->delete();
            }
        }

        return true;
    }


    /**
     * Discount title
     */
    public function title()
    {
        return ( $this->discount_type === 'code' && !empty($this->code) ) ? $this->code : $this->name;
    }



    /**
     * Return cache for array with product ID's of discountProducts models
     */
    private function _get_product_ids()
    {
        // Check if it was cached before
        if ( empty($this->_vec_product_ids) )
        {
            // Product restriction?
            if ( $this->is_product_restriction == 1 && $this->discountProducts )
            {
                foreach ( $this->discountProducts as $discount_product_model )
                {
                    $this->_vec_product_ids[$discount_product_model->product_id] = $discount_product_model->product_id;
                }
            }
        }

        return $this->_vec_product_ids;
    }
}
