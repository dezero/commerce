<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\category\models\Category;
use dzlab\commerce\models\_base\ProductOption as BaseProductOption;
use dzlab\commerce\models\TranslatedProductOption;
use user\models\User;
use Yii;

/**
 * ProductOption model class for "commerce_product_option" database table
 *
 * Columns in table "commerce_product_option" available as properties of the model,
 * followed by relations of table "commerce_product_option" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $option_id
 * @property string $option_type
 * @property integer $weight
 * @property string $name
 * @property double $price
 * @property integer $category_id
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $category
 * @property mixed $createdUser
 * @property mixed $disableUser
 * @property mixed $updatedUser
 * @property mixed $variants
 */
class ProductOption extends BaseProductOption
{
    /**
     * Options type configuration
     */
    protected $vec_config = [];


	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['option_type, name, created_date, created_uid, updated_date, updated_uid', 'required'],
			['weight, category_id, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['price', 'numerical'],
			['option_type', 'length', 'max'=> 32],
			['name', 'length', 'max'=> 255],
			['uuid', 'length', 'max'=> 36],
			['weight, price, category_id, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['option_id, option_type, weight, name, price, category_id, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'category' => [self::BELONGS_TO, Category::class, 'category_id'],
            'variants' => [self::HAS_MANY, Variant::class, 'default_option_id'],
			'disableUser' => [self::BELONGS_TO, User::class, ['disable_uid' => 'id']],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
            'translations' => [self::HAS_MANY, TranslatedProductOption::class, 'option_id'],
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

            // Currency format
            'CurrencyBehavior' => [
                'class' => '\dz\behaviors\CurrencyBehavior',
                'columns' => [
                    'price',
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'option_id' => Yii::t('app', 'Option'),
			'option_type' => Yii::t('app', 'Option Type'),
			'weight' => Yii::t('app', 'Weight'),
			'name' => Yii::t('app', 'Name'),
			'price' => Yii::t('app', 'Extra Price'),
            'category_id' => Yii::t('app', 'Category'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => null,
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
            'category' => Yii::t('app', 'Category'),
			'createdUser' => null,
			'disableUser' => null,
			'updatedUser' => null,
			'variants' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.option_id', $this->option_id);
        $criteria->compare('t.option_type', $this->option_type);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.category_id', $this->category_id);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => false,
            'sort' => ['defaultOrder' => ['weight' => false]]
        ]);
    }


    /**
     * ProductOption models list
     * 
     * @return array
     */
    public function option_list($option_type = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['option_id', 'name'];
        $criteria->compare('option_type', $option_type);
        $criteria->order = 't.weight ASC';
        
        $vec_models = ProductOption::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('option_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }



    /**
     * Event "beforeValidate"
     *
     * This method is invoked before validation starts
     *
     * @return bool
     */
    public function beforeValidate()
    {
        // Create new ProductOption model
        switch ( $this->scenario )
        {
            // Create new ProductOption
            case 'insert':
                // Get last weight
                $criteria = new DbCriteria();
                $criteria->compare('option_type', $this->option_type);
                $criteria->order = 'weight DESC';
                $criteria->limit = 1;
                $last_product_option_model = ProductOption::model()->find($criteria);
                if ( $last_product_option_model )
                {
                    $this->weight = $last_product_option_model->weight + 1;
                    if ( $this->weight > 255 )
                    {
                        $this->weight = 255;
                    }
                }
            break;
        }

        return parent::beforeValidate();
    }


    /**
     * Event "afterSave"
     *
     * This method is invoked after saving a record successfully
     */
    public function afterSave()
    {
        switch ( $this->scenario )
        {
            case 'insert':
            case 'update':
                if ( $this->is_multilanguage() && Yii::isMultilanguage() )
                {
                    // Copy translatable attributes from "commerce_product_option" table to "tcommerce_ranslated_product_option" table
                    $this->copy_translation();
                }
            break;
        }

        return parent::afterSave();
    }


    /**
     * Load and returns schema configuration of product types
     */
    public function get_config($option_type = '')
    {
        if ( empty($this->vec_config) )
        {
            $this->vec_config = Yii::app()->config->get('components.product_options');
        }

        if ( !empty($this->vec_config) )
        {
            // Return full configuration options
            if ( $option_type == 'all' )
            {
                return $this->vec_config;
            }

            // Current category type
            if ( empty($option_type) )
            {
                $option_type = $this->option_type;
            }

            // Return configuration given input params
            if ( !empty($option_type) && isset($this->vec_config[$option_type]) )
            {
                return $this->vec_config[$option_type];
            }
        }

        // Default values
        return [
            'name'                  => Yii::t('app', 'Option'),
            'is_editable'           => true,
            'is_sortable'           => true,
            'is_price'              => true,
            'is_multilanguage'      => true,
            'is_disable_allowed'    => true,
            'is_delete_allowed'     => true,

            // View files path
            'views' => [
                'index'         => '//commerce/option/_base/index',
                'create'        => '//commerce/option/_base/create',    
                'update'        => '//commerce/option/_base/update',
                '_grid_column'  => '//commerce/option/_base/_grid_column',
                '_form'         => '//commerce/option/_base/_form',
                '_tree'         => '//commerce/option/_base/_tree',
            ],

            // Texts
            'texts' => [
                'entity_label'      => 'Option',
                'entities_label'    => 'Options',

                'index_title'       => 'Manage options',
                'add_button'        => 'Add option',
                'create_title'      => 'Create option',
                'list_title'        => 'Options list',

                // Success messages
                'created_success'   => 'New option created successfully',
                'updated_success'   => 'Option updated successfully',

                // Disable
                'disable_success'   => 'Option DISABLED successfully',
                'disable_error'     => 'Option could not be DISABLED',
                'disable_confirm'   => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this option?</h3><p><u class=\'text-danger\'>WARNING:</u> Options will not be available for products.</p>',

                // Enable
                'enable_success'    => 'Option ENABLED successfully',
                'enable_error'      => 'Option could not be ENABLED',
                'enable_confirm'    => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this option?</h3><p>Options will be available for products.</p>',

                // Other
                'empty_text'        => 'No options found',
                'delete_confirm'    => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this option?</h3><p><strong>WARNING:</strong> All the product variants with this option will be permanently removed. Consider disabling it.</p>',
            ]
        ];
    }


    /**
     * Returns the view file path
     */
    public function get_view_path($view_name)
    {
        // Default view paths
        $vec_views = [
            'index'         => '//commerce/option/_base/index',
            'create'        => '//commerce/option/_base/create',    
            'update'        => '//commerce/option/_base/update',
            '_grid_column'  => '//commerce/option/_base/_grid_column',
            '_form'         => '//commerce/option/_base/_form',
            '_tree'         => '//commerce/option/_base/_tree',
        ];

        // Return view path from configuration file
        if ( !empty($this->option_type) )
        {
            $vec_config = $this->get_config($this->option_type);
            if ( !empty($vec_config) && isset($vec_config['views']) && isset($vec_config['views'][$view_name]) )
            {
                return $vec_config['views'][$view_name];
            }
        }

        // Return view path from default values
        if ( isset($vec_views[$view_name]) )
        {
            return $vec_views[$view_name];
        }

        // No view has been found
        return $view_name;
    }



    /**
     * Return the corresponding text
     */
    public function text($text_key)
    {
        $vec_config = $this->get_config();
        if ( !empty($vec_config) && isset($vec_config['texts']) && isset($vec_config['texts'][$text_key]) )
        {
            return Yii::t('app', $vec_config['texts'][$text_key]);
        }

        return '';
    }


    /**
     * Check if this ProductOption type has multilanguage option enabled
     */
    public function is_multilanguage()
    {
        $vec_config = $this->get_config($this->option_type);
        return ( !empty($vec_config) && isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] );
    }


    /**
     * Get TranslatedProduct model given a language
     */
    public function get_translation($language_id)
    {
        return TranslatedProductOption::get()
            ->where([
                'option_id'     => $this->option_id,
                'language_id'   => $language_id
            ])
            ->one();
    }


    /**
     * Save a TranslatedProductOption model
     */
    public function save_translation($translated_product_option_model)
    {
        if ( empty($translated_product_option_model->option_id) || $translated_product_option_model->option_id == $this->option_id )
        {
            $translated_product_option_model->option_id = $this->option_id;

            if ( ! $translated_product_option_model->save() )
            {
                Log::save_model_error($translated_product_option_model);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Copy current translatable fields to TranslatedProductOption model
     * 
     * Copy from ProductOption model to TranslatedProductOption model
     */
    public function copy_translation($language_id = '')
    {
        // Copy translation with default language
        if ( empty($language_id) )
        {
            $language_id = Yii::defaultLanguage();
        }

        if ( !empty($language_id) )
        {
            $translated_product_option_model = $this->get_translation($language_id);
            if ( ! $translated_product_option_model )
            {
                $translated_product_option_model = Yii::createObject(TranslatedProductOption::class);
                $translated_product_option_model->setAttributes([
                    'option_id'     => $this->option_id,
                    'language_id'   => $language_id,
                ]);
            }

            // Translatable attributes
            $translated_product_option_model->setAttributes([
                'name'          => $this->name
            ]);

            if ( ! $translated_product_option_model->save() )
            {
                Log::save_model_error($translated_product_model);
                return false;
            }

            return true;
        }

        return false;
    }


    /**
     * Get translated title
     */
    public function translated_title($language_id = '')
    {
        // Current language
        if ( empty($language_id) )
        {
            $language_id = Yii::currentLanguage();
        }

        $translated_model = $this->get_translation($language_id);
        if ( $translated_model )
        {
            return $translated_model->title();
        }

        return $this->title();
    }


    /**
     * Title used for this model
     */
    public function title()
    {
        return $this->name;
    }   
}