<?php
/**
 * @package dzlab\commerce\models
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\Transaction as BaseTransaction;
use dzlab\commerce\models\Gateway;
use dzlab\commerce\models\Order;
use dz\modules\settings\models\Currency;
use user\models\User;
use Yii;

/**
 * Transaction model class for "commerce_transaction" database table
 *
 * Columns in table "commerce_transaction" available as properties of the model,
 * followed by relations of table "commerce_transaction" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $transaction_id
 * @property integer $order_id
 * @property integer $order_transaction_num
 * @property integer $gateway_id
 * @property string $gateway_alias
 * @property string $status_type
 * @property string $transaction_type
 * @property double $amount
 * @property string $currency_id
 * @property string $gateway_reference
 * @property string $request_json
 * @property string $response_json
 * @property string $response_code
 * @property string $response_message
 * @property string $error_code
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $orders
 * @property mixed $createdUser
 * @property mixed $currency
 * @property mixed $gateway
 * @property mixed $order
 * @property mixed $updatedUser
 */
class Transaction extends BaseTransaction
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['order_id, gateway_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['order_id, order_transaction_num, gateway_id, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['amount', 'numerical'],
            ['gateway_alias, response_code, error_code', 'length', 'max'=> 32],
			['currency_id', 'length', 'max'=> 4],
			['gateway_reference', 'length', 'max'=> 255],
			['uuid', 'length', 'max'=> 36],
			['status_type', 'in', 'range' => ['pending', 'accepted', 'rejected', 'cancelled']],
			['transaction_type', 'in', 'range' => ['authorize', 'capture', 'purchase', 'refund']],
			['order_transaction_num, gateway_alias, status_type, transaction_type, amount, currency_id, gateway_reference, request_json, response_json, response_code, response_message, error_code, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['request_json, response_json, response_message', 'safe'],
			['transaction_id, order_id, order_transaction_num, gateway_id, gateway_alias, status_type, transaction_type, amount, currency_id, gateway_reference, request_json, response_json, response_code, response_message, error_code, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'order' => [self::BELONGS_TO, Order::class, 'order_id'],
            'gateway' => [self::BELONGS_TO, Gateway::class, 'gateway_id'],
            'currency' => [self::BELONGS_TO, Currency::class, 'currency_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],
            'orders' => [self::HAS_MANY, Order::class, 'transaction_id'],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Currency format
            'CurrencyBehavior' => [
                'class' => '\dz\behaviors\CurrencyBehavior',
                'columns' => [
                    // 'amount',        <-- DO NOT ADD "amount" TO AVOID CONVERSION PROBLEMS IN PAYMENT
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'transaction_id' => Yii::t('app', 'Transaction'),
			'order_id' => Yii::t('app', 'Order'),
            'order_transaction_num' => Yii::t('app', 'Order Transaction Number'),
			'gateway_id' => Yii::t('app', 'Gateway'),
            'gateway_alias' => Yii::t('app', 'Gateway Alias'),
			'status_type' => Yii::t('app', 'Status Type'),
			'transaction_type' => Yii::t('app', 'Transaction Type'),
			'amount' => Yii::t('app', 'Amount'),
			'currency_id' => Yii::t('app', 'Currency'),
			'gateway_reference' => Yii::t('app', 'Gateway Reference'),
			'request_json' => Yii::t('app', 'Request Json'),
			'response_json' => Yii::t('app', 'Response Json'),
			'response_code' => Yii::t('app', 'Response Code'),
			'response_message' => Yii::t('app', 'Response Message'),
			'error_code' => Yii::t('app', 'Error Code'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'orders' => null,
			'createdUser' => null,
			'currency' => null,
			'gateway' => null,
			'order' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            'pending' => Yii::t('app', 'Pending'),
            'accepted' => Yii::t('app', 'Accepted'),
            'rejected' => Yii::t('app', 'Rejected'),
            'cancelled' => Yii::t('app', 'Cancelled'),
        ];
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type)
    {
        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * Get "transaction_type" labels
     */
    public function transaction_type_labels()
    {
        return [
            'authorize' => Yii::t('app', 'Authorize'),
            'capture' => Yii::t('app', 'Capture'),
            'purchase' => Yii::t('app', 'Purchase'),
            'refund' => Yii::t('app', 'Refund'),
        ];
    }


    /**
     * Get "transaction_type" specific label
     */
    public function transaction_type_label($transaction_type)
    {
        $vec_labels = $this->transaction_type_labels();
        return isset($vec_labels[$transaction_type]) ? $vec_labels[$transaction_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.transaction_id', $this->transaction_id);
        $criteria->compare('t.order_id', $this->order_id);
        $criteria->compare('t.order_transaction_num', $this->order_transaction_num);
        $criteria->compare('t.gateway_id', $this->gateway_id);
        $criteria->compare('t.gateway_alias', $this->gateway_alias, true);
        $criteria->compare('t.status_type', $this->status_type, true);
        $criteria->compare('t.transaction_type', $this->transaction_type, true);
        $criteria->compare('t.amount', $this->amount);
        $criteria->compare('t.gateway_reference', $this->gateway_reference, true);
        $criteria->compare('t.request_json', $this->request_json, true);
        $criteria->compare('t.response_json', $this->response_json, true);
        $criteria->compare('t.response_code', $this->response_code, true);
        $criteria->compare('t.response_message', $this->response_message, true);
        $criteria->compare('t.error_code', $this->error_code, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['transaction_id' => true]]
        ]);
    }


    /**
     * Transaction models list
     * 
     * @return array
     */
    public function transaction_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['transaction_id', 'status_type'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Transaction::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('transaction_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Event "afterSave"
     *
     * This method is invoked after validation end
     */
    public function afterSave()
    {
        switch ( $this->scenario )
        {
            // Transaction has been accepted. Update the ORDER
            case 'accepted':
                if ( $this->order )
                {
                    $this->order->mark_as_complete($this);
                }
            break;
        }

        return parent::afterSave();
    }


    /**
     * Save the model as accepted
     */
    public function accepted()
    {
        $this->status_type = 'accepted';
        $this->scenario = 'accepted';

        if ( ! $this->save() )
        {
            Log::save_model_error($this);
            return false;
        }

        return true;
    }


    /**
     * Save the model as rejected
     */
    public function rejected()
    {
        $this->status_type = 'rejected';
        $this->scenario = 'rejected';

        if ( ! $this->save() )
        {
            Log::save_model_error($this);
            return false;
        }

        return true;
    }


    /**
     * Save the model as cancelled
     */
    public function cancelled()
    {
        $this->status_type = 'cancelled';
        $this->scenario = 'cancelled';

        if ( ! $this->save() )
        {
            Log::save_model_error($this);
            return false;
        }

        return true;
    }


    /**
     * Return JSON request in an array format
     */
    public function get_request_data()
    {
        $vec_data = Json::decode($this->request_json);
        if ( !empty($this->gateway_alias) )
        {
            $gateway_model = Yii::app()->checkoutManager->get_gateway($this->gateway_alias);
            if ( $gateway_model )
            {
                return $gateway_model->view_request_data($vec_data, $this);
            }
        }

        return $vec_data;
    }


    /**
     * Return JSON response in an array format
     */
    public function get_response_data()
    {
        $vec_data = Json::decode($this->response_json);
        if ( !empty($this->gateway_alias) )
        {
            $gateway_model = Yii::app()->checkoutManager->get_gateway($this->gateway_alias);
            if ( $gateway_model )
            {
                return $gateway_model->view_response_data($vec_data, $this);
            }
        }

        return $vec_data;
    }


    /**
     * Returns gateway reference
     */
    public function get_gateway_reference()
    {
        if ( !empty($this->gateway_reference) )
        {
            return $this->gateway_reference;
        }

        // Load Gateway model to make the transaction
        $gateway_model = Gateway::findOne($this->gateway_id);    // <-- This method load Gateway subclass (PaypalGateway, RedsysGateway, ...)
        if ( $gateway_model )
        {
            return $gateway_model->get_order_reference($this->order_id, $this->order_transaction_num);
        }

        return $this->order_id .'-'. $this->order_transaction_num;
    }


    /**
     * Find an Order model by reference
     */
    public function findByReference($gateway_reference)
    {
        return self::get()
            ->where([
                'gateway_reference' => $gateway_reference
            ])
            ->one();
    }
}
