<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\ShippingMethod as BaseShippingMethod;
use dz\modules\settings\models\Currency;
use user\models\User;
use Yii;

/**
 * ShippingMethod model class for "commerce_shipping_method" database table
 *
 * Columns in table "commerce_shipping_method" available as properties of the model,
 * followed by relations of table "commerce_shipping_method" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $shipping_method_id
 * @property string $alias
 * @property string $name
 * @property string $description
 * @property string $settings_json
 * @property double $amount
 * @property string $currency_id
 * @property integer $is_disabled
 * @property integer $disable_date
 * @property integer $disable_uid
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $orders
 * @property mixed $createdUser
 * @property mixed $currency
 * @property mixed $updatedUser
 */
class ShippingMethod extends BaseShippingMethod
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['alias, name, created_date, created_uid, updated_date, updated_uid', 'required'],
			['is_disabled, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['amount', 'numerical'],
			['alias', 'length', 'max'=> 32],
			['name', 'length', 'max'=> 128],
			['description', 'length', 'max'=> 255],
			['currency_id', 'length', 'max'=> 4],
			['uuid', 'length', 'max'=> 36],
			['description, settings_json, amount, currency_id, is_disabled, disable_date, disable_uid, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['settings_json', 'safe'],
			['shipping_method_id, alias, name, description, settings_json, amount, currency_id, is_disabled, disable_date, disable_uid, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'currency' => [self::BELONGS_TO, Currency::class, 'currency_id'],
			'orders' => [self::HAS_MANY, Order::class, 'shipping_method_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Currency format
            'CurrencyBehavior' => [
                'class' => '\dz\behaviors\CurrencyBehavior',
                'columns' => [
                    'amount',
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'shipping_method_id' => Yii::t('app', 'Shipping Method'),
			'alias' => Yii::t('app', 'Alias'),
			'name' => Yii::t('app', 'Name'),
			'description' => Yii::t('app', 'Description'),
			'settings_json' => Yii::t('app', 'Settings Json'),
			'amount' => Yii::t('app', 'Amount'),
			'currency_id' => null,
			'is_disabled' => Yii::t('app', 'Is Disabled'),
			'disable_date' => Yii::t('app', 'Disable Date'),
			'disable_uid' => Yii::t('app', 'Disable Uid'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'orders' => null,
			'createdUser' => null,
			'currency' => null,
			'updatedUser' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.shipping_method_id', $this->shipping_method_id);
        $criteria->compare('t.alias', $this->alias, true);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.settings_json', $this->settings_json, true);
        $criteria->compare('t.amount', $this->amount);
        $criteria->compare('t.is_disabled', $this->is_disabled);
        $criteria->compare('t.disable_date', $this->disable_date);
        $criteria->compare('t.disable_uid', $this->disable_uid);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['shipping_method_id' => true]]
        ]);
    }


    /**
     * ShippingMethod models list
     * 
     * @return array
     */
    public function shippingmethod_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['shipping_method_id', 'name'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = ShippingMethod::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('shipping_method_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}