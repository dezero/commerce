<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\OrderHistory as BaseOrderHistory;
use dzlab\commerce\models\Order;
use user\models\User;
use Yii;

/**
 * OrderHistory model class for "commerce_order_history" database table
 *
 * Columns in table "commerce_order_history" available as properties of the model,
 * followed by relations of table "commerce_order_history" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $order_history_id
 * @property integer $order_id
 * @property string $status_type
 * @property string $comments
 * @property integer $created_date
 * @property integer $created_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $order
 */
class OrderHistory extends BaseOrderHistory
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['order_id, created_date, created_uid', 'required'],
			['order_id, created_date, created_uid', 'numerical', 'integerOnly' => true],
            ['status_type', 'length', 'max'=> 32],
			['comments', 'default', 'setOnEmpty' => true, 'value' => null],
			['comments', 'safe'],
			['order_history_id, order_id, status_type, comments, created_date, created_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'order' => [self::BELONGS_TO, Order::class, 'order_id'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'order_history_id' => Yii::t('app', 'Order History'),
			'order_id' => Yii::t('app', 'Order'),
			'status_type' => Yii::t('app', 'Order Status'),
			'comments' => Yii::t('app', 'Comments'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'createdUser' => null,
			'order' => null,
		];
	}


    /**
     * Get "status_type" labels
     */
    public function status_type_labels()
    {
        return [
            'cart' => Yii::t('app', 'Cart'),
            'payment_received' => Yii::t('app', 'Payment Received'),
            'payment_failed' => Yii::t('app', 'Payment Failed'),
            'in_progress' => Yii::t('app', 'In progress'),
            'shipped' => Yii::t('app', 'Shipped'),
            'completed' => Yii::t('app', 'Completed'),
            'canceled' => Yii::t('app', 'Canceled'),
            'on_hold' => Yii::t('app', 'On Hold'),
        ];
    }


    /**
     * Get "status_type" specific label
     */
    public function status_type_label($status_type)
    {
        $vec_labels = $this->status_type_labels();
        return isset($vec_labels[$status_type]) ? $vec_labels[$status_type] : '';
    }


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.order_history_id', $this->order_history_id);
        $criteria->compare('t.status_type', $this->status_type, true);
        $criteria->compare('t.comments', $this->comments, true);
        $criteria->compare('t.created_date', $this->created_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['order_history_id' => true]]
        ]);
    }


    /**
     * OrderHistory models list
     * 
     * @return array
     */
    public function orderhistory_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['order_history_id', 'status_type'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = OrderHistory::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('order_history_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}