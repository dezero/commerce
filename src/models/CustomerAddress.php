<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\CustomerAddress as BaseCustomerAddress;
use dzlab\commerce\models\Customer;
use dzlab\commerce\models\Order;
use dz\modules\settings\models\Country;
use user\models\User;
use Yii;

/**
 * CustomerAddress model class for "commerce_customer_address" database table
 *
 * Columns in table "commerce_customer_address" available as properties of the model,
 * followed by relations of table "commerce_customer_address" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $address_id
 * @property integer $user_id
 * @property integer $order_id
 * @property string $firstname
 * @property string $lastname
 * @property string $vat_code
 * @property string $address_line
 * @property string $city
 * @property string $province
 * @property string $postal_code
 * @property string $country_code
 * @property string $phone
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $customers
 * @property mixed $customers1
 * @property mixed $countryCode
 * @property mixed $createdUser
 * @property mixed $order
 * @property mixed $updatedUser
 * @property mixed $user
 * @property mixed $orders
 * @property mixed $orders1
 */
class CustomerAddress extends BaseCustomerAddress
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['user_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['user_id, order_id, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['firstname, lastname', 'length', 'max'=> 100],
			['vat_code, city', 'length', 'max'=> 128],
			['address_line', 'length', 'max'=> 255],
			['province, phone', 'length', 'max'=> 64],
			['postal_code', 'length', 'max'=> 32],
			['country_code', 'length', 'max'=> 4],
			['uuid', 'length', 'max'=> 36],
			['order_id, firstname, lastname, vat_code, address_line, city, province, postal_code, country_code, phone, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['address_id, user_id, order_id, firstname, lastname, vat_code, address_line, city, province, postal_code, country_code, phone, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],

            // Custom validation for CHECKOUT process
            ['firstname, lastname, vat_code, address_line, city, postal_code, country_code, phone', 'required', 'on' => 'checkout'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
			'customer' => [self::HAS_MANY, Customer::class, 'user_id'],
			'user' => [self::BELONGS_TO, User::class, 'user_id'],
            'order' => [self::BELONGS_TO, Order::class, 'order_id'],
			'country' => [self::BELONGS_TO, Country::class, 'country_code'],
			'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
			'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'address_id' => Yii::t('app', 'Address'),
			'user_id' => null,
			'order_id' => null,
			'firstname' => Yii::t('app', 'First Name'),
			'lastname' => Yii::t('app', 'Last Name'),
			'vat_code' => Yii::t('app', 'Tax / VAT Number'),
			'address_line' => Yii::t('app', 'Address Line'),
			'city' => Yii::t('app', 'City'),
			'province' => Yii::t('app', 'Province'),
			'postal_code' => Yii::t('app', 'Postal Code'),
			'country_code' => Yii::t('app', 'Country'),
			'phone' => Yii::t('app', 'Phone'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'customers' => null,
			'customers1' => null,
			'countryCode' => null,
			'createdUser' => null,
			'order' => null,
			'updatedUser' => null,
			'user' => null,
			'orders' => null,
			'orders1' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.address_id', $this->address_id);
        $criteria->compare('t.firstname', $this->firstname, true);
        $criteria->compare('t.lastname', $this->lastname, true);
        $criteria->compare('t.vat_code', $this->vat_code, true);
        $criteria->compare('t.address_line', $this->address_line, true);
        $criteria->compare('t.city', $this->city, true);
        $criteria->compare('t.province', $this->province, true);
        $criteria->compare('t.postal_code', $this->postal_code, true);
        $criteria->compare('t.phone', $this->phone, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['address_id' => true]]
        ]);
    }


    /**
     * CustomerAddress models list
     * 
     * @return array
     */
    public function customeraddress_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['address_id', 'uuid'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = CustomerAddress::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('address_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}