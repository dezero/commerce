<?php
/**
 * @package dzlab\commerce\models
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\OrderJson as BaseOrderJson;
use dzlab\commerce\Customer;
use dzlab\commerce\Order;
use user\models\User;
use Yii;

/**
 * OrderJson model class for "commerce_order_json" database table
 *
 * Columns in table "commerce_order_json" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $order_id
 * @property integer $user_id
 * @property string $email
 * @property string $json_data
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class OrderJson extends BaseOrderJson
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['order_id, user_id, created_date, created_uid, updated_date, updated_uid', 'required'],
			['order_id, user_id, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['email', 'length', 'max'=> 255],
			['uuid', 'length', 'max'=> 36],
			['email, json_data, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['json_data', 'safe'],
			['order_id, user_id, email, json_data, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [

            // Custom relations
            'order' => [self::BELONGS_TO, Order::class, 'order_id'],
            'user' => [self::BELONGS_TO, User::class, ['user_id' => 'id']],
            'customer' => [self::BELONGS_TO, Customer::class, ['user_id' => 'user_id']],
		];
	}


	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'order_id' => Yii::t('app', 'Order'),
			'user_id' => Yii::t('app', 'User'),
			'email' => Yii::t('app', 'Email'),
			'json_data' => Yii::t('app', 'Json Data'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => Yii::t('app', 'Created Uid'),
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => Yii::t('app', 'Updated Uid'),
			'uuid' => Yii::t('app', 'Uuid'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.order_id', $this->order_id);

        // Customer
        if ( $this->user_id )
        {
            if ( preg_match("/\@/", $this->user_id) )
            {
                $criteria->compare('t.email', $this->user_id);
            }
            else
            {
                $criteria->with['user'] = 'user';
                $criteria->addCondition('user.firstname LIKE CONCAT(:name_filter, "%") OR user.lastname LIKE CONCAT(:name_filter, "%") OR CONCAT(user.firstname, " ", user.lastname) LIKE CONCAT(:name_filter, "%")');
                $criteria->addParams([':name_filter' => $this->user_id]);
            }
        }

        /*
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.json_data', $this->json_data, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.created_uid', $this->created_uid);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.updated_uid', $this->updated_uid);
        $criteria->compare('t.uuid', $this->uuid, true);
        */

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 50],
            'sort' => ['defaultOrder' => ['order_id' => true]]
        ]);
    }


    /**
     * OrderJson models list
     * 
     * @return array
     */
    public function orderjson_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['order_id', 'uuid'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = OrderJson::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('order_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Generate JSON content from an existing Order model
     */
    public function get_data_from_order($order_model)
    {
        $vec_data = [
            'order'             => $order_model->raw_attributes,
            'customer'          => $order_model->customer ? $order_model->customer->raw_attributes : null,
            'user'              => $order_model->user ? $order_model->user->raw_attributes : null,
            'shipping_address'  => $order_model->shippingAddress ? $order_model->shippingAddress->raw_attributes : null,
            'billing_address'   => $order_model->billingAddress ? $order_model->billingAddress->raw_attributes : null,
            'line_items'        => []
        ];


        // Line items
        if ( $order_model->lineItems )
        {
            foreach ( $order_model->lineItems as $line_item_model )
            {
                $vec_data['line_items'][] = $line_item_model->raw_attributes;
            }
        }

        return $vec_data;
    }
}
