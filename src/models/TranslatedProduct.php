<?php
/**
 * @package dzlab\commerce\models 
 */

namespace dzlab\commerce\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dzlab\commerce\models\_base\TranslatedProduct as BaseTranslatedProduct;
use dzlab\commerce\models\Product;
use dz\modules\settings\models\Language;
use user\models\User;
use Yii;

/**
 * TranslatedProduct model class for "commerce_translated_product" database table
 *
 * Columns in table "commerce_translated_product" available as properties of the model,
 * and there are no model relations.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $product_id
 * @property string $language_id
 * @property string $name
 * @property string $description
 * @property string $seo_url
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 */
class TranslatedProduct extends BaseTranslatedProduct
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['product_id, name, updated_date, updated_uid', 'required'],
			['product_id, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['language_id', 'length', 'max'=> 4],
			['name, seo_url', 'length', 'max'=> 255],
			['uuid', 'length', 'max'=> 36],
			['language_id, description, seo_url, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['description', 'safe'],
			['product_id, language_id, name, description, seo_url, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'product' => [self::BELONGS_TO, Product::class, 'product_id'],
            'language' => [self::BELONGS_TO, Language::class, 'language_id'],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // Custom relations
		];
	}

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'product_id' => null,
			'language_id' => null,
			'name' => Yii::t('app', 'Name'),
			'description' => Yii::t('app', 'Description'),
			'seo_url' => Yii::t('app', 'Seo Url'),
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.seo_url', $this->seo_url, true);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['product_id' => true]]
        ]);
    }


    /**
     * TranslatedProduct models list
     * 
     * @return array
     */
    public function translatedproduct_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['product_id', 'product_id', 'language_id'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = TranslatedProduct::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('product_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}