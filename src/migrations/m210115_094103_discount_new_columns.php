<?php
/**
 * Migration class m210115_094103_discount_new_columns
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210115_094103_discount_new_columns extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new column "discount_type" to "commerce_discount" table
        $this->addColumn('commerce_discount', 'discount_type', $this->enum('type', ['code', 'auto'])->notNull()->defaultValue('code')->after('code'));

        // Create index for this column
        $this->createIndex(null, 'commerce_discount', ['discount_type'], false);


        // Add discount columns to "commerce_line_item" table
        $this->addColumn('commerce_line_item', 'discount_id', $this->integer()->unsigned()->after('price_alias'));
        $this->addColumn('commerce_line_item', 'product_base_price', $this->float()->unsigned()->notNull()->defaultValue(0)->after('discount_id'));
        
        // Create FOREIGN KEY on "commerce_order" table
        $this->addForeignKey(null, 'commerce_line_item', ['discount_id'], 'commerce_discount', ['discount_id'], 'SET NULL', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

