<?php
/**
 * Migration class m200303_145959_create_product_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200303_145959_create_product_tables extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        // Create "commerce_product" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_product', true);

        $this->createTable('commerce_product', [
            // Product data
            'product_id' => $this->primaryKey(),
            'product_type' => $this->char(32)->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'main_category_id' => $this->integer()->unsigned(),
            'seo_url' => $this->string(255),

            // Default data
            'default_ean_code' => $this->string(16),
            'default_sku' => $this->string(128),
            'default_image_id' => $this->integer()->unsigned(),
            'default_variant_id' => $this->integer()->unsigned(),
            'default_base_price' => $this->float()->unsigned(),

            // Status information
            'is_on_offer' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'is_disabled' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),

            // Dates
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),

            // UUID
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'commerce_product', ['product_type'], false);
        $this->createIndex(null, 'commerce_product', ['default_sku'], false);
        $this->createIndex(null, 'commerce_product', ['default_ean_code'], false);
        $this->createIndex(null, 'commerce_product', ['seo_url'], false);
        $this->createIndex(null, 'commerce_product', ['is_disabled'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_product', ['main_category_id'], 'category', ['category_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_product', ['default_image_id'], 'asset_file', ['file_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_product', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "commerce_product_option" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_product_option', true);

        $this->createTable('commerce_product_option', [
            'option_id' => $this->primaryKey(),
            'option_type' => $this->char(32)->notNull(),
            'weight' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1),
            'name' => $this->string(255)->notNull(),
            'price' => $this->float()->notNull()->defaultValue(0),
            'category_id' => $this->integer()->unsigned(),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'commerce_product_option', ['option_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_product_option', ['category_id'], 'category', ['category_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_product_option', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_option', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_option', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "commerce_variant" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_variant', true);

        $this->createTable('commerce_variant', [
            'variant_id' => $this->primaryKey(),
            'product_id' => $this->integer()->unsigned()->notNull(),

            // Use this column optionally, if variant has only 1 option
            'default_option_id' => $this->integer()->unsigned(),

            'sku' => $this->string(128),
            'ean_code' => $this->string(16),
            'is_default' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'base_price' => $this->float()->unsigned(),
            'total_stock' => $this->integer()->defaultValue(0),
            'is_unlimited_stock' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'image_id' => $this->integer()->unsigned(),

            // Status
            'available_from_date' => $this->date(),
            'available_to_date' => $this->date(),
            'is_available' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'is_disabled' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),

            // Dates
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'commerce_variant', ['sku'], false);
        $this->createIndex(null, 'commerce_variant', ['ean_code'], false);
        $this->createIndex(null, 'commerce_variant', ['is_default'], false);
        $this->createIndex(null, 'commerce_variant', ['is_available'], false);
        $this->createIndex(null, 'commerce_variant', ['is_disabled'], false);

        // Create FOREIGN KEYS on "commerce_variant" table
        $this->addForeignKey(null, 'commerce_variant', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_variant', ['default_option_id'], 'commerce_product_option', ['option_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_variant', ['image_id'], 'asset_file', ['file_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_variant', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_variant', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_variant', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Create FOREIGN KEY on "product" table for "default_variant_id" column
        $this->addForeignKey(null, 'commerce_product', ['default_variant_id'], 'commerce_variant', ['variant_id'], 'SET NULL', null);


        // Create "commerce_variant_option" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_variant_option', true);

        $this->createTable('commerce_variant_option', [
            // Primary key
            'variant_id' => $this->integer()->unsigned()->notNull(),
            'option_id' => $this->integer()->unsigned()->notNull(),

            // Foreign keys
            'product_id' => $this->integer()->unsigned()->notNull(),

            // Creation data
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
        ]);

        // Primary key (multiple columns)
        $this->addPrimaryKey(null, 'commerce_variant_option', ['variant_id', 'option_id']);

        // Create indexes
        $this->createIndex(null, 'commerce_variant_option', ['product_id', 'option_id'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_variant_option', ['variant_id'], 'commerce_variant', ['variant_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_variant_option', ['option_id'], 'commerce_product_option', ['option_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_variant_option', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_variant_option', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);



        // Create "commerce_product_category" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_product_category', true);

        $this->createTable('commerce_product_category', [
            'product_id' => $this->integer()->unsigned()->notNull(),
            'category_id' => $this->integer()->unsigned()->notNull(),
            'category_type' => $this->string(32)->notNull(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
        ]);

        // Primary key (multiple columns)
        $this->addPrimaryKey(null, 'commerce_product_category', ['product_id', 'category_id']);

        // Create indexes
        $this->createIndex(null, 'commerce_product_category', ['category_id', 'category_type'], false);
        $this->createIndex(null, 'commerce_product_category', ['product_id', 'category_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_product_category', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_category', ['category_id'], 'category', ['category_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_category', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "commerce_product_image" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_product_image', true);

        $this->createTable('commerce_product_image', [
            'product_id' => $this->integer()->unsigned()->notNull(),
            'image_id' => $this->integer()->unsigned()->notNull(),
            'variant_id' => $this->integer()->unsigned(),
            'is_default' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'weight' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
        ]);

        // Primary key (multiple columns)
        $this->addPrimaryKey(null, 'commerce_product_image', ['product_id', 'image_id']);

        // Create indexes
        $this->createIndex(null, 'commerce_product_image', ['is_default'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_product_image', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_image', ['image_id'], 'asset_file', ['file_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_image', ['variant_id'], 'commerce_variant', ['variant_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_product_image', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "commerce_translated_product" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_translated_product', true);

         $this->createTable('commerce_translated_product', [
            'product_id' => $this->integer()->unsigned()->notNull(),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'seo_url' => $this->string(255),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Primary key
        $this->addPrimaryKey(null, 'commerce_translated_product', ['product_id', 'language_id']);

        // Create indexes
        $this->createIndex(null, 'commerce_translated_product', ['seo_url'], false);

        // Foreign keys
        $this->addForeignKey(null, 'commerce_translated_product', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_translated_product', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_translated_product', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "commerce_translated_product_option" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_translated_product_option', true);

         $this->createTable('commerce_translated_product_option', [
            'option_id' => $this->integer()->unsigned()->notNull(),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'name' => $this->string(255)->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Primary key
        $this->addPrimaryKey(null, 'commerce_translated_product_option', ['option_id', 'language_id']);

        // Foreign keys
        $this->addForeignKey(null, 'commerce_translated_product_option', ['option_id'], 'commerce_product_option', ['option_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_translated_product_option', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_translated_product_option', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "search_product" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('search_product', true);

         $this->createTable('search_product', [
            'product_id' => $this->integer()->unsigned()->notNull(),
            'language_id' => $this->string(4)->notNull()->defaultValue('es'),
            'is_disabled' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'name' => $this->string(255)->notNull(),
            'content' => $this->text(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull()
        ]);

        // Primary key
        $this->addPrimaryKey(null, 'search_product', ['product_id', 'language_id']);

        // Create indexes
        $this->createIndex(null, 'search_product', ['is_disabled'], false);

        // Foreign keys
        $this->addForeignKey(null, 'search_product', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'search_product', ['language_id'], 'language', ['language_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'search_product', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Permissions for PRODUCT
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'commerce.product.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Products - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.product.create',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Products - Create new products',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.product.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Products - Edit products',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.product.view',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Products - View products',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'product_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Products - Full access to products',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'product_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Products - Edit products',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'product_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Products - View products',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'product_manage',
                'child'     => 'commerce.product.*'
            ],
            [
                'parent'    => 'product_edit',
                'child'     => 'commerce.product.update'
            ],
            [
                'parent'    => 'product_edit',
                'child'     => 'commerce.product.view'
            ],
            [
                'parent'    => 'product_view',
                'child'     => 'commerce.product.view'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'product_manage'
            ],
            [
                'parent'    => 'editor',
                'child'     => 'product_edit'
            ],
        ]);


        // Permissions for PRODUCT OPTIONS
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'commerce.option.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Product Options - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.option.create',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Product Options - Create new product options',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.option.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Product Options - Edit product options',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.option.view',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Product Options - View product options',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'product_option_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Product Options - Full access to product options',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'product_option_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Product Options - Edit product options',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'product_option_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Product Options - View product options',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'product_option_manage',
                'child'     => 'commerce.option.*'
            ],
            [
                'parent'    => 'product_option_edit',
                'child'     => 'commerce.option.update'
            ],
            [
                'parent'    => 'product_option_edit',
                'child'     => 'commerce.option.view'
            ],
            [
                'parent'    => 'product_option_view',
                'child'     => 'commerce.option.view'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'product_option_manage'
            ],
            [
                'parent'    => 'editor',
                'child'     => 'product_option_edit'
            ],
        ]);

        return true;
    }


    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        // $this->dropTable('my_table');
        return false;
    }
}

