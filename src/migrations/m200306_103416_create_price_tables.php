<?php
/**
 * Migration class m200306_101134_create_price_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200306_103416_create_price_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "commerce_price" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_price', true);

        $this->createTable('commerce_price', [
            'price_id' => $this->primaryKey(),
            'alias' => $this->char(32)->notNull(),
            'name' => $this->string(64)->notNull(),
            'description' => $this->string(255),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create index for "alias" column
        $this->createIndex(null, 'commerce_price', ['alias'], false);
    
        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_price', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_price', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Insert default values
        $this->insertMultiple('commerce_price', [
            [
                'alias'         => 'base_price',
                'name'          => 'Base Price',
                'description'   => 'Product base price',
                'created_date'  => time(),
                'created_uid'   => 1,
                'updated_date'  => time(),
                'updated_uid'   => 1,
                'uuid'          => StringHelper::UUID()
            ],
            [
                'alias'         => 'reduced_price',
                'name'          => 'Reduced Price (offer)',
                'description'   => 'Offer / reduced price',
                'created_date'  => time(),
                'created_uid'   => 1,
                'updated_date'  => time(),
                'updated_uid'   => 1,
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        
        // Create "commerce_tax" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_tax', true);

        $this->createTable('commerce_tax', [
            'tax_id' => $this->primaryKey(),
            'alias' => $this->char(32)->notNull(),
            'name' => $this->string(64)->notNull(),
            'description' => $this->string(255),
            'percentage' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Create index for "alias" column
        $this->createIndex(null, 'commerce_tax', ['alias'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_tax', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_tax', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Insert default values for SPAIN
        $this->insertMultiple('commerce_tax', [
            [
                'alias'         => 'iva_21',
                'name'          => 'IVA 21%',
                'description'   => 'IVA general (21%)',
                'percentage'    => 21,
                'created_date'  => time(),
                'created_uid'   => 1,
                'updated_date'  => time(),
                'updated_uid'   => 1,
                'uuid'          => StringHelper::UUID()
            ],
            [
                'alias'         => 'iva_10',
                'name'          => 'IVA 10%',
                'description'   => 'IVA reducido (10%)',
                'percentage'    => 10,
                'created_date'  => time(),
                'created_uid'   => 1,
                'updated_date'  => time(),
                'updated_uid'   => 1,
                'uuid'          => StringHelper::UUID()
            ],
            [
                'alias'         => 'iva_4',
                'name'          => 'IVA 4%',
                'description'   => 'IVA superreducido (4%)',
                'percentage'    => 4,
                'created_date'  => time(),
                'created_uid'   => 1,
                'updated_date'  => time(),
                'updated_uid'   => 1,
                'uuid'          => StringHelper::UUID()
            ],
        ]);


        // Create "commerce_product_price" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_product_price', true);

        $this->createTable('commerce_product_price', [
            'price_id' => $this->integer()->unsigned()->notNull(),
            'variant_id' => $this->integer()->unsigned()->notNull(),
            'product_id' => $this->integer()->unsigned()->notNull(),
            'warehouse_id' => $this->integer()->unsigned()->notNull()->defaultValue(1),
            'amount' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'currency_id' => $this->string(4)->notNull()->defaultValue('EUR'),
            'tax_id' => $this->integer()->unsigned()->notNull()->defaultValue(1),
            'is_taxes_included' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
        ]);

        // Primary key (multiple columns)
        $this->addPrimaryKey(null, 'commerce_product_price', ['price_id', 'variant_id']);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_product_price', ['price_id'], 'commerce_price', ['price_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_price', ['variant_id'], 'commerce_variant', ['variant_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_price', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_price', ['warehouse_id'], 'commerce_warehouse', ['warehouse_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_price', ['currency_id'], 'currency', ['currency_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_price', ['tax_id'], 'commerce_tax', ['tax_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_price', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_price', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

