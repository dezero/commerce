<?php
/**
 * Migration class m201207_110554_search_product_columns
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m201207_110554_search_product_columns extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        $this->addColumn('search_product', 'main_category_id', $this->integer()->unsigned()->after('content'));
		$this->addColumn('search_product', 'weight', $this->integer()->unsigned()->notNull()->defaultValue(0)->after('main_category_id'));

        $this->addForeignKey(null, 'search_product', ['main_category_id'], 'category', ['category_id'], 'SET NULL', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

