<?php
/**
 * Migration class m221024_132646_brand_category
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m221024_132646_brand_category extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new column "brand_category_id" to Product table and ProductSearch table
        $this->addColumn('commerce_product', 'brand_category_id', $this->integer()->unsigned()->after('main_category_id'));
        $this->addColumn('search_product', 'brand_category_id', $this->integer()->unsigned()->after('main_category_id'));

        // Create FOREIGN KEY
        $this->addForeignKey(null, 'commerce_product', ['brand_category_id'], 'category', ['category_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'search_product', ['brand_category_id'], 'category', ['category_id'], 'SET NULL', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

