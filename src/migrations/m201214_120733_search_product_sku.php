<?php
/**
 * Migration class m201214_120733_search_product_sku
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m201214_120733_search_product_sku extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        $this->addColumn('search_product', 'product_type', $this->char(32)->after('language_id'));
        $this->addColumn('search_product', 'default_sku', $this->string(128)->after('product_type'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

