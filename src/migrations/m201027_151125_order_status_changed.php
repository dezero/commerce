<?php
/**
 * Migration class m201027_151125_order_status_changed
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m201027_151125_order_status_changed extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Alter column via $this->alterColumn
		$this->alterColumn('commerce_order', 'status_type', $this->string(32)->notNull()->defaultValue('cart'));
        $this->alterColumn('commerce_order_history', 'status_type', $this->string(32)->notNull()->defaultValue('cart'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

