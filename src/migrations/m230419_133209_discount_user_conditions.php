<?php
/**
 * Migration class m230419_133209_discount_user_conditions
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m230419_133209_discount_user_conditions extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        $this->addColumn('commerce_discount', 'is_user_restriction', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('product_restriction_type'));
        $this->addColumn('commerce_discount', 'user_restriction_type', $this->enum('user_restriction_type', ['users', 'roles'])->notNull()->defaultValue('users')->after('is_user_restriction'));
        $this->addColumn('commerce_discount', 'user_restriction_values', $this->string()->after('user_restriction_type'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

