<?php
/**
 * Migration class m200708_163257_product_search_weight
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200708_163257_product_search_weight extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new columns "weight" to product and variants
        $this->addColumn('commerce_product', 'weight', $this->integer()->unsigned()->notNull()->defaultValue(0)->after('default_base_price'));
        $this->addColumn('commerce_variant', 'weight', $this->integer()->unsigned()->notNull()->defaultValue(0)->after('image_id'));        

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

