<?php
/**
 * Migration class m210329_100403_order_receive_invoice
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210329_100403_order_receive_invoice extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Add new column "is_receive_invoice" into customer and order tables
        $this->addColumn('commerce_customer', 'is_receive_invoice', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('is_newsletter'));
        $this->addColumn('commerce_order', 'is_receive_invoice', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('paid_date'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

