<?php
/**
 * Migration class m211110_145037_discount_product_conditions
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m211110_145037_discount_product_conditions extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Create "commerce_discount_product" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_discount_product', true);

        $this->createTable('commerce_discount_product', [
            'discount_id' => $this->integer()->unsigned()->notNull(),
            'product_id' => $this->integer()->unsigned()->notNull(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull()
        ]);

        // Primary key (alternative method)
        $this->addPrimaryKey(null, 'commerce_discount_product', ['discount_id', 'product_id']);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_discount_product', ['discount_id'], 'commerce_discount', ['discount_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_discount_product', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_discount_product', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Add columns "category_restriction_type" and "product_restriction_type" to "commerce_discount" table
        $this->addColumn('commerce_discount', 'is_category_restriction', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('minimum_amount'));
        $this->addColumn('commerce_discount', 'category_restriction_type', $this->enum('category_restriction_type', ['include', 'exclude'])->notNull()->defaultValue('include')->after('is_category_restriction'));
        $this->addColumn('commerce_discount', 'is_product_restriction', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('category_restriction_type'));
        $this->addColumn('commerce_discount', 'product_restriction_type', $this->enum('product_restriction_type', ['include', 'exclude'])->notNull()->defaultValue('include')->after('is_product_restriction'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

