<?php
/**
 * Migration class m201027_085838_line_item_option_quantity
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m201027_085838_line_item_option_quantity extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new columns "unit_price" and "quantity" to "commerce_line_item_option" table
        $this->addColumn('commerce_line_item_option', 'unit_price', $this->float()->notNull()->defaultValue(0)->after('option_type'));
        $this->addColumn('commerce_line_item_option', 'quantity', $this->integer()->notNull()->defaultValue(1)->after('unit_price'));

        // Rename column "price" to "total_price"
        $this->renameColumn('commerce_line_item_option', 'price', 'total_price');


		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

