<?php
/**
 * Migration class m200921_084541_payment_transaction
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200921_084541_payment_transaction extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Remove old table "commerce_payment"
        $this->dropTableIfExists('commerce_payment', true);
        $this->dropForeignKey('commerce_order_payment_id_fk', 'commerce_order');

		// Create "commerce_transaction" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_transaction', true);

        $this->createTable('commerce_transaction', [
            'transaction_id' => $this->primaryKey(),
            'order_id' => $this->integer()->unsigned()->notNull(),
            'gateway_id' => $this->integer()->unsigned()->notNull(),
            'status_type' => $this->enum('status_type', ['pending', 'accepted', 'rejected', 'cancelled'])->notNull()->defaultValue('pending'),
            'transaction_type' => $this->enum('type', ['authorize', 'capture', 'purchase', 'refund'])->notNull()->defaultValue('purchase'),
            'amount' => $this->float()->unsigned(),
            'currency_id' => $this->string(4)->notNull()->defaultValue('EUR'),
            'gateway_reference' => $this->string(),
            'request_json' => $this->text(),
            'response_json' => $this->text(),
            'response_code' => $this->string(32),
            'response_message' => $this->text(),
            'error_code' => $this->string(32),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create index for "status_type" column
        $this->createIndex(null, 'commerce_transaction', ['status_type'], false);
        $this->createIndex(null, 'commerce_transaction', ['order_id', 'status_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_transaction', ['order_id'], 'commerce_order', ['order_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_transaction', ['gateway_id'], 'commerce_gateway', ['gateway_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_transaction', ['currency_id'], 'currency', ['currency_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_transaction', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_transaction', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Rename "payment_id" to "transaction_id" on "commerce_order" table
        $this->renameColumn('commerce_order', 'payment_id', 'transaction_id');
        $this->addForeignKey(null, 'commerce_order', ['transaction_id'], 'commerce_transaction', ['transaction_id'], 'SET NULL', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('commerce_transaction');
		return false;
	}
}

