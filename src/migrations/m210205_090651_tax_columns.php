<?php
/**
 * Migration class m210205_090651_tax_columns
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210205_090651_tax_columns extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new TAX columns to "commerce_line_item" table
        $this->addColumn('commerce_line_item', 'tax_price', $this->float()->unsigned()->notNull()->defaultValue(0)->after('price_alias'));
        $this->addColumn('commerce_line_item', 'tax_id', $this->integer()->unsigned()->defaultValue(1)->after('tax_price'));
        $this->addColumn('commerce_line_item', 'is_taxes_included', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('tax_id'));

        // Foreign Keys
        $this->addForeignKey(null, 'commerce_line_item', ['tax_id'], 'commerce_tax', ['tax_id'], 'SET NULL', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

