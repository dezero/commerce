<?php
/**
 * Migration class m200424_121607_create_line_item_option_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200424_121607_create_line_item_option_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "commerce_line_item_option" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_line_item_option', true);

        $this->createTable('commerce_line_item_option', [
            'line_item_id' => $this->integer(10)->unsigned()->notNull(),
            'option_id' => $this->integer(10)->unsigned()->notNull(),
            'order_id' => $this->integer()->unsigned()->notNull(),
            'option_type' => $this->char(32)->notNull(),
            'price' => $this->float()->notNull()->defaultValue(0),

            // Dates
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
        ]);

        // Primary key (multiple columns)
        $this->addPrimaryKey(null, 'commerce_line_item_option', ['line_item_id', 'option_id']);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_line_item_option', ['line_item_id'], 'commerce_line_item', ['line_item_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_line_item_option', ['option_id'], 'commerce_product_option', ['option_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_line_item_option', ['order_id'], 'commerce_order', ['order_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_line_item_option', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);

        return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

