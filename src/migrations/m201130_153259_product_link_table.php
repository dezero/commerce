<?php
/**
 * Migration class m201130_153259_product_link_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m201130_153259_product_link_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Create "commerce_product_link" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_product_link', true);

        $this->createTable('commerce_product_link', [
            'link_id' => $this->primaryKey(),
            'product_id' => $this->integer()->unsigned()->notNull(),
            'url' => $this->string()->notNull(),
            'title' => $this->string(),
            'is_video' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'weight' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1),
            'source' => $this->string(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_product_link', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_link', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_link', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

