<?php
/**
 * Migration class m200701_132158_line_item_variant_index
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200701_132158_line_item_variant_index extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Create indexes for "commerce_line_item" table
        $this->createIndex(null, 'commerce_line_item', ['order_id', 'variant_id'], false);
        $this->createIndex(null, 'commerce_line_item', ['order_id', 'product_id'], false);

        // New line item prices
        $this->addColumn('commerce_line_item', 'unit_product_price', $this->float()->notNull()->defaultValue(0)->after('options_json'));
        $this->addColumn('commerce_line_item', 'unit_options_price', $this->float()->notNull()->defaultValue(0)->after('unit_product_price'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

