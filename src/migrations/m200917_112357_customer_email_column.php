<?php
/**
 * Migration class m200917_112357_customer_email_column
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200917_112357_customer_email_column extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new columns "weight" to product and variants
        $this->addColumn('commerce_customer', 'email', $this->string()->notNull()->after('user_id'));

        // Create indexes
        $this->createIndex(null, 'commerce_customer', ['email'], false);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

