<?php
/**
 * Migration class m200306_123610_create_order_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200306_123610_create_order_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Create "commerce_gateway" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_gateway', true);

        $this->createTable('commerce_gateway', [
            'gateway_id' => $this->primaryKey(),
            'alias' => $this->char(32)->notNull(),
            'name' => $this->string(128)->notNull(),
            'description' => $this->string(255),
            'settings_json' => $this->text(),
            'is_disabled' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'commerce_gateway', ['alias'], false);
        $this->createIndex(null, 'commerce_gateway', ['is_disabled'], false);
    
        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_gateway', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_gateway', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Insert default gateway for testing
        $this->insertMultiple('commerce_gateway', [
            [
                'alias'         => 'dummy',
                'name'          => 'Dummy Payment',
                'description'   => 'Dummy payment for testing purposes',
                'is_disabled'   => 0,
                'created_date'  => time(),
                'created_uid'   => 1,
                'updated_date'  => time(),
                'updated_uid'   => 1,
                'uuid'          => StringHelper::UUID()
            ],
        ]);


        // Create "commerce_payment" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_payment', true);

        $this->createTable('commerce_payment', [
            'payment_id' => $this->primaryKey(),
            'order_id' => $this->integer()->unsigned()->notNull(),
            'gateway_id' => $this->integer()->unsigned()->notNull(),
            'status_type' => $this->enum('status_type', ['pending', 'accepted', 'rejected', 'cancelled'])->notNull()->defaultValue('pending'),
            'amount' => $this->float()->unsigned()->notNull(),
            'currency_id' => $this->string(4)->notNull()->defaultValue('EUR'),
            'gateway_request_json' => $this->text(),
            'gateway_response_json' => $this->text(),
            'gateway_response_code' => $this->string(16),
            'gateway_error_code' => $this->string(16),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create index for "status_type" column
        $this->createIndex(null, 'commerce_payment', ['status_type'], false);
        $this->createIndex(null, 'commerce_payment', ['order_id', 'status_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_payment', ['gateway_id'], 'commerce_gateway', ['gateway_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_payment', ['currency_id'], 'currency', ['currency_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_payment', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_payment', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "commerce_shipping_type" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_shipping_method', true);

        $this->createTable('commerce_shipping_method', [
            'shipping_method_id' => $this->primaryKey(),
            'alias' => $this->char(32)->notNull(),
            'name' => $this->string(128)->notNull(),
            'description' => $this->string(255),
            'settings_json' => $this->text(),
            'amount' => $this->float()->unsigned(),
            'currency_id' => $this->string(4),
            'is_disabled' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'commerce_shipping_method', ['alias'], false);
        $this->createIndex(null, 'commerce_shipping_method', ['is_disabled'], false);
    
        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_shipping_method', ['currency_id'], 'currency', ['currency_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_shipping_method', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_shipping_method', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Insert default shipping type for testing
        $this->insertMultiple('commerce_shipping_method', [
            [
                'alias'         => 'default',
                'name'          => 'Default Shipping Method',
                'description'   => 'Default shipping method',
                'is_disabled'   => 0,
                'created_date'  => time(),
                'created_uid'   => 1,
                'updated_date'  => time(),
                'updated_uid'   => 1,
                'uuid'          => StringHelper::UUID()
            ],
        ]);

		
        // Create "commerce_order" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_order', true);

        $this->createTable('commerce_order', [
            // Order identification
            'order_id' => $this->primaryKey(),
            'order_reference' => $this->string(128),
            'status_type' => $this->enum('status_type', ['cart', 'ordered', 'paid', 'processing', 'shipped', 'completed', 'cancelled'])->notNull()->defaultValue('cart'),

            // Customer information
            'user_id' => $this->integer()->unsigned()->notNull(),
            'billing_address_id' => $this->integer()->unsigned(),
            'shipping_address_id' => $this->integer()->unsigned(),
            'is_billing_different' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'is_cart_completed' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'is_new_account' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'language_id' => $this->string(4),
            
            // Price columns
            'items_price' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'shipping_price' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'tax_price' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'discount_price' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'paid_price' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'total_price' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'currency_id' => $this->string(4),

            // More order columns
            'coupon_code' => $this->string(128),
            'discount_id' => $this->integer()->unsigned(),
            'payment_id' => $this->integer()->unsigned(),
            'shipping_method_id' => $this->integer()->unsigned(),
            'ordered_date' => $this->date(),
            'paid_date' => $this->date(),
            'source_type' => $this->enum('source_type', ['web', 'admin', 'external'])->notNull()->defaultValue('web'),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'commerce_order', ['order_reference'], false);
        $this->createIndex(null, 'commerce_order', ['status_type'], false);
        $this->createIndex(null, 'commerce_order', ['user_id', 'is_cart_completed'], false);

        // Create FOREIGN KEYS for "commerce_order" table
        $this->addForeignKey(null, 'commerce_order', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_order', ['billing_address_id'], 'commerce_customer_address', ['address_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_order', ['shipping_address_id'], 'commerce_customer_address', ['address_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_order', ['language_id'], 'language', ['language_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_order', ['currency_id'], 'currency', ['currency_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_order', ['payment_id'], 'commerce_payment', ['payment_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_order', ['shipping_method_id'], 'commerce_shipping_method', ['shipping_method_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_order', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_order', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Create FOREIGN KEYS for other tables
        $this->addForeignKey(null, 'commerce_customer_address', ['order_id'], 'commerce_order', ['order_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_payment', ['order_id'], 'commerce_order', ['order_id'], 'CASCADE', null);


        // Create "commerce_order_history" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_order_history', true);

        $this->createTable('commerce_order_history', [
            'order_history_id' => $this->primaryKey(),
            'order_id' => $this->integer()->unsigned()->notNull(),
            'status_type' => $this->enum('status_type', ['cart', 'ordered', 'paid', 'processing', 'shipped', 'completed', 'cancelled'])->notNull()->defaultValue('cart'),
            'comments' => $this->text(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
        ]);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_order_history', ['order_id'], 'commerce_order', ['order_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_order_history', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "commerce_line_item" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_line_item', true);

        $this->createTable('commerce_line_item', [
            'line_item_id' => $this->primaryKey(),
            'order_id' => $this->integer()->unsigned()->notNull(),
            'product_id' => $this->integer()->unsigned()->notNull(),
            'variant_id' => $this->integer()->unsigned()->notNull(),
            'sku' => $this->string(128),
            'title' => $this->string(255),
            'description' => $this->text(),
            'options_json' => $this->text(),
            'unit_price' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'quantity' => $this->integer()->notNull()->defaultValue(1),
            'total_price' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_line_item', ['order_id'], 'commerce_order', ['order_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_line_item', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_line_item', ['variant_id'], 'commerce_variant', ['variant_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_line_item', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_line_item', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        
        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'commerce.order.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Orders - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.order.create',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Orders - Create new orders',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.order.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Orders - Edit orders',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.order.view',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Orders - View orders',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'order_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Orders - Full access to orders',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'order_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Orders - Edit orders',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'order_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Orders - View orders',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'order_manage',
                'child'     => 'commerce.order.*'
            ],
            [
                'parent'    => 'order_edit',
                'child'     => 'commerce.order.update'
            ],
            [
                'parent'    => 'order_edit',
                'child'     => 'commerce.order.view'
            ],
            [
                'parent'    => 'order_view',
                'child'     => 'commerce.order.view'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'order_manage'
            ],
            [
                'parent'    => 'editor',
                'child'     => 'order_edit'
            ],
        ]);


		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

