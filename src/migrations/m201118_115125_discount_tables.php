<?php
/**
 * Migration class m201118_115125_discount_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m201118_115125_discount_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "commerce_discount" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_discount', true);

        $this->createTable('commerce_discount', [
            'discount_id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string(),
            'is_active' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'limit_uses' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'total_uses' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'start_date' => $this->date(),
            'expiration_date' => $this->date(),
            'amount' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'is_fixed_amount' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'application_type' => $this->enum('application_type', ['total', 'items', 'shipping'])->defaultValue('total'),
            'is_free_shipping' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'internal_comments' => $this->text(),
            'disable_date' => $this->date(),
            'disable_uid' => $this->integer()->unsigned(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'commerce_discount', ['code'], false);
        $this->createIndex(null, 'commerce_discount', ['is_active'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_discount', ['disable_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_discount', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_discount', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Create FOREIGN KEY on "commerce_order" table
        $this->addForeignKey(null, 'commerce_order', ['discount_id'], 'commerce_discount', ['discount_id'], 'SET NULL', null);


        // Create "commerce_discount_use" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_discount_use', true);

        $this->createTable('commerce_discount_use', [
            'usage_id' => $this->primaryKey(),
            'discount_id' => $this->integer()->unsigned()->notNull(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'order_id' => $this->integer()->unsigned(),
            'discount_json' => $this->text(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull()
        ]);

        // Create indexes
        $this->createIndex(null, 'commerce_discount_use', ['discount_id', 'user_id'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_discount_use', ['discount_id'], 'commerce_discount', ['discount_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_discount_use', ['user_id'], 'commerce_customer', ['user_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_discount_use', ['order_id'], 'commerce_order', ['order_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_discount_use', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "commerce_discount_category" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_discount_category', true);

        $this->createTable('commerce_discount_category', [
            'discount_id' => $this->integer()->unsigned()->notNull(),
            'category_id' => $this->integer()->unsigned()->notNull(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull()
        ]);

        // Primary key (alternative method)
        $this->addPrimaryKey(null, 'commerce_discount_category', ['discount_id', 'category_id']);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_discount_category', ['discount_id'], 'commerce_discount', ['discount_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_discount_category', ['category_id'], 'category', ['category_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_discount_category', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);

        
        // Permissions for PRODUCT
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'commerce.discount.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Discounts - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.discount.create',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Discounts - Create new discounts',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.discount.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Discounts - Edit discounts',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.discount.view',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Discounts - View discounts',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'discount_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Discounts - Full access to discounts',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'discount_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Discounts - Edit discounts',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'discount_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Discounts - View discounts',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'discount_manage',
                'child'     => 'commerce.discount.*'
            ],
            [
                'parent'    => 'discount_edit',
                'child'     => 'commerce.discount.update'
            ],
            [
                'parent'    => 'discount_edit',
                'child'     => 'commerce.discount.view'
            ],
            [
                'parent'    => 'discount_view',
                'child'     => 'commerce.discount.view'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'discount_manage'
            ],
            [
                'parent'    => 'editor',
                'child'     => 'discount_edit'
            ],
        ]);


		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

