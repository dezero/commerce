<?php
/**
 * Migration class m200306_115646_create_customer_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200306_115646_create_customer_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "commerce_customer_address" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_customer_address', true);

        $this->createTable('commerce_customer_address', [
            'address_id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'order_id' => $this->integer()->unsigned(),
            'firstname' => $this->string(100),
            'lastname' => $this->string(100),
            'vat_code' => $this->string(128),
            'address_line' => $this->string(255),
            'city' => $this->string(128),
            'province' => $this->string(64),
            'postal_code' => $this->string(32),
            'country_code' => $this->string(4),
            'phone' => $this->string(64),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_customer_address', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_customer_address', ['country_code'], 'country', ['country_code'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_customer_address', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_customer_address', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);


        // Create "commerce_customer" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_customer', true);

        $this->createTable('commerce_customer', [
            'user_id' => $this->integer()->unsigned()->notNull(),
            'billing_address_id' => $this->integer()->unsigned(),
            'shipping_address_id' => $this->integer()->unsigned(),
            'is_newsletter' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create indexes
        $this->createIndex(null, 'commerce_customer', ['is_newsletter'], false);

        // Primary key
        $this->addPrimaryKey(null, 'commerce_customer', ['user_id']);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_customer', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_customer', ['billing_address_id'], 'commerce_customer_address', ['address_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_customer', ['shipping_address_id'], 'commerce_customer_address', ['address_id'], 'SET NULL', null);
        $this->addForeignKey(null, 'commerce_customer', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_customer', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        
        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'commerce.customer.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Customers - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.customer.create',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Customers - Create new customers',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.customer.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Customers - Edit customers',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'commerce.customer.view',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Commerce - Customers - View customers',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'customer_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Customers -Full access to customers',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'customer_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Customers -Edit customers',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'customer_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Customers -View customers',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            [
                'parent'    => 'customer_manage',
                'child'     => 'commerce.customer.*'
            ],
            [
                'parent'    => 'customer_edit',
                'child'     => 'commerce.customer.update'
            ],
            [
                'parent'    => 'customer_edit',
                'child'     => 'commerce.customer.view'
            ],
            [
                'parent'    => 'customer_view',
                'child'     => 'commerce.customer.view'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'customer_manage'
            ],
            [
                'parent'    => 'editor',
                'child'     => 'customer_edit'
            ],
        ]);


		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

