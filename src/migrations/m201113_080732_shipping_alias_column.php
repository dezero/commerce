<?php
/**
 * Migration class m201113_080732_shipping_alias_column
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m201113_080732_shipping_alias_column extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Add new column "shipping_method_alias" to "commerce_order" table
        $this->addColumn('commerce_order', 'shipping_method_alias', $this->char(32)->after('shipping_method_id'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

