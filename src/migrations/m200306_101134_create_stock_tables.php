<?php
/**
 * Migration class m200306_103416_create_stock_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200306_101134_create_stock_tables extends Migration
{

	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Create "commerce_warehouse" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_warehouse', true);

        $this->createTable('commerce_warehouse', [
            'warehouse_id' => $this->primaryKey(),
            'alias' => $this->char(32)->notNull(),
            'name' => $this->string(64)->notNull(),
            'description' => $this->string(255),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);

        // Create index for "alias" column
        $this->createIndex(null, 'commerce_warehouse', ['alias'], false);
    
        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_warehouse', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_warehouse', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Insert default warehouse for ecommerce
        $this->insertMultiple('commerce_warehouse', [
            [
                'alias'         => 'ecommerce',
                'name'          => 'Ecommerce',
                'description'   => 'Ecommerce Warehouse',
                'created_date'  => time(),
                'created_uid'   => 1,
                'updated_date'  => time(),
                'updated_uid'   => 1,
                'uuid'          => StringHelper::UUID()
            ],
        ]);


        // Create "commerce_stock" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_stock', true);

        $this->createTable('commerce_stock', [
            'warehouse_id' => $this->integer()->unsigned()->notNull()->defaultValue(1),
            'variant_id' => $this->integer()->unsigned()->notNull(),
            'product_id' => $this->integer()->unsigned()->notNull(),
            'stock' => $this->integer()->notNull()->defaultValue(0),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
        ]);

        // Primary key (multiple columns)
        $this->addPrimaryKey(null, 'commerce_stock', ['warehouse_id', 'variant_id']);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_stock', ['warehouse_id'], 'commerce_warehouse', ['warehouse_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_stock', ['variant_id'], 'commerce_variant', ['variant_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_stock', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_stock', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_stock', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

