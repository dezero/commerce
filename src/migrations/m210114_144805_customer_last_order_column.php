<?php
/**
 * Migration class m210114_144805_customer_last_order_column
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210114_144805_customer_last_order_column extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new column "last_paid_order_id" to "commerce_customer" table
        $this->addColumn('commerce_customer', 'last_paid_order_id', $this->integer()->unsigned()->after('shipping_address_id'));

        // Add the foreign key
        $this->addForeignKey(null, 'commerce_customer', ['last_paid_order_id'], 'commerce_order', ['order_id'], 'SET NULL', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

