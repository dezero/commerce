<?php
/**
 * Migration class m201024_111801_gateway_alias_columns
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m201024_111801_gateway_alias_columns extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new columns "gateway_alias" to "commerce_order" and "commerce_transaction" tables
        $this->addColumn('commerce_order', 'gateway_alias', $this->char(32)->after('gateway_id'));
        $this->addColumn('commerce_transaction', 'gateway_alias', $this->char(32)->after('gateway_id'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

