<?php
/**
 * Migration class m220525_112101_discount_after_order
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m220525_112101_discount_after_order extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new columns to "commerce_discount" table
        $this->addColumn('commerce_discount', 'is_after_order_restriction', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('product_restriction_type'));
        $this->addColumn('commerce_discount', 'after_order_num_days', $this->smallInteger()->unsigned()->notNull()->defaultValue(0)->after('is_after_order_restriction'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

