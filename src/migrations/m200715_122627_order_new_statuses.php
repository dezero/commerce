<?php
/**
 * Migration class m200715_122627_order_new_statuses
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200715_122627_order_new_statuses extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// New statuses
        $this->dropColumn('commerce_order', 'status_type');
        $this->dropColumn('commerce_order_history', 'status_type');
        $this->addColumn('commerce_order', 'status_type', $this->enum('status_type', ['cart', 'payment_received', 'payment_failed', 'in_progress', 'shipped', 'completed', 'canceled', 'on_hold'])->notNull()->defaultValue('cart')->after('order_number'));
        $this->addColumn('commerce_order_history', 'status_type', $this->enum('status_type', ['cart', 'payment_received', 'payment_failed', 'in_progress', 'shipped', 'completed', 'canceled', 'on_hold'])->notNull()->defaultValue('cart')->after('order_id'));

        // Create indexes
        $this->createIndex(null, 'commerce_order', ['status_type'], false);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

