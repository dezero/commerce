<?php
/**
 * Migration class m210114_160149_transaction_num_column
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210114_160149_transaction_num_column extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new column "order_transaction_num" to "commerce_transaction" table
        $this->addColumn('commerce_transaction', 'order_transaction_num', $this->integer()->unsigned()->notNull()->defaultValue(0)->after('order_id'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

