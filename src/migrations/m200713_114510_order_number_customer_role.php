<?php
/**
 * Migration class m200713_114510_order_number_customer_role
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m200713_114510_order_number_customer_role extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new column "order_number"
        $this->addColumn('commerce_order', 'order_number', $this->string(64)->after('order_reference'));

        // Reduce length of "order_reference" to 32 characters
        $this->alterColumn('commerce_order', 'order_reference', $this->string(32));

        // Add new column "email"
        $this->addColumn('commerce_order', 'email', $this->string(255)->after('user_id'));

        // Add new column "gateway_id"
        $this->addColumn('commerce_order', 'gateway_id', $this->integer()->unsigned()->after('discount_id'));
        $this->addForeignKey(null, 'commerce_order', ['gateway_id'], 'commerce_gateway', ['gateway_id'], 'SET NULL', null);

        // Add new "customer" role
        $this->insert('user_auth_item', [            
            'name'          => 'customer',
            'type'          => 2,
            'item_type'     => 'role',
            'description'   => 'Customer',
            'created_date'  => time(),
            'uuid'          => StringHelper::UUID()
        ]);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

