<?php
/**
 * Migration class m230108_120051_order_json_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m230108_120051_order_json_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "commerce_order_json" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_order_json', true);

        $this->createTable('commerce_order_json', [
            'order_id' => $this->integer()->unsigned()->notNull(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'email' => $this->string(255),
            'json_data' => $this->text(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
    
        // Primary key (alternative method)
        $this->addPrimaryKey(null, 'commerce_order_json', 'order_id');

        // Create indexes
        $this->createIndex(null, 'commerce_order_json', ['user_id'], false);

        //
        // IMPORTANT
        //
        // DO NOT CREATE RELATIONS (FOREIGN KEYS) FOR THIS TABLE
        // This table was created to LOG an Order in a specific moment
        // and it it could be the case the Order or User does not exist in database


		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

