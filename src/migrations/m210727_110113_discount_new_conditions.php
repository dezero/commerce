<?php
/**
 * Migration class m210727_110113_discount_new_conditions
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210727_110113_discount_new_conditions extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add columns "is_per_user_limit" and "minimum_amount" to "commerce_discount" table
        $this->addColumn('commerce_discount', 'is_per_user_limit', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('limit_uses'));
        $this->addColumn('commerce_discount', 'minimum_amount', $this->float()->unsigned()->notNull()->defaultValue(0)->after('is_free_shipping'));

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

