<?php
/**
 * Migration class m201203_093931_price_alias
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m201203_093931_price_alias extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Add new column "price_alias" to "commerce_product_price" table
        $this->addColumn('commerce_product_price', 'price_alias', $this->char(32)->after('price_id'));

        // Add new columns "price_id" and "price_alias" to "commerce_line_item" table
        $this->addColumn('commerce_line_item', 'price_id', $this->integer()->unsigned()->after('total_price'));
        $this->addColumn('commerce_line_item', 'price_alias', $this->char(32)->after('price_id'));

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_line_item', ['price_id'], 'commerce_price', ['price_id'], 'SET NULL', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

