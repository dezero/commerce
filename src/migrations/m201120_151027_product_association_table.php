<?php
/**
 * Migration class m201120_151027_product_association_table
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m201120_151027_product_association_table extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
        // Create "commerce_product_association" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('commerce_product_association', true);

        $this->createTable('commerce_product_association', [
            'association_id' => $this->primaryKey(),
            'product_id' => $this->integer()->unsigned()->notNull(),
            'product_associated_id' => $this->integer()->unsigned()->notNull(),
            'association_type' => $this->enum('association_type', ['related', 'cross-sell', 'upsell', 'reconditioned', 'substitution', 'pack'])->defaultValue('related'),
            'weight' => $this->integer()->unsigned()->notNull()->defaultValue(1),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
        ]);

        // Create indexes
        $this->createIndex(null, 'commerce_product_association', ['product_id', 'association_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'commerce_product_association', ['product_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_association', ['product_associated_id'], 'commerce_product', ['product_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_association', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'commerce_product_association', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		return false;
	}
}

