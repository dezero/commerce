<?php
/**
 * Module to manage commerce entities for DZ Framework
 */

namespace dzlab\commerce;

use Yii;

class Module extends \dz\web\Module
{
    /**
     * @var array mapping from controller ID to controller configurations.
     */
    public $controllerMap = [
        // Controller for Customer models
        'customer' => [
            'class' => 'dzlab\commerce\controllers\CustomerController',
        ],

        // Controller for Discount models
        'discount' => [
            'class' => 'dzlab\commerce\controllers\DiscountController',
        ],

        // Controller for DiscountProduct models
        'discountProduct' => [
            'class' => 'dzlab\commerce\controllers\DiscountProductController',
        ],

        // Controller to make exports
        'export' => [
            'class' => 'dzlab\commerce\controllers\ExportController',
        ],

        // Controller for extra (OPTION TYPE)
        'extra' => [
            'class' => 'dzlab\commerce\controllers\ExtraController',
        ],

        // Controller for LineItem models
        'lineItem' => [
            'class' => 'dzlab\commerce\controllers\LineItemController',
        ],

        // Controller for Order models
        'order' => [
            'class' => 'dzlab\commerce\controllers\OrderController',
        ],

        // Controller for OrderJson models
        'orderJson' => [
            'class' => 'dzlab\commerce\controllers\OrderJsonController',
        ],

        // Controller for Product models (PRODUCT TYPE SAMPLE)
        'product' => [
            'class' => 'dzlab\commerce\controllers\ProductController',
        ],

        // Controller for ProductAssociation models
        'productAssociation' => [
            'class' => 'dzlab\commerce\controllers\ProductAssociationController',
        ],

        // Controller for ProductImage models
        'productImage' => [
            'class' => 'dzlab\commerce\controllers\ProductImageController',
        ],

        // Controller for ProductLink models (is_video = 0)
        'productLink' => [
            'class' => 'dzlab\commerce\controllers\ProductLinkController',
        ],

        // Controller for ProductLink models (is_video = 1)
        'productVideo' => [
            'class' => 'dzlab\commerce\controllers\ProductVideoController',
        ],

        // Redsys TPV gateway
        'redsys' => [
            'class' => 'dzlab\commerce\controllers\RedsysController',
        ],

        // Controller for size (OPTION TYPE SAMPLE)
        'size' => [
            'class' => 'dzlab\commerce\controllers\SizeController',
        ],
    ];


    /**
     * Default controller
     */
    public $defaultController = 'product';


    /**
     * Load specific CSS or JS files for this module
     */
    public $cssFiles = null; // ['commerce.css'];
    public $jsFiles = [
        'dz.orderStatus.js',
        'dz.lineItem.js',
        'dz.productImage.js',
        'dz.productVideo.js',
        'dz.productLink.js',
        'dz.productAssociation.js',
        'dz.discountProduct.js',
        'commerce.js',
    ];


    /**
     * This method is called when the module is being created
     * you may place code here to customize the module or the application
     */
    public function init()
    {
        // Init this module with current path
        $this->init_module(__DIR__);

        // Going on with the init process
        parent::init();
    }
}
