<?php
/**
 * CheckoutManager
 * 
 * Component to manage checkout process
 */

namespace dzlab\commerce\components;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dzlab\commerce\models\Customer;
use dzlab\commerce\models\CustomerAddress;
use dzlab\commerce\models\Gateway;
use dzlab\commerce\models\Order;
use dzlab\commerce\models\ShippingMethod;
use dzlab\commerce\models\Transaction;
use dz\modules\settings\models\Country;
use user\models\User;
use Yii;

class CheckoutManager extends ApplicationComponent
{
    /**
     * Array with gateways (payment methods) information
     */
    public $gateways;


    /**
     * Order model
     */
    protected $cart_order_model;


    /**
     * Submit checkout data
     * 
     * @see CheckoutController::actionBeforePayment()
     */
    public function save_checkout($cart_order_model, $vec_input_data = [])
    {
        $vec_output = [
            'error_code'    => 0,
            'errors'        => []
        ];

        // --------------------------------------------------------------------------
        // CUSTOMER DATA
        // --------------------------------------------------------------------------

        // User & customer model
        $user_model = Yii::createObject(User::class);
        $customer_model = Yii::createObject(Customer::class);
        
        if ( $cart_order_model->user_id > 0 && $cart_order_model->user )
        {
            $user_model = $cart_order_model->user;
            if ( $cart_order_model->customer )
            {
                $customer_model = $cart_order_model->customer;
            }
        }
        else if ( Yii::app()->user->id > 0 )
        {
            $user_model = User::findOne(Yii::app()->user->id);
            $customer_model = Customer::findOne(Yii::app()->user->id);
        }


        // --------------------------------------------------------------------------
        // SHIPPING & BILLING ADDRESSES
        // --------------------------------------------------------------------------

        // Shipping address
        $shipping_address_model = Yii::createObject(CustomerAddress::class);
        if ( $cart_order_model->shippingAddress )
        {
            $shipping_address_model = $cart_order_model->shippingAddress;
        }
        else if ( ! $customer_model->isNewRecord && $customer_model->shippingAddress )
        {
            $shipping_address_model = $customer_model->shippingAddress; 
        }
        else if ( ! $user_model->isNewRecord )
        {
            $shipping_address_model->setAttributes([
                'firstname' => $user_model->firstname,
                'lastname'  => $user_model->lastname
            ]);
        }

        // Billing address
        $billing_address_model = Yii::createObject(CustomerAddress::class);
        if ( $cart_order_model->billingAddress )
        {
            $billing_address_model = $cart_order_model->billingAddress;
        }
        else if ( ! $customer_model->isNewRecord && $customer_model->billingAddress )
        {
            $billing_address_model = $customer_model->billingAddress; 
        }
        else if ( ! $user_model->isNewRecord )
        {
            $billing_address_model->setAttributes([
                'firstname' => $user_model->firstname,
                'lastname'  => $user_model->lastname
            ]);
        }

        // Scencario "checkout" is needed to validation (required fields)
        $shipping_address_model->scenario = 'checkout';
        $billing_address_model->scenario = 'checkout';
        $cart_order_model->scenario = 'checkout';


        // --------------------------------------------------------------------------
        // SAVING DATA
        // --------------------------------------------------------------------------
        if ( isset($vec_input_data['CustomerAddress']) && isset($vec_input_data['Order']) )
        {
            $cart_order_model->setAttributes($vec_input_data['Order']);

            // #1 - EMAIL VALIDATION
            // -----------------------------------------
            if ( isset($vec_input_data['Order']['email']) )
            {
                $email = StringHelper::trim($vec_input_data['Order']['email']);
                if ( empty($email) )
                {
                    $vec_output['error_code'] = 100;
                    $vec_output['errors']['email'] = 'Email address is required';
                }
                else if ( ! StringHelper::validate_email($email) )
                {
                    $vec_output['error_code'] = 101;
                    $vec_output['errors']['email'] = $vec_input_data['Order']['email']. ' is not a valid email address.';
                }
            }
            

            // #2 - CREATE / UPDATE USER MODELS
            // -----------------------------------------
            if ( $user_model->id == 0 )
            {
                // #2.1 - User model
                if ( $vec_output['error_code'] == 0 )
                {
                    $user_model = Yii::app()->customerManager->create_user_model($vec_input_data['Order']['email']);
                    if ( ! $user_model )
                    {
                        // $vec_output['error_msg'] = 'Ha ocurrido un error al guardar los datos. Por favor, inténtalo de nuevo o <a href="'. Url::to('/contact') .'" target="_blank">ponte en contacto con nosotros</a>.';
                        $vec_output['error_code'] = 102;
                        $vec_output['errors'] = Log::last_model_error(true);
                    }
                }

                // #2.2 - Customer model
                if ( $vec_output['error_code'] == 0 && $customer_model->user_id != $user_model->id )
                {
                    $user_model = User::model()->cache(0)->findByPk($user_model->id);
                    $customer_model = $user_model->customer;
                    if ( ! $customer_model )
                    {
                        // $vec_output['error_msg'] = 'Ha ocurrido un error al guardar los datos. Por favor, inténtalo de nuevo o <a href="'. Url::to('/contact') .'" target="_blank">ponte en contacto con nosotros</a>.';
                        $vec_output['error_code'] = 103;
                        $vec_output['errors'] = Log::last_model_error(true);
                    }
                }
            }

            // Get email from user / customer
            else if ( ! isset($vec_input_data['Order']['email']) )
            {
                $cart_order_model->email = $user_model->email;
                if ( $user_model->customer && !empty($user_model->customer->email) )
                {
                    $cart_order_model->email = $user_model->customer->email;
                }
            }

            // #3 - CUSTOMER ADDRESS
            // -----------------------------------------
            if ( $vec_output['error_code'] == 0 )
            {
                // Shipping address
                if ( isset($vec_input_data['CustomerAddress']['shipping']) )
                {
                    $shipping_address_model->setAttributes($vec_input_data['CustomerAddress']['shipping']);
                    $shipping_address_model->user_id = $customer_model->user_id;
                    if ( $shipping_address_model->save() )
                    {
                        $cart_order_model->shipping_address_id = $shipping_address_model->address_id;
                    }
                    else
                    {
                        $vec_output['error_code'] = 104;
                        $vec_output['errors']['CustomerAddress'] = $shipping_address_model->getErrors();
                    }
                }

                // Billing address
                if ( isset($vec_input_data['Order']['is_billing_different']) && isset($vec_input_data['CustomerAddress']['billing']) )
                {
                    $billing_address_model->setAttributes($vec_input_data['CustomerAddress']['billing']);
                    $billing_address_model->user_id = $customer_model->user_id;
                    if ( $billing_address_model->save() )
                    {
                        $cart_order_model->billing_address_id = $billing_address_model->address_id;
                    }
                    else
                    {
                        $vec_output['error_code'] = 105;
                        $vec_output['errors']['CustomerAddress'] = $billing_address_model->getErrors();
                    } 
                }
                else if ( !empty($cart_order_model->shipping_address_id) )
                {
                    $cart_order_model->billing_address_id = $cart_order_model->shipping_address_id;
                }
            }

            // #4 - ORDER MODEL
            // -----------------------------------------
            if ( $vec_output['error_code'] == 0 )
            {
                $cart_order_model->user_id = $customer_model->user_id;                

                // Gateway and customer addresses are required
                if ( ! $cart_order_model->save() )
                {
                    Log::save_model_error($cart_order_model);

                    // $vec_output['error_msg'] = 'Ha ocurrido un error al guardar los datos. Por favor, inténtalo de nuevo o <a href="'. Url::to('/contact') .'" target="_blank">ponte en contacto con nosotros</a>.';
                    $vec_output['error_code'] = 111;
                    $vec_output['errors'] = Log::last_model_error(true);
                }

                // Reload Order model and re-update prices
                else
                {
                    $cart_order_model = Order::model()->cache(0)->findByPk($cart_order_model->order_id);
                    $cart_order_model->update_prices();
                }
            }

            // #5 - LAST UPDATES
            // -----------------------------------------
            if ( $vec_output['error_code'] == 0 )
            {
                $user_model->saveAttributes([
                    'firstname' => !empty($billing_address_model->firstname) ? $billing_address_model->firstname : $user_model->firstname,
                    'lastname'  => !empty($billing_address_model->lastname) ? $billing_address_model->lastname : $user_model->lastname,
                ]);

                // #6 - Customer model -> Update data
                $customer_model->setAttributes([
                    'email'                 => $cart_order_model->email,
                    'shipping_address_id'   => $shipping_address_model->address_id,
                    'billing_address_id'    => $billing_address_model->address_id
                ]);
                if ( ! $customer_model->save() )
                {
                    Log::save_model_error($customer_model);

                    // $vec_output['error_msg'] = 'Ha ocurrido un error al guardar los datos. Por favor, inténtalo de nuevo o <a href="'. Url::to('/contact') .'" target="_blank">ponte en contacto con nosotros</a>.';
                    $vec_output['error_code'] = 112;
                    $vec_output['errors'] = Log::last_model_error(true);
                }
            }
        }


        // --------------------------------------------------------------------------
        // ORDER BACKUP - Save an order data "backup" in a OrderJson model
        // @since 08/01/2023
        // --------------------------------------------------------------------------
        Yii::app()->orderManager->save_order_json($cart_order_model->order_id);



        // Errors?
        $error_html = '';
        if ( !empty($vec_output['errors']) )
        {
            // Convert errors into compact mode
            $vec_output['errors'] = Log::compact_errors($vec_output['errors']);
            if ( isset($vec_output['errors']['Customer_email']) )
            {
                $vec_output['errors']['Order_email'] = $vec_output['errors']['Customer_email'];
                unset($vec_output['errors']['Customer_email']);
            }

            // Output as HTML
            $error_html = Yii::app()->controller->renderPartial('//checkout/_checkout_error', [
                'vec_errors' => $vec_output['errors']
            ], true);
        }

        return [
            // Errors?
            'error_code'                => $vec_output['error_code'],
            'errors'                    => $vec_output['errors'],
            'error_html'                => $error_html,

            // Customer data
            'user_model'                => $user_model,
            'customer_model'            => $customer_model,
            'billing_address_model'     => $billing_address_model,
            'shipping_address_model'    => $shipping_address_model,

            // Current cart
            'cart_order_model'          => $cart_order_model,

            // More information
            'vec_gateway_list'          => Gateway::model()->gateway_list(),
            'vec_country_list'          => Country::model()->country_list(),
            'free_shipping_model'       => Yii::app()->checkoutManager->get_free_shipping_model(),
            'default_shipping_model'    => Yii::app()->checkoutManager->get_default_shipping_model(),
        ];
    }


    /**
     * Register a new "purchase" Transaction
     */
    public function purchase($order_model, $payment_amount = null, $vec_input_data = [])
    {
        return Yii::app()->checkoutManager->create_transaction($order_model, 'purchase', $payment_amount, $vec_input_data);
    }


    /**
     * Register a new "authorize" Transaction
     */
    public function authorize($order_model, $payment_amount = null, $vec_input_data = [])
    {
        return Yii::app()->checkoutManager->create_transaction($order_model, 'authorize', $payment_amount, $vec_input_data);
    }


    /**
     * Register a new "refund" Transaction
     */
    public function refund($order_model, $payment_amount = null, $vec_input_data = [])
    {
        return Yii::app()->checkoutManager->create_transaction($order_model, 'refund', $payment_amount, $vec_input_data);
    }


    /**
     * Register a new Transaction
     */
    public function create_transaction($order_model, $transaction_type = 'purchase', $payment_amount = null, $vec_input_data = [])
    {
        $vec_output = [
            'error_msg'         => '',
            'error_code'        => 0,
            'transaction_id'    => 0,
            'gateway'           => ''
        ];

        // Total amount to be paid (deposit)
        if ( $payment_amount === null )
        {
            // $payment_amount = $order_model->raw_attributes['total_price'];
            $payment_amount = $this->get_payment_amount($order_model);
        }

        // Load Gateway model to make the transaction
        $gateway_model = Gateway::findOne($order_model->gateway_id);    // <-- This method load Gateway subclass (PaypalGateway, RedsysGateway, ...)
        if ( $gateway_model )
        {
            switch ( $transaction_type )
            {
                case 'purchase':
                    $vec_output = $gateway_model->purchase($order_model, $payment_amount, $vec_input_data);
                break;
            }
        }

        return $vec_output;
    }


    /**
     * Process the GATEWAY response (accept or reject) from a Transaction model
     */
    public function gateway_response($order_model, $transaction_type = 'purchase', $payment_amount = null, $vec_input_data = [])
    {
        // Total amount to be paid (deposit)
        if ( $payment_amount === null )
        {
            $payment_amount = $this->get_payment_amount($order_model);
        }

        // Load Gateway model to make the transaction
        $gateway_model = Gateway::findOne($order_model->gateway_id);    // <-- This method load Gateway subclass (PaypalGateway, RedsysGateway, ...)
        if ( $gateway_model )
        {
            return $gateway_model->response($order_model, $transaction_type, $payment_amount, $vec_input_data);
        }

        return false;
    }


    /**
     * Returns the total amount to be paid via the checkout
     */
    public function get_payment_amount($order_model, $is_formatted_amount = false)
    {
        $payment_amount = $order_model->get_total_price();
        if ( $is_formatted_amount )
        {
            return Yii::app()->number->formatNumber($payment_amount);
        }

        return $payment_amount;
    }


    /**
     * Returns the total number of items added into the order
     */
    public function get_total_quantity($order_model)
    {
        $total_quantity = 0;
        if ( $order_model->lineItems )
        {
            foreach ( $order_model->lineItems as $line_item_model )
            {
                $total_quantity += $line_item_model->quantity;
            }
        }

        return $total_quantity;
    }


    /**
     * Load and return a specific Gateway (with own subclass)
     */
    public function get_gateway($alias)
    {
        if ( isset($this->gateways[$alias]) && isset($this->gateways[$alias]['class']) )
        {
            $class_name = $this->gateways[$alias]['class'];
            return $class_name::get()->where(['alias' => $alias])->one();
        }

        return null;
    }


    /**
     * Get shipping amount
     */
    public function get_shipping_amount($shipping_method_alias, $is_formatted_amount = false)
    {
        $shipping_amount = ShippingMethod::get()->select('amount')->where(['alias' => $shipping_method_alias])->scalar();
        if ( $is_formatted_amount )
        {
            return Yii::app()->number->formatNumber($shipping_amount);
        }

        return $shipping_amount;
    }


    /**
     * Get total Order amount with a Shipping Method applied
     */
    public function get_total_with_shipping($order_model, $shipping_method_alias, $is_formatted_amount = false)
    {
        $total_amount = $order_model->raw_attributes['items_price'] + $this->get_shipping_amount($shipping_method_alias) - $order_model->raw_attributes['discount_price'];
        if ( $is_formatted_amount )
        {
            return Yii::app()->number->formatNumber($total_amount);
        }

        return $total_amount;
    }


    /**
     * Return the FREE ShippingMethod model
     */
    public function get_free_shipping_model()
    {
        return $this->get_shipping_model('free');
    }


    /**
     * Return the DEFAULT ShippingMethod model
     */
    public function get_default_shipping_model()
    {
        return $this->get_shipping_model('default');
    }


    /**
     * Return a ShippingMethod model given an alias
     */
    public function get_shipping_model($alias)
    {
        return ShippingMethod::model()->findByAlias($alias);
    }
}
