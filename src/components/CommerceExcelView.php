<?php
/**
 * Special override of CGridView for EExcelView
 */

use dz\batch\Batch;

Yii::import('@dz.utils.excel.ExcelView');

class CommerceExcelView extends ExcelView
{
    /**
     * Excel sheets - DEZERO extension
     */
    public $sheets = [];


    /**
     * Custom AfterRender event
     */
    public $afterRender ='afterRender'; // '$this->afterRender';


    /**
     * Init callback
     */
    public function init()
    {
        parent::init();
    }


    /**
     * After render callback
     */
    public function afterRender($total_rows, $num_sheet)
    {
        // foreach( $this->sheets as $num_sheet => $que_sheet )
        // {
        // }
        
        // Activate current sheet
        $this->objPHPExcel->setActiveSheetIndex($num_sheet);

        $last_row = $this->objPHPExcel->getActiveSheet()->getHighestRow();
        $last_column = $this->objPHPExcel->getActiveSheet()->getHighestColumn();
        $full_worksheet_dimension = $this->objPHPExcel->getActiveSheet()->calculateWorksheetDimension();

        // Header in bold
        $this->objPHPExcel->getActiveSheet()->getStyle('A1:'. $last_column .'1')->getFont()->setBold(TRUE);

        // Vertical align
        $this->objPHPExcel->getActiveSheet()->getStyle($full_worksheet_dimension)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        // Freeze columns "A2" and "C1" - Header and first column fixed
        $this->objPHPExcel->getActiveSheet()->freezePane('B2');

        // Apply formats
        $this->apply_column_formats();

        // We want first sheet selected when user opens Excel for the first time
        $this->objPHPExcel->setActiveSheetIndex(0);

        // PRODUCTS EXPORT
        if ( preg_match("/^export\_products/", $this->title) )
        {
            $this->objPHPExcel->getActiveSheet()->getStyle($full_worksheet_dimension)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            
            // Wrap text for "description" and "categories"
            $this->objPHPExcel->getActiveSheet()->getStyle('E2:E'. $last_row)->getAlignment()->setWrapText(TRUE);
            $this->objPHPExcel->getActiveSheet()->getStyle('F2:F'. $last_row)->getAlignment()->setWrapText(TRUE);
        }
    }
}