<?php
/**
 * Redsys component class used to connect with the TPV
 */

namespace dzlab\commerce\components;

use dz\base\ApplicationComponent;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\helpers\Url;
use dzlab\commerce\models\Order;
use Yii;

class RedsysComponent extends ApplicationComponent
{
    /**
     * @var string. JSON formatted string and encoded in base64
     */
    public $merchant_data;


    /**
     * @var int. Order id
     */
    public $order_id = null;


    /**
     * @var string. Encoded order id (with prefix and transaction number)
     */
    public $encoded_order_id = null;


    /**
     * @var array. Merchant params
     */
    public $vec_merchant_params = [];


    /**
     * @var string. Payment URL
     */ 
    public $payment_url;


    /**
     * @var array. Configuration options
     */
    private $_vec_config = null;


    /**
     * Init function
     */
    public function init()
    {
        // Load configuration
        $vec_config = $this->get_config();

        if ( empty($this->payment_url) )
        {
            // Connection URL -> Live mode
            // $this->payment_url = 'https://sis.redsys.es/sis/realizarPago'
            $this->payment_url = $this->get_config('live_url');

            // Connection URL - Test mode
            if ( $this->is_test() )
            {
                // $this->payment_url = 'https://sis-t.redsys.es:25443/sis/realizarPago';
                $this->payment_url = $this->get_config('test_url');
            }
        }

        // Language: 001 - Español / 002 - English / 003 - Català
        // @see https://pagosonline.redsys.es/parametros-entrada-salida.html
        $language_code = $this->get_config('language');
        if ( empty($language_code) )
        {
            $language_code = '001';
        }

        // Init merchant params
        $this->vec_merchant_params = [
            'DS_MERCHANT_MERCHANTCODE'      => $this->get_config('merchant_code'),
            'DS_MERCHANT_CURRENCY'          => $this->get_config('currency_code'),
            'DS_MERCHANT_TRANSACTIONTYPE'   => "0",
            'DS_MERCHANT_TERMINAL'          => $this->get_config('terminal'),

            // Force to be in English
            'DS_MERCHANT_CONSUMERLANGUAGE'  => $language_code,

            // URL for notifications (TPV response)
            'DS_MERCHANT_MERCHANTURL'       => Url::to($this->get_config('merchant_url')),

            // Redirect URL if OK or wrong
            'DS_MERCHANT_URLOK'             => Url::to($this->get_config('redirect_ok_url')),
            'DS_MERCHANT_URLKO'             => Url::to($this->get_config('redirect_ko_url'), ['response_type' => 'tpv_error']),
        ];

        parent::init();
    }


    /**
     * Get Redsys configuration
     */
    public function get_config($param_name = '')
    {
        if ( $this->_vec_config === null )
        {
            $vec_commerce_config = Yii::app()->config->get('components.commerce');
            if ( !empty($vec_commerce_config) && isset($vec_commerce_config['redsys']) )
            {
                $this->_vec_config = $vec_commerce_config['redsys'];
            }
        }

        // Return a specific configuration param
        if ( $this->_vec_config !== null )
        {
            if ( !empty($param_name) )
            {
                if ( isset($this->_vec_config[$param_name]) )
                {
                    return $this->_vec_config[$param_name];
                }

                return null;
            }
        }

        // Return all the configuration params
        return $this->_vec_config;
    }


    /**
     * Check if we're working on TEST mode
     */
    public function is_test()
    {
        if ( isset(Yii::app()->params['redsys']) && isset(Yii::app()->params['redsys']['is_test']) )
        {
            return Yii::app()->params['redsys']['is_test'];
        }

        return $this->get_config('is_test');
    }


    /**
     * Set and encode order number
     */
    public function set_order($order_model, $transaction_num = 0)
    {
        if ( $order_model )
        {
            $this->order_id = $order_model->order_id;
            $this->vec_merchant_params['DS_MERCHANT_ORDER'] = $this->encode_order_id($order_model->order_id, $transaction_num);
            
            // Redirect URL if OK            
            if ( !empty($order_model->order_reference) )
            {
                $this->vec_merchant_params['DS_MERCHANT_URLOK'] = Url::to($this->get_config('redirect_ok_url'), ['order' => $order_model->order_reference]);
            }
            else
            {
                $this->vec_merchant_params['DS_MERCHANT_URLOK'] = Url::to($this->get_config('redirect_ok_url'), ['id' => $order_model->order_id]);
            }

            // Redirect URL if wrong
            $this->vec_merchant_params['DS_MERCHANT_URLKO'] = Url::to($this->get_config('redirect_ko_url'), ['id' => $order_model->order_id, 'response_type' => 'tpv_error']);
        }

        return $this;
    }


    /**
     * Set amount to pay 
     */
    public function set_amount($amount)
    {
        $this->vec_merchant_params['DS_MERCHANT_AMOUNT'] = $this->encode_amount($amount);

        return $this;
    }


    /**
     * Set Order description. It is showed on the TPV backend
     */
    public function set_description($description)
    {
        // Order description
        if ( !empty($description) )
        {
            if ( StringHelper::strlen($description) > 125 )
            {
                $description = StringHelper::substr($description, 0, 124);
            }
            $this->vec_merchant_params['DS_MERCHANT_PRODUCTDESCRIPTION'] = $description;
        }

        return $this;
    }


    /**
     * Get signature version
     */
    public function get_signature_version()
    {
        return $this->get_config('signature_version');
    }


    /**
     * Get Merchant Parameters
     */
    public function get_merchant_parameters()
    {
        // Get data in JSON object
        $merchant_json = Json::encode($this->vec_merchant_params);

        // Debug input
        if ( $this->is_test() )
        {
            Log::redsys_dev($this->vec_merchant_params);
        }

        // Json data encoded in MIME base64
        return base64_encode($merchant_json);
    }


    /**
     * Get signature
     */
    public function get_signature()
    {
        // #1 - Get decoded signature key
        $key = base64_decode($this->get_config('signature_key'));
        if ( $this->is_test() )
        {
            $key = base64_decode($this->get_config('signature_key_test'));
        }

        // #2 - Se diversifica la clave con el Número de Pedido
        $key = $this->encrypt_3DES($this->encoded_order_id, $key);

        // #3 - MAC256 from parameter Ds_MerchantParameters
        $merchant_data = $this->get_merchant_parameters();
        $result = hash_hmac('sha256', $merchant_data, $key, true);//(PHP 5 >= 5.1.2)

        // #4 - Finally, return data enconded in Base64
        return base64_encode($result);
    }


    /**
     * Get encoded amount
     */
    public function get_amount()
    {
        if ( !empty($this->vec_merchant_params) && isset($this->vec_merchant_params['DS_MERCHANT_AMOUNT']) )
        {
            return $this->vec_merchant_params['DS_MERCHANT_AMOUNT'];
        }

        return 0;
    }


    /**
     * Check and process the response received from the TPV
     */
    public function process_response($vec_data)
    {
        if ( !empty($vec_data) && isset($vec_data['Ds_SignatureVersion']) && isset($vec_data['Ds_MerchantParameters']) && isset($vec_data['Ds_Signature']) )
        {
            // Create new signature using signature key
            $signature = null;
            $order_id = null;
            $encoded_order_id = null;
            $encrypted_key = null;
            
            // #1 - Get decoded signature key
            $key = base64_decode($this->get_config('signature_key'));
            if ( $this->is_test() )
            {
                $key = base64_decode($this->get_config('signature_key_test'));
            }

            // #2 - Decode Ds_MerchantParameters
            $merchant_json = base64_decode(strtr($vec_data['Ds_MerchantParameters'], '-_', '+/'));
            $vec_merchant_data = Json::decode($merchant_json);

            // #3 - Se diversifica la clave con el Número de Pedido
            if ( !empty($vec_merchant_data) )
            {
                if ( isset($vec_merchant_data['Ds_Order']) )
                {
                    $encoded_order_id = $vec_merchant_data['Ds_Order'];
                }
                else if ( isset($vec_merchant_data['DS_ORDER']) )
                {
                    $encoded_order_id = $vec_merchant_data['DS_ORDER'];
                }
                else if ( isset($vec_merchant_data['DS_MERCHANT_ORDER']) )
                {
                    $encoded_order_id = $vec_merchant_data['DS_MERCHANT_ORDER'];
                }

                if ( !empty($encoded_order_id) )
                {
                    $encrypted_key = $this->encrypt_3DES($encoded_order_id, $key);
                    $order_id = $this->decode_order_id($encoded_order_id);
                }
            }

            // #4 - MAC256 from parameter Ds_MerchantParameters and get data enconded in Base64
            if ( !empty($encrypted_key) )
            {
                $result = hash_hmac('sha256', $vec_data['Ds_MerchantParameters'], $encrypted_key, true);//(PHP 5 >= 5.1.2)
                $signature = strtr(base64_encode($result), '+/', '-_');
            }

            // #5 - Compare signatures (received and generated)
            if ( !empty($signature) && $signature === $vec_data['Ds_Signature'] )
            {
                Log::redsys("\n#". $order_id ." --> SUCCESS SIGNATURES - Payment received for order ". $order_id ." - Merchant data received ". print_r($vec_merchant_data, true));

                // #6 - Process payment response
                $order_model = Order::findOne($order_id);
                if ( ! $order_model )
                {
                    // Log::redsys_error("\n#". $order_id ." --> ERROR - Order ". $order_id ." cannot be found in database.");
                    $order_error_message = "ERROR - Order ". $order_id ." cannot be found in database.";
                    $this->register_payment_received_order_problem($order_id, $order_error_message, $vec_merchant_data);
                }
                else
                {
                    if ( Yii::app()->checkoutManager->gateway_response($order_model, 'purchase', null, $vec_merchant_data) )
                    {
                        return [
                            'order_id'          => $order_id,
                            'vec_merchant_data' => $vec_merchant_data
                        ];
                    }
                    else
                    {
                        // Log::redsys_error("\n#". $order_id ." --> ERROR - Order ". $order_id ." method RedsysComponent::process_response() has failed.");
                        $order_error_message = "ERROR - Order ". $order_id ." method RedsysComponent::process_response() has failed.";
                        $this->register_payment_received_order_problem($order_id, $order_error_message, $vec_merchant_data);
                    }
                }

                // return TRUE;
            }
            else
            {
                // Log::redsys_error("\n#". $order_id ." --> ERROR SIGNATURES - Payment received for order ". $order_id ." - Merchant data received ". print_r($vec_merchant_data, true));
                $order_error_message = "ERROR SIGNATURES - Payment received for order ". $order_id ." - Merchant data received ". print_r($vec_merchant_data, true);
                $this->register_payment_received_order_problem($order_id, $order_error_message, $vec_merchant_data);
            }
        }

        return false;
    }



    /**
     * WARNING MESSAGE: PROBLEM PROCESSING ORDER WHEN PAYMENT IS RECEIVED
     *
     * Send an email to ADMIN(s) to notify that payment has been received
     * but there is some problem with the order
     *
     * IMPORTANT: Order model probably does not exist in the database
     *
     * @since 08/01/2023
     */
    public function register_payment_received_order_problem($order_id, $error_message, $vec_merchant_data)
    {
        $log_error_message = "\n#". $order_id ." --> ". $error_message;
        if ( !empty($vec_merchant_data) && isset($vec_merchant_data['Ds_Response']) )
        {
            $responde_code = (int)$vec_merchant_data['Ds_Response'];

            // Payment is OK from TPV
            if ( $responde_code === 0 )
            {
                $log_error_message .= '. Payment is ACCEPTED! (ACTION IS NEEDED)';
                Log::redsys_error($log_error_message);
            }

            // Payment code error from TPV
            else
            {
                $log_error_message .= '. Payment was rejected. Error code response from TPV (Ds_Response) = '. $vec_merchant_data['Ds_Response'];
            }
        }

        Log::redsys($log_error_message);
    }


    /**
     * 3DES encrypt function
     */
    public function encrypt_3DES($message, $key)
    {
        /*
        // Se establece un IV por defecto
        $bytes = [0,0,0,0,0,0,0,0]; //byte [] IV = {0, 0, 0, 0, 0, 0, 0, 0}
        $iv = implode(array_map("chr", $bytes)); //PHP 4 >= 4.0.2

        // Se cifra
        return mcrypt_encrypt(MCRYPT_3DES, $key, $message, MCRYPT_MODE_CBC, $iv); //PHP 4 >= 4.0.2
        */

        $l = ceil(strlen($message) / 8) * 8;
        return substr(openssl_encrypt($message . str_repeat("\0", $l - strlen($message)), 'des-ede3-cbc', $key, OPENSSL_RAW_DATA, "\0\0\0\0\0\0\0\0"), 0, $l);
    }


    /**
     * Encode order_id. We need this id is unique every time is proceed from TPV
     * 
     * Ex.: order_id = 209 will be encoded to dz209-3
     */
    public function encode_order_id($order_id, $transaction_num = 0)
    {
        if ( empty($this->encoded_order_id) )
        {
            $this->encoded_order_id = $this->get_encoded_order($order_id, $transaction_num);
        }
        return $this->encoded_order_id;
    }


    /**
     * Return a encoded order
     * 
     * Ex.: order_id = 209 will be encoded to dz209-3
     */
    public function get_encoded_order($order_id, $transaction_num = 0)
    {
        return $this->get_config('order_prefix') . $order_id .'-'. $transaction_num;
    }


    /**
     * Decode order_id received from TPV in DS_Order parameter
     * 
     * Ex.: dz209-3 will be decoded to 209
     */
    public function decode_order_id($encoded_order_id)
    {
        // Remove prefix. From "dz209-3" to "209-3"
        $order_id = str_replace($this->get_config('order_prefix'), '', $encoded_order_id);
        
        // Get order_id and transaction_id
        $vec_order = explode("-", $order_id);

        // $vec_order = [ 0 => 'order_id', 1 => 'transaction_id' ]
        if ( !empty($vec_order) && is_array($vec_order) && count($vec_order) === 2 )
        {
            return $vec_order[0];
        }

        return $order_id;
    }


    /**
     * Encode amount with TPV format (value * 100)
     */
    public function encode_amount($amount)
    {
        return intval(strval($amount * 100));
    }


    /**
     * Get TPV response in string format
     */
    public function get_response_label($response_code)
    {
        // Load responde code table
        $vec_response = [
            // CODIGOS DE RESPUESTA PARA INDICAR QUE LA TRANSACCION HA SIDO APROBADA 
            0       => 'TRANSACCION APROBADA',
            1       => 'TRANSACCION APROBADA PREVIA IDENTIFICACION DE TITULAR',
            2       => 'TRANSACCION APROBADA',
            900     => 'TRANSACCIÓN APROBADA',

            // CODIGOS DE RESPUESTA PARA INDICAR QUE LA TRANSACCION HA SIDO DENEGADA 
            101     => 'TARJETA CADUCADA',
            102     => 'TARJETA BLOQUEDA TRANSITORIAMENTE O BAJO SOSPECHA DE FRAUDE',
            104     => 'OPERACIÓN NO PERMITIDA',
            106     => 'NUM. INTENTOS EXCEDIDO',
            107     => 'CONTACTAR CON EL EMISOR',
            109     => 'IDENTIFICACIÓN INVALIDA DEL COMERCIO O TERMINAL',
            110     => 'IMPORTE INVALIDO',
            114     => 'TARJETA NO SOPORTA EL TIPO DE OPERACIÓN SOLICITADO',
            116     => 'DISPONIBLE INSUFICIENTE',
            118     => 'TARJETA NO REGISTRADA',
            119     => 'DENEGACION SIN ESPECIFICAR MOTIVO',
            125     => 'TARJETA NO EFECTIVA',
            129     => 'ERROR CVV2/CVC2',
            167     => 'CONTACTAR CON EL EMISOR: SOSPECHA DE FRAUDE',
            180     => 'TARJETA AJENA AL SERVICIO',
            181     => 'TARJETA CON RESTRICCIONES DE DEBITO O CREDITO',
            182     => 'TARJETA CON RESTRICCIONES DE DEBITO O CREDITO',
            184     => 'ERROR EN AUTENTICACION',
            190     => 'DENEGACION SIN ESPECIFICAR EL MOTIVO',
            191     => 'FECHA DE CADUCIDAD ERRONEA',

            // CODIGOS DE RESPUESTA PARA INDICAR QUE LA TRANSACCION HA SIDO DENEGADA 
            // Y QUE, ADEMAS, EL BANCO EMISOR CONSIDERA QUE LA TARJETA ESTA EN UNA SITUACION
            // DE POSIBLE FRAUDE Y, POR ELLO, SOLICITA DE RETENERLA FISICAMENTE O
            // DE ACTIVARLA VIRTUALMENTE EN “LISTAS NEGRAS”
            201     => 'TARJETA CADUCADA',
            202     => 'TARJETA BLOQUEDA TRANSITORIAMENTE O BAJO SOSPECHA DE FRAUDE',
            204     => 'OPERACION NO PERMITIDA',
            207     => 'CONTACTAR CON EL EMISOR',
            208     => 'TARJETA PERDIDA O ROBADA',
            209     => 'TARJETA PERDIDA O ROBADA',
            280     => 'ERROR CVV2/CVC2',
            290     => 'DENEGACION SIN ESPECIFICAR EL MOTIVO',

            // CODIGOS DE RESPUESTA REFERIDOS A ANULACION O RETROCESION PARCIAL
            // (Ds_Merchant_TransactionType = 3) 
            400     => 'ANULACION ACEPTADA',
            480     => 'NO ENCONTRADA LA OPERACIÓN ORIGINAL O TIME-OUT EXCEDIDO',
            481     => 'ANULACION ACEPTADA',

            // CODIGOS DE RESPUESTA REFERIDOS A CONCILIACIONES DE PRE-AUTORIZACIONES O PRE-AUTENTICACIONES
            // (Ds_Merchant_TransactionType = 2 o 8) 
            500     => 'CONCILIACION ACEPTADA',
            501     => 'NO ENCONTRADA LA OPERACION ORIGINAL O TIME-OUT EXCEDIDO',
            502     => 'NO ENCONTRADA LA OPERACION ORIGINAL O TIME-OUT EXCEDIDO',
            503     => 'NO ENCONTRADA LA OPERACION ORIGINAL O TIME-OUT EXCEDIDO',

            // CODIGOS DE RESPUESTA REFERIDOS A CONCILIACIONES DE PRE-AUTORITZACIONES DIFERIDAS O
            // PRE-AUTORITZACIONES DIFERIDAS RECURRENTES (Ds_Merchant_TransactionType = O o R) 
            9928    => 'ANULACIÓN DE PREAUTORITZACIÓN REALIZADA POR EL SISTEMA',
            9929    => 'ANULACIÓN DE PREAUTORITZACIÓN REALIZADA POR EL COMERCIO',

            // CODIGOS DE RESPUESTA PARA INDICAR QUE LA TRANSACCION HA SIDO RECHAZADA
            // POR LA PLATAFORMA DE PAGOS DE BBVA 
            904     => 'COMERCIO NO REGISTRADO EN EL FUC',
            909     => 'ERROR DE SISTEMA',
            912     => 'EMISOR NO DISPONIBLE',
            913     => 'TRANSMISION DUPLICADA',
            916     => 'IMPORTE DEMASIADO PEQUEÑO',
            928     => 'TIME-OUT EXCEDIDO',
            940     => 'TRANSACCION ANULADA ANTERIORMENTE',
            941     => 'TRANSACCION DE AUTORIZACION YA ANULADA POR UNA ANULACION ANTERIOR',
            942     => 'TRANSACCION DE AUTORIZACION ORIGINAL DENEGADA',
            943     => 'DATOS DE LA TRANSACCION ORIGINAL DISTINTOS',
            944     => 'SESION ERRONEA',
            945     => 'TRANSMISION DUPLICADA',
            946     => 'OPERACION A ANULAR EN PROCESO',
            947     => 'TRANSMISION DUPLICADA EN PROCESO',
            949     => 'TERMINAL INOPERATIVO',
            950     => 'DEVOLUCION NO PERMITIDA',
            965     => 'VIOLACIÓN NORMATIVA',
            9064    => 'LONGITUD TARJETA INCORRECTA',
            9078    => 'NO EXISTE METODO DE PAGO',
            9093    => 'TARJETA NO EXISTE',
            9094    => 'DENEGACION DE LOS EMISORES',
            9104    => 'OPER. SEGURA NO ES POSIBLE',
            9126    => 'OPERACIÓN DUPLICADA',
            9142    => 'TIEMPO DE PAGO EXCEDIDO',
            9216    => 'CODIGO CVV ERRONEO',
            9218    => 'NO SE PUEDEN HACER OPERACIONES',
            9253    => 'SEGURAS CHECK-DIGIT ERRONEO',
            9256    => 'PREAUTORITZACION NO HABILITADA EN COMERCI',
            9257    => 'PREAUT. NO HABILITADA PARA LA TARGETA',
            9261    => 'OPERACIÓN SUPERA ALERTA BBVA',
            9282    => 'OPERACIÓN SUPERA ALERTA BBVA',
            9281    => 'SUPERA ALERTAS BLOQUEANTES',
            9283    => 'SUPERA ALERTAS BLOQUANTES',
            9334    => 'OPERACIÓN SUPERA ALERTA BBVA',
            9454    => 'METODO DE PAGO NO DISPONIBLE',
            9456    => 'METODO DE PAGO NO DISPONIBLE',
            9912    => 'EMISOR NO DISPONIBLE',
            9913    => 'ERROR EN CONFIRMACION',
            9914    => 'CONFIRMACION “KO”',
            9915    => 'PAGO CANCELADO',
            9928    => 'AUTORIZACIÓN EN DIFERIDO ANULADA',
            9929    => 'AUTORIZACIÓN EN DIFERIDO ANULADA',
            9997    => 'DOBLE OPERACIÓN SIMULTANEA',
            9998    => 'ESTADO OPERACIÓN: SOLICITADA',
            9999    => 'ESTADO OPERACIÓN: AUTENTICANDO',
        ];

        // Check if response code exists
        $response_code = (int)$response_code;

        // Transaction between 2 and 99 means "Transacción autorizada por el banco emisor."
        if ( $response_code >= 2 && $response_code <= 99 )
        {
            $response_code = 2;
        }

        return isset($vec_response[$response_code]) ? $vec_response[$response_code] : '';
    }
}
