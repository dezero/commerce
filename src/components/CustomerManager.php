<?php
/**
 * CustomerManager
 * 
 * Component to manage customers
 */

namespace dzlab\commerce\components;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\Customer;
use user\models\User;
use Yii;

class CustomerManager extends ApplicationComponent
{
    /**
     * Customer prefix for user models (username attribute)
     */
    public $customer_prefix = 'customer_';


    /**
     * Anonymous prefix for user models
     */
    public $anonymous_prefix = 'guest_';


    /**
     * @var string Login URL
     */
    public $login_url = '/account/login';


    /**
     * @var string Register URL
     */
    public $register_url = '/account/register';


    /**
     * @var string Reset password URL
     */
    public $recovery_url = '/account/password';


    /**
     * @var string Change password URL
     */
    public $change_password_url = '/account/password/change';


    /**
     * @var string Return URL after Login
     */
    public $after_login_url = '/';


    /**
     * @var string Return URL after Logout
     */
    public $after_logout_url = '/';


    /**
     * Create new User model
     */
    public function create_user_model($email)
    {
        // Create new User
        $user_model = Yii::createObject(User::class);
        $now = time();
        $user_model->setAttributes([
            'username'          => Yii::app()->customerManager->customer_prefix . $now,
            'password'          => 'anonymous-'. $now,
            'verify_password'   => 'anonymous-'. $now,
        ]);
        $user_model->verification_code = StringHelper::encrypt(microtime() . $user_model->password);

        // Email unique
        $user_model->email = $user_model->username .'---'. $email;

        if ( ! $user_model->validate() )
        {
            Log::save_model_error($user_model);
        }
        else
        {
            // Encrypt password
            $user_model->password = StringHelper::encrypt($user_model->password);
            $user_model->save(false);

             // Add user to customer ROLE
            $user_model->add_role('customer');

            // Create Customer model
            $customer_model = Yii::createObject(Customer::class);
            $customer_model->user_id = $user_model->id;
            $customer_model->email = $email;
            if ( ! $customer_model->save() )
            {
                Log::save_model_error($customer_model);
            }
        }

        return $user_model;
    }


    /**
     * Check if an user has "Customer" role
     */
    public function is_customer($user_id = null)
    {
        if ( $user_id === null )
        {
            $user_id = Yii::app()->user->id;
        }

        $user_model = User::findOne($user_id);
        if ( $user_model )
        {
            return $user_model->isCustomer();
        }

        return false;
    }


    /**
     * Check if user is a "Customer" registered as GUEST (anonymous)
     */
    public function is_guest($user_id = null)
    {
        if ( $user_id === null )
        {
            $user_id = Yii::app()->user->id;
        }

        $user_model = User::findOne($user_id);
        if ( $user_model )
        {
            return preg_match("/". Yii::app()->customerManager->anonymous_prefix ."\_/", $user_model->username);
        }

        return false;
    }


    /**
     * Get "status_type" labels
     */
    public function status_labels()
    {
        return Customer::model()->status_type_labels();
    }


    /**
     * Get total number of customer grouped by status type
     */
    public function total_by_status()
    {
        // Get existing statuses
        $vec_totals = [
            'active'        => 0,
            'disabled'      => 0,
            'banned'        => 0,
            'pending'       => 0,
            'newsletter'    => 0
        ];

        $customer_table = Customer::model()->tableName();
        $user_table = User::model()->tableName();

        // Total vy status
        $vec_results = Yii::app()->db->createCommand("SELECT COUNT(*) AS `total_customers`, user.`status_type` AS status_type FROM {$customer_table} customer INNER JOIN {$user_table} user ON customer.user_id = user.id GROUP BY user.`status_type` ORDER BY user.`status_type`")->queryAll();
        if ( !empty($vec_results) )
        {
            foreach ( $vec_results as $que_result )
            {
                $vec_totals[$que_result['status_type']] = $que_result['total_customers'];
            }
        }

        // Newsletter
        $vec_totals['newsletter'] = Yii::app()->db->createCommand("SELECT COUNT(*) FROM `{$customer_table}` t WHERE t.is_newsletter = 1")->queryScalar();

        return $vec_totals;
    }


    /**
     * Called after customer has logged into the platform
     */
    public function afterLogin($user_model, $is_checkout = false)
    {
        $user_model = User::model()->findByUsername($user_model->username);
        if ( $user_model )
        {
            if ( ! $user_model->isCustomer() )
            {
                return Yii::app()->userManager->afterLogin($user_model);
            }

            // Register login access in database
            $user_model->saveAttributes([
                'last_login_date' => time(),
                'last_login_ip' => Yii::app()->request->getUserIP()
            ]);

            // Register login access in log files
            if ( Yii::app()->userManager->registerLoginAccess )
            {
                Log::login_access('User '. $user_model->id . ' - '. $user_model->username .' ('. $user_model->email .') logged in from IP '. Yii::app()->request->getUserIP());
            }

            Yii::app()->cartManager->afterLogin($user_model, $is_checkout);

            // Final redirect
            if ( ! $is_checkout )
            {
                // Welcome message
                $success_message = Yii::t('frontend', 'Bienvenido a '. Yii::app()->name);
                Yii::app()->user->addFlash('success', $success_message);

                // Force change password?
                if ( $user_model->is_force_change_password == 1 )
                {
                    Yii::app()->controller->redirect([Yii::app()->customerManager->change_password_url]);
                }
                else
                {
                    Yii::app()->controller->redirect([Yii::app()->customerManager->after_login_url]);
                }
            }
            else
            {
                Yii::app()->controller->redirect(['/checkout']);
            }
        }

        return false;
    }


    /**
     * After password change
     * 
     * Called from PasswordController::actionIndex() method
     * 
     * Redirect the user after password is changed
     */
    public function afterChangePassword($user_model)
    {
        if ( ! $user_model->isCustomer() )
        {
            return Yii::app()->userManager->afterChangePassword($user_model);
        }
        
        Yii::app()->controller->redirect([Yii::app()->customerManager->after_login_url]);
    }


    /**
     * Return customer email
     */
    public function get_customer_email($user_model = null)
    {
        if ( $user_model === null )
        {
            $user_model = Yii::app()->user->model();
        }

        if ( $user_model )
        {
            return $user_model->customer ? $user_model->customer->email : $user_model->get_email();
        }

        return false;
    }
}