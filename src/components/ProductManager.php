<?php
/**
 * ProductManager
 * 
 * Component to manage products
 */

namespace dzlab\commerce\components;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\App;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetImage;
use dz\modules\category\models\Category;
use dzlab\commerce\models\Price;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\ProductCategory;
use dzlab\commerce\models\ProductPrice;
use dzlab\commerce\models\TranslatedProduct;
use dzlab\commerce\models\Variant;
use dz\modules\web\models\Seo;
use Yii;

class ProductManager extends ApplicationComponent
{
    /**
     * Input data
     */
    protected $vec_input_data;


    /**
     * Some error detected?
     */
    public $is_error = false;


    /**
     * Array with prices
     */
    protected $vec_prices;


    /**
     * CREATE action for Product model
     * 
     * @see BaseProductController::actionCreate()
     */
    public function create_product($product_model, $vec_input_data)
    {
        // Product type configuration
        $vec_config = $product_model->get_config();

        // Save input data
        $this->vec_input_data = $vec_input_data;

        // Default language
        $default_language = Yii::defaultLanguage();

        // Variant model
        $variant_model = Yii::createObject(Variant::class);
        $variant_model->is_default = 1;

        // Image file
        $image_model = Yii::createObject(AssetImage::class);
        $image_model->setAttributes([
            'entity_type'   => 'Product'
        ]);

        // Main category
        $main_category_model = Yii::createObject(ProductCategory::class);
        $main_category_model->category_type = $vec_config['main_category_type'];

        // Other categories
        $product_category_model = Yii::createObject(ProductCategory::class);
        $product_category_model->category_type = $vec_config['main_category_type'];
        $vec_product_category_models = [$product_category_model];

        // Price models
        $vec_price_models = [];

        // Base price
        $product_price_model = Yii::createObject(ProductPrice::class);
        $product_price_model->setAttributes([
            'price_id'      => 1,
            'price_alias'   => 'base_price'
        ]);
        $vec_price_models['base_price'] = $product_price_model;

        // Reduced price
        $product_price_model = Yii::createObject(ProductPrice::class);
        $product_price_model->setAttributes([
            'price_id'      => 2,
            'price_alias'   => 'reduced_price'
        ]);
        $vec_price_models['reduced_price'] = $product_price_model;

        // Translations
        $vec_translated_models = [];
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();
        if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_extra_languages) )
        {
            foreach ( $vec_extra_languages as $language_id )
            {
                $translated_product_model = Yii::createObject(TranslatedProduct::class);
                $translated_product_model->language_id = $language_id;
                $vec_translated_models[$language_id] = $translated_product_model;
            }
        }

        // SEO fields
        $vec_seo_models = [];
        if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] )
        {
            // First, add SEO for default language
            $seo_model = Yii::createObject(Seo::class);
            $seo_model->setAttributes([
                'entity_type'   => 'product',
                'language_id'   => $default_language
            ]);
            $vec_seo_models[$default_language] = $seo_model;

            // Add SEO for more languages
            /*
            if ( !empty($vec_extra_languages) )
            {
                foreach ( $vec_extra_languages as $language_id )
                {
                    $seo_model = Yii::createObject(Seo::class);
                    $seo_model->setAttributes([
                        'entity_type'   => 'product',
                        'language_id'   => $language_id
                    ]);
                    $vec_seo_models[$language_id] = $seo_model;
                }
            }
            */
        }

        // Create new Product?
        if ( isset($vec_input_data['Product']) && isset($vec_input_data['Variant']) )
        {
            $product_model->setAttributes($vec_input_data['Product']);
            $variant_model->setAttributes($vec_input_data['Variant']);

            // Custom trigger "before_save_product" method
            $product_model = $this->before_save_product($product_model, $variant_model, 'create', $vec_input_data);

            // Save product
            if ( $product_model->save() )
            {                
                // Save VARIANT model
                $variant_model->product_id = $product_model->product_id;
                if ( $variant_model->save() )
                {
                    // Reload product and ensure default Variant exists
                    $product_model->add_default_variant($variant_model);
                    $product_model = Product::model()->cache(0)->findByPk($product_model->product_id);

                    // Image
                    $product_model = $this->save_product($product_model, $variant_model, 'image', $image_model, $vec_input_data);

                    // Categories
                    $product_model = $this->save_product($product_model, $variant_model, 'categories', null, $vec_input_data);

                    // Prices
                    $product_model = $this->save_product($product_model, $variant_model, 'price', $vec_price_models, $vec_input_data);

                    // Translations
                    $product_model = $this->save_product($product_model, $variant_model, 'translation', $vec_translated_models, $vec_input_data);

                    // SEO
                    $product_model = $this->save_product($product_model, $variant_model, 'seo', $vec_seo_models, $vec_input_data);

                    // Save data from default variant & trigger custom "after_save_product" method
                    if ( $product_model->save() )
                    {
                        $this->after_save_product($product_model, $variant_model, 'create', $vec_input_data);
                    }
                }
            }

            else
            {
                // Set "error" detected
                $this->is_error = true;
            }


            // Some error(s) detected
            if ( $this->is_error )
            {
                $product_model->afterFind(new \CEvent($product_model));
                $variant_model->afterFind(new \CEvent($variant_model));

                // IMAGE - Try to save file as TEMPORARY
                if ( isset($vec_config['is_image']) && $vec_config['is_image'] )
                {
                    $temp_destination_path = $image_model->getTempPath() . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR;
                    $image_model = $product_model->upload_temp_image('default_image_id', $temp_destination_path);
                    if ( ! $image_model )
                    {
                        $image_model = Yii::createObject(AssetImage::class);
                        $image_model->setAttributes([
                            'entity_type'   => 'Product'
                        ]);
                    }
                }

                // Recover ProductCategory data
                if ( isset($vec_input_data['ProductCategory']) )
                {                    
                    $vec_product_category_models = $vec_input_data['ProductCategory'];
                }

                // Recover ProductPrice data
                if ( isset($vec_input_data['ProductPrice']) )
                {
                    if ( isset($vec_input_data['ProductPrice']['base_price']) )
                    {
                        $vec_price_models['base_price']->setAttributes($vec_input_data['ProductPrice']['base_price']);
                    }

                    if ( isset($vec_input_data['ProductPrice']['reduced_price']) )
                    {
                        $vec_price_models['reduced_price']->setAttributes($vec_input_data['ProductPrice']['reduced_price']);
                    }
                }

                // Recover SEO data
                if ( isset($vec_input_data['Seo']) )
                {
                    foreach ( $vec_input_data['Seo'] as $language_id => $vec_seo_data )
                    {
                        if ( isset($vec_seo_models[$language_id]) )
                        {
                            $vec_seo_models[$language_id]->setAttributes($vec_seo_data);
                        }
                    }
                }
            }
        }

        return [
            'product_model'                 => $product_model,
            'variant_model'                 => $variant_model,
            'image_model'                   => $image_model,
            'main_category_model'           => $main_category_model,
            'vec_config'                    => $vec_config,
            'vec_product_category_models'   => $vec_product_category_models,
            'vec_price_models'              => $vec_price_models,
            'vec_translated_models'         => $vec_translated_models,
            'vec_seo_models'                => $vec_seo_models,
            'default_language'              => $default_language,
            'vec_extra_languages'           => $vec_extra_languages,
            'form_id'                       => 'product-'. $product_model->product_type .'-form'
        ];
    }


    /**
     * UPDATE action for Product model
     * 
     * @see BaseProductController::actionUpdate()
     */
    public function update_product($product_model, $vec_input_data)
    {
        // Product type configuration
        $vec_config = $product_model->get_config();

        // Save input data
        $this->vec_input_data = $vec_input_data;

        // Default language
        $default_language = Yii::defaultLanguage();

        // Default Variant model
        $variant_model = $product_model->defaultVariant;

        // Choose a default variant
        if ( ! $variant_model )
        {
            // Select the first enabled Variant model
            if ( $product_model->variants )
            {
                foreach ( $product_model->variants as $que_variant_model )
                {
                    if ( ! $variant_model && $que_variant_model->is_enabled() )
                    {
                        $variant_model = $que_variant_model;
                        $variant_model->is_default = 1;
                    }
                }
            }
            
            // Create a new Default Variant model
            if ( ! $variant_model )
            {
                $variant_model = Yii::createObject(Variant::class);
                $variant_model->product_id = $product_model->product_id;
                $variant_model->is_default = 1;
            }


            if ( isset($vec_input_data['Variant']) )
            {
                $variant_model->setAttributes($vec_input_data['Variant']);
            }
            if ( ! $variant_model->save() )
            {
                Log::save_model_error($variant_model);
            }
            else
            {
                $product_model->defaultVariant = $variant_model;
            }
        }

        // Image file
        $image_model = $product_model->defaultImage;
        if ( ! $image_model )
        {
            $image_model = Yii::createObject(AssetImage::class);
            $image_model->setAttributes([
                'entity_type'   => 'Product',
                'entity_id'     => $product_model->product_id
            ]);
        }

        // Main category
        $main_category_model = $product_model->mainCategory;
        if ( ! $main_category_model )
        {
            $main_category_model = Yii::createObject(ProductCategory::class);
            $main_category_model->product_id = $product_model->product_id;
            $main_category_model->category_type = $vec_config['main_category_type'];
        }

        // Other categories
        $vec_product_category_models = $product_model->otherCategories;
        if ( empty($vec_product_category_models) )
        {
            $product_category_model = Yii::createObject(ProductCategory::class);
            $product_category_model->product_id = $product_model->product_id;
            $product_category_model->category_type = $vec_config['main_category_type'];
            $vec_product_category_models = [$product_category_model];
        }

        // Price models
        $vec_price_models = $product_model->get_price_models();

        // Base price
        if ( ! isset($vec_price_models['base_price']) )
        {
            $product_price_model = Yii::createObject(ProductPrice::class);
            $product_price_model->setAttributes([
                'price_id'      => 1,
                'price_alias'   => 'base_price',
                'product_id'    => $product_model->product_id
            ]);
            $vec_price_models['base_price'] = $product_price_model;
        }

        // Reduced price
        if ( ! isset($vec_price_models['reduced_price']) )
        {
            $product_price_model = Yii::createObject(ProductPrice::class);
            $product_price_model->setAttributes([
                'price_id'      => 2,
                'price_alias'   => 'reduced_price',
                'product_id'    => $product_model->product_id
            ]);
            $vec_price_models['reduced_price'] = $product_price_model;
        }

        // Translations
        $vec_translated_models = [];
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();
        if ( !empty($vec_extra_languages) && isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] )
        {
            foreach ( $vec_extra_languages as $language_id )
            {
                $translated_product_model = TranslatedProduct::get()
                    ->where([
                        'language_id'   => $language_id,
                        'product_id'    => $product_model->product_id
                    ])
                    ->one();

                if ( ! $translated_product_model )
                {
                    $translated_product_model = Yii::createObject(TranslatedProduct::class);
                    $translated_product_model->language_id = $language_id;
                    $translated_product_model->product_id = $product_model->product_id;
                }
                $vec_translated_models[$language_id] = $translated_product_model;
            }
        }

        // SEO fields
        $vec_seo_models = [];
        if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] )
        {
            $vec_seo_models = $product_model->seo;
            if ( empty($vec_seo_models) ) 
            {
                // First, add SEO for default language
                $vec_seo_attributes = [
                    'entity_id'     => $product_model->product_id,
                    'entity_type'   => 'product',
                    'language_id'   => $default_language
                ];
                $seo_model = Yii::createObject(Seo::class);
                $seo_model->setAttributes($vec_seo_attributes);
                $vec_seo_models[$default_language] = $seo_model;

                // Add SEO for more languages
                if ( !empty($vec_extra_languages) )
                {
                    foreach ( $vec_extra_languages as $language_id )
                    {
                        $vec_seo_attributes['language_id'] = $language_id;
                        $seo_model = Yii::createObject(Seo::class);
                        $seo_model->setAttributes($vec_seo_attributes);
                        $vec_seo_models[$language_id] = $seo_model;
                    }
                }
            }
            else
            {
                // Group SEO models by language
                $vec_seo_language_models = [];
                foreach ( $vec_seo_models as $seo_model )
                {
                    $vec_seo_language_models[$seo_model->language_id] = $seo_model;
                }

                // Check if there's some language with no SEO model
                if ( !empty($vec_extra_languages) )
                {
                    foreach ( $vec_extra_languages as $language_id )
                    {
                        if ( !isset($vec_seo_language_models[$language_id]) )
                        {
                            $seo_model = Yii::createObject(Seo::class);
                            $seo_model->setAttributes([
                                'entity_id'     => $product_model->product_id,
                                'entity_type'   => 'product',
                                'language_id'   => $language_id
                            ]);
                            $vec_seo_language_models[$language_id] = $seo_model;
                        }
                    }
                }
                
                $vec_seo_models = $vec_seo_language_models;
            }
        }


        // Update Product?
        if ( isset($vec_input_data['Product']) )
        {
            $product_model->setAttributes($vec_input_data['Product']);

            // Custom trigger "before_save_product" method
            $product_model = $this->before_save_product($product_model, $variant_model, 'update', $vec_input_data);

            // Save product
            if ( $product_model->save() )
            {
                if ( isset($vec_input_data['Variant']) )
                {
                    $variant_model->setAttributes($vec_input_data['Variant']);
                    if ( ! $variant_model->save() )
                    {
                        Log::save_model_error($variant_model);
                    }
                }

                // Reload product and ensure default Variant exists
                if ( empty($product_model->default_variant_id) )
                {
                    $product_model->add_default_variant($variant_model);
                    $product_model = Product::model()->cache(0)->findByPk($product_model->product_id);
                }

                // Image
                $product_model = $this->save_product($product_model, $variant_model, 'image', $image_model, $vec_input_data);

                // Categories
                $product_model = $this->save_product($product_model, $variant_model, 'categories', null, $vec_input_data);

                // Prices
                $product_model = $this->save_product($product_model, $variant_model, 'price', $vec_price_models, $vec_input_data);

                // Translations
                $product_model = $this->save_product($product_model, $variant_model, 'translation', $vec_translated_models, $vec_input_data);

                // SEO
                $product_model = $this->save_product($product_model, $variant_model, 'seo', $vec_seo_models, $vec_input_data);

                // Save data from default variant & trigger custom "after_save_product" method
                if ( ! $this->is_error && $product_model->save() )
                {
                    $this->after_save_product($product_model, $variant_model, 'update', $vec_input_data);
                }
                else
                {
                    // Set "error" detected
                    $this->is_error = true;       
                }
            }
            else
            {
                // Set "error" detected
                $this->is_error = true;
            }


            // Some error(s) detected
            if ( $this->is_error )
            {
                $product_model->afterFind(new \CEvent($product_model));
                $variant_model->afterFind(new \CEvent($variant_model));

                // IMAGE - Try to save file as TEMPORARY
                if ( isset($vec_config['is_image']) && $vec_config['is_image'] )
                {
                    $temp_destination_path = $image_model->getTempPath() . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR;
                    $image_model = $product_model->upload_temp_image('default_image_id', $temp_destination_path);
                    if ( ! $image_model )
                    {
                        $image_model = Yii::createObject(AssetImage::class);
                        $image_model->setAttributes([
                            'entity_type'   => 'Product',
                            'entity_id'     => $product_model->product_id
                        ]);
                    }
                }

                // Recover ProductCategory data
                if ( isset($vec_input_data['ProductCategory']) )
                {                    
                    $vec_product_category_models = $vec_input_data['ProductCategory'];
                }

                // Recover ProductPrice data
                if ( isset($vec_input_data['ProductPrice']) )
                {
                    if ( isset($vec_input_data['ProductPrice']['base_price']) )
                    {
                        $vec_price_models['base_price']->setAttributes($vec_input_data['ProductPrice']['base_price']);
                    }

                    if ( isset($vec_input_data['ProductPrice']['reduced_price']) )
                    {
                        $vec_price_models['reduced_price']->setAttributes($vec_input_data['ProductPrice']['reduced_price']);
                    }
                }

                // Recover SEO data
                if ( isset($vec_input_data['Seo']) )
                {
                    foreach ( $vec_input_data['Seo'] as $language_id => $seo_values )
                    {
                        if ( isset($vec_seo_models[$language_id]) && isset($seo_values['meta_title']) )
                        {
                            $vec_seo_models[$language_id]->setAttributes($seo_values);
                            if ( ! $vec_seo_models[$language_id]->save() )
                            {
                                Log::save_model_error($vec_seo_models[$language_id]);
                            }
                        }
                    }
                }
            }
        }

        return [
            'product_model'                 => $product_model,
            'variant_model'                 => $variant_model,
            'image_model'                   => $image_model,
            'main_category_model'           => $main_category_model,
            'vec_config'                    => $vec_config,
            'vec_product_category_models'   => $vec_product_category_models,
            'vec_price_models'              => $vec_price_models,
            'vec_translated_models'         => $vec_translated_models,
            'vec_seo_models'                => $vec_seo_models,
            'default_language'              => $default_language,
            'vec_extra_languages'           => $vec_extra_languages,
            'form_id'                       => 'product-'. $product_model->product_type .'-form'
        ];
    }


    /**
     * Save entities related with a Product: image, categories, prices, translations or seo
     */
    public function save_product($product_model, $variant_model, $entity_type, $entity_data = null, $vec_input_data = [])
    {
        $is_save_error = false;

        // Get input data
        if ( empty($vec_input_data) )
        {
            $vec_input_data = $this->vec_input_data;
        }

        // Product type configuration
        $vec_config = $product_model->get_config();

        switch ( $entity_type )
        {
            // Image
            case 'image':
                // if ( isset($vec_config['is_image']) && $vec_config['is_image'] && isset($_POST['AssetImage']) )
                if ( isset($vec_config['is_image']) && $vec_config['is_image'] )
                {
                    if ( ! $product_model->add_default_image($entity_data) )
                    {
                        $is_save_error = true;
                    }
                }
            break;

            // Categories
            case 'categories';
                $vec_product_category_data = [];
                if ( !empty($product_model->main_category_id) )
                {
                    $vec_product_category_data[] = $product_model->main_category_id;
                }
                if ( !empty($product_model->brand_category_id) )
                {
                    $vec_product_category_data[] = $product_model->brand_category_id;
                }
                if ( isset($vec_input_data['ProductCategory']) )
                {
                    foreach ( $vec_input_data['ProductCategory'] as $que_category_id )
                    {
                        $vec_product_category_data[] = $que_category_id;
                    }
                }

                if ( ! $product_model->save_categories($vec_product_category_data) )
                {
                    $is_save_error = true;
                }
            break;

            // Price
            case 'price':
                $vec_price_models = $entity_data;
                if ( isset($vec_input_data['ProductPrice']) )
                {
                    foreach ( $vec_input_data['ProductPrice'] as $price_alias => $vec_price_attributes )
                    {
                        if ( isset($vec_price_models[$price_alias]) )
                        {
                            $product_price_model = $vec_price_models[$price_alias];
                            $vec_price_attributes['product_id'] = $variant_model->product_id;
                            $vec_price_attributes['variant_id'] = $variant_model->variant_id;
                            $product_price_model->setAttributes($vec_price_attributes);
                            if ( ! $product_model->save_price($product_price_model) )
                            {
                                $is_save_error = true;
                            }
                        }
                    }
                }
            break;

            // Translation
            case 'translation':
                $vec_translated_models = $entity_data;
                if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_translated_models) && isset($vec_input_data['TranslatedProduct']) )
                {
                    foreach ( $vec_input_data['TranslatedProduct'] as $language_id => $vec_translated_product_attributes )
                    {
                        if ( isset($vec_translated_models[$language_id]) && isset($vec_translated_product_attributes['name']) )
                        {
                            $translated_product_model = $vec_translated_models[$language_id];
                            $vec_translated_product_attributes['language_id'] = $language_id;
                            $vec_translated_product_attributes['product_id'] = $product_model->product_id;
                            $translated_product_model->setAttributes($vec_translated_product_attributes);
                            if ( ! $product_model->save_translation($translated_product_model) )
                            {
                                $is_save_error = true;
                            }
                        }
                    }
                }
            break;

            // SEO
            case 'seo':
                $vec_seo_models = $entity_data;
                if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] && !empty($vec_seo_models) && isset($vec_input_data['Seo']) )
                {
                    foreach ( $vec_input_data['Seo'] as $language_id => $seo_values )
                    {
                        if ( isset($vec_seo_models[$language_id]) && isset($seo_values['meta_title']) )
                        {
                            $seo_model = $vec_seo_models[$language_id];
                            $seo_model->setAttributes($seo_values);
                            $seo_model->entity_id = $product_model->product_id;
                            if ( ! $product_model->save_seo($seo_model) )
                            {
                                $is_save_error = true;
                            }
                        }
                    }
                }
            break;
        }

        // Error detected?
        if ( $is_save_error )
        {
            $this->is_error = true;
            $vec_errors = Log::errors_list();
            foreach ( $vec_errors as $que_error )
            {
                $product_model->addError('product_id', $que_error);
            }
        }

        return $product_model;
    }


    /**
     * This method is invoked before a Product is going to be saved via form submitting
     */
    public function before_save_product($product_model, $variant_model, $action = 'update', $vec_input_data)
    {
        return $product_model;
    }


    /**
     * This method is invoked after a Product is saved via form submitting
     */
    public function after_save_product($product_model, $variant_model, $action = 'update', $vec_input_data)
    {
        if ( ! App::is_console() )
        {
            // Product configuration for current product type
            $vec_config = $product_model->get_config();

            if ( $action == 'create' )
            {
                Yii::app()->user->addFlash('success', $product_model->text('created_success'));

                // Redirect to update page
                Yii::app()->controller->redirect(['update', 'id' => $product_model->product_id]);
                // Yii::app()->controller->redirect(['index']);
            }

            else if ( $action == 'update' )
            {
                // Has an action been triggered?
                if ( isset($vec_input_data['StatusChange']) && !empty($vec_input_data['StatusChange']) )
                {
                    switch ( $vec_input_data['StatusChange'] )
                    {
                        case 'disable':
                            if ( $product_model->disable() )
                            {
                                Yii::app()->user->addFlash('success', $product_model->text('disable_success'));
                            }
                            else
                            {
                                Yii::app()->user->addFlash('error', $product_model->text('disable_error'));
                            }
                        break;

                        case 'enable':
                            if ( $product_model->enable() )
                            {
                                Yii::app()->user->addFlash('success', $product_model->text('enable_success'));
                            }
                            else
                            {
                                Yii::app()->user->addFlash('error', $product_model->text('enable_error'));
                            }
                        break;

                        case 'delete':
                            // Do not allow DELETE a product, if it has been registered on an order
                            if ( $product_model->lineItems )
                            {
                                Yii::app()->user->addFlash('error', Yii::t('app', 'Product cannot be DELETED because it is related with one or more orders.'));
                            }
                            else if ( $product_model->delete() )
                            {
                                Yii::app()->user->addFlash('success', $product_model->text('delete_success'));

                                // Redirect to product page, because product no longer exists
                                Yii::app()->controller->redirect(['index']);
                            }
                            else
                            {
                                Yii::app()->user->addFlash('error', $product_model->text('delete_error'));
                            }
                        break;
                    }
                }
                else
                {
                    // Success message
                    Yii::app()->user->addFlash('success', $product_model->text('updated_success'));
                }

                // Redirect to index page
                // Yii::app()->controller->redirect(['index']);
                Yii::app()->controller->refresh();
            }
        }
    }


    /**
     * Get total number of products grouped by category type
     */
    public function total_by_category($product_type, $category_type)
    {
        $vec_totals = [];

        $vec_category_list = Category::model()->category_list($category_type, ['max_depth' => 1]);
        if ( !empty($vec_category_list) )
        {
            foreach ( $vec_category_list as $category_id => $category_name )
            {
                $vec_totals[$category_id] = 0;
            }
        }

        // Total products by category
        $vec_results = Yii::app()->db->createCommand("SELECT COUNT(*) AS total_products, product_category.category_id FROM `commerce_product_category` product_category INNER JOIN `commerce_product` product ON product_category.product_id = product.product_id  WHERE product.product_type = '". $product_type ."' AND product_category.category_type = '". $category_type ."' GROUP BY product_category.category_id ORDER BY product_category.category_id")->queryAll();
        if ( !empty($vec_results) )
        {
            foreach ( $vec_results as $que_result )
            {
                $vec_totals[$que_result['category_id']] = (int)$que_result['total_products'];
            }
        }

        return $vec_totals;
    }


    /**
     * Get the price_id given an alias
     */
    public function price_by_alias($alias, $is_return_model = true)
    {
        // Return Price model
        if ( $is_return_model )
        {
            if ( ! isset($this->vec_prices[$alias .'_model']) )
            {
                $this->vec_prices[$alias .'_model'] = Price::model()->findByAlias($alias);
            }

            return $this->vec_prices[$alias .'_model'];
        }

        // Return "price_id" column
        if ( ! isset($this->vec_prices[$alias .'_id']) )
        {
            $this->vec_prices[$alias .'_id'] = Price::get()
                                                ->select('price_id')
                                                ->where(['alias' => $alias])
                                                ->scalar();
        }

        return $this->vec_prices[$alias .'_id'];
    }


     /**
     * Price list (usually used on a SELECT2 dropdown)
     * 
     * @return array
     */
    public function price_list($variant_model = null, $vec_options = [])
    {
        //--> DEFAULT OPTION VALUES
        
        // Return Yii models objects or category names?
        $is_return_model = isset($vec_options['is_return_model']) ? $vec_options['is_return_model'] : false;

        // Order list
        $criteria_order = isset($vec_options['order']) ? $vec_options['order'] : 'price_id ASC';

        // Filter by prices available for a Variant (Product)
        $vec_prices = [];
        if ( $variant_model && $variant_model->prices )
        {
            foreach ( $variant_model->prices as $product_price_model )
            {
                $vec_prices[$product_price_model->price_id] = $product_price_model->raw_attributes['amount'];
            }
        }

        // Get selected Price models
        if ( !empty($vec_prices) )
        {
            $criteria = new DbCriteria;
            $criteria->addInCondition('t.price_id', array_keys($vec_prices));
            $criteria->order = $criteria_order;
            $vec_price_models = Price::model()->findAll($criteria);
        }

        // Get all Price models
        else
        {
            $vec_price_models = Price::get()
                ->orderBy($criteria_order)
                ->all();
        }

        // Return models
        if ( $is_return_model )
        {
            return $vec_price_models;
        }

        // Return an associative array indexed by key
        $vec_price_list = [];
        if ( !empty($vec_price_models) )
        {
            foreach ( $vec_price_models as $price_model )
            {
                $vec_price_list[$price_model->price_id] = $price_model->title();
            }
        }

        return $vec_price_list;
    }


    /**
     * Copy translations from "commerce_product" table to "commerce_translated_product" table
     * 
     * @return total copied translations
     */
    public function copy_translations($product_type, $language_id = 'en')
    {
        $num_copied_translations = 0;
        $vec_product_models = Product::get()->where(['product_type' => $product_type])->all();
        if ( !empty($vec_product_models) )
        {
            foreach ( $vec_product_models as $product_model )
            {
                if ( $product_model->copy_translation($language_id) )
                {
                    $num_copied_translations++;
                }
            }
        }

        return $num_copied_translations;
    }


    /**
     * Get product images path
     */
    public function get_images_path($product_id = null)
    {
        $images_path = Yii::app()->path->imagesPath() . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR;
        if ( $product_id !== null )
        {
            $images_path .= $product_id . DIRECTORY_SEPARATOR;
        }

        return $images_path;
    }
}
