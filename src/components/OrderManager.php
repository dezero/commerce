<?php
/**
 * OrderManager
 * 
 * Component to manage orders
 */

namespace dzlab\commerce\components;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\Order;
use dzlab\commerce\models\OrderJson;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\Variant;
use Yii;

class OrderManager extends ApplicationComponent
{
    /**
     * Get "status_type" labels
     */
    public function status_labels()
    {
        $vec_statuses = Order::model()->status_type_labels();
        $vec_statuses['payment_received'] = Yii::t('app', 'Paid');
        $vec_statuses['paid'] = Yii::t('app', 'Paid Orders');
        $vec_statuses['unpaid'] = Yii::t('app', 'Unpaid Orders');

        return $vec_statuses;
    }


    /**
     * Get "status_type" colors
     */
    public function status_colors()
    {
        return [
            'payment_received'  => 'green-800',
            'in_progress'       => 'blue-800',
            'shipped'           => 'purple-800',
            'completed'         => 'grey-800',
            'on_hold'           => 'yellow-800',
            'canceled'          => 'red-800',
            'cart'              => 'grey-500',
            'payment_failed'    => 'red-500',
        ];
    }


    /**
     * Get total number of orders grouped by status type
     */
    public function total_by_status()
    {
        // Get existing statuses
        $vec_totals = [];
        $vec_statuses = Yii::app()->orderManager->status_labels();
        if ( !empty($vec_statuses) )
        {
            foreach ( $vec_statuses as $status_type => $status_label )
            {
                $vec_totals[$status_type] = 0;
            }
        }

        // Total by status
        $vec_results = Yii::app()->db->createCommand('SELECT COUNT(*) AS `total_orders`, `status_type` FROM `commerce_order` GROUP BY `status_type` ORDER BY `status_type`')->queryAll();
        if ( !empty($vec_results) )
        {
            foreach ( $vec_results as $que_result )
            {
                $vec_totals[$que_result['status_type']] = $que_result['total_orders'];

                // Also, group orders by PAID / UNPAID
                if ( $que_result['status_type'] === 'cart' || $que_result['status_type'] === 'payment_failed' )
                {
                    $vec_totals['unpaid'] += $que_result['total_orders'];
                }
                else
                {
                    $vec_totals['paid'] += $que_result['total_orders'];
                }
            }
        }

        return $vec_totals;
    }


    /**
     * Get tax price of an amount
     */
    public function get_tax_amount($total_amount, $tax_percentage)
    {
        if ( !empty($tax_percentage) )
        {
            return $total_amount - ( $total_amount / (1 + ($tax_percentage / 100)) );
        }

        return 0;
    }


    /**
     * Get an amount without tax
     */
    public function get_amount_without_tax($total_amount, $tax_percentage)
    {
        if ( !empty($tax_percentage) )
        {
            return $total_amount - $this->get_tax_amount($total_amount, $tax_percentage);
        }

        return $total_amount;
    }


    /**
     * Save OrderJson model
     */
    public function save_order_json($order_id)
    {
        if ( empty($order_id) )
        {
            return false;
        }

        $order_model = Order::model()->cache(0)->findByPk($order_id);
        if ( empty($order_model) )
        {
            return false;
        }

        // Check if OrderJson model already exists
        $order_json_model = OrderJson::findOne($order_model->order_id);
        if ( ! $order_json_model )
        {
            $order_json_model = Yii::createObject(OrderJson::class);
            $order_json_model->order_id = $order_model->order_id;
        }
        $order_json_model->user_id = $order_model->user_id;
        if ( !empty($order_model->email) )
        {
            $order_json_model->email = $order_model->email;
        }

        // Save JSON data
        $vec_order_data = $order_json_model->get_data_from_order($order_model);
        if ( !empty($vec_order_data) )
        {
            $order_json_model->json_data = Json::encode($vec_order_data);
        }

        if ( ! $order_json_model->save() )
        {
            Log::save_model_error($order_json_model);

            return false;
        }

        return true;
    }
}
