<?php
/**
 * DiscountManager
 * 
 * Component to manage discount
 */

namespace dzlab\commerce\components;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\StringHelper;
use dzlab\commerce\models\Discount;
use dzlab\commerce\models\DiscountUse;
use Yii;

class DiscountManager extends ApplicationComponent
{
    /**
     * @var Discount. Active discount
     */
    private $_active_discount_model = '';


    /**
     * Check if it exists an ACTIVE & VALID Discount (discount_type = "auto")
     */
    public function is_active_discount()
    {
        // Active & valid discount?
        $this->_active_discount_model = $this->get_active_discount();
        
        if ( $this->_active_discount_model )
        {
            return true;
        }

        return false;
    }



    /**
     * Get an ACTIVE & VALID Discount (discount_type = "auto")
     */
    public function get_active_discount()
    {
        if ( $this->_active_discount_model === '' )
        {
            $this->_active_discount_model = Discount::get()
                ->where([
                    'discount_type' => 'auto',
                    'is_active'     => 1
                ])
                ->orderBy('discount_id DESC')
                ->one();

            // Check if Discount is valid
            if ( $this->_active_discount_model && ! $this->_active_discount_model->is_valid() )
            {
                // Mark Discount as NOT active
                $this->_active_discount_model->set_active(false);

                // Reset active discount
                $this->_active_discount_model = '';

                // Try with another discount
                return $this->get_active_discount();
            }
        }

        return $this->_active_discount_model;
    }


    /**
     * Check all ENABLED discounts
     */
    public function check_all_enabled_discounts()
    {
        $vec_discount_models = Discount::get()
            ->where('disable_date IS NULL')
            ->orderBy('discount_id DESC')
            ->all();

        if ( !empty($vec_discount_models) )
        {
            $is_changed_discount = false;

            foreach ( $vec_discount_models as $discount_model )
            {
                $is_valid = $discount_model->is_valid();
                if ( ! $is_valid && $discount_model->is_active == 1 )
                {
                    // Mark Discount as NOT active
                    $discount_model->set_active(false);

                    $is_changed_discount = true;
                }
                else if ( $is_valid && $discount_model->is_active == 0 )
                {
                    // Mark Discount as active
                    $discount_model->set_active(true);

                    $is_changed_discount = true;
                }
            }

            // Return FALSE if some discount has been changed
            if ( $is_changed_discount )
            {
                return false;
            }
        }

        return true;
    }


    /**
     * Get total number of discounts grouped by status type
     */
    public function total_by_status()
    {        
        $discount_table = Discount::model()->tableName();
        return [
            'active'    => Yii::app()->db->createCommand("SELECT COUNT(*) FROM `{$discount_table}` t WHERE t.is_active = 1 && (t.disable_date IS NULL OR t.disable_date = 0)")->queryScalar(),
            'inactive'  => Yii::app()->db->createCommand("SELECT COUNT(*) FROM `{$discount_table}` t WHERE t.is_active = 0 && (t.disable_date IS NULL OR t.disable_date = 0)")->queryScalar(),
            'disabled'  => Yii::app()->db->createCommand("SELECT COUNT(*) FROM `{$discount_table}` t WHERE t.disable_date IS NOT NULL AND t.disable_date > 0")->queryScalar(),
        ];
    }


    /**
     * Generate a coupon code with 12 chars
     */
    public function generate_coupon_code($length = 12)
    {
        $valid_chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $coupon_code = StringHelper::random_string_with_chars($valid_chars, $length);
        
        // If coupon code exists, try with another one
        $discount_model = Discount::model()->findByCode($coupon_code);
        if ( $discount_model )
        {
            return self::generate_coupon_code($length);
        }

        return $coupon_code;
    }


    /**
     * Get active permanent discounts
     */
    public function get_permanent_discounts()
    {
        $now = time();
        $criteria = new DbCriteria;
        $criteria->addCondition('code IS NULL AND disable_date IS NULL');
        $criteria->addCondition('start_date IS NULL OR start_date <= '. $now);
        $criteria->addCondition('expiration_date IS NULL OR expiration_date >= '. $now);
        $criteria->order = 'created_date DESC';
        return Discount::model()->findAll($criteria);
    }


    /**
     * This method is invoked after a Discount is saved via form submitting
     */
    public function after_save_discount($discount_model, $action = 'update', $vec_input_data)
    {
        if ( $action == 'create' )
        {
            // Success message
            Yii::app()->user->addFlash('success', 'New discount created successfully');

            // Redirect to index page
            Yii::app()->controller->redirect(['index']);
        }

        else if ( $action == 'update' )
        {
            // Has an action been triggered?
            if ( isset($vec_input_data['StatusChange']) && !empty($vec_input_data['StatusChange']) )
            {
                switch ( $vec_input_data['StatusChange'] )
                {
                    case 'disable':
                        if ( $discount_model->disable() )
                        {
                            Yii::app()->user->addFlash('success', Yii::t('app', 'Discount DISABLED successfully') );
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', Yii::t('app', 'Discount could not be DISABLED') );
                        }
                    break;

                    case 'enable':
                        if ( $discount_model->enable() )
                        {
                            Yii::app()->user->addFlash('success', Yii::t('app', 'Discount ENABLED successfully') );
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', Yii::t('app', 'Discount could not be ENABLED') );
                        }
                    break;

                    case 'delete':
                        if ( $discount_model->delete() )
                        {
                            Yii::app()->user->addFlash('success', Yii::t('app', 'Discount DELETED successfully') );
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', Yii::t('app', 'Discount could not be DELETED') );
                        }
                    break;
                }
            }
            else
            {
                // Flash message
                Yii::app()->user->addFlash('success', 'Discount updated successfully');
            }


            // Redirect to same page
            Yii::app()->controller->redirect(['update', 'id' => $discount_model->discount_id]);
            // Yii::app()->controller->redirect(['index']);
            // Yii::app()->controller->refresh();
        }
    }
}