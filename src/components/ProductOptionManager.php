<?php
/**
 * ProductOptionManager
 * 
 * Component to manage product options
 */

namespace dzlab\commerce\components;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;
use dz\modules\category\models\Category;
use dz\modules\category\models\TranslatedCategory;
use dzlab\commerce\models\ProductOption;
use dzlab\commerce\models\TranslatedProductOption;
use Yii;

class ProductOptionManager extends ApplicationComponent
{
    /**
     * Input data
     */
    protected $vec_input_data;


    /**
     * CREATE action for ProductOption model
     * 
     * @see BaseOptionController::actionCreate()
     */
    public function create_option($product_option_model, $vec_input_data)
    {
        // Option type configuration
        $vec_config = $product_option_model->get_config();

        // Save input data
        $this->vec_input_data = $vec_input_data;

        // Translations
        $vec_translated_models = [];
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();
        if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_extra_languages) )
        {
            foreach ( $vec_extra_languages as $language_id )
            {
                $translated_option_model = Yii::createObject(TranslatedProductOption::class);
                $translated_option_model->language_id = $language_id;
                $vec_translated_models[$language_id] = $translated_option_model;
            }
        }

        // Create new ProductOption?
        if ( isset($vec_input_data['ProductOption']) )
        {
            $product_option_model->setAttributes($vec_input_data['ProductOption']);
            if ( $product_option_model->save() )
            {                
                // Translations
                $product_option_model = self::save_option($product_option_model, 'translation', $vec_translated_models);

                // Trigger custom "after_save_option" method
                self::after_save_option($product_option_model, 'create', $vec_input_data);
            }

            // Some error(s) detected
            else
            {
                $product_option_model->afterFind(new \CEvent($product_option_model));
            }
        }

        return [
            'product_option_model'  => $product_option_model,
            'vec_config'            => $vec_config,
            'vec_translated_models' => $vec_translated_models,
            'vec_extra_languages'   => $vec_extra_languages,
            'form_id'               => 'option-'. $product_option_model->option_type .'-form'
        ];
    }


    /**
     * UPDATE action for ProductOption model
     * 
     * @see BaseOptionController::actionUpdate()
     */
    public function update_option($product_option_model, $vec_input_data)
    {
        // Option type configuration
        $vec_config = $product_option_model->get_config();

        // Save input data
        $this->vec_input_data = $vec_input_data;

        // Translations
        $vec_translated_models = [];
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();
        if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_extra_languages) )
        {
            foreach ( $vec_extra_languages as $language_id )
            {
                $translated_option_model = TranslatedProductOption::get()->where([
                    'language_id' => $language_id,
                    'option_id' => $product_option_model->option_id
                ])->one();
                if ( ! $translated_option_model )
                {
                    $translated_option_model = Yii::createObject(TranslatedProductOption::class);
                    $translated_option_model->language_id = $language_id;
                    $translated_option_model->option_id = $product_option_model->option_id;
                }
                $vec_translated_models[$language_id] = $translated_option_model;
            }
        }


        // Update ProductOption?
        if ( isset($vec_input_data['ProductOption']) )
        {
            $product_option_model->setAttributes($vec_input_data['ProductOption']);
            if ( $product_option_model->save() )
            {                
                // Translations
                $product_option_model = self::save_option($product_option_model, 'translation', $vec_translated_models);

                // Trigger custom "after_save_option" method
                self::after_save_option($product_option_model, 'update', $vec_input_data);
            }

            // Some error(s) detected
            else
            {
                $product_option_model->afterFind(new \CEvent($product_option_model));
            }
        }

        return [
            'product_option_model'  => $product_option_model,
            'vec_config'            => $vec_config,
            'vec_translated_models' => $vec_translated_models,
            'vec_extra_languages'   => $vec_extra_languages,
            'form_id'               => 'option-'. $product_option_model->option_type .'-form'
        ];
    }


    /**
     * Save entities related with a ProductOption: translations
     */
    public function save_option($product_option_model, $entity_type, $entity_data = null, $vec_input_data = [])
    {
        // Get input data
        if ( empty($vec_input_data) )
        {
            $vec_input_data = $this->vec_input_data;
        }

        // Option type configuration
        $vec_config = $product_option_model->get_config();

        switch ( $entity_type )
        {
            // Translation
            case 'translation':
                $vec_translated_models = $entity_data;
                if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_translated_models) && isset($vec_input_data['TranslatedProductOption']) )
                {
                    foreach ( $vec_input_data['TranslatedProductOption'] as $language_id => $vec_translated_option_attributes )
                    {
                        if ( isset($vec_translated_models[$language_id]) && isset($vec_translated_option_attributes['name']) )
                        {
                            $translated_option_model = $vec_translated_models[$language_id];
                            $vec_translated_option_attributes['language_id'] = $language_id;
                            $vec_translated_option_attributes['option_id'] = $product_option_model->option_id;
                            $translated_option_model->setAttributes($vec_translated_option_attributes);
                            $product_option_model->save_translation($vec_translated_models[$language_id]);
                        }
                    }
                }
            break;
        }

        return $product_option_model;
    }


    /**
     * This method is invoked after a ProductOption is saved via form submitting
     */
    public function after_save_option($product_option_model, $action = 'update', $vec_input_data)
    {
        // Option type configuration
        $vec_config = $product_option_model->get_config();

        if ( $action == 'create' )
        {
            Yii::app()->user->addFlash('success', $product_option_model->text('created_success'));

            // Redirect to index page
            Yii::app()->controller->redirect(['index']);
        }

        else if ( $action == 'update' )
        {
            // Has an action been triggered?
            if ( isset($vec_input_data['StatusChange']) && !empty($vec_input_data['StatusChange']) )
            {
                switch ( $vec_input_data['StatusChange'] )
                {
                    case 'disable':
                        if ( $product_option_model->disable() )
                        {
                            Yii::app()->user->addFlash('success', $product_option_model->text('disable_success'));
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', $product_option_model->text('disable_error'));
                        }
                    break;

                    case 'enable':
                        if ( $product_option_model->enable() )
                        {
                            Yii::app()->user->addFlash('success', $product_option_model->text('enable_success'));
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', $product_option_model->text('enable_error'));
                        }
                    break;
                }
            }
            else
            {
                // Success message
                Yii::app()->user->addFlash('success', $product_option_model->text('updated_success'));
            }

            // Redirect to index page
            Yii::app()->controller->redirect(['index']);
        }
    }



    /**
     * CREATE action for extras
     * 
     * @see ExtraController::actionCreate()
     */
    public function create_extra($product_option_model, $category_model, $vec_input_data)
    {
        // Save input data
        $this->vec_input_data = $vec_input_data;

        // Option type configuration
        $vec_config = $product_option_model->get_config();

        // Check form action
        $form_action = 'create_category';
        if ( !empty($category_model->category_id) )
        {
            $form_action = 'create_option';
        }

        // Create new Category?
        if ( isset($vec_input_data['Category']) )
        {
            $category_model->setAttributes($vec_input_data['Category']);
            if ( $category_model->save() )
            {                
                // Trigger custom "after_save_extra" method
                self::after_save_extra($product_option_model, $category_model, $form_action, $vec_input_data);
            }

            // Some error(s) detected
            else
            {
                $category_model->afterFind(new \CEvent($category_model));
            }
        }


        // Create new ProductOption?
        else if ( isset($vec_input_data['ProductOption']) )
        {
            $product_option_model->setAttributes($vec_input_data['ProductOption']);
            if ( $product_option_model->save() )
            {                
                // Trigger custom "after_save_extra" method
                self::after_save_extra($product_option_model, $category_model, $form_action, $vec_input_data);
            }

            // Some error(s) detected
            else
            {
                $product_option_model->afterFind(new \CEvent($product_option_model));
            }
        }

        return [
            'product_option_model'  => $product_option_model,
            'category_model'        => $category_model,
            'form_action'           => $form_action,
            'vec_config'            => $vec_config,
            'form_id'               => 'option-'. $product_option_model->option_type .'-form'
        ];
    }


    /**
     * UPDATE action for extras
     * 
     * @see ExtraController::actionUpdate()
     */
    public function update_extra($product_option_model, $category_model, $vec_input_data)
    {
        // Option type configuration
        $vec_config = $product_option_model->get_config();

        // Save input data
        $this->vec_input_data = $vec_input_data;

        // Check form action
        $form_action = 'update_category';
        if ( !empty($product_option_model->option_id) )
        {
            $form_action = 'update_option';
        }

        // Default language
        $default_language = Yii::defaultLanguage();

        // Extra supported languages
        $vec_extra_languages = Yii::app()->i18n->get_extra_languages();

        // Update Category
        if ( $form_action == 'update_category' )
        {
            // Translations
            $vec_translated_models = [];
            if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_extra_languages) )
            {
                foreach ( $vec_extra_languages as $language_id )
                {
                    $translated_category_model = TranslatedCategory::get()->where([
                        'language_id' => $language_id,
                        'category_id' => $category_model->category_id
                    ])->one();
                    if ( ! $translated_category_model )
                    {
                        $translated_category_model = Yii::createObject(TranslatedCategory::class);
                        $translated_category_model->language_id = $language_id;
                        $translated_category_model->category_id = $category_model->category_id;
                    }
                    $vec_translated_models[$language_id] = $translated_category_model;
                }
            }
        }

        // Update ProductOption
        else
        {
            // Translations
            $vec_translated_models = [];
            if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_extra_languages) )
            {
                foreach ( $vec_extra_languages as $language_id )
                {
                    $translated_option_model = TranslatedProductOption::get()->where([
                        'language_id'   => $language_id,
                        'option_id'     => $product_option_model->option_id
                    ])->one();
                    if ( ! $translated_option_model )
                    {
                        $translated_option_model = Yii::createObject(TranslatedProductOption::class);
                        $translated_option_model->language_id = $language_id;
                        $translated_option_model->option_id = $product_option_model->option_id;
                    }
                    $vec_translated_models[$language_id] = $translated_option_model;
                }
            }
        }

        // Update Category?
        if ( isset($vec_input_data['Category']) )
        {
            $category_model->setAttributes($vec_input_data['Category']);
            if ( $category_model->save() )
            {                
                // Translations for Category
                $product_option_model = self::save_extra($product_option_model, $category_model, 'category_translation', $vec_translated_models);

                // Trigger custom "after_save_extra" method
                self::after_save_extra($product_option_model, $category_model, $form_action, $vec_input_data);
            }

            // Some error(s) detected
            else
            {
                $category_model->afterFind(new \CEvent($category_model));
            }
        }

        // Update ProductOption?
        else if ( isset($vec_input_data['ProductOption']) )
        {
            $product_option_model->setAttributes($vec_input_data['ProductOption']);
            if ( $product_option_model->save() )
            {                
                // Translations
                $product_option_model = self::save_extra($product_option_model, $category_model, 'product_option_translation', $vec_translated_models);

                // Trigger custom "after_save_option" method
                self::after_save_extra($product_option_model, $category_model, $form_action, $vec_input_data);
            }

            // Some error(s) detected
            else
            {
                $product_option_model->afterFind(new \CEvent($product_option_model));
            }
        }

        return [
            'product_option_model'  => $product_option_model,
            'category_model'        => $category_model,
            'form_action'           => $form_action,
            'vec_config'            => $vec_config,
            'vec_translated_models' => $vec_translated_models,
            'default_language'      => $default_language,
            'vec_extra_languages'   => $vec_extra_languages,
            'form_id'               => 'option-'. $product_option_model->option_type .'-form'
        ];
    }


    /**
     * Save entities related with a ProductOption: category_translation, product_option_translation
     */
    public function save_extra($product_option_model, $category_model, $entity_type, $entity_data = null, $vec_input_data = [])
    {
        // Get input data
        if ( empty($vec_input_data) )
        {
            $vec_input_data = $this->vec_input_data;
        }

        // Option type configuration
        $vec_config = $product_option_model->get_config();

        switch ( $entity_type )
        {
            // Translation for Category models
            case 'category_translation':
                $vec_translated_models = $entity_data;
                if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_translated_models) && isset($vec_input_data['TranslatedCategory']) )
                {
                    foreach ( $vec_input_data['TranslatedCategory'] as $language_id => $vec_translated_category_attributes )
                    {
                        if ( isset($vec_translated_models[$language_id]) && isset($vec_translated_category_attributes['name']) )
                        {
                            $translated_category_model = $vec_translated_models[$language_id];
                            $vec_translated_category_attributes['language_id'] = $language_id;
                            $vec_translated_category_attributes['category_id'] = $category_model->category_id;
                            $translated_category_model->setAttributes($vec_translated_category_attributes);
                            $category_model->save_translation_model($vec_translated_models[$language_id]);
                        }
                    }
                }
            break;

            // Translation for ProductOption models
            case 'product_option_translation':
                $vec_translated_models = $entity_data;
                if ( isset($vec_config['is_multilanguage']) && $vec_config['is_multilanguage'] && !empty($vec_translated_models) && isset($vec_input_data['TranslatedProductOption']) )
                {
                    foreach ( $vec_input_data['TranslatedProductOption'] as $language_id => $vec_translated_option_attributes )
                    {
                        if ( isset($vec_translated_models[$language_id]) && isset($vec_translated_option_attributes['name']) )
                        {
                            $translated_option_model = $vec_translated_models[$language_id];
                            $vec_translated_option_attributes['language_id'] = $language_id;
                            $vec_translated_option_attributes['option_id'] = $product_option_model->option_id;
                            $translated_option_model->setAttributes($vec_translated_option_attributes);
                            $product_option_model->save_translation($vec_translated_models[$language_id]);
                        }
                    }
                }
            break;
        }

        return $product_option_model;
    }


    /**
     * This method is invoked after a ProductOption (EXTRA option_type) is saved via form submitting
     */
    public function after_save_extra($product_option_model, $category_model, $form_action = 'update_option', $vec_input_data)
    {
        // Option type configuration
        $vec_config = $product_option_model->get_config();

        // Create Category model
        if ( $form_action == 'create_category' )
        {
            Yii::app()->user->addFlash('success', $product_option_model->text('created_success'));

            // Redirect to index page
            Yii::app()->controller->redirect(['index']);
        }

        // Create ProductOption model
        else if ( $form_action == 'create_option' )
        {
            Yii::app()->user->addFlash('success', $product_option_model->text('option_created_success'));

            // Redirect to category update page
            Yii::app()->controller->redirect(['update', 'id' => $category_model->category_id]);
        }

        // Update Category model
        else if ( $form_action == 'update_category' )
        {
            // Has an action been triggered?
            if ( isset($vec_input_data['StatusChange']) && !empty($vec_input_data['StatusChange']) )
            {
                switch ( $vec_input_data['StatusChange'] )
                {
                    case 'disable':
                        if ( $category_model->disable() )
                        {
                            Yii::app()->user->addFlash('success', $product_option_model->text('disable_success'));
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', $product_option_model->text('disable_error'));
                        }
                    break;

                    case 'enable':
                        if ( $category_model->enable() )
                        {
                            Yii::app()->user->addFlash('success', $product_option_model->text('enable_success'));
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', $product_option_model->text('enable_error'));
                        }
                    break;

                    case 'delete':
                        if ( $category_model->delete() )
                        {
                            Yii::app()->user->addFlash('success', $product_option_model->text('delete_success'));
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', $product_option_model->text('delete_error'));
                        }
                    break;
                }
            }
            else
            {
                // Success message
                Yii::app()->user->addFlash('success', $product_option_model->text('updated_success'));
            }

            // Redirect to index page
            Yii::app()->controller->redirect(['index']);
        }

        // Update ProductOption model
        else
        {
            // Has an action been triggered?
            if ( isset($vec_input_data['StatusChange']) && !empty($vec_input_data['StatusChange']) )
            {
                switch ( $vec_input_data['StatusChange'] )
                {
                    case 'disable':
                        if ( $product_option_model->disable() )
                        {
                            Yii::app()->user->addFlash('success', $product_option_model->text('option_disable_success'));
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', $product_option_model->text('option_disable_error'));
                        }
                    break;

                    case 'enable':
                        if ( $product_option_model->enable() )
                        {
                            Yii::app()->user->addFlash('success', $product_option_model->text('option_enable_success'));
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', $product_option_model->text('option_enable_error'));
                        }
                    break;

                    case 'delete':
                        if ( $product_option_model->delete() )
                        {
                            Yii::app()->user->addFlash('success', $product_option_model->text('option_delete_success'));
                        }
                        else
                        {
                            Yii::app()->user->addFlash('error', $product_option_model->text('option_delete_error'));
                        }
                    break;
                }
            }
            else
            {
                // Success message
                Yii::app()->user->addFlash('success', $product_option_model->text('option_updated_success'));
            }

            // Redirect to category update page
            Yii::app()->controller->redirect(['update', 'id' => $category_model->category_id]);
        }
    }


    /**
     * ProductOption list (usually used on a SELECT2 dropdown)
     * 
     * @return array
     */
    public function option_list($option_type, $vec_options = [])
    {
        //--> DEFAULT OPTION VALUES
        
        // Return Yii models objects or category names?
        $is_return_model = isset($vec_options['is_return_model']) ? $vec_options['is_return_model'] : false;

        // Order list
        $criteria_order = isset($vec_options['order']) ? $vec_options['order'] : 'weight ASC';


        // Get all ProductOption models
        $vec_product_option_models = ProductOption::get()
            ->where(['option_type' => $option_type])
            ->orderBy($criteria_order)
            ->all();

        // Return models
        if ( $is_return_model )
        {
            return $vec_product_option_models;
        }

        // Return an associative array indexed by key
        $vec_option_list = [];
        if ( !empty($vec_product_option_models) )
        {
            foreach ( $vec_product_option_models as $product_option_model )
            {
                $vec_option_list[$product_option_model->option_id] = $product_option_model->name;
            }
        }

        return $vec_option_list;
    }


    /**
     * Get all product extras (Category models)
     * 
     * @see CategoryManager::category_list()
     */
    public function extras_list($vec_options = [])
    {
        return Yii::app()->categoryManager->category_list('product_option_extra', $vec_options);
    }


    /**
     * Return an array with ProductOption models of a $category_id
     */
    public function get_options_by_category($category_id)
    {
        return ProductOption::get()
            ->where([
                'option_type'   => 'extra',
                'category_id'   => $category_id
            ])->all();
    }


    /**
     * Copy translations from "commerce_product_option" table to "commerce_translated_product_option" table
     * 
     * @return total copied translations
     */
    public function copy_translations($option_type, $language_id = 'en')
    {
        $num_copied_translations = 0;
        $vec_product_option_models = ProductOption::get()->where(['option_type' => $option_type])->all();
        if ( !empty($vec_product_option_models) )
        {
            foreach ( $vec_product_option_models as $product_option_model )
            {
                if ( $product_option_model->copy_translation($language_id) )
                {
                    $num_copied_translations++;
                }
            }
        }

        return $num_copied_translations;
    }
}