<?php
/**
 * CartManager
 * 
 * Component to manage cart (orders)
 */

namespace dzlab\commerce\components;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\Order;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\Variant;
use user\models\User;
use Yii;

class CartManager extends ApplicationComponent
{
    /**
     * @var string Session key for storing the cart number
     */
    protected $cart_name = 'commerce_cart';


    /**
     * @var Order
     */
    protected $cart_order_model = null;


    /**
     * Magic GET method. Get Order model
     */
    public function getcart_order_model()
    {
        return $this->cart_order_model;
    }


    /**
     * Initializes the cart, if it does not exist
     */
    public function init_cart()
    {
        if ( ! Yii::app()->session->has($this->cart_name) )
        {
            $this->reset_order_reference();
        }

        return true;
    }


    /**
     * Get the current cart for this session
     */
    public function get_order_model($is_recover = false)
    {
        // Init cart, if it does not exist
        $this->init_cart();

        // Registered user? Check if user has a pending shopping cart
        $cart_order_reference = $this->get_order_reference();
        if ( Yii::app()->user->id > 0 && $cart_order_reference === 'init' )
        {
            $this->cart_order_model = $this->get_pending_cart();
            if ( $this->cart_order_model )
            {
                $this->set_order_reference($this->cart_order_model->order_reference);
                return $this->cart_order_model;
            }
        }

        // Cart exists?
        $order_reference = $this->get_order_reference();
        if ( $order_reference !== 'init'  )
        {
            // Load Order model
            $existing_cart_order_model = $this->findByReference($order_reference);

            if ( $existing_cart_order_model )
            {
                // Cart belongs to another user. Reset it!
                if ( $existing_cart_order_model->created_uid != Yii::app()->user->id  )
                {
                    $this->reset_order_reference();
                }

                // Cart is completed
                else if ( $existing_cart_order_model->is_cart_completed == 1 )
                {
                    // Try to recover (from ORDERED status to NEW)
                    if ( $is_recover )
                    {
                        $this->cart_order_model = $existing_cart_order_model;
                        $this->cart_order_model->change_status('cart', 'Error from TPV');
                    }

                    // Order has been completed. Reset it!
                    else
                    {
                        $this->reset_order_reference();
                    }
                }

                // Cart is recovered from database
                else
                {
                    $this->cart_order_model = $existing_cart_order_model;
                }
            }
        }

        // Cart is empty
        $cart_order_reference = $this->get_order_reference();
        if ( $cart_order_reference === 'init' || empty($this->cart_order_model) )
        {
            $this->generate_new_order();
        }

        return $this->cart_order_model;
    }


    /**
     * Get total items added into the cart
     */
    public function total_items()
    {
        // Current cart
        if ( $this->cart_order_model !== null )
        {
            $this->cart_order_model = Yii::app()->cartManager->get_order_model();
        }

        if ( $this->cart_order_model )
        {
            return $this->cart_order_model->itemsCount;
        }

        return 0;
    }


    /**
     * Add an item to the cart
     * 
     * @return LineItem
     */
    public function add_line_item($variant_id, $quantity = 1, $price_alias = null, $vec_product_options = [])
    {
        // Current cart
        $this->cart_order_model = Yii::app()->cartManager->get_order_model();

        // Check if Variant model exists and is enabled?
        $variant_model = Variant::findOne($variant_id);
        if ( $this->cart_order_model && $variant_model )
        {
            // Create CART for the first time
            if ( $this->cart_order_model->isNewRecord && ! $this->cart_order_model->save() )
            {
                Log::save_model_error($this->cart_order_model);
                return false;
            }

            // Add new product to the cart
            if ( $this->cart_order_model && $this->cart_order_model->add_line_item($variant_id, $quantity, $price_alias, $vec_product_options) )
            {
                // Check if there's an active discount
                $is_active_discount = $this->check_active_discount();

                return true;
            }
        }
        else
        {
            Log::error("CartManager::add_line_item() - Cart Order model or Variant model does not exist (variant_id = {$variant_id})");
        }

        return false;
    }


    /**
     * Update quantity of an item from the cart
     */
    public function update_line_item($line_item_id, $quantity = 1, $price_alias = null, $vec_product_options = [])
    {
        // Current cart
        $this->cart_order_model = Yii::app()->cartManager->get_order_model();

        // Check if LineItem model exists and it belongs to current order
        if ( $this->cart_order_model && $this->cart_order_model->update_line_item($line_item_id, $quantity, $price_alias, $vec_product_options) )
        {
            // Check if there's an active discount
            $is_active_discount = $this->check_active_discount();

            return true;
        }

        return false;
    }


    /**
     * Remove an item from the cart
     */
    public function remove_line_item($line_item_id)
    {
        // Current cart
        $this->cart_order_model = Yii::app()->cartManager->get_order_model();

        if ( $this->cart_order_model && $this->cart_order_model->remove_line_item($line_item_id) )
        {
            return true;
        }

        return false;
    }


    /**
     * Find a Cart (Order model) by reference
     */
    public function findByReference($order_reference)
    {
        return Order::model()->findByReference($order_reference);
    }


    /**
     * Get pending cart from an user
     */
    public function get_pending_cart($user_id = '')
    {
        return Order::get()->where([
            'status_type'       => 'cart',
            'created_uid'       => !empty($user_id) ? $user_id : Yii::app()->user->id,
            'is_cart_completed' => 0
        ])->one();
    }


    /**
     * Generate new order
     */
    public function generate_new_order()
    {
        $this->cart_order_model = Yii::createObject(Order::class);
        $this->cart_order_model->order_reference = $this->cart_order_model->generate_order_reference();
        $this->cart_order_model->user_id = Yii::app()->user->id;
        $this->cart_order_model->language_id = Yii::currentLanguage();
        $this->set_order_reference($this->cart_order_model->order_reference);

        // Registered user
        if ( $this->cart_order_model->user_id > 0 )
        {
            $user_model = User::findOne($this->cart_order_model->user_id);
            if ( $user_model && $user_model->customer )
            {
                $this->cart_order_model->shipping_address_id = $user_model->customer->shipping_address_id;
                $this->cart_order_model->billing_address_id = $user_model->customer->billing_address_id;
            }
        }
    }


    /**
     * Reset order reference (from session)
     */
    public function reset_order_reference()
    {
        Yii::app()->session->set($this->cart_name, 'init');
    }


    /**
     * Get order reference from session
     */
    public function get_order_reference()
    {
        return Yii::app()->session->get($this->cart_name, 'init');
    }


    /**
     * Set order reference from session
     */
    public function set_order_reference($order_reference)
    {
        Yii::app()->session->set($this->cart_name, $order_reference);
    }



    /**
     * Add a discount code
     */
    public function add_discount_code($discount_code)
    {
        return $this->cart_order_model->add_discount_code($discount_code);
    }


    /**
     * Remove applied discount
     */
    public function remove_discount()
    {
        if ( $this->cart_order_model->remove_discount() )
        {
            // Check if there's an active discount
            $is_active_discount = $this->check_active_discount();

            return true;
        }

        return false;
    }

    /**
     * Check if there's an active discount. If so, apply it
     */
    public function check_active_discount()
    {
        if ( ! $this->cart_order_model->discount )
        {
            $active_discount_model = Yii::app()->discountManager->get_active_discount();
            if ( $active_discount_model )
            {
                $this->cart_order_model->add_discount_code($active_discount_model->code);
                return true;
            }
        }

        return false;
    }


    /**
     * Called after the customer has logged into the platform
     * 
     * Check if there's a pending cart to be assigned to this customer
     */
    public function afterLogin($user_model, $is_checkout = false)
    {
        // #1 - Check if customer has a pending Cart saved on database
        $db_cart_order_model = $this->get_pending_cart($user_model->id);

        // Reset cart, if the old one has no items added
        if ( $db_cart_order_model && empty($db_cart_order_model->lineItems) )
        {
            $db_cart_order_model->delete();
            $db_cart_order_model = null;
        }

        // Get current CART ORDER reference
        $order_reference = Yii::app()->cartManager->get_order_reference();

        // #2 - Not existing cart in database but it exists on session variables
        if ( ! $db_cart_order_model && $order_reference !== 'init' )
        {
            // Get current CART ORDER model from session
            $current_cart_order_model = $this->findByReference($order_reference);

            // Assign from ANONYMOUS to current user
            if ( $current_cart_order_model && $current_cart_order_model->user_id == 0 && $current_cart_order_model->lineItems )
            {
                $current_cart_order_model->assign_customer($user_model->id);
                return true;
            }
        }

        // #3 - Existing Cart in database
        else if ( $db_cart_order_model )
        {
            // Get current CART ORDER model from session
            $current_cart_order_model = $this->findByReference($order_reference);

            // Keep OLD cart (database) and remove current (session)
            if ( ! $is_checkout )
            {
                if ( $current_cart_order_model )
                {
                    $current_cart_order_model->delete();
                }

                // Set OLD cart (database) as the active current cart order
                $this->set_order_reference($db_cart_order_model->order_reference);
            }

            // Keep NEW current cart (session) and remove old (database)
            else
            {
                if ( $current_cart_order_model )
                {
                    $this->set_order_reference($current_cart_order_model->order_reference);
                    $db_cart_order_model->delete();

                    if ( $current_cart_order_model->user_id == 0 && $current_cart_order_model->lineItems )
                    {
                        $current_cart_order_model->assign_customer($user_model->id);
                        return true;
                    }
                }

                // Set OLD cart (database) as the active current cart order
                else
                {
                    $this->set_order_reference($db_cart_order_model->order_reference);
                }
            }
        }

        return true;
    }


    /**
     * Repeat previous Order model
     * 
     * It loads every line item from original Order into the cart
     */
    public function repeat_order($order_model)
    {
        if ( $order_model->lineItems )
        {
            foreach ( $order_model->lineItems as $line_item_model )
            {
                Yii::app()->cartManager->add_line_item($line_item_model->variant_id, $line_item_model->quantity, $line_item_model->price_alias);
            }
        }

        return true;
    }


    /**
     * Check if some Product or Variant has been changed since the customer added it into the cart
     *
     * @since 21/07/2022
     */
    public function check_product_changes()
    {
        $is_product_changed = false;

        if ( $this->cart_order_model->lineItems )
        {
            foreach ( $this->cart_order_model->lineItems as $line_item_model )
            {
                // Product or Variant has changed -> Reinsert line item
                if ( $line_item_model->is_product_or_variant_updated() )
                {
                    // Save original line item with all the options
                    $vec_product_options = [];
                    if ( $line_item_model->options )
                    {
                        foreach ( $line_item_model->options as $line_item_option_model )
                        {
                            if ( $line_item_option_model->option )
                            $vec_product_options[$line_item_option_model->option->category_id] = $line_item_option_model->option_id;
                        }
                    }
                    $original_line_item_model = $line_item_model;

                    // Remove line item from cart
                    $this->remove_line_item($line_item_model->line_item_id);

                    // Add line item to the cart with new price
                    $this->add_line_item($original_line_item_model->variant_id, $original_line_item_model->quantity, $original_line_item_model->price_alias, $vec_product_options);

                    // Mark as a product changed
                    $is_product_changed = true;
                }
            }
        }

        return $is_product_changed;
    }
}
