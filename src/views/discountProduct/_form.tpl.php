<?php
/*
|--------------------------------------------------------------------------
| Form partial page of a DiscountProduct model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $discount_model: Discount model
|  - $discount_product_model: DiscountProduct model
|  - $current_action: Current action name
|
*/

  use dz\helpers\Html;
  use dz\helpers\Url;;

?>
<?php
  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => 'discount-product-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);
?>
  <div id="slidepanel-errors" class="messages-error alert alert-dismissible dark summary-errors alert-danger hide"></div>
  <div class="row">
    <label class="col-lg-3 form-control-label"><?= $discount_product_model->getAttributeLabel('product_id'); ?></label>
    <div class="col-lg-9">
      <select id="DiscountProduct_product_id" name="DiscountProduct[product_id]"><option></option></select>
    </div>
  </div>

  <div class="row">
    <label class="col-lg-3 form-control-label"></label>
    <div class="col-lg-9 form-actions">
      <button id="discount-product-save-btn" class="btn btn-primary" data-dismiss="modal" type="button" data-url="<?= Url::to('/commerce/discountProduct/create', ['discount_id' => $discount_model->discount_id]); ?>" data-discount="<?= $discount_model->discount_id; ?>"><?= Yii::t('app', 'Save'); ?></button>
      <a class="btn btn-dark cancel slidePanel-close" href="#"><?= Yii::t('app', 'Cancel'); ?></a>
    </div>
  </div>
<?php
  // End model form
  $this->endWidget();
?>