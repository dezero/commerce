<?php
/*
|--------------------------------------------------------------------------
| Create partial page of a DiscountProduct model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $discount_model: Discount model
|  - $discount_product_model: DiscountProduct model
|
*/

  use dz\helpers\Html;
?>
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-pure btn-inverse slidePanel-close actions-top icon wb-close" aria-hidden="true"></button>
  </div>
  <h1><?= Yii::t('app', 'Add product'); ?></h1>
</header>
<div id="discount-product-slidepanel-action" class="slidePanel-inner discount-product-slidepanel-wrapper" data-action="create">
  <section class="slidePanel-inner-section">
    <?php
      // Render form
      $this->renderPartial('//commerce/discountProduct/_form', [
        'discount_model'          => $discount_model,
        'discount_product_model'  => $discount_product_model,
        'current_action'          => 'create'
      ]);
    ?>
  </section>
</div>