<?php
/*
|--------------------------------------------------------------------------
| Discount Product Restriction index page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $discount_model: Discount model
|
*/
  use dz\helpers\Url;

  // Header title
  $this->renderPartial('//commerce/discount/_header_title', [
    'discount_model' => $discount_model
  ]);
?>

<div class="page-content container-fluid">
  <?php
    // Header menu
    $this->renderPartial('//commerce/discount/_header_menu', [
      'discount_model' => $discount_model,
    ]);
  ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | PRODUCT TABLE
    |--------------------------------------------------------------------------
    */
  ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= $discount_model->product_restriction_type === 'exclude' ? Yii::t('app', 'Excluded Products') : Yii::t('app', 'Included Products'); ?></h3>
      <div class="panel-actions">
        <a id="discount-product-delete-all-btn" href="#"  class="btn btn-danger" data-discount="<?= $discount_model->discount_id; ?>" data-url="<?= Url::to('/commerce/discountProduct/deleteAll', ['discount_id' => $discount_model->discount_id]); ?>" data-confirm="<h3><?= Yii::t('app', 'Are you sure you want to DELETE all the products?'); ?></h3>">
          <i class="icon wb-trash"></i> <?= Yii::t('app', 'Delete all'); ?>
        </a>
        <a id="discount-product-create-btn"  class="btn btn-primary" href="<?= Url::to('/commerce/discountProduct/create', ['discount_id' => $discount_model->discount_id]); ?>" data-action="create" data-discount="<?= $discount_model->discount_id; ?>">
          <i class="icon wb-plus"></i> <?= Yii::t('app', 'Add product'); ?>
        </a>
      </div>
    </header>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
          <?php
            $this->renderPartial('//commerce/discountProduct/_table_products', [
              'discount_model'  => $discount_model,
              'is_view_mode'    => false
            ]);
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
