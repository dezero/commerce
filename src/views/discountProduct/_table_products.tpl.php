<?php
/*
|--------------------------------------------------------------------------
| Discount products partial
|--------------------------------------------------------------------------
|
| Available variables:
|  - $discount_model: Product model
|
*/

  use dz\helpers\Url;
  use dzlab\commerce\models\Product;
?>
<div class="table-responsive">
  <table id="discount-product-table" class="discount-product-table table table-striped table-hover grid-view-loading head-uppercase" data-url="<?= Url::to('/commerce/discountProduct', ['discount_id' => $discount_model->discount_id]); ?>">
    <?php if ( $discount_model->discountProducts ) : ?>
      <thead>
        <tr>
          <th class="image-column"><?= Product::model()->getAttributeLabel('image'); ?></th>
          <th class="sku-column"><?= Product::model()->getAttributeLabel('default_sku'); ?></th>
          <th class="title-column text-left"><?= Product::model()->getAttributeLabel('name'); ?></th>
          <th class="actions-column text-center"><?= Yii::t('app', 'ACTIONS'); ?></th>
        </tr>
      </thead>
    <?php endif; ?>
    <tbody>
      <?php if ( $discount_model->discountProducts ) : ?>
        <?php foreach ( $discount_model->discountProducts as $discount_product_model ) : ?>
          <?php
            $product_model = $discount_product_model->product;
            if ( $product_model ) :
          ?>
            <tr>
              <td class="image-column">
                <?php
                  $this->renderPartial($product_model->get_view_path("_grid_column"), [
                    'column' => 'image',
                    'model' => $product_model
                  ]);
                ?>
              </td>
              <td class="sku-column">
                <?php
                  $this->renderPartial($product_model->get_view_path("_grid_column"), [
                    'column' => 'sku',
                    'model' => $product_model
                  ]);
                ?>
              </td>
              <td class="title-column">
                <?php
                  $this->renderPartial($product_model->get_view_path("_grid_column"), [
                    'column' => 'name',
                    'model' => $product_model
                  ]);
                ?>
              </td>
              <td class="actions-column text-center">
                <a class="delete-action btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip" data-action="delete" href="<?= Url::to('/commerce/discountProduct/delete', ['discount_id' => $discount_model->discount_id, 'product_id' => $product_model->product_id]); ?>" data-original-title="<?= Yii::t('app', 'Delete'); ?>" data-product="<?= $product_model->product_id; ?>" data-discount="<?= $discount_model->discount_id; ?>"><i class="wb-trash"></i></a>
              </td>
            </tr>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php else : ?>
        <tr>
          <td colspan="5">
            <h5 class="mt-15"><?= Yii::t('app', 'No products have been added'); ?></h5>
          </td>
        </tr>
      <?php endif; ?>
    </tbody>
  </table>
</div>        
