<?php
/*
|--------------------------------------------------------------------------
| Create partial page of a ProductLink model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $product_link_model: ProductLink model
|
*/
?>
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-pure btn-inverse slidePanel-close actions-top icon wb-close" aria-hidden="true"></button>
  </div>
  <h1><?= Yii::t('app', 'Add video'); ?></h1>
</header>
<div id="product-video-slidepanel-action" class="slidePanel-inner product-video-slidepanel-wrapper" data-action="create">
  <section class="slidePanel-inner-section">
    <?php
      // Render form
      $this->renderPartial('//commerce/productVideo/_form', [
        'product_model'       => $product_model,
        'product_link_model'  => $product_link_model,
        'current_action'      => 'create'
      ]);
    ?>
  </section>
</div>