<?php
/*
|--------------------------------------------------------------------------
| Product VIDEOS index page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', 'Product') .' '. $product_model->title();
?>

<div class="page-content container-fluid">
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'Videos'); ?></h3>
      <div class="panel-actions">
        <a id="product-video-create-btn" data-action="create" data-product="<?= $product_model->product_id; ?>" href="<?= Url::to('/commerce/productVideo/create', ['product_id' => $product_model->product_id]); ?>" class="btn btn-primary">
          <i class="icon wb-plus"></i> <?= Yii::t('app', 'Add video'); ?>
        </a>
      </div>
    </header>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
          <?php
            $this->renderPartial('//commerce/productVideo/_table_videos', [
              'product_model'     => $product_model,
              'is_view_mode'      => false
            ]);
          ?>
        </div>
      </div>
    </div>
  </div>
</div>