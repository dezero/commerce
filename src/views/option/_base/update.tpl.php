<?php
/*
|--------------------------------------------------------------------------
| Update form page for ProductOption model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_option_model: ProductOption model class
|  - $vec_config: Option type configuration
|  - $vec_translated_models: Array with TranslatedProductOption model class
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Current controller
  $current_controller = Yii::currentController();
  
  // Page title
  $this->pageTitle = $product_option_model->title();
?>

<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php if ( ! $product_option_model->is_enabled() ) : ?>
      <div class="badge badge-danger" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= Yii::t('app', 'DISABLED'); ?>"><?= Yii::t('app', 'DISABLED'); ?></div>
    <?php endif; ?>
  </h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => $product_option_model->text('entities_label'),
        'url' => ['/commerce/'. $current_controller],
      ],
      $this->pageTitle
    ]);
  ?>
  <div class="page-header-actions">
    <?=
      // Back
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app',  'Back'), ['/commerce/'. $current_controller], [
          'class' => 'btn btn-default btn-back',
      ]);
    ?>
  </div>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial($product_option_model->get_view_path('_form'), [
      'product_option_model'  => $product_option_model,
      'vec_config'            => $vec_config,
      'vec_translated_models' => $vec_translated_models,
      'default_language'      => $default_language,
      'vec_extra_languages'   => $vec_extra_languages,
      'form_id'               => $form_id,
      'button'                => Yii::t('app', 'Save')
    ]);
  ?>
</div>
