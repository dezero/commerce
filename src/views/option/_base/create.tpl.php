<?php
/*
|--------------------------------------------------------------------------
| Create form page for ProductOption model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_option_model: ProductOption model class
|  - $vec_config: Option type configuration
|  - $vec_translated_models: Array with TranslatedProductOption model class
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Current controller
  $current_controller = Yii::currentController();

  // Page title
  $this->pageTitle = Yii::t('app',  $product_option_model->text('create_title'));
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => $product_option_model->text('index_title'),
        'url' => ['/commerce/'. $current_controller],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial($product_option_model->get_view_path('_form'), [
      'product_option_model'  => $product_option_model,
      'vec_config'            => $vec_config,
      'vec_translated_models' => $vec_translated_models,
      'vec_extra_languages'   => $vec_extra_languages,
      'form_id'               => $form_id,
      'button'                => Yii::t('app', 'Save')
    ]);
  ?>
</div>