<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for ProductOption model
|--------------------------------------------------------------------------
|
| Available variables:
| $model: CGridView model
| $column: Column name
|
*/
  use dz\helpers\Html;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "NAME"
    |--------------------------------------------------------------------------
    */
    case 'name':
  ?>
    <?php $que_label = $model->name; ?>
    <?php if ( $model->translations ) : ?>
      <?php
        $vec_translated_name = [];
        foreach ( $model->translations as $translated_option_model )
        {
          if ( $translated_option_model->language_id !== Yii::defaultLanguage() )
          {
            $vec_translated_name[] = $translated_option_model->name;
          }
        }
        $que_label .= ' <span class="translated-name-field">('. implode(" ,", $vec_translated_name) .')</span>';
      ?>
    <?php endif; ?>
    <?= Html::link($que_label, ['update', 'id' => $model->option_id]); ?>
    <?php if ( ! $model->is_enabled() ) : ?>
      <span class="badge badge-danger ml-5 mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= $model->disable_date; ?>"><?= Yii::t('app', 'disabled'); ?></span>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "PRICE"
    |--------------------------------------------------------------------------
    */
    case 'price':
  ?>
    <?= $model->price !== null ? $model->price .' &euro;' : '-'; ?>
  <?php break; ?>
<?php endswitch; ?>