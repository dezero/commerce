<?php
/*
|--------------------------------------------------------------------------
| Form partial page for ProductOption model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_option_model: ProductOption model class
|  - $vec_config: Option type configuration
|  - $vec_translated_models: Array with TranslatedProductOption model class
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;
  
  $current_controller = Yii::currentController();
  $current_action = Yii::currentAction(true);

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class'   => 'form-horizontal option-form option-'. $this->option_type .'-form',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  $vec_error_models = [$product_option_model];
  if ( $current_action != 'create' && isset($vec_translated_models) && !empty($vec_translated_models) )
  {
    $vec_error_models = CMap::mergeArray($vec_error_models, array_values($vec_translated_models));
  }
  $errors = $form->errorSummary($vec_error_models);
  if ( $errors )
  {
    echo $errors;
  }
?>
<input name="StatusChange" id="status-change" class="form-control" type="hidden" value="">

<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= Yii::t('app', 'Option Information'); ?></h3>
  </header>
  <div class="panel-body">
    <?php
      /**
       * EXTRA LANGUAGES in TABS
       */
      if ( $current_action != 'create' && $vec_config['is_multilanguage'] ) :
    ?>
      <div class="nav-tabs-horizontal pt-10" data-plugin="tabs">
        <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist">
          <li class="nav-item" role="presentation">
            <a class="nav-link active" data-toggle="tab" href="#lang-<?= $default_language; ?>" aria-controls="lang-<?= $default_language; ?>" role="tab" aria-expanded="false" data-language="<?= $default_language; ?>"><span class="flag-icon flag-icon-<?= $default_language; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($default_language)); ?></a>
          </li>
          <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
            <?php foreach ( $vec_extra_languages as $language_id ) : ?>
              <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
                <li class="nav-item" role="presentation">
                  <a class="nav-link" data-toggle="tab" href="#lang-<?= $language_id; ?>" aria-controls="lang-<?= $language_id; ?>" role="tab" aria-expanded="false" data-language="<?= $language_id; ?>"><span class="flag-icon flag-icon-<?= $language_id; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($language_id)); ?></a>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>
        </ul>
      </div>
      <div class="tab-content pt-20">
        <div class="tab-pane active" id="lang-<?= $default_language; ?>" role="tabpanel" aria-expanded="false">
    <?php endif; ?>

    <div class="form-group row<?php if ( $product_option_model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($product_option_model, 'name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?php if ( $vec_config['is_editable'] ) : ?>
          <?=
            $form->textField($product_option_model, 'name', [
              'maxlength'   => 255,
              'placeholder' => '',
              'style'       => 'max-width: 400px;',
            ]);
          ?>
        <?php else : ?>
          <div class="item-content"><?= $product_option_model->name; ?></div>
        <?php endif; ?>
        <?= $form->error($product_option_model,'name'); ?>
      </div>
    </div>

    <?php
      //----------------------------------------------------------------
      // PRICE
      //----------------------------------------------------------------
      if ( isset($vec_config['is_price']) && $vec_config['is_price'] ) :
    ?>
      <div class="form-group form-currency-group row<?php if ( $product_option_model->hasErrors('price') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($product_option_model, 'price', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <?php if ( $vec_config['is_editable'] ) : ?>
            <?php
              // TouchSpin needs number as float (with "." for decimals)
              $product_option_model->price = Yii::app()->number->unformatNumber($product_option_model->price);
            ?>
            <?=
              $form->textField($product_option_model, 'price', [
                'placeholder' => '',
                'data-step'   => '0.01',
                'data-plugin' => 'dz-currency-touchspin'
              ]);
            ?>
            <?= $form->error($product_option_model, 'price'); ?>
            <p class="help-block">Extra price added to the product. Amount to 2 decimal places.</p>
          <?php else : ?>
            <div class="item-content"><?= $product_option_model->price; ?> &euro;</div>
          <?php endif; ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      /**
       * EXTRA LANGUAGES in TABS
       */
      if ( $current_action != 'create' && $vec_config['is_multilanguage'] ) :
    ?>
      </div><!-- .tab-pane -->
      <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
        <?php foreach ( $vec_extra_languages as $language_id ) : ?>
          <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
            <?php $translated_option_model = $vec_translated_models[$language_id]; ?>
            <div class="tab-pane" id="lang-<?= $language_id; ?>" role="tabpanel" aria-expanded="false">

              <div class="form-group row<?php if ( $translated_option_model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
                <?= $form->label($translated_option_model, 'name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                <div class="col-lg-10">
                  <?php if ( $vec_config['is_editable'] ) : ?>
                    <?=
                      $form->textField($translated_option_model, 'name', [
                        'name'        => 'TranslatedProductOption['. $language_id .'][name]',
                        'placeholder' => '',
                        'style'       => 'max-width: 400px;',
                      ]);
                    ?>
                    <?= $form->error($translated_option_model,'name'); ?>
                  <?php else : ?>
                    <div class="item-content"><?= $translated_option_model->name; ?></div>
                  <?php endif; ?>
                </div>
              </div>

            </div><!-- .tab-pane -->
          <?php endif; ?>
        <?php endforeach; ?>
        </div><!-- .tab-content -->
      <?php endif; ?>
    <?php endif; ?>
  </div>
</div>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACTIONS
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'  => 'submit',
          'type'        => 'primary',
          'label'       => $button
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/commerce/'. $current_controller], ['class' => 'btn btn-dark']);

        if ( $current_action !== 'create' )
        {
          if ( isset($vec_config['is_disable_allowed']) && $vec_config['is_disable_allowed'] )
          {
            // Disable option button
            if ( $product_option_model->is_enabled() )
            {
              echo Html::link(Yii::t('app', 'Disable'), ['#'], [
                'id'            => 'disable-option-btn',
                'class'         => 'btn btn-danger right',
                'data-confirm'  => $product_option_model->text('disable_confirm'),
                'data-form'     => $form_id,
                'data-value'    => 'disable',
                'data-plugin'   => 'dz-status-button'
              ]);
            }

            // Enable option button
            else
            {
              echo Html::link(Yii::t('app', 'Enable'), ['#'], [
                'id'            => 'enable-option-btn',
                'class'         => 'btn btn-success right',
                'data-confirm'  => $product_option_model->text('enable_confirm'),
                'data-form'     => $form_id,
                'data-value'    => 'enable',
                'data-plugin'   => 'dz-status-button'
              ]);
            }
          }

          // Delete option button
          if ( isset($vec_config['is_delete_allowed']) && $vec_config['is_delete_allowed'] )
          {
            echo Html::link(Yii::t('app', 'Delete'), ['#'], [
              'id'            => 'delete-option-btn',
              'class'         => 'btn btn-delete right',
              'data-confirm'  => $product_option_model->text('delete_confirm'),
              'data-form'     => $form_id,
              'data-value'    => 'delete',
              'data-plugin'   => 'dz-status-button'
            ]);
          }
        }
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>