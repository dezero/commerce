<?php
/*
|--------------------------------------------------------------------------
| Form partial page for ProductOption tree widget
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: ProductOption model class
|  - $is_ajax: Is this partial loaded via AJAX?
*/

  use dz\helpers\Html;
  use dz\helpers\Url;

  if ( ! $is_ajax )
  {
    $current_action = Yii::currentAction(true);
    if ( isset($_GET['id']) )
    {
      $current_id = $_GET['id'];
    }
  }
  else {
    $current_action = 'update';
    $current_id = 0;
  }

  // Current controller
  $current_controller = Yii::currentController();

  // Get all ProductOption models
  $vec_product_option_models = Yii::app()->productOptionManager->option_list($model->option_type, ['is_return_model' => true]);
?>
<?php if ( !$is_ajax ) : ?>
  <div id="option-loading-tree" class='dz-loading center hide'></div>
  <div class="dd dd-option-group" id="option-nestable-wrapper" data-name="option" data-url="<?= Url::to('/commerce/'. $current_controller .'/updateWeight', ['option_type' => $model->option_type]); ?>">
<?php endif; ?>
<?php if ( !empty($vec_product_option_models) ) : ?>
  <ol class="dd-list">
    <?php foreach ( $vec_product_option_models as $product_option_model ) : ?>
      <li class="dd-item dd3-item dd-item-group dd-level1" data-rel="level1" data-id="<?= $product_option_model->option_id; ?>" id="dd-item-<?= $product_option_model->option_id; ?>">
        <div class="dd-handle dd3-handle"></div>
        <div class="dd3-content">
          <?php
            $vec_html_attributes = [];
            if ( ! $product_option_model->is_enabled() )
            {
              $vec_html_attributes['class'] = 'text-danger';
            }
            echo Html::link($product_option_model->title(), ['/commerce/'. $current_controller .'/update', 'id' => $product_option_model->option_id], $vec_html_attributes);
          ?>
        </div>
      </li>
    <?php endforeach; ?>
  </ol>
<?php else : ?>
  <p><?= Yii::t('app', $model->text('empty_text')); ?></p>
<?php endif; ?>

<hr class="mb-20 mt-20">

<div class="buttons">
  <?=
    Html::link('<i class="icon wb-plus"></i> '. $model->text('add_button'), ['create'], [
      'class' => 'btn btn-primary btn-full'
    ]);
  ?>
</div>

<?php if ( ! $is_ajax ) : ?>
  </div>
<?php endif; ?>

<?php
// Custom Javascript nestable code for this page
if ( ! $is_ajax )
{
  Yii::app()->clientscript
    ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/jquery-nestable/jquery.nestable.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl. '/js/dz.nestable.js', CClientScript::POS_END);
}

// Load option nestable
Yii::app()->clientscript->registerScript('option_tree_js', "$('#option-nestable-wrapper').dzNestable();", CClientScript::POS_READY);
?>