<?php
/*
|--------------------------------------------------------------------------
| Form partial page for Category tree widget
|--------------------------------------------------------------------------
|
| Available variables:
|  - $category_model: Category model class
|  - $product_option_model: ProductOption model class
|  - $vec_config: Category configuration options
|  - $is_ajax: Is this partial loaded via AJAX?
*/
  use dz\modules\category\models\Category;
  use dz\helpers\Html;
  use dz\helpers\Url;

  $current_controller = Yii::currentController(true);

  // Get all categories from LEVEL 1
  $vec_categories_models = Yii::app()->categoryManager->getAllByDepth($category_model->category_type, 1);
?>
<?php if ( !$is_ajax ) : ?>
  <div id="category-loading-tree" class='dz-loading center hide'></div>
  <div class="dd dd-category-group" id="category-nestable-wrapper" data-name="category" data-url="<?= Url::to("/category/category/updateWeight"); ?>?category_id=0">
<?php endif; ?>
<?php if ( !empty($vec_categories_models) ) : ?>
  <ol class="dd-list">
    <?php foreach ( $vec_categories_models as $category_model ) : ?>
      <li class="dd-item dd3-item dd-item-group dd-level1" data-rel="level1" data-id="<?= $category_model->category_id; ?>" id="dd-item-<?= $category_model->category_id; ?>">
        <div class="dd-handle dd3-handle"></div>
        <div class="dd3-content">
          <?php
            $vec_html_attributes = [];
            if ( ! $category_model->is_enabled() )
            {
              $vec_html_attributes['class'] = 'text-danger';
            }
            echo Html::link($category_model->name, ['//commerce/'. $current_controller .'/update', 'id' => $category_model->category_id], $vec_html_attributes);
          ?>
        </div>
      </li>
    <?php endforeach; ?>
  </ol>
<?php else : ?>
  <p><?= $product_option_model->text('empty_text'); ?></p>
<?php endif; ?>
<?php if ( ! $is_ajax ) : ?>
  </div>
<?php endif; ?>
<?php if ( ! $is_ajax && $vec_config['is_editable'] ) : ?>
  <hr>
  <div class="buttons">
    <?php
      // Add new category
      echo Html::link('<i class="wb-plus"></i> '. $product_option_model->text('add_button') , ['//commerce/'. $current_controller .'/create'], [
          'class' => 'btn mr-10 mb-10 btn-primary',
      ]);
    ?>
  </div>
<?php endif; ?>
<?php
  // Custom Javascript nestable code for this page
  if ( ! $is_ajax )
  {
    Yii::app()->clientscript
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/jquery-nestable/jquery.nestable.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/js/dz.nestable.js', CClientScript::POS_END);
  }
    
  if ( ! $vec_config['is_editable'] )
  {
    // Load category nestable
    Yii::app()->clientscript->registerScript('category_tree_js',
      "$('#category-nestable-wrapper').dzNestable({
          maxDepth: 1,
          readOnly: true
      });"
      , CClientScript::POS_READY);
  }
  else
  {
    // Load category nestable
    Yii::app()->clientscript->registerScript('category_tree_js',
      "$('#category-nestable-wrapper').dzNestable({
        maxDepth: 1
      });"
      , CClientScript::POS_READY);
  }
?>