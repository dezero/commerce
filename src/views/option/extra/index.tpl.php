<?php
/*
|--------------------------------------------------------------------------
| Admin list page for EXTRAS
|--------------------------------------------------------------------------
|
| Available variables:
|  - $category_model: Category model class
|  - $product_option_model: ProductOption model class
|  - $vec_config: Category type configuration
|
*/  
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = Yii::t('app', $product_option_model->text('index_title'));
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <?php
      if ( $vec_config['is_editable'] && Yii::app()->user->checkAccess("commerce.option.create") ):
    ?>
      <div class="page-header-actions">
        <?=
          Html::link('<i class="icon wb-plus"></i> '. $product_option_model->text('add_button'), ['create'], [
            'class' => 'btn btn-primary'
          ]);
        ?>
      </div>
    <?php endif; ?>
  </div>
  <div class="page-content">
    <div class="row row-lg">
      <div class="col-lg-3">
        <div class="panel category-tree-wrapper">
          <header class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('app', $product_option_model->text('entities_label')); ?></h3>
          </header>
          <div class="panel-body">
            <?php
              /*
              |--------------------------------------------------------------------------
              | CATEGORY TREE (TABLE OF CONTENT)
              |--------------------------------------------------------------------------
              */
              $this->renderPartial($product_option_model->get_view_path("_tree_main"), [
                'category_model'        => $category_model,
                'product_option_model'  => $product_option_model,
                'vec_config'            => $vec_config,
                'is_ajax'               => false
              ]);
            ?>
          </div>
        </div>
      </div>
      <div class="col-lg-9">  
        <div class="panel panel-top-summary">
          <header class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('app', $product_option_model->text('list_title')); ?></h3>
          </header>
          <div class="panel-body container-fluid">
            <?php
              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                $this->widget('dz.grid.GridView', [
                  'id'            => 'option-'. $this->option_type .'-grid',
                  'dataProvider'  => $category_model->search(),
                  'filter'        => $category_model,
                  'emptyText'     => $product_option_model->text('empty_text'),
                  'enableHistory' => true,
                  'loadModal'     => true,
                  'enableSorting' => false,
                  'type'          => ['striped', 'hover'],
                  'beforeAjaxUpdate' => 'js:function() { $("html, body").animate({scrollTop: 0}, 100); }',
                  'columns'       => [
                    [
                      'name' => 'name',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("'. $product_option_model->get_view_path("_grid_column") .'", ["column" => "name", "model" => $data]))',
                    ],
                    [
                      'header' => $product_option_model->text('options_label'),
                      'value' => 'trim($this->grid->getOwner()->renderPartial("'. $product_option_model->get_view_path("_grid_column") .'", ["column" => "options", "model" => $data]))',
                      'filter' => false
                    ],
                    [
                      'class' => 'dz.grid.ButtonColumn',
                      'header' => Yii::t('app', 'ACTION'),
                      'templateWithAction' => '{update}',
                      'deleteButton' => false,
                      'clearButton' => true,
                      'viewButton' => false,
                      'updateButton' => true,
                      'menuAction' => function($data, $row) {
                        return [
                          [
                          'label' => Yii::t('app', 'Enable'),
                          'icon' => 'arrow-up',
                          'url' => ['/commerce/extra/enable', 'id' => $data->category_id],
                          'visible' => '!$data->is_enabled() && Yii::app()->user->checkAccess("commerce.option.create")',
                          'confirm' => $model->text('enable_confirm'),
                          'htmlOptions' => [
                            'id' => 'enable-extra-'. $data->option_type .'-'. $data->category_id,
                            'data-gridview' => 'option-'. $data->option_type .'-grid'
                            ]
                          ],
                          [
                            'label' => Yii::t('app', 'Disable'),
                            'icon' => 'arrow-down',
                            'url' => ['/commerce/extra/disable', 'id' => $data->category_id],
                            'visible' => '$data->is_enabled() && Yii::app()->user->checkAccess("commerce.option.create")',
                            'confirm' => $model->text('disable_confirm'),
                            'htmlOptions' => [
                              'id' => 'disable-extra-'. $data->option_type .'-'. $data->category_id,
                              'data-gridview' => 'option-'. $data->option_type .'-grid'
                            ]
                          ]
                        ];
                      },
                    ],
                  ],
                ]);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>