<?php
/*
|--------------------------------------------------------------------------
| Form partial page for ProductOption tree widget
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_option_model: ProductOption model class
|  - $category_model: Category model
|  - $is_ajax: Is this partial loaded via AJAX?
*/

  use dzlab\commerce\models\ProductOption;
  use dz\helpers\Html;
  use dz\helpers\Url;

  $current_action = 'update';
  $current_id = 0;
  if ( ! $is_ajax )
  {
    $current_action = Yii::currentAction(true);
    if ( isset($_GET['option_id']) )
    {
      $current_id = $_GET['option_id'];
    }
  }

  // Current controller
  $current_controller = Yii::currentController();

  // Get all ProductOption models
  $vec_product_option_models = Yii::app()->productOptionManager->get_options_by_category($category_model->category_id);
?>
<?php if ( !$is_ajax ) : ?>
  <div id="option-loading-tree" class='dz-loading center hide'></div>
  <div class="dd dd-option-group" id="option-nestable-wrapper" data-name="option" data-url="<?= Url::to('/commerce/'. $current_controller .'/updateWeight', ['option_type' => $product_option_model->option_type]); ?>">
<?php endif; ?>
<?php if ( !empty($vec_product_option_models) ) : ?>
  <ol class="dd-list">
    <?php foreach ( $vec_product_option_models as $product_option_model ) : ?>
      <li class="dd-item dd3-item dd-item-group dd-level1<?php if ( $current_id == $product_option_model->option_id) : ?> active<?php endif; ?>" data-rel="level1" data-id="<?= $product_option_model->option_id; ?>" id="dd-item-<?= $product_option_model->option_id; ?>">
        <div class="dd-handle dd3-handle"></div>
        <div class="dd3-content">
          <?php
            $vec_html_attributes = [];
            if ( ! $product_option_model->is_enabled() )
            {
              $vec_html_attributes['class'] = 'text-danger';
            }
            echo Html::link($product_option_model->title(), ['/commerce/'. $current_controller .'/update', 'id' => $category_model->category_id, 'option_id' => $product_option_model->option_id], $vec_html_attributes);
          ?>
        </div>
      </li>
    <?php endforeach; ?>
  </ol>
<?php else : ?>
  <p><?= Yii::t('app', $product_option_model->text('option_empty_text')); ?></p>
<?php endif; ?>

<?php if ( $current_action != 'create' ) : ?>
  <hr class="mb-20 mt-20">

  <div class="buttons">
    <?=
      Html::link('<i class="icon wb-plus"></i> '. $product_option_model->text('option_add_button'), ['create', 'id' => $category_model->category_id], [
        'class' => 'btn btn-primary btn-full'
      ]);
    ?>
  </div>
<?php endif; ?>

<?php if ( ! $is_ajax ) : ?>
  </div>
<?php endif; ?>

<?php
// Custom Javascript nestable code for this page
if ( ! $is_ajax )
{
  Yii::app()->clientscript
    ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/jquery-nestable/jquery.nestable.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl. '/js/dz.nestable.js', CClientScript::POS_END);
}

// Load option nestable
Yii::app()->clientscript->registerScript('option_tree_js', "$('#option-nestable-wrapper').dzNestable();", CClientScript::POS_READY);
?>