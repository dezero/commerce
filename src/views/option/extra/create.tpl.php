<?php
/*
|--------------------------------------------------------------------------
| Create form page for EXTRAS
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_option_model: ProductOption model class
|  - $category_model: Category model class (category_type = 'product_option_extra')
|  - $form_action: Form action: create_category or create_option
|  - $vec_config: Option type configuration
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Current controller
  $current_controller = Yii::currentController();

  // Page title
  if ( $form_action == 'create_category' )
  {
    $this->pageTitle = Yii::t('app',  $product_option_model->text('create_title'));
  }
  else
  {
    $this->pageTitle = $category_model->title(true) .' - '. Yii::t('app',  $product_option_model->text('option_create_title'));
  }
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?php
    // Breadcrumbs - Create a new category
    if ( $form_action == 'create_category' ) :
  ?>
    <?=
      // Breadcrumbs
      Html::breadcrumbs([
        [
          'label' => $product_option_model->text('index_title'),
          'url' => ['/commerce/'. $current_controller],
        ],
        $this->pageTitle
      ]);
    ?>
  <?php
    // Breadcrumbs - Create a new option
    else :
  ?>
    <?=
      // Breadcrumbs
      Html::breadcrumbs([
        [
          'label' => $product_option_model->text('index_title'),
          'url' => ['/commerce/'. $current_controller],
        ],
        [
          'label' => $category_model->title(true),
          'url' => ['/commerce/'. $current_controller .'/update', 'id' => $category_model->category_id],
        ],
        $this->pageTitle
      ]);
    ?>
  <?php endif; ?>
</div>

<div class="page-content container-fluid">
  <div class="row row-lg">
    <?php
      // Create a new category
      if ( $form_action == 'create_category' ) :
    ?>
      <div class="col-lg-12">
    <?php
      // Create a new option
      else :
    ?>
      <div class="col-lg-3">
        <div class="panel option-tree-wrapper">
          <header class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('app', $product_option_model->text('options_label')); ?></h3>
          </header>
          <div class="panel-body">
            <?php
            /*
            |--------------------------------------------------------------------------
            | CATEGORY TREE (TABLE OF CONTENT)
            |--------------------------------------------------------------------------
            */
              // Render form
              $this->renderPartial($product_option_model->get_view_path('_tree'), [
                'product_option_model'  => $product_option_model,
                'category_model'        => $category_model,
                'vec_config'            => $vec_config,
                'is_ajax'               => false
              ]);
            ?>
          </div>
        </div>
      </div>
      <div class="col-lg-9">
    <?php endif; ?>
      <?php
        // Render form
        $this->renderPartial($product_option_model->get_view_path('_form'), [
          'product_option_model'  => $product_option_model,
          'category_model'        => $category_model,
          'form_action'           => $form_action,
          'vec_config'            => $vec_config,
          'form_id'               => $form_id,
          'button'                => Yii::t('app', 'Save')
        ]);
      ?>
    </div>
  </div>
</div>