<?php
/*
|--------------------------------------------------------------------------
| Update form page for ProductOption model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_option_model: ProductOption model class
|  - $category_model: Category model class (category_type = 'product_option_extra')
|  - $vec_config: Option type configuration
|  - $vec_translated_models: Array with TranslatedProductOption model class
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Current controller
  $current_controller = Yii::currentController();
  
  // Page title
  if ( $form_action == 'update_category' )
  {
    $this->pageTitle = $category_model->title(true);
  }
  else
  {
    $this->pageTitle = $category_model->title(true) .' - '. $product_option_model->title();
  }
?>

<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php
      // Disabled category?
      if ( $form_action == 'update_category' && ! $category_model->is_enabled() ) :
    ?>
      <div class="badge badge-danger" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $category_model->disable_date; ?>">DISABLED</div>    
    <?php
      // Disabled option?
      elseif ( $form_action == 'update_option' && ! $product_option_model->is_enabled() ) :
    ?>
      <div class="badge badge-danger" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= $product_option_model->disable_date; ?>">DISABLED</div>    
    <?php endif; ?>
  </h1>
  <?php
    // Breadcrumbs - Update category
    if ( $form_action == 'update_category' ) :
  ?>
    <?=
      // Breadcrumbs
      Html::breadcrumbs([
        [
          'label' => $product_option_model->text('index_title'),
          'url' => ['/commerce/'. $current_controller],
        ],
        $this->pageTitle
      ]);
    ?>
    <div class="page-header-actions">
      <?=
        // Back
        Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app',  'Back'), ['/commerce/'. $current_controller], [
            'class' => 'btn btn-default btn-back',
        ]);
      ?>
    </div>
  <?php
    // Breadcrumbs - Update option
    else :
  ?>
    <?=
      // Breadcrumbs
      Html::breadcrumbs([
        [
          'label' => $product_option_model->text('index_title'),
          'url' => ['/commerce/'. $current_controller],
        ],
        [
          'label' => $category_model->title(true),
          'url' => ['/commerce/'. $current_controller .'/update', 'id' => $category_model->category_id],
        ],
        $this->pageTitle
      ]);
    ?>
    <div class="page-header-actions">
      <?=
        // Back
        Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app',  'Back'), ['/commerce/'. $current_controller.'/update', 'id' => $category_model->category_id], [
            'class' => 'btn btn-default btn-back',
        ]);
      ?>
    </div>
  <?php endif; ?>
</div>

<div class="page-content container-fluid">
  <div class="row row-lg">
    <div class="col-lg-3">
      <div class="panel option-tree-wrapper">
        <header class="panel-heading">
          <h3 class="panel-title"><?= Yii::t('app', $product_option_model->text('options_label')); ?></h3>
        </header>
        <div class="panel-body">
          <?php
          /*
          |--------------------------------------------------------------------------
          | CATEGORY TREE (TABLE OF CONTENT)
          |--------------------------------------------------------------------------
          */
            // Render form
            $this->renderPartial($product_option_model->get_view_path('_tree'), [
              'product_option_model'  => $product_option_model,
              'category_model'        => $category_model,
              'vec_config'            => $vec_config,
              'is_ajax'               => false
            ]);
          ?>
        </div>
      </div>
    </div>
    <div class="col-lg-9">
      <?php
        // Render form
        $this->renderPartial($product_option_model->get_view_path('_form'), [
          'product_option_model'  => $product_option_model,
          'category_model'        => $category_model,
          'form_action'           => $form_action,
          'vec_config'            => $vec_config,
          'vec_translated_models' => $vec_translated_models,
          'default_language'      => $default_language,
          'vec_extra_languages'   => $vec_extra_languages,
          'form_id'               => $form_id,
          'button'                => Yii::t('app', 'Save')
        ]);
      ?>
    </div>
  </div>
</div>
