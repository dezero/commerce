<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for ProductOption model
|--------------------------------------------------------------------------
|
| Available variables:
| $model: CGridView model
| $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "NAME"
    |--------------------------------------------------------------------------
    */
    case 'name':
  ?>
    <?php $extra_title = $model->title(); ?>
    <?php if ( $model->translations ) : ?>
      <?php
        $vec_translated_name = [];
        foreach ( $model->translations as $translated_option_model )
        {
          if ( $translated_option_model->language_id !== Yii::defaultLanguage() )
          {
            $vec_translated_name[] = $translated_option_model->name;
          }
        }
        $extra_title .= ' <span class="translated-name-field">('. implode(", ", $vec_translated_name) .')</span>';
      ?>
    <?php endif; ?>
    <a href="<?= Url::to('/commerce/extra/update', ['id' => $model->category_id]); ?>"><?= $extra_title; ?></a>
    <?php if ( ! $model->is_enabled() ) : ?>
      <div class="badge badge-danger ml-5 mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= $model->disable_date; ?>">DISABLED</div>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "OPTIONS"
    |--------------------------------------------------------------------------
    */
    case 'options':
  ?>
    <?php
      $vec_product_option_models = Yii::app()->productOptionManager->get_options_by_category($model->category_id);
      if ( !empty($vec_product_option_models) ) :
    ?>
      <ul>
        <?php foreach ( $vec_product_option_models as $product_option_model ) : ?>
          <li><a href="<?= Url::to('/commerce/extra/update', ['id' => $model->category_id, 'option_id' => $product_option_model->option_id]); ?>"><?= $product_option_model->title(); ?></a></li>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>