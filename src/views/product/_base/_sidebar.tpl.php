<?php
/*
|--------------------------------------------------------------------------
| Sidebar - Product model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Product model
|  - $vec_category_list: Array with Category list. Used as filter on Category column
|  - $vec_totals: Array with total numbers
|
*/

  use dz\helpers\Url;

  // Current controller
  $current_controller = Yii::currentController();

?>
<div class="page-aside">
  <div class="page-aside-switch">
    <i class="icon wb-chevron-left" aria-hidden="true"></i>
    <i class="icon wb-chevron-right" aria-hidden="true"></i>
  </div>
  <div class="page-aside-inner page-aside-scroll">
    <div data-role="container">
      <div data-role="content">
        <div class="page-aside-section">
          <div class="list-group mb-10">
            <a class="list-group-item justify-content-between<?php if ( empty($model->category_filter) ) : ?> active<?php endif; ?>" href="<?= Url::to('/commerce/'. $current_controller); ?>">
              <span><i class="icon wb-edit" aria-hidden="true"></i> All products</span>
            </a>
          </div>
        </div>
        <section class="page-aside-section">
          <div class="list-group mb-20">
            <?php foreach ( $vec_totals as $category_id => $total_products ) : ?>
              <?php if ( isset($vec_category_list[$category_id]) ) : ?>
                <a class="list-group-item<?php if ( $model->category_filter == $category_id ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/commerce/'. $current_controller, ['Product[category_filter]' => $category_id]); ?>">
                  <span><?= $vec_category_list[$category_id]; ?></span>
                  <span class="item-right"><?= $vec_totals[$category_id]; ?></span>
                </a>
              <?php endif; ?>
            <?php endforeach; ?>
        </section>
      </div>
    </div>
  </div>
</div>