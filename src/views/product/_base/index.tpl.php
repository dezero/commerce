<?php
/*
|--------------------------------------------------------------------------
| Admin list page for Product model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Product model
|  - $vec_config: Product type configuration
|  - $vec_category_list: Array with Category list. Used as filter on Category column
|
*/
  use dz\helpers\Html;

  // Page title
  $page_title = Yii::t('app', $model->text('index_title'));
  /*
  if ( $model->disable_filter == 1 )
  {
    $page_title .= ' - Inactive';
  }
  else if ( $model->disable_filter == 2 )
  {
    $page_title .= ' - Active';
  }
  */
  $this->pageTitle = Yii::t('app', $page_title);
?>
<?php
  // Sidebar menu with totals
  /*
  $this->renderPartial($model->get_view_path("_sidebar"), [
    'model'       => $model,
    'vec_totals'  => $vec_totals
  ]);
  */
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <?php
      // CREATE PRODUCT
      if ( Yii::app()->user->checkAccess("commerce.product.create") ):
    ?>
      <div class="page-header-actions">
        <?=
          Html::link('<i class="icon wb-plus"></i> '. $model->text('add_button'), ['create'], [
            'class' => 'btn btn-primary'
          ]);
        ?>
      </div>
    <?php endif; ?>
  </div>
  <div class="page-content">
    <div class="row row-lg">
      <div class="col-lg-12">
        <div class="panel">
          <div class="panel-body container-fluid">
            <?php
              /*
              |--------------------------------------------------------------------------
              | HEADER GRIDVIEW - Search & buttons
              |--------------------------------------------------------------------------
              */
              $this->renderPartial($model->get_view_path('_header_grid'), [
                'product_model' => $model,
                'product_type'  => $this->product_type,
                'grid_id'       => 'product-'. $this->product_type .'-grid'
              ]);
            ?>
            <?php
              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                $this->widget('dz.grid.GridView', [
                  'id'                => 'product-'. $this->product_type .'-grid',
                  'dataProvider'      => $model->search(),
                  'filter'            => $model,
                  'emptyText'         => $model->text('empty_text'),
                  'enableHistory'     => true,
                  'loadModal'         => true,
                  'enableSorting'     => false,
                  'enableSelect2'     => true,
                  'type'              => ['striped', 'hover'],
                  'beforeAjaxUpdate'  => 'js:function(id, options){ options.url += "&Product[disable_filter]='. $model->disable_filter .'"; $.dzGridView.beforeAjaxUpdate(id, options); }',
                  'columns'           => [
                    [
                      'name' => 'default_image_id',
                      'value' => 'trim($this->grid->getOwner()->renderPartial($data->get_view_path("_grid_column"), ["column" => "image", "model" => $data]))',
                      'filter' => false
                    ],
                    [
                      'name' => 'name',
                      'header' => Yii::t('app', 'Product'),
                      'value' => 'trim($this->grid->getOwner()->renderPartial($data->get_view_path("_grid_column"), ["column" => "name", "model" => $data]))',
                    ],
                    [
                      'name' => 'category_filter',
                      'filter' => $vec_category_list,
                      'value' => 'trim($this->grid->getOwner()->renderPartial($data->get_view_path("_grid_column"), ["column" => "category", "model" => $data]))',
                    ],
                    [
                      'name' => 'default_base_price',
                      'header' => Yii::t('app', 'Price'),
                      'value' => 'trim($this->grid->getOwner()->renderPartial($data->get_view_path("_grid_column"), ["column" => "price", "model" => $data]))',
                      'filter' => false
                    ],
                    [
                      'class' => 'dz.grid.ButtonColumn',
                      'header' => Yii::t('app', 'ACTION'),
                      'templateWithAction' => '{update}{images}',
                      'deleteButton' => false,
                      'clearButton' => true,
                      'viewButton' => false,
                      'updateButton' => true,
                      'buttons' => [
                        'update' => [
                          'label' => Yii::t('app', 'Update'),
                          'icon' => 'edit',
                          'url' => 'Yii::app()->createAbsoluteUrl("/commerce/'. $this->product_type .'/update", ["id" => $data->product_id])',
                        ],
                        'images' => [
                          'label' => Yii::t('app', 'Images'),
                          'icon' => 'image',
                          'url' => 'Yii::app()->createAbsoluteUrl("/commerce/productImage", ["product_id" => $data->product_id])',
                        ],
                      ],
                      'menuAction' => function($data, $row) {
                        return [
                          [
                            'label' => Yii::t('app', 'Orders'),
                            'icon' => 'order',
                            'url' => ['/commerce/orders', 'product_id' => $data->product_id]
                          ],
                          '---',
                          [
                            'label' => Yii::t('app', 'Enable'),
                            'icon' => 'arrow-up',
                            'url' => ['/commerce/product/enable', 'id' => $data->product_id],
                            'visible' => '!$data->is_enabled() && Yii::app()->user->checkAccess("commerce.product.create")',
                            'confirm' => $data->text('enable_confirm'),
                            'htmlOptions' => [
                              'id' => 'enable-product-'. $this->product_type .'-'. $data->product_id,
                              'data-gridview' => 'product-'. $this->product_type .'-grid'
                            ]
                          ],
                          [
                            'label' => Yii::t('app', 'Cancel'),
                            'icon' => 'arrow-down',
                            'url' => ['/commerce/product/disable', 'id' => $data->product_id],
                            'visible' => '$data->is_enabled() && Yii::app()->user->checkAccess("commerce.product.create")',
                            'confirm' => $data->text('disable_confirm'),
                            'htmlOptions' => [
                              'id' => 'disable-product-'. $this->product_type .'-'. $data->product_id,
                              'data-gridview' => 'product-'. $this->product_type .'-grid'
                            ]
                          ]
                        ];
                      },
                    ],
                  ],
                ]);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- .page-main -->