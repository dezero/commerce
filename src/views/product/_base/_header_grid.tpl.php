<?php
/*
|--------------------------------------------------------------------------
| Header zone for product Gridview
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $grid_id: GridView id
|
*/
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  $current_controller = StringHelper::strtolower($this->currentControllerName());
  $current_action = StringHelper::strtolower($this->currentActionName());
?>
<div class="page-content-actions <?= $grid_id; ?>-actions">
  <div class="float-right">
    <div class="grid-action-col grid-search-wrapper form-icons">
      <?php
        // Search form
        $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
          'id' => $grid_id .'-search-form',
          'method' => 'get',
          'action' => Url::to($this->route),
          'enableAjaxValidation' => false,
          'htmlOptions' => [
            'autocomplete' => 'off',
            'data-grid' => $grid_id
          ]
        ]);
      ?>
        <div class="input-group input-search-success">
          <i class="form-control-icon wb-search"></i>
          <?=
            $form->textField($product_model, 'global_search_filter', [
              'class'         => 'form-control form-control-search',
              'placeholder'   => Yii::t('app', 'Search'),
              'autocomplete'  => 'false',
            ]);
          ?>
        </div>
      <?php
        // End model form
        $this->endWidget();
      ?>
    </div>
  </div>
  <?php
  /*
  <div class="grid-action-col">
    <?php
      // Export Excel / CSV actions
      $this->renderPartial('//commerce/export/_actions', [
        'export_url'  => '/commerce/export',
        'model'       => $product_model,
        'export_id'   => 'products',
        'grid_id'     => $grid_id,
        'action_id'   => $current_action
      ]);
    ?>
  </div>
  */
  ?>
</div>