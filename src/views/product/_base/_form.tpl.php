<?php
/*
|--------------------------------------------------------------------------
| Form partial page for Product model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $variant_model: Variant model
|  - $image_model: AssetImage model
|  - $main_category_model: Main ProductCategory model
|  - $vec_config: Product configuration options
|  - $vec_product_category_models: Array with other ProductCategory models
|  - $vec_price_models: Array with ProductPrice models
|  - $vec_translated_models: Array with TranslatedProduct model class
|  - $vec_seo_models: Array with SEO models
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;
  use dz\modules\category\models\Category;
  use dzlab\commerce\models\ProductCategory;

  $current_controller = Yii::currentController();
  $current_action = Yii::currentAction(true);

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal product-form product-'. $this->product_type .'-form',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  $vec_error_models = [$product_model, $variant_model, $image_model];
  if ( $current_action != 'create' && isset($vec_translated_models) && !empty($vec_translated_models) )
  {
    $vec_error_models = CMap::mergeArray($vec_error_models, array_values($vec_translated_models));
  }
  $errors = $form->errorSummary($vec_error_models);
  if ( $errors )
  {
    echo $errors;
  }
?>
<input name="StatusChange" id="status-change" class="form-control" type="hidden" value="">

<?php
  /*
  |--------------------------------------------------------------------------
  | PRODUCT INFORMATION
  |--------------------------------------------------------------------------
  */
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= Yii::t('app', 'Product Information'); ?></h3>
  </header>

  <div class="panel-body">

    <div class="form-group row<?php if ( $variant_model->hasErrors('sku') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($variant_model, 'sku', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-4" style="max-width: 300px;">
        <?=
          $form->textField($variant_model, 'sku', [
            'maxlength' => 128,
            'placeholder' => ''
          ]);
        ?>
        <p class="help-block"><?= Yii::t('app', 'Optional. Product reference code.'); ?></p>
        <?= $form->error($variant_model, 'sku'); ?>
      </div>

      <?= $form->label($variant_model, 'ean_code', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-4" style="max-width: 300px;">
        <?=
          $form->textField($variant_model, 'ean_code', [
            'maxlength' => 16,
            'placeholder' => ''
          ]);
        ?>
        <p class="help-block"><?= Yii::t('app', 'Optional. EAN code.'); ?> <a href="https://www.ean-search.org/" target="_blank"><?= Yii::t('app', 'Search your product'); ?></a></p>
        <?= $form->error($variant_model, 'ean_code'); ?>
      </div>
    </div>

    <?php
      /**
       * EXTRA LANGUAGES in TABS
       */
      if ( $current_action != 'create' && $vec_config['is_multilanguage'] ) :
    ?>
      <div class="nav-tabs-horizontal pt-10" data-plugin="tabs">
        <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist">
          <li class="nav-item" role="presentation">
            <a class="nav-link active" data-toggle="tab" href="#lang-<?= $default_language; ?>" aria-controls="lang-<?= $default_language; ?>" role="tab" aria-expanded="false" data-language="<?= $default_language; ?>"><span class="flag-icon flag-icon-<?= $default_language; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($default_language)); ?></a>
          </li>
          <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
            <?php foreach ( $vec_extra_languages as $language_id ) : ?>
              <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
                <li class="nav-item" role="presentation">
                  <a class="nav-link" data-toggle="tab" href="#lang-<?= $language_id; ?>" aria-controls="lang-<?= $language_id; ?>" role="tab" aria-expanded="false" data-language="<?= $language_id; ?>"><span class="flag-icon flag-icon-<?= $language_id; ?>"></span> <?= StringHelper::strtoupper(Yii::app()->i18n->language_name($language_id)); ?></a>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>
        </ul>
      </div>
      <div class="tab-content pt-20">
        <div class="tab-pane active" id="lang-<?= $default_language; ?>" role="tabpanel" aria-expanded="false">
    <?php endif; ?>

    <div class="form-group row<?php if ( $product_model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($product_model, 'name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($product_model, 'name', [
            'maxlength' => 255,
            'placeholder' => ''
          ]);
        ?>
        <?= $form->error($product_model, 'name'); ?>
      </div>
    </div>

    <?php
      //----------------------------------------------------------------
      // DESCRIPTION
      //----------------------------------------------------------------
      if ( isset($vec_config['is_description']) && $vec_config['is_description'] ) :
    ?>
      <div class="form-group row<?php if ( $product_model->hasErrors('description') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($product_model, 'description', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <?php
            /*
            $this->widget('@lib.redactor.ERedactorWidget', [
              'model'     => $product_model,
              'attribute' => 'description',
              'options'   => [
                'minHeight' => 150,
              ]
            ]);
            */
            $this->widget('@ext.DzMarkdown.DzMarkdown', [
              'model'     => $product_model,
              'attribute' => 'description',
              // 'options'   => ['minHeight' => 150]
            ]);
          ?>
        </div>
      </div>
    <?php endif; ?>

    <?php
      /**
       * EXTRA LANGUAGES in TABS
       */
      if ( $current_action != 'create' && $vec_config['is_multilanguage'] ) :
    ?>
      </div><!-- .tab-pane -->
      <?php if ( isset($vec_extra_languages) && !empty($vec_extra_languages) ) : ?>
        <?php foreach ( $vec_extra_languages as $language_id ) : ?>
          <?php if ( isset($vec_translated_models[$language_id]) ) : ?>
            <?php $translated_product_model = $vec_translated_models[$language_id]; ?>
            <div class="tab-pane" id="lang-<?= $language_id; ?>" role="tabpanel" aria-expanded="false">

              <div class="form-group row<?php if ( $translated_product_model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
                <?= $form->label($translated_product_model, 'name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                <div class="col-lg-10">
                  <?=
                    $form->textField($translated_product_model, 'name', [
                      'name'        => 'TranslatedProduct['. $language_id .'][name]',
                      'placeholder' => '',
                    ]);
                  ?>
                  <?= $form->error($translated_product_model,'name'); ?>
                </div>
              </div>

              <?php if ( isset($vec_config['is_description']) && $vec_config['is_description'] ) : ?>
                <div class="form-group row<?php if ( $translated_product_model->hasErrors('description') ) : ?> has-danger<?php endif; ?>">
                  <?= $form->label($translated_product_model, 'description', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
                  <div class="col-lg-10">
                    <?php
                      /*
                      $form->textArea($translated_product_model, 'description', [
                        'name'      => 'TranslatedProduct['. $language_id .'][description]',
                        'rows'       => 3
                      ]);
                      */
                      $this->widget('@ext.DzMarkdown.DzMarkdown', [
                        'model'     => $translated_product_model,
                        'name'      => 'TranslatedProduct['. $language_id .'][description]',
                        'attribute' => 'description',
                        // 'options'   => ['minHeight' => 150]
                      ]);
                    ?>
                    <?= $form->error($translated_product_model,'description'); ?>
                  </div>
                </div>
              <?php endif; ?>

            </div><!-- .tab-pane -->
          <?php endif; ?>
        <?php endforeach; ?>
        </div><!-- .tab-content -->
      <?php endif; ?>
    <?php endif; ?>

  </div><!-- .panel-body -->
</div><!-- .panel -->

<?php
  //----------------------------------------------------------------
  // IMAGE (main image when the product is creating)
  //----------------------------------------------------------------
  if ( $current_action === 'create' && isset($vec_config['is_image']) && $vec_config['is_image'] ) :
?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'Images'); ?></h3>
    </header>

    <div class="panel-body">
      <div class="form-group row<?php if ( $image_model->hasErrors('file_name') ) : ?> has-danger<?php endif; ?>">
        <label class="col-lg-2 col-sm-2 form-control-label" for="AssetImage_file_name"><?= Yii::t('app', 'Image'); ?></label>
        <div class="col-lg-10">
          <?php
            $this->widget('\dz\widgets\Upload', [
              'model'     => $image_model,
              'attribute' => 'file_name',
              'view_mode' => false,
              'is_image'  => true,
            ]);
            /*
            $this->widget('@ext.DzFilepond.DzFilepond', [
              'model'     => $image_model,
              'attribute' => 'file_name',
            ]);
            */
          ?>
          <?= $form->error($image_model,'file_name'); ?>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<?php
  /*
  |--------------------------------------------------------------------------
  | CATEGORIES
  |--------------------------------------------------------------------------
  */
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= Yii::t('app', 'Categories'); ?></h3>
  </header>

  <div class="panel-body">
    <div class="form-group row<?php if ( $product_model->hasErrors('main_category_id') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($product_model, 'main_category_id', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10" style="max-width: 300px;">
        <?php
          $vec_product_categoy_values = $vec_product_category_models;
          if ( !empty($vec_product_category_models) && ! $errors )
          {
            $vec_product_categoy_values = ProductCategory::model()->get_category_values($vec_product_category_models);
          }
          echo Html::dropDownList(
            'Product[main_category_id]',
            $product_model->main_category_id,
            Category::model()->category_list($vec_config['main_category_type'], [
              'is_suffix_all'       => false,
              'is_select_prefix'    => true,
              'is_included_parent'  => true,
              'max_depth'           => 3
            ]),
            [
              'class'             => 'form-control',
              'placeholder'       => 'Select a category',
              'data-placeholder'  => 'Select a category',
              'empty'             => 'Select a category',
              'data-plugin'       => 'select2',
              'multiple'          => false,
              'data-allow-clear'  => true,
              'style'             => 'max-width: 300px',
              'autocomplete'      => 'false'
            ]);
        ?>
        <?= $form->error($product_model, 'main_category_id'); ?>
      </div>
    </div>

    <div class="form-group row<?php if ( $product_model->hasErrors('productCategories') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($product_model, 'productCategories', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?php
          echo Html::dropDownList(
            'ProductCategory[]',
            $vec_product_categoy_values,
            Category::model()->category_list($vec_config['main_category_type'], [
              'is_suffix_all'       => false,
              'is_select_prefix'    => true,
              'is_included_parent'  => true,
              'max_depth'           => 3
            ]),
            [
              'class'             => 'form-control',
              'placeholder'       => 'Select one or more categories',
              'data-placeholder'  => 'Select one or more categories',
              'empty'             => 'Select one or more categories',
              'data-plugin'       => 'select2',
              'style'             => 'max-width: 300px',
              'multiple'          => true,
              'autocomplete'      => 'false'
            ]);
        ?>
        <?= $form->error($product_model, 'productCategories'); ?>
      </div>
    </div>
  </div>
</div>


<?php
  /*
  |--------------------------------------------------------------------------
  | PRICES
  |--------------------------------------------------------------------------
  */
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= Yii::t('app', 'Prices'); ?></h3>
  </header>

  <div class="panel-body">
    <?php
      // BASE PRICE - TouchSpin needs number as float (with "." for decimals)
      $base_price_model = $vec_price_models['base_price'];
      $base_price_model->amount = Yii::app()->number->unformatNumber($base_price_model->amount);
    ?>
    <div class="form-group form-currency-group row<?php if ( $base_price_model->hasErrors('amount') ) : ?> has-danger<?php endif; ?>">
      <label class="col-lg-2 col-sm-2 form-control-label"><?= Yii::t('app', 'Base Price'); ?></label>
      <div class="col-lg-10">
       <?=
          $form->textField($base_price_model, 'amount', [
            'name'        => 'ProductPrice[base_price][amount]',
            'placeholder' => '',
            'data-step'   => '0.01',
            'data-plugin' => 'dz-currency-touchspin'
          ]);
        ?>
        <?= $form->error($base_price_model, 'amount'); ?>
        <p class="help-block">Amount to 2 decimal places.</p>
      </div>
    </div>

    <div class="form-group row<?php if ( $product_model->hasErrors('is_on_offer') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($product_model, 'is_on_offer', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <div class="form-group form-radio-group">
          <div class="radio-custom radio-default radio-inline">
            <input type="radio" id="is_on_offer-0" name="Product[is_on_offer]" value="0"<?php if ( $product_model->is_on_offer == 0 ) : ?> checked<?php endif; ?>>
            <label for="is_on_offer-0">No</label>
          </div>
          <div class="radio-custom radio-default radio-inline">
            <input type="radio" id="is_on_offer-1" name="Product[is_on_offer]" value="1"<?php if ( $product_model->is_on_offer == 1 ) : ?> checked<?php endif; ?>>
            <label for="is_on_offer-1">Yes</label>
          </div>
        </div>
        <?= $form->error($product_model, 'is_on_offer'); ?>
        <p class="help-block">If yes, <u>reduced price</u> will be applied.</p> 
      </div>
    </div>

    <?php
      // REDUCED PRICE - TouchSpin needs number as float (with "." for decimals)
      $reduced_price_model = $vec_price_models['reduced_price'];
      $reduced_price_model->amount = Yii::app()->number->unformatNumber($reduced_price_model->amount);
    ?>
    <div class="form-group form-currency-group row<?php if ( $reduced_price_model->hasErrors('amount') ) : ?> has-danger<?php endif; ?>">
      <label class="col-lg-2 col-sm-2 form-control-label"><?= Yii::t('app', 'Reduced Price'); ?></label>
      <div class="col-lg-10">
       <?=
          $form->textField($reduced_price_model, 'amount', [
            'name'        => 'ProductPrice[reduced_price][amount]',
            'placeholder' => '',
            'data-step'   => '0.01',
            'data-plugin' => 'dz-currency-touchspin'
          ]);
        ?>
        <?= $form->error($reduced_price_model, 'amount'); ?>
        <p class="help-block">Amount to 2 decimal places.</p>
      </div>
    </div>
  </div>
</div>

<?php
  /*
  |--------------------------------------------------------------------------
  | SEO DATA
  |--------------------------------------------------------------------------
  */
  if ( isset($vec_config['is_seo_fields']) && $vec_config['is_seo_fields'] && !empty($vec_seo_models) ) :
?>
  <?php
    // SEO form partial
    $this->renderPartial($product_model->get_view_path('_form_seo'), [
      'form'                => $form,
      'model'               => $product_model,
      'vec_config'          => $vec_config,
      'vec_seo_models'      => $vec_seo_models,
      'default_language'    => $default_language,
      'vec_extra_languages' => $vec_extra_languages,
      'current_action'      => $current_action
    ]);
  ?>
<?php endif; ?>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACTIONS
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'=> 'submit',
          'type'      => 'primary',
          'label'     => $button,
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/commerce/'. $current_controller], ['class' => 'btn btn-dark'] );

        if ( $current_action !== 'create' )
        {
          if ( isset($vec_config['is_disable_allowed']) && $vec_config['is_disable_allowed'] )
          {
            // Disable product button
            if ( $product_model->is_enabled() )
            {
              echo Html::link(Yii::t('app', 'Disable'), ['#'], [
                'id'            => 'disable-product-btn',
                'class'         => 'btn btn-danger right',
                'data-confirm'  => $product_model->text('disable_confirm'),
                'data-form'     => $form_id,
                'data-value'    => 'disable',
                'data-plugin'   => 'dz-status-button'
              ]);
            }

            // Enable product button
            else
            {
              echo Html::link(Yii::t('app', 'Enable'), ['#'], [
                'id'            => 'enable-product-btn',
                'class'         => 'btn btn-success right',
                'data-confirm'  => $product_model->text('enable_confirm'),
                'data-form'     => $form_id,
                'data-value'    => 'enable',
                'data-plugin'   => 'dz-status-button'
              ]);
            }
          }

          // Delete product button
          if ( Yii::app()->user->id == 1 || ( isset($vec_config['is_delete_allowed']) && $vec_config['is_delete_allowed'] ) )
          {
            echo Html::link(Yii::t('app', 'Delete'), ['#'], [
              'id'            => 'delete-product-btn',
              'class'         => 'btn btn-delete right',
              'data-confirm'  => $product_model->text('delete_confirm'),
              'data-form'     => $form_id,
              'data-value'    => 'delete',
              'data-plugin'   => 'dz-status-button'
            ]);
          }
        }
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>