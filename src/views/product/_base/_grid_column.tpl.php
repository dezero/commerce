<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for Product model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Product model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "name"
    |--------------------------------------------------------------------------
    */
    case 'name':
  ?>
    <a href="<?=  Url::to('/commerce/product/update', ['id' => $model->product_id]); ?>"><?= $model->name; ?></a>
    <?php if ( !empty($model->default_sku) ) : ?><br><?= $model->default_sku; ?><?php endif; ?>
    <?php
      // Disabled?
      if ( $model->is_disabled() ) :
    ?>
      <span class="badge badge-danger ml-5 mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= $model->disable_date; ?>"><?= Yii::t('app', 'disabled'); ?></span>
    <?php endif; ?>
    <?php if ( !empty($model->default_ean_code) ) : ?>
      <p class="ean-code">EAN: <?= $model->default_ean_code; ?></p>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "image"
    |--------------------------------------------------------------------------
    */
    case 'image':
  ?>
    <?php if ( $model->defaultImage ) : ?>
      <a href="<?= Url::to('/commerce/product/update', ['id' => $model->product_id]); ?>"><img src="<?= $model->defaultImage->image_url('small'); ?>"></a>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "price"
    |--------------------------------------------------------------------------
    */
    case 'price':
  ?>
    <p><?= $model->get_price(); ?> &euro;</p>
    <?php if ( $model->is_on_offer == 1 ) :?><i>(on offer)</i><?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "category"
    |--------------------------------------------------------------------------
    */
    case 'category':
  ?>
    <?php if ( $model->productCategories ) : ?>
      <ul>
        <?php
          // Uset "getRelated" to refresh the relationship data
          foreach ( $model->getRelated('productCategories', true) as $product_category_model ) :
        ?>
          <li><?= $product_category_model->category->title(); ?></li>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>
