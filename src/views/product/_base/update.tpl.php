<?php
/*
|--------------------------------------------------------------------------
| Update form page for Product model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $variant_model: Variant model
|  - $image_model: AssetImage model
|  - $main_category_model: Main ProductCategory model
|  - $vec_config: Category configuration options
|  - $vec_product_category_models: Array with other ProductCategory models
|  - $vec_price_models: Array with ProductPrice models
|  - $vec_translated_models: Array with TranslatedProduct model class
|  - $vec_seo_models: Array with SEO models
|  - $default_language: Default language code
|  - $vec_extra_languages: Array with Extra languages
|  - $form_id: Form identifier
|
*/
  // Header title
  $this->renderPartial($product_model->get_view_path('_header_title'), [
    'product_model' => $product_model,
    'vec_config'    => $vec_config,
  ]);
?>
<div class="page-content container-fluid">
  <?php
    // Header menu
    $this->renderPartial($product_model->get_view_path('_header_menu'), [
      'product_model' => $product_model,
      'vec_config'    => $vec_config,
    ]);

    // Render form
    $this->renderPartial($product_model->get_view_path('_form'), [
      'product_model'               => $product_model,
      'variant_model'               => $variant_model,
      'image_model'                 => $image_model,
      'main_category_model'         => $main_category_model,
      'vec_config'                  => $vec_config,
      'vec_product_category_models' => $vec_product_category_models,
      'vec_price_models'            => $vec_price_models,
      'vec_translated_models'       => $vec_translated_models,
      'vec_seo_models'              => $vec_seo_models,
      'vec_extra_languages'         => $vec_extra_languages,
      'default_language'            => $default_language,
      'form_id'                     => $form_id,
      'button'                      => Yii::t('app', 'Save')
    ]);
  ?>
</div>