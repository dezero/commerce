<?php
/*
|--------------------------------------------------------------------------
| Header menu zone for a Product model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $vec_config: Product configuration options
|
*/
  use dz\helpers\Url;

  // Controller and action names in lowercase
  $current_controller = Yii::currentController(true);
  $current_action = Yii::currentAction(true);
?>
<ul class="nav-quick-header nav-quick nav-quick-bordered row">
  <li class="nav-item col-md-4<?php if ( $current_action == 'update' ) : ?> active<?php endif; ?>">
    <a class="nav-link" href="<?= Url::to('/commerce/product/update', ['id' => $product_model->product_id]); ?>">
      <i class="icon wb-shopping-cart" aria-hidden="true"></i>
      <span><?= Yii::t('app', 'Product Information'); ?></span>
    </a>
  </li>
  <?php
    //----------------------------------------------------------------
    // IMAGE
    //----------------------------------------------------------------
    if ( isset($vec_config['is_image']) && $vec_config['is_image'] ) :
  ?>
    <li class="nav-item col-md-4<?php if ( $current_controller == 'productimage' ) : ?> active<?php endif; ?>">
      <a class="nav-link" href="<?= Url::to('/commerce/productImage', ['product_id' => $product_model->product_id]); ?>">
        <i class="icon wb-gallery" aria-hidden="true"></i>
        <span><?= Yii::t('app', 'Images & Videos'); ?></span>
      </a>
    </li>
  <?php endif; ?>
  <?php
    //----------------------------------------------------------------
    // RELATED PRODUCTS
    //----------------------------------------------------------------
    if ( isset($vec_config['is_related_products']) && $vec_config['is_related_products'] ) :
  ?>
    <li class="nav-item col-md-4<?php if ( $current_controller == 'productassociation' ) : ?> active<?php endif; ?>">
      <a class="nav-link" href="<?= Url::to('/commerce/productAssociation', ['product_id' => $product_model->product_id]); ?>">
        <i class="icon wb-table" aria-hidden="true"></i>
        <span><?= Yii::t('app', 'Related Products'); ?></span>
      </a>
    </li>
  <?php endif; ?>
</ul>