<?php
/*
|--------------------------------------------------------------------------
| Header title zone for a Product model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $vec_config: Product configuration options
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = $product_model->title();

  // Controller and action names in lowercase
  $current_controller = Yii::currentController(true);
  $current_action = Yii::currentAction(true);
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php if ( ! $product_model->is_enabled() ) : ?>
      <div class="badge badge-danger" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= Yii::t('app', 'DISABLED'); ?>"><?= Yii::t('app', 'DISABLED'); ?></div>
    <?php endif; ?>
  </h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => $product_model->text('index_title'),
        'url' => ['/commerce/'. $current_controller],
      ],
      $this->pageTitle
    ]);
  ?>
  <div class="page-header-actions">
    <?=
      // Back
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app',  'Back'), ['/commerce/'. $current_controller], [
          'class' => 'btn btn-default btn-back',
      ]);
    ?>
  </div>
</div>