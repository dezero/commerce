<?php
/*
|--------------------------------------------------------------------------
| Form partial page of a LineItem model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|  - $line_item_model: LineItem model
|  - $product_model: Product model
|  - $vec_extra_category_models: Array with Category models (category_type = "product_option_extra")
|  - $vec_selected_extras: Array with selected extras ['category_id' => 'option_id']
|  - $current_action: Current action name
|
*/

  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php
  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => 'line-item-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);
?>
  <div id="slidepanel-errors" class="messages-error alert alert-dismissible dark summary-errors alert-danger hide"></div>
  <div class="row">
    <label class="col-lg-3 form-control-label"><?= $line_item_model->getAttributeLabel('product_id'); ?></label>
    <div class="col-lg-9">
      <div class="form-control view-field">
        <a href="<?= Url::to('/commerce/product/update', ['id' => $line_item_model->product_id]); ?>" target="_blank" title="<?= Yii::t('app', 'Go to product'); ?>"><?= $line_item_model->title(); ?></a>
      </div>
    </div>
  </div>

  <div class="row">
    <label class="col-lg-3 pt-5 form-control-label"><?= Yii::t('app', 'SKU'); ?></label>
    <div class="col-lg-9">
      <?php
        echo Html::dropDownList('LineItem[variant_id]', $line_item_model->variant_id, $product_model->variant_list(), [
          'id'              => 'LineItem_variant_id',
          'data-init-value' => $line_item_model->variant_id
        ]);
      ?>
    </div>
  </div>

  <div class="row">
    <label class="col-lg-3 form-control-label"><?= $line_item_model->getAttributeLabel('unit_price'); ?></label>
    <div class="col-lg-4">
      <div class="form-control view-field text-right">
        <span id="line-item-product-price" data-price="<?= $line_item_model->raw_attributes['unit_product_price']; ?>"><?= $line_item_model->unit_product_price; ?></span> &euro;
      </div>
    </div>
    <div class="col-lg-5">
      <select id="LineItem_price_alias" data-init-value="<?= $line_item_model->price_alias; ?>" name="LineItem[price_alias]">
        <?php
          if ( $line_item_model->variant && $line_item_model->variant->prices ) :
        ?>
          <?php foreach ( $line_item_model->variant->prices as $product_price_model ) : ?>
            <?php $product_price_alias = !empty($product_price_model->price_alias) ? $product_price_model->price_alias : $product_price_model->price->alias; ?>
            <option value="<?= $product_price_alias; ?>" data-format-price="<?= $product_price_model->amount; ?>" data-price="<?= $product_price_model->raw_attributes['amount']; ?>"<?php if ( $product_price_model->price_alias === $line_item_model->price_alias ) : ?> selected<?php endif; ?>>
              <?= $product_price_model->price ? $product_price_model->price->title() : $product_price_alias; ?> (<?= $product_price_model->amount; ?> &euro;)
            </option>
          <?php endforeach; ?>
        <?php endif; ?>
      </select>
    </div>
  </div>

  <div class="form-group form-currency-group row">
    <label class="col-lg-3 form-control-label"><?= $line_item_model->getAttributeLabel('quantity'); ?></label>
    <div class="col-lg-9">
      <?=
        $form->textField($line_item_model, 'quantity', [
          'data-plugin'         => 'TouchSpin',
          // 'class'               => 'form-control touchspin quantity',
          'data-min'            => '1',
          'data-max'            => '999',
          'data-decimals'       => '0',
          // 'data-step'           => '1',
          // 'data-boostat'        => '1',
          // 'data-maxboostedstep' => '1',
          'placeholder'         => ''
        ]);
      ?>
    </div>
  </div>

  <div class="row">
    <label class="col-lg-3 form-control-label"><?= $line_item_model->getAttributeLabel('total_price'); ?></label>
    <div class="col-lg-4">
      <div class="form-control view-field text-right">
        <span id="line-item-total-price" data-price="<?= $line_item_model->raw_attributes['total_price']; ?>"><?= $line_item_model->total_price; ?></span> &euro;
      </div>
    </div>
  </div>

  <?php if ( !empty($vec_extra_category_models) ) : ?>
    <hr class="mb-30">
    <?php foreach ( $vec_extra_category_models as $extra_category_model ) : ?>
      <div class="row">
        <label class="col-lg-3 form-control-label"><?= $extra_category_model->name; ?></label>
        <div class="col-lg-9">
          <?php
            $vec_product_option_models = Yii::app()->productOptionManager->get_options_by_category($extra_category_model->category_id);
            if ( !empty($vec_product_option_models) ) :
          ?>
            <select class="product-option-select" name="ProductOption[<?= $extra_category_model->category_id; ?>]">
              <option disabled data-price="0"<?php if ( empty($vec_selected_extras) || !isset($vec_selected_extras[$extra_category_model->category_id]) ) : ?> selected<?php endif; ?>><?= Yii::t('app', 'Choose an option'); ?></option>
              <?php foreach ( $vec_product_option_models as $product_option_model ) : ?>
                <option value="<?= $product_option_model->option_id; ?>" data-price="<?= $product_option_model->raw_attributes['price']; ?>"<?php if ( !empty($vec_selected_extras) && isset($vec_selected_extras[$extra_category_model->category_id]) && $vec_selected_extras[$extra_category_model->category_id] == $product_option_model->option_id ) : ?> selected<?php endif; ?>>
                  <?= $product_option_model->title(); ?><?php if ( !empty($product_option_model->raw_attributes['price']) )  : ?> (+<?= $product_option_model->price; ?> &euro;)<?php endif; ?>
                </option>
              <?php endforeach; ?>
            </select>
          <?php endif; ?>
        </div>
      </div>
    <?php endforeach; ?>
  <?php endif; ?>

  <div class="row">
    <label class="col-lg-3 form-control-label"></label>
    <div class="col-lg-9 form-actions">
      <button id="line-item-save-btn" class="btn btn-primary" data-dismiss="modal" type="button" data-url="<?= Url::to('/commerce/lineItem/update', ['order_id' => $order_model->order_id, 'line_item_id' => $line_item_model->line_item_id]); ?>" data-line-item="<?= $line_item_model->line_item_id; ?>" data-order="<?= $line_item_model->order_id; ?>"><?= Yii::t('app', 'Save'); ?></button>
      <a class="btn btn-dark cancel slidePanel-close" href="#"><?= Yii::t('app', 'Cancel'); ?></a>
    </div>
  </div>
<?php
  // End model form
  $this->endWidget();
?>