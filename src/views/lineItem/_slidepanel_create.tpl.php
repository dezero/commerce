<?php
/*
|--------------------------------------------------------------------------
| Create action of an LineItem model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|  - $line_item_model: LineItem model
|  - $product_model: Product model
|
*/
  use dz\helpers\Html;
?>
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-pure btn-inverse slidePanel-close actions-top icon wb-close" aria-hidden="true"></button>
  </div>
  <h1><?= Yii::t('app', 'Add new product'); ?></h1>
</header>
<div id="line-item-slidepanel-action" class="slidePanel-inner line-item-slidepanel-wrapper" data-action="create">
  <section class="slidePanel-inner-section">
    <?php
      // Render form
      $this->renderPartial('//commerce/lineItem/_form_create', [
        'order_model'               => $order_model,
        'line_item_model'           => $line_item_model,
        'product_model'             => $product_model,
        'current_action'            => 'create'
      ]);
    ?>
  </section>
</div>