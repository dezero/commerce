<?php
/*
|--------------------------------------------------------------------------
| Form CREATE partial page of a LineItem model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|  - $line_item_model: LineItem model
|  - $product_model: Product model
|  - $current_action: Current action name
|
*/

  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php
  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => 'line-item-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);
?>
  <div id="slidepanel-errors" class="messages-error alert alert-dismissible dark summary-errors alert-danger hide"></div>
  <div class="row">
    <label class="col-lg-3 form-control-label"><?= $line_item_model->getAttributeLabel('product_id'); ?></label>
    <div class="col-lg-9">
      <select id="LineItem_product_id" name="LineItem[product_id]"><option></option></select>
    </div>
  </div>

  <div class="row">
    <label class="col-lg-3 form-control-label"></label>
    <div class="col-lg-9 form-actions">
      <button id="line-item-continue-btn" class="btn btn-primary" data-dismiss="modal" type="button" data-url="<?= Url::to('/commerce/lineItem/create', ['order_id' => $order_model->order_id]); ?>" data-order="<?= $line_item_model->order_id; ?>"><?= Yii::t('app', 'Continue'); ?></button>
      <a class="btn btn-dark cancel slidePanel-close" href="#"><?= Yii::t('app', 'Cancel'); ?></a>
    </div>
  </div>
<?php
  // End model form
  $this->endWidget();
?>