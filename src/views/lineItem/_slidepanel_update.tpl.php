<?php
/*
|--------------------------------------------------------------------------
| Update action of an LineItem model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|  - $line_item_model: LineItem model
|  - $product_model: Product model
|  - $vec_extra_category_models: Array with Category models (category_type = "product_option_extra")
|  - $vec_selected_extras: Array with selected extras ['category_id' => 'option_id']
|
*/
  use dz\helpers\Html;

  $line_item_title = $line_item_model->title();
?>
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-pure btn-inverse slidePanel-close actions-top icon wb-close" aria-hidden="true"></button>
  </div>
  <h1><?= !empty($line_item_title) ? $line_item_title : Yii::t('app', 'Update line item'); ?></h1>
</header>
<div id="line-item-slidepanel-action" class="slidePanel-inner line-item-slidepanel-wrapper" data-action="update">
  <section class="slidePanel-inner-section">
    <?php
      // Render form
      $this->renderPartial('//commerce/lineItem/_form_update', [
        'order_model'               => $order_model,
        'line_item_model'           => $line_item_model,
        'product_model'             => $product_model,
        'vec_extra_category_models' => $vec_extra_category_models,
        'vec_selected_extras'       => $vec_selected_extras,
        'current_action'            => 'update'
      ]);
    ?>
  </section>
</div>