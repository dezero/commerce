<?php
/*
|--------------------------------------------------------------------------
| Product list - Line Items table
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|  - $is_view_mode: View mode?
|
*/
  use dz\helpers\Url;

  // Recalculate "item_price" with all the discounts and "reduced_price" applied
  $items_price_raw = $order_model->recalculate_items_price();

  // Recalculate "discount_price" with all the discounts and "reduced_price" applied
  $discount_price_raw = $order_model->recalculate_discount_price();

  // Tax price
  $tax_price_raw = Yii::app()->orderManager->get_tax_amount($items_price_raw, 21);
?>
<div class="table-responsive">
  <table id="line-item-table" class="table-line-items table table-striped table-hover text-right" data-url="<?= Url::to('/commerce/lineItem', ['id' => $order_model->order_id]); ?>">
    <thead>
      <tr>
        <th class="text-left text-uppercase"><?= Yii::t('app', 'Products'); ?></th>
        <th class="text-right text-uppercase"><?= Yii::t('app', 'Price'); ?></th>
        <th class="text-right text-uppercase"><?= Yii::t('app', 'Quantity'); ?></th>
        <th class="text-right text-uppercase"><?= Yii::t('app', 'Total'); ?></th>
        <th class="center text-uppercase"><?= Yii::t('app', 'Discount'); ?></th>
        <th class="center text-uppercase"><?php if ( ! $is_view_mode ) : ?><?= Yii::t('app', 'Actions'); ?><?php endif; ?></th>
      </tr>
    </thead>
    <tbody>
      <?php if ( $order_model->lineItems ) : ?>
        <?php foreach ( $order_model->lineItems as $line_item_model ) : ?>
          <tr id="line-item-row-<?= $line_item_model->line_item_id; ?>">
            <td class="product-col text-left">
              <?php if ( $line_item_model->product->defaultImage ) : ?>
                <div class="image mr-25">
                  <a href="<?= Url::to('/commerce/product/update', array('id' => $line_item_model->product_id)); ?>" target="_blank"><img src="<?= $line_item_model->product->defaultImage->image_url('small'); ?>" alt="<?= $line_item_model->product->name; ?>"></a>
                </div>
              <?php endif; ?>
              <div class="title">
                <a href="<?= Url::to('/commerce/product/update', array('id' => $line_item_model->product_id)); ?>" target="_blank"><?= $line_item_model->product->name; ?></a>
                <br>SKU: <?= $line_item_model->variant->sku; ?>
                <?php if ( !empty($line_item_model->options) ) : ?>
                  <hr class="mb-5 mt-5"><?= Yii::t('app', 'Options'); ?>:
                  <ul>
                    <?php foreach ( $line_item_model->options as $line_item_option_model ) : ?>
                      <?php if ( $line_item_option_model->option && $line_item_option_model->option->category ) : ?>
                        <li><?= $line_item_option_model->option->category->title(); ?>: <?= $line_item_option_model->option->title(); ?><?php if ( isset($line_item_option_model->raw_attributes['total_price']) && !empty($line_item_option_model->raw_attributes['total_price'] ) ) : ?> (+<?= $line_item_option_model->total_price; ?> &euro;)<?php endif; ?></li>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </ul>
                <?php endif; ?>
              </div>
            </td>
            <td>
              <?php
                // Show price with APPLIED discount (only percentage discounts)
                if ( $line_item_model->discount && $line_item_model->discount->is_fixed_amount == 0 ) :
              ?>
                <?= $line_item_model->get_unit_price_with_discount(false); ?> &euro;
                <br><i><span class="strike"><?= $line_item_model->unit_price; ?> &euro;</span><i>
              <?php
                // Show price without discount or reduced price
                else :
              ?>
                <?= $line_item_model->unit_price; ?> &euro;
                <?php if ( $line_item_model->price_alias === 'reduced_price') : ?>
                  <br><i><span class="strike"><?= $line_item_model->product_base_price; ?> &euro;</span></i>
                <?php endif; ?>
              <?php endif; ?>
            </td>
            <td>
              <?= $line_item_model->quantity; ?>
            </td>
            <td>
              <?php
                // Show price with APPLIED discount (only percentage discounts)
                if ( $line_item_model->discount && $line_item_model->discount->is_fixed_amount == 0 ) :
              ?>
                <?= $line_item_model->get_total_price_with_discount(false); ?> &euro;
                <br><i><span class="strike"><?= $line_item_model->total_price; ?> &euro;</span></i>
              <?php
                // Show price without discount or reduced price
                else :
              ?>
                <?= $line_item_model->total_price; ?> &euro;
                <?php if ( $line_item_model->price_alias === 'reduced_price' ) : ?>
                  <br><i><span class="strike"><?= Yii::app()->number->formatNumber($line_item_model->quantity * $line_item_model->raw_attributes['product_base_price']); ?> &euro;</span></i>
                <?php endif; ?>
              <?php endif; ?>
            </td>
            <?php if ( !empty($line_item_model->discount) ) : ?>
              <td class="text-center"><a href="<?= Url::to('/commerce/discount/update', ['id' => $line_item_model->discount_id]); ?>" target="_blank"><?= $line_item_model->discount->code; ?></a><br><i>(<?= Yii::t('app', 'Discount'); ?> <?= $line_item_model->discount->amount_label(); ?>)</i></td>
            <?php
              // Show reduced price
              elseif ( $line_item_model->price_alias === 'reduced_price' && $line_item_model->product ) :
            ?>
              <td class="text-center"><i><?= Yii::app()->number->formatNumber($line_item_model->raw_attributes['total_price'] - ($line_item_model->quantity *  $line_item_model->product->get_base_price(true))); ?> &euro;</i></td>
            <?php else : ?>
              <td></td>
            <?php endif; ?>
            <?php if ( ! $is_view_mode ) : ?>
              <td class="actions-column center">
                <a class="update-action btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip" data-action="update" data-panel="line-item-slidepanel" href="<?= Url::to('/commerce/lineItem/update', ['order_id' => $line_item_model->order_id, 'line_item_id' => $line_item_model->line_item_id]); ?>" data-original-title="<?= Yii::t('app', 'Update'); ?>"><i class="wb-edit"></i></a>
                <a class="delete-action btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip" data-action="delete" data-panel="line-item-slidepanel" href="<?= Url::to('/commerce/lineItem/delete', ['order_id' => $line_item_model->order_id, 'line_item_id' => $line_item_model->line_item_id]); ?>" data-original-title="<?= Yii::t('app', 'Delete'); ?>" data-order="<?= $line_item_model->order_id; ?>" data-line-item="<?= $line_item_model->line_item_id; ?>"><i class="wb-trash"></i></a>
              </td>
            <?php else : ?>
              <td></td>
            <?php endif; ?>
          </tr>
        <?php endforeach; ?>
      <?php else : ?>
        <tr>
          <td colspan="6" class="center">
            <h4 class="pt-20"><?= Yii::t('app', 'No line items have been added'); ?></h4>
          </td>
        </tr>
      <?php endif; ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="4"><?= Yii::t('app', 'Subtotal (excluded tax)'); ?></td>
        <?php /*<td><?= $order_model->items_price; ?> &euro;</td>*/ ?>
        <td><?= Yii::app()->number->formatNumber($items_price_raw - $tax_price_raw); ?> &euro;</td>
        <td></td>
      </tr>
      <tr>
        <td colspan="4"><?= Yii::t('app', 'Tax'); ?></td>
        <?php /*<td><?= $order_model->items_price; ?> &euro;</td>*/ ?>
        <td><?= Yii::app()->number->formatNumber($tax_price_raw); ?> &euro;</td>
        <td></td>
      </tr>
      <tr>
        <td colspan="4"><?= Yii::t('app', 'Subtotal (included tax)'); ?></td>
        <?php /*<td><?= $order_model->items_price; ?> &euro;</td>*/ ?>
        <td><?= Yii::app()->number->formatNumber($items_price_raw); ?> &euro;</td>
        <td></td>
      </tr>
      <tr>
        <td colspan="4"><?= Yii::t('app', 'Shipping price'); ?></td>
        <td><?= $order_model->shipping_price; ?> &euro;</td>
        <?php if ( !empty($order_model->shipping_method_alias) ) : ?>
          <td class="text-center"><i><?= $order_model->shipping_method_alias; ?></i></td>
        <?php else : ?>
          <td></td>
        <?php endif; ?>
      </tr>
      <?php
        // if ( $order_model->raw_attributes['discount_price'] > 0 ) :
        if ( $discount_price_raw > 0 ) :
      ?>
        <tr>
          <td colspan="4"><?= Yii::t('app', 'Discount'); ?></td>
          <?php /*<td>- <?= $order_model->discount_price; ?> &euro;</td>*/ ?>
          <td>- <?= Yii::app()->number->formatNumber($discount_price_raw); ?> &euro;</td>
          <?php if ( !empty($order_model->coupon_code) ) : ?>
            <td class="text-center"><a href="<?= Url::to('/commerce/discount/update', ['id' => $order_model->discount_id]); ?>" target="_blank"><?= $order_model->coupon_code; ?></a></td>
          <?php else : ?>
            <td></td>
          <?php endif; ?>
        </tr>
      <?php endif; ?>
      <tr>
        <td colspan="4"><?= Yii::t('app', 'TOTAL'); ?></td>
        <td class="total"><?= $order_model->total_price; ?> &euro;</td>
        <td></td>
      </tr>
    </tfoot>
  </table>
</div>