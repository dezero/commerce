<?php
/*
|--------------------------------------------------------------------------
| Order LINE ITEM index page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', 'Order') .' '. $order_model->title();
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php if ( ! $order_model->is_total_paid() ) : ?>
      <span class="badge badge-danger"><?= Yii::t('app', 'UNPAID'); ?></span>
    <?php endif; ?>
  </h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ['/commerce/order'], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Manage Orders'),
        'url' => ['/commerce/order'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Header menu
    $this->renderPartial('//commerce/order/_header_menu', [
      'model' => $order_model
    ]);
  ?>

  <?php
    /*
    |--------------------------------------------------------------------------
    | PRODUCT LIST - LINE ITEMS
    |--------------------------------------------------------------------------
    */
  ?>
  <div id="products-panel" class="panel">
    <header class="panel-heading mb-10">
      <h3 class="panel-title pt-30"><?= Yii::t('app', 'Products List'); ?></h3>
      <ul class="panel-actions panel-actions-keep pt-10">
        <li>
          <a id="line-item-create-btn" href="<?= Url::to('/commerce/lineItem/create', ['order_id' => $order_model->order_id]); ?>" class="btn btn-primary btn-outline" data-action="create">
            <i class="icon wb-plus"></i> <?= Yii::t('app', 'Add Product'); ?>
          </a>
        </li>
      </ul>
    </header>
    <div class="panel-body panel-view-content">
      <?php
        $this->renderPartial('//commerce/lineItem/_table_products', [
          'order_model'       => $order_model,
          'is_view_mode'      => false
        ]);
      ?>
    </div>
  </div>
</div>