<?php
/*
|--------------------------------------------------------------------------
| Order VIEW page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $customer_model: Customer model
|  - $user_model: User model
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = $customer_model->title();
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php if ( $user_model->is_disabled() ) : ?>
      <span class="badge badge-danger"><?= Yii::t('app', 'DISABLED'); ?></span>
    <?php else : ?>
      <?php if ( $customer_model->is_newsletter == 1 ) : ?>
        <span class="badge badge-info" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= Yii::t('app', 'Subscribed Newsletter'); ?>"><?= Yii::t('app', 'NEWSLETTER'); ?></span>
      <?php endif; ?>
    <?php endif; ?>
  </h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ['/commerce/customer'], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Manage Customers'),
        'url' => ['/commerce/customer'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>
<div class="page-content container-fluid">
  
  <div id="customer-info-panel" class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'Customer Information'); ?> <a class="btn btn-xs btn-primary" href="<?= Url::to('/commerce/customer/update', ['id' => $customer_model->user_id]); ?>"><?= Yii::t('app', 'Change'); ?></a></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-sm-3">
          <h5><?= $user_model->getAttributeLabel('name'); ?></h5>
          <div class="item-content"><?= $user_model->fullname(); ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $customer_model->getAttributeLabel('email'); ?></h5>
          <div class="item-content"><a href="mailto:<?= $customer_model->email; ?>" target="_blank"><?= $customer_model->email; ?></a></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $user_model->getAttributeLabel('created_date'); ?></h5>
          <div class="item-content"><?= $user_model->created_date; ?></div>
        </div>

        <div class="col-sm-3">
          <h5><?= $user_model->getAttributeLabel('last_login_date'); ?></h5>
          <div class="item-content"><?= !empty($user_model->last_login_date) ? $user_model->last_login_date : Yii::t('app', 'Never'); ?></div>
        </div>
      </div>

      <?php /*
      <div class="row">
        <div class="col-sm-3">
          <h5><?= $customer_model->getAttributeLabel('is_newsletter'); ?></h5>
          <div class="item-content"><?= ($customer_model->is_newsletter == 1) ? Yii::t('app', 'Yes') : Yii::t('app', 'No'); ?></div>
        </div>
      </div>
      */ ?>
    </div>
  </div>

  <?php
    /*
    |--------------------------------------------------------------------------
    | CUSTOMER ADDRESSES
    |--------------------------------------------------------------------------
    */
  ?>
  <?php
    // SHIPPING ADDRESS
    if ( $customer_model->shippingAddress ) :
  ?> 
    <div id="shipping-address-panel" class="panel address-panel">
      <header class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('app', 'Shipping Address') ;?> <a class="btn btn-xs btn-primary" href="<?= Url::to('/commerce/customer/update', ['id' => $customer_model->user_id]); ?>#shipping"><?= Yii::t('app', 'Change'); ?></a></h3>
      </header>
      <div class="panel-body panel-view-content">
        <?php
          $this->renderPartial('//commerce/address/_view', [
            'address_model' => $customer_model->shippingAddress,
            'address_type'  => 'shipping',
          ]);
        ?>
      </div>
    </div>
  <?php endif; ?>
  <?php
    // BILLING ADDRESS
    if ( $customer_model->billingAddress ) :
  ?> 
    <div id="billing-address-panel" class="panel address-panel">
      <header class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('app', 'Billing Address'); ?> <a class="btn btn-xs btn-primary" href="<?= Url::to('/commerce/customer/update', ['id' => $customer_model->user_id]); ?>"><?= Yii::t('app', 'Change'); ?></a></h3>
      </header>
      <div class="panel-body panel-view-content">
        <?php
          $this->renderPartial('//commerce/address/_view', [
            'address_model' => $customer_model->billingAddress,
            'address_type'  => 'billing',
          ]);
        ?>
      </div>
    </div>
  <?php endif; ?>

  <?php
    /*
    |--------------------------------------------------------------------------
    | ORDER HISTORY
    |--------------------------------------------------------------------------
    */
  ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'Order History'); ?></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-sm-12">
          <?php
            $this->renderPartial('//commerce/customer/_order_history', [
              'customer_model'   => $customer_model
            ]);
          ?>
        </div>
      </div>
    </div>
  </div>

  <?php
    /*
    |--------------------------------------------------------------------------
    | MAIL HISTORY
    |--------------------------------------------------------------------------
    */
  ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'Mail History'); ?></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-sm-12">
          <?php
            $this->renderPartial('//commerce/customer/_mail_history', [
              'customer_model'   => $customer_model
            ]);
          ?>
        </div>
      </div>
    </div>
  </div>
</div>