<?php
/*
|--------------------------------------------------------------------------
| Form partial page for Customer model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $customer_model: Customer model
|  - $user_model: User model
|  - $billing_address_model: CustomerAddress model for billing address
|  - $shipping_address_model: CustomerAddress model for shipping address
|  - $vec_country_list: Array with full country list
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal customer-form-wrapper',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  $vec_form_models = [$customer_model, $user_model];
  if ( $billing_address_model )
  {
    $vec_form_models[] = $billing_address_model;
  }
  if ( $shipping_address_model )
  {
    $vec_form_models[] = $shipping_address_model;
  }
  $errors = $form->errorSummary($vec_form_models);
  if ( $errors )
  {
    echo $errors;
  }
?>
<?php
  /*
  |--------------------------------------------------------------------------
  | CUSTOMER INFORMATION
  |--------------------------------------------------------------------------
  */
?>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= Yii::t('app', 'Customer Information'); ?></h3>
  </header>
  <div class="panel-body">
    <div class="row">
      <div class="col-lg-6">
        <div class="form-group row<?php if ( $user_model->hasErrors('firstname') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'firstname', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($user_model, 'firstname', [
                'maxlength' => 100,
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($user_model, 'firstname'); ?>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="form-group row<?php if ( $user_model->hasErrors('lastname') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'lastname', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($user_model, 'lastname', [
                'maxlength' => 100,
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($user_model, 'lastname'); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <div class="form-group row<?php if ( $user_model->hasErrors('email') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'email', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($user_model, 'email', [
                'maxlength' => 255,
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($user_model, 'email'); ?>
            <p class="help-block"><?= Yii::t('app', 'It will be used as email login'); ?></p>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="form-group row<?php if ( $user_model->hasErrors('status_type') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($user_model, 'status_type', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->dropDownList($user_model, 'status_type', $customer_model->status_type_labels(), [
                'class'       => 'form-control',
                'data-plugin' => 'select2',
                'style'       => 'max-width: 300px',
              ]);
            ?>
            <?= $form->error($user_model,'status'); ?>
            <p class="text-help"><?= Yii::t('app', 'DISABLED users cannot access to the system'); ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <div class="form-group row<?php if ( $customer_model->hasErrors('is_newsletter') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($customer_model, 'is_newsletter', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <div class="form-group form-radio-group">
              <div class="radio-custom radio-default radio-inline">
                  <input type="radio" id="is_newsletter-1" name="Customer[is_newsletter]" value="1"<?php if ( $customer_model->is_newsletter == 1 ) : ?> checked<?php endif; ?>>
                  <label for="is_newsletter-1">Yes</label>
                </div>
                <div class="radio-custom radio-default radio-inline">
                  <input type="radio" id="is_newsletter-0" name="Customer[is_newsletter]" value="0"<?php if ( $customer_model->is_newsletter == 0 ) : ?> checked<?php endif; ?>>
                  <label for="is_newsletter-0">No</label>
                </div>
              </div>
            <?= $form->error($customer_model,'is_newsletter'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
  /*
  |--------------------------------------------------------------------------
  | SHIPPING ADDRESS
  |--------------------------------------------------------------------------
  */
  if ( $shipping_address_model ) :
?>
  <?php
    $this->renderPartial('//commerce/address/_form', [
      'form'              => $form,
      'address_model'     => $shipping_address_model,
      'address_type'      => 'shipping',
      'vec_country_list'  => $vec_country_list,
      'panel_title'       => Yii::t('app', 'Shipping Address'),
      'is_include_email'  => false
    ]);
  ?>
<?php endif; ?>
<?php
  /*
  |--------------------------------------------------------------------------
  | BILLING ADDRESS
  |--------------------------------------------------------------------------
  */
  if ( $billing_address_model ) :
?>
  <?php
    $this->renderPartial('//commerce/address/_form', [
      'form'              => $form,
      'address_model'     => $billing_address_model,
      'address_type'      => 'billing',
      'vec_country_list'  => $vec_country_list,
      'panel_title'       => Yii::t('app', 'Billing Address'),
      'is_include_email'  => false
    ]);
  ?>
<?php endif; ?>
<?php
  /*
  |--------------------------------------------------------------------------
  | ACTIONS
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'  => 'submit',
          'type'        => 'primary',
          'label'       => Yii::t('app', 'Save')
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/commerce/customer/view', 'id' => $customer_model->user_id], ['class' => 'btn btn-dark']);
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>