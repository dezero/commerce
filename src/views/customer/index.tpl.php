<?php
/*
|--------------------------------------------------------------------------
| Admin list page for Customer model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $customer_model: Customer model class
|  - $vec_totals: Array with totals customers grouped by status
|
*/  
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = Yii::t('app', 'Manage Customers');
?>
<?php
  // Sidebar menu with totals
  $this->renderPartial('//commerce/customer/_sidebar', [
    'customer_model'  => $customer_model,
    'vec_totals'      => $vec_totals
  ]);
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  </div>
  <div class="page-content">
    <div class="row row-lg">
      <div class="col-lg-12">
        <div class="panel">
          <div class="panel-body container-fluid">
            <?php
              /*
              |--------------------------------------------------------------------------
              | HEADER GRIDVIEW - Search & buttons
              |--------------------------------------------------------------------------
              */
              $this->renderPartial('//commerce/customer/_header_grid', [
                'customer_model' => $customer_model,
              ]);
            ?>
            <?php
              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                $this->widget('dz.grid.GridView', [
                  'id'                => 'customer-grid',
                  'dataProvider'      => $customer_model->search(),
                  'filter'            => $customer_model,
                  'emptyText'         => Yii::t('app', 'No customers have been found'),
                  'enableHistory'     => false,
                  'loadModal'         => true,
                  'enableSorting'     => false,
                  'enableSelect2'     => false,
                  'type'              => ['striped', 'hover'],
                  'beforeAjaxUpdate'  => 'js:function(id, options){ $.dzGridView.beforeAjaxUpdate(id, options); }',
                  // 'afterAjaxUpdate'   => 'js:function() { $.customerGridUpdate(); }',
                  'columns'           => [
                    [
                      'header' => 'Customer',
                      'name' => 'name_filter',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/customer/_grid_column", ["column" => "name", "model" => $data]))',
                    ],
                    [
                      'header' => 'Email',
                      'name' => 'email',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/customer/_grid_column", ["column" => "email", "model" => $data]))',
                    ],
                    [
                      'header' => 'Phone',
                      'name' => 'phone_filter',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/customer/_grid_column", ["column" => "phone", "model" => $data]))',
                    ],
                    [
                      'header' => 'Last order',
                      'name' => 'orders',
                      'filter' => false,
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/customer/_grid_column", ["column" => "last_order", "model" => $data]))',
                    ],
                    [
                      'class' => 'dz.grid.ButtonColumn',
                      'header' => Yii::t('app', 'ACTION'),
                      'template' => '{view}{update}',
                      'clearButton' => true,
                      'viewButton' => true,
                      'updateButton' => true,
                      'deleteButton' => false,
                    ],
                  ],
                ]);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- .page-content -->
</div><!-- .page-main -->