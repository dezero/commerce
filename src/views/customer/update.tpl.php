<?php
/*
|--------------------------------------------------------------------------
| Customer UPDATE page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $customer_model: Customer model
|  - $user_model: User model
|  - $billing_address_model: CustomerAddress model for billing address
|  - $shipping_address_model: CustomerAddress model for shipping address
|  - $vec_country_list: Array with full country list
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = $customer_model->title();;
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php if ( $user_model->is_disabled() ) : ?>
      <span class="badge badge-danger"><?= Yii::t('app', 'DISABLED'); ?></span>
    <?php else : ?>
      <?php if ( $customer_model->is_newsletter == 1 ) : ?>
        <span class="badge badge-info" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= Yii::t('app', 'Subscribed Newsletter'); ?>"><?= Yii::t('app', 'NEWSLETTER'); ?></span>
      <?php endif; ?>
    <?php endif; ?>
  </h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ['/commerce/customer'], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Manage Customers'),
        'url' => ['/commerce/customer'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('_form', [
      'customer_model'          => $customer_model,
      'user_model'              => $user_model,
      'billing_address_model'   => $billing_address_model,
      'shipping_address_model'  => $shipping_address_model,
      'vec_country_list'        => $vec_country_list,
      'form_id'                 => $form_id
    ]);
  ?>
</div>