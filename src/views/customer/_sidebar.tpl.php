<?php
/*
|--------------------------------------------------------------------------
| Sidebar - Customer model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $customer_model: Customer model
|  - $vec_totals: Array with total numbers
|
*/

  use dz\helpers\Url;

  $vec_status_labels = Yii::app()->customerManager->status_labels();
?>
<div class="page-aside">
  <div class="page-aside-switch">
    <i class="icon wb-chevron-left" aria-hidden="true"></i>
    <i class="icon wb-chevron-right" aria-hidden="true"></i>
  </div>
  <div class="page-aside-inner page-aside-scroll">
    <div data-role="container">
      <div data-role="content">
        <div class="page-aside-section">
          <div class="list-group mb-10">
            <a class="list-group-item justify-content-between<?php if ( empty($customer_model->status_filter) ) : ?> active<?php endif; ?>" href="<?= Url::to('/commerce/customer'); ?>">
              <span><i class="icon wb-user-circle" aria-hidden="true"></i> <?= Yii::t('app', 'All customers'); ?></span>
            </a>
          </div>
        </div>
        <section class="page-aside-section">
          <div class="list-group mb-20">
            <?php foreach ( $vec_status_labels as $status_type => $status_label ) : ?>
              <?php if ( isset($vec_totals[$status_type]) ) : ?>
                <a class="list-group-item<?php if ( $customer_model->status_filter == $status_type ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/commerce/customer', ['Customer[status_filter]' => $status_type]); ?>">
                  <span><?= $status_label; ?></span>
                  <span class="item-right"><?= $vec_totals[$status_type]; ?></span>
                </a>
              <?php endif; ?>
            <?php endforeach; ?>
        </section>
      </div>
    </div>
  </div>
</div>