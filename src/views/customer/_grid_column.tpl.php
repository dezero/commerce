<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for Customer model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Customer model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "customer"
    |--------------------------------------------------------------------------
    */
    case 'name':
  ?>
    <?php if ( $model->user_id > 0 && $model->user ) : ?>
      <a href="<?= Url::to('/commerce/customer/view', ['id' => $model->user_id]); ?>">#<?= $model->user_id; ?></a><br>
      <?= $model->user->fullname(); ?>
      <?php
        // Disabled user?
        if ( $model->user->is_disabled() ) :
      ?>
        <span class="badge badge-danger ml-5 mr-5" data-toggle="tooltip" data-placement="top" data-original-title="<?php if ( !empty($model->user->disable_date) ) : ?>From <?= $model->user->disable_date; ?><?php else : ?><?= Yii::t('app', 'Disabled'); ?><?php endif; ?>"><?= Yii::t('app', 'DISABLED'); ?></span>
      <?php endif; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "email"
    |--------------------------------------------------------------------------
    */
    case 'email':
  ?>
    <a href="mailto:<?= $model->email; ?>" target="_blank"><?= $model->email; ?></a>
    <p>
    <?php if ( Yii::app()->customerManager->is_guest($model->user_id) ) : ?>
      <span class="badge badge-default" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= Yii::t('app', 'Customer NOT registered'); ?>"><?= Yii::t('app', 'GUEST'); ?></span>
    <?php endif; ?>
    <?php if ( $model->is_newsletter == 1 ) : ?>
      <span class="badge badge-info" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= Yii::t('app', 'Subscribed to Newsletter'); ?>"><?= Yii::t('app', 'NEWSLETTER'); ?></span>
    <?php endif; ?>
  </p>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "phone"
    |--------------------------------------------------------------------------
    */
    case 'phone':
  ?>
    <?php if ( $model->shippingAddress ) : ?>
      <p><?= $model->shippingAddress->phone; ?></p>
    <?php endif; ?>
    <?php if ( $model->billingAddress ) : ?>
      <p><?= $model->billingAddress->phone; ?></p>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "last_order"
    |--------------------------------------------------------------------------
    */
    case 'last_order':
  ?>
    <?php if ($model->lastOrder ) : ?>
      <a href="<?=  Url::to('/commerce/order/view', ['id' => $model->lastOrder->order_id]); ?>" target="_blank"><?= $model->lastOrder->title(); ?></a>
      <br><?= $model->lastOrder->ordered_date; ?>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>