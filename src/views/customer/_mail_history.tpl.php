<?php
/*
|--------------------------------------------------------------------------
| Mail history table
|--------------------------------------------------------------------------
|
| Available variables:
|  - $customer_model: Cusomter model
|
*/

  use dz\helpers\Url;

?>
<div class="table-responsive">
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th><?= Yii::t('app', 'Date'); ?></th>
        <th><?= Yii::t('app', 'Mail'); ?></th>
        <th><?= Yii::t('app', 'Recipient'); ?></th>
        <th><?= Yii::t('app', 'Subject'); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php if ( $customer_model->mailHistory ) : ?>
        <?php foreach( $customer_model->mailHistory as $mail_history_model ) : ?>
          <tr>
            <td><?= $mail_history_model->created_date; ?></td>
            <td>
                <?= $mail_history_model->template ? $mail_history_model->template->name .'<br><small>'. $mail_history_model->template_alias .'</small>' : '<i>'. Yii::t('app', 'Free email') .'</i>'; ?>
            </td>
            <td><?= $mail_history_model->recipient_emails; ?></td>
            <td><a href="<?= Url::to('/admin/mail/view', ['id' => $mail_history_model->history_id]); ?>" target="_blank"><?= $mail_history_model->subject; ?></a></td>
          </tr>
        <?php endforeach; ?>
      <?php else : ?>
        <tr>
          <td colspan="4"><?= Yii::t('app', 'No emails have been sent') ; ?></td>
        </tr>
      <?php endif; ?>
    </tbody>
  </table>
</div>
<a class="btn btn-primary mr-10" href="<?= Url::to('/admin/mail/send', ['user_id' => $customer_model->user_id]); ?>" target="_blank"><i class="wb-envelope pr-5"></i> <?= Yii::t('app', 'Send Email'); ?></a>