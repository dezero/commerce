<?php
/*
|--------------------------------------------------------------------------
| Order summary table
|--------------------------------------------------------------------------
|
| Available variables:
|  - $customer_model: Order model
|
*/
  use dz\helpers\Url;
?>
<div class="table-responsive">
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th><?= Yii::t('app', 'Order'); ?></th>
        <th><?= Yii::t('app', 'Customer'); ?></th>
        <th><?= Yii::t('app', 'Price'); ?></th>
        <th><?= Yii::t('app', 'Date'); ?></th>
        <th><?= Yii::t('app', 'Status'); ?></th>
        <th><?= Yii::t('app', 'Action'); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php if ( $customer_model->orders ) : ?>
        <?php foreach( $customer_model->orders as $order_model ) : ?>
          <tr>
            <td>
              <?php
                $this->renderPartial('//commerce/order/_grid_column', [
                  'column' => 'order_number',
                  'model' => $order_model
                ]);
              ?>
            </td>
            <td>
              <?php
                $this->renderPartial('//commerce/order/_grid_column', [
                  'column' => 'customer',
                  'model' => $order_model
                ]);
              ?>
            </td>
            <td><?= $order_model->total_price; ?> &euro;</td>
            <td><?= $order_model->ordered_date; ?></td>
            <td class="col-status">
              <?php
                $this->renderPartial('//commerce/order/_view_status', [
                  'vec_status_types' => $order_model->status_type_labels(),
                  'status_type' => $order_model->status_type
                ]);
              ?>
            </td>
            <td>
              <a class="update-action btn btn-sm btn-icon btn-pure btn-default" href="<?= Url::to('/commerce/order/view', ['id' => $order_model->order_id]); ?>" data-original-title="<?= Yii::t('app', 'Order'); ?>" target="_blank"><i class="wb-edit"></i></a>
            </td>
          </tr>
        <?php endforeach; ?>
      <?php else : ?>
        <tr>
          <td colspan="4"><?= Yii::t('app', 'No orders has been registered'); ?></td>
        </tr>
      <?php endif; ?>
    </tbody>
  </table>
</div>