<?php
/*
|--------------------------------------------------------------------------
| Checkout error (loaded via AJAX)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $vec_errors: Array with errors
|
*/
?>
<?php if ( !empty($vec_errors) ) : ?>
  <ul>
    <?php foreach ( $vec_errors as $vec_field_errors ) : ?>
      <?php if ( is_array($vec_field_errors) ) : ?>
        <?php foreach ( $vec_field_errors as $que_field_error ) : ?>
          <?php if ( is_array($que_field_error) ) : ?>
            <?php foreach ( $que_field_error as $que_error ) : ?>
              <li><?= $que_error; ?></li>
            <?php endforeach; ?>
          <?php else : ?>
            <li><?= $que_field_error; ?></li>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php else : ?>
        <li><?= $vec_field_errors; ?></li>
      <?php endif; ?>
    <?php endforeach; ?>
  </ul>
<?php endif; ?>
