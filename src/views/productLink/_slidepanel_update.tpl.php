<?php
/*
|--------------------------------------------------------------------------
| Create partial page of a ProductLink model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $product_link_model: ProductLink model
|
*/
?>
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-pure btn-inverse slidePanel-close actions-top icon wb-close" aria-hidden="true"></button>
  </div>
  <h1><?= Yii::t('app', 'Update link'); ?></h1>
</header>
<div id="product-link-slidepanel-action" class="slidePanel-inner product-link-slidepanel-wrapper" data-action="update">
  <section class="slidePanel-inner-section">
    <?php
      // Render form
      $this->renderPartial('//commerce/productLink/_form', [
        'product_model'       => $product_model,
        'product_link_model'  => $product_link_model,
        'current_action'      => 'update'
      ]);
    ?>
  </section>
</div>