<?php
/*
|--------------------------------------------------------------------------
| Product link partial
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $is_view_mode: View mode?
|
*/

  use dz\helpers\Url;
?>
<div class="table-responsive">
  <table id="product-link-table" class="product-link-table table table-striped table-hover head-uppercase grid-view-loading" data-url="<?= Url::to('/commerce/productLink', ['product_id' => $product_model->product_id]); ?>">
    <?php if ( $product_model->links ) : ?>
      <thead>
        <tr>
          <th class="title-column text-left"><?= Yii::t('app', 'TITLE'); ?></th>
          <th class="url-column text-left"><?= Yii::t('app', 'LINK'); ?></th>
          <th class="actions-column text-center"><?= Yii::t('app', 'ACTIONS'); ?></th>
        </tr>
      </thead>
    <?php endif; ?>
    <tbody>
      <?php if ( $product_model->links ) : ?>
        <?php foreach ( $product_model->links as $product_link_model ) : ?>
          <tr>
            <td class="title-column">
              <?= !empty($product_link_model->title) ? $product_link_model->title : ''; ?>
            </td>
            <td class="url-column">
              <a href="<?= $product_link_model->url; ?>" target="_blank"><?= $product_link_model->url; ?></a>
            </td>
            <td class="actions-column text-center">
              <a class="update-action btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip" data-action="update" href="<?= Url::to('/commerce/productLink/update', ['product_id' => $product_link_model->product_id, 'link_id' => $product_link_model->link_id]); ?>" data-original-title="<?= Yii::t('app', 'Update'); ?>"><i class="wb-edit"></i></a>
              <a class="delete-action btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip" data-action="delete" href="<?= Url::to('/commerce/productLink/delete', ['product_id' => $product_link_model->product_id, 'link_id' => $product_link_model->link_id]); ?>" data-original-title="<?= Yii::t('app', 'Delete'); ?>" data-product="<?= $product_link_model->product_id; ?>" data-link="<?= $product_link_model->link_id; ?>"><i class="wb-trash"></i></a>
            </td>
          </tr>
        <?php endforeach; ?>
      <?php else : ?>
        <tr>
          <td colspan="5">
            <h5 class="mt-15"><?= Yii::t('app', 'No links have been added'); ?></h5>
          </td>
        </tr>
      <?php endif; ?>
    </tbody>
  </table>
</div>        
