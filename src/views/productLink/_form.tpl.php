<?php
/*
|--------------------------------------------------------------------------
| Form partial page of a ProductLink model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $product_link_model: ProductLink model
|  - $current_action: Current action name
|
*/

  use dz\helpers\Html;
  use dz\helpers\Url;;

  // Generate form URL submit link
  if ( $current_action == 'create' )
  {
    $form_url = Url::to('/commerce/productLink/create', ['product_id' => $product_model->product_id]);
  }
  else
  {
    $form_url = Url::to('/commerce/productLink/update', ['product_id' => $product_model->product_id, 'link_id' => $product_link_model->link_id]);
  }
?>
<?php
  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => 'product-link-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);
?>
  <div id="slidepanel-errors" class="messages-error alert alert-dismissible dark summary-errors alert-danger hide"></div>

  <?= $form->hiddenField($product_link_model, 'is_video'); ?>

  <div class="row">
    <?= $form->label($product_link_model, 'title', ['class' => 'col-lg-3 form-control-label']); ?>
    <div class="col-lg-9">
      <?=
        $form->textField($product_link_model, 'title', [
          'maxlength' => 255,
          'placeholder' => ''
        ]);
      ?>
    </div>
  </div>

  <div class="row">
    <?= $form->label($product_link_model, 'url', ['class' => 'col-lg-3 form-control-label']); ?>
    <div class="col-lg-9">
      <?=
        $form->textField($product_link_model, 'url', [
          'maxlength' => 255,
          'placeholder' => ''
        ]);
      ?>
    </div>
  </div>

  <div class="row">
    <label class="col-lg-3 form-control-label"></label>
    <div class="col-lg-9 form-actions">
      <button id="product-link-save-btn" class="btn btn-primary" data-dismiss="modal" type="button" data-url="<?= $form_url; ?>" data-product="<?= $product_model->product_id; ?>" data-link="<?= $product_link_model->link_id; ?>"><?= Yii::t('app', 'Save'); ?></button>
      <a class="btn btn-dark cancel slidePanel-close" href="#"><?= Yii::t('app', 'Cancel'); ?></a>
    </div>
  </div>
<?php
  // End model form
  $this->endWidget();
?>