<?php
/*
|--------------------------------------------------------------------------
| CustomerAddress form partial
|--------------------------------------------------------------------------
|
| Available variables:
|  - $form: Form object
|  - $order_model: Order model
|  - $address_model: CustomerAddress model
|  - $address_type: Address type [shipping | billing]
|  - $vec_country_list: Array with full country list
|  - $is_include_email: Include mail? (boolean)
|  - $panel_title: Panel title
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<a name="<?= $address_type; ?>"></a>
<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= $panel_title; ?></h3>
  </header>
  <div class="panel-body">
    <?php if ( isset($order_model) && $is_include_email ) : ?>
      <div class="row">
        <div class="col-lg-6">
          <div class="form-group row<?php if ( $order_model->hasErrors('email') ) : ?> has-danger<?php endif; ?>">
            <?= $form->label($order_model, 'email', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
            <div class="col-lg-9">
              <?=
                $form->textField($order_model, 'email', [
                  'maxlength' => 255,
                  'placeholder' => ''
                ]);
              ?>
              <?= $form->error($order_model, 'email'); ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <div class="row">
      <div class="col-lg-6">
        <div class="form-group row<?php if ( $address_model->hasErrors('firstname') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($address_model, 'firstname', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($address_model, 'firstname', [
                'maxlength'   => 100,
                'name'        => 'CustomerAddress['. $address_type .'][firstname]',
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($address_model, 'firstname'); ?>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="form-group row<?php if ( $address_model->hasErrors('lastname') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($address_model, 'lastname', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($address_model, 'lastname', [
                'maxlength'   => 100,
                'name'        => 'CustomerAddress['. $address_type .'][lastname]',
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($address_model, 'lastname'); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <div class="form-group row<?php if ( $address_model->hasErrors('address_line') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($address_model, 'address_line', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($address_model, 'address_line', [
                'maxlength'   => 255,
                'name'        => 'CustomerAddress['. $address_type .'][address_line]',
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($address_model, 'address_line'); ?>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="form-group row<?php if ( $address_model->hasErrors('city') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($address_model, 'city', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($address_model, 'city', [
                'maxlength'   => 128,
                'name'        => 'CustomerAddress['. $address_type .'][city]',
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($address_model, 'city'); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <div class="form-group row<?php if ( $address_model->hasErrors('postal_code') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($address_model, 'postal_code', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($address_model, 'postal_code', [
                'maxlength'   => 32,
                'name'        => 'CustomerAddress['. $address_type .'][postal_code]',
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($address_model, 'postal_code'); ?>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="form-group row<?php if ( $address_model->hasErrors('province') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($address_model, 'province', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($address_model, 'province', [
                'maxlength'   => 32,
                'name'        => 'CustomerAddress['. $address_type .'][province]',
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($address_model, 'province'); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <div class="form-group row<?php if ( $address_model->hasErrors('country_code') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($address_model, 'country_code', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?php
              echo Html::dropDownList(
                'CustomerAddress['. $address_type .'][country_code]',
                $address_model->country_code,
                $vec_country_list,
                [
                  'class'             => 'form-control',
                  'data-plugin'       => 'select2',
                  'placeholder'       => 'Select a country',
                  'data-placeholder'  => 'Select a country',
                  'empty'             => 'Select a country',
                  'multiple'          => false,
                  'data-allow-clear'  => false,
                  'style'             => 'max-width: 300px',
                  'autocomplete'      => 'false',
                ]
              );
            ?>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="form-group row<?php if ( $address_model->hasErrors('phone') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($address_model, 'phone', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($address_model, 'phone', [
                'maxlength' => 64,
                'name'        => 'CustomerAddress['. $address_type .'][phone]',
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($address_model, 'phone'); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <div class="form-group row<?php if ( $address_model->hasErrors('vat_code') ) : ?> has-danger<?php endif; ?>">
          <?= $form->label($address_model, 'vat_code', ['class' => 'col-lg-3 col-sm-3 form-control-label']); ?>
          <div class="col-lg-9">
            <?=
              $form->textField($address_model, 'vat_code', [
                'maxlength'   => 128,
                'name'        => 'CustomerAddress['. $address_type .'][vat_code]',
                'placeholder' => ''
              ]);
            ?>
            <?= $form->error($address_model, 'vat_code'); ?>
          </div>
        </div>
      </div>
    </div>
  </div><!-- .panel-body -->
</div><!-- .panel -->

