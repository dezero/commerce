<?php
/*
|--------------------------------------------------------------------------
| Address information (fragment HTML)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $address_model: CustomerAddress model
|  - $address_type: Address type [shipping | billing]
|
*/
?>
<div class="row">
  <div class="col-sm-3">
    <h5><?= $address_model->getAttributeLabel('firstname'); ?></h5>
    <div class="item-content"><?= $address_model->firstname; ?></div>
  </div>
  
  <div class="col-sm-3">
    <h5><?= $address_model->getAttributeLabel('lastname'); ?></h5>
    <div class="item-content"><?= $address_model->lastname; ?></div>
  </div>

  <div class="col-sm-3">
    <h5><?= $address_model->getAttributeLabel('vat_code'); ?></h5>
    <div class="item-content"><?= $address_model->vat_code; ?></div>
  </div>

  <div class="col-sm-3">
    <h5><?= $address_model->getAttributeLabel('phone'); ?></h5>
    <div class="item-content"><?= $address_model->phone; ?></div>
  </div>
</div>

<div class="row">
  <div class="col-sm-3">
    <h5><?= $address_model->getAttributeLabel('address_line'); ?></h5>
    <div class="item-content"><?= $address_model->address_line; ?></div>
  </div>

  <div class="col-sm-3">
    <h5><?= $address_model->getAttributeLabel('postal_code'); ?></h5>
    <div class="item-content"><?= $address_model->postal_code; ?></div>
  </div>
  
  <div class="col-sm-3">
    <h5><?= $address_model->getAttributeLabel('city'); ?></h5>
    <div class="item-content">
      <?= $address_model->city; ?>
      <?php if ( !empty($address_model->province) ) : ?> (<?= $address_model->province; ?>)<?php endif; ?>
    </div>
  </div>

  <div class="col-sm-3">
    <h5><?= $address_model->getAttributeLabel('country_code'); ?></h5>
    <div class="item-content"><?= $address_model->country ? $address_model->country->name : '-'; ?></div>
  </div>
</div>