<?php
/*
|--------------------------------------------------------------------------
| Change status of an Order model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|
*/
  use dz\helpers\Html;

?>
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-pure btn-inverse slidePanel-close actions-top icon wb-close" aria-hidden="true"></button>
    <?php /*
    <div class="btn-group actions-bottom" role="group">
      <button type="button" class="btn btn-pure btn-inverse icon wb-chevron-left" aria-hidden="true"></button>
      <button type="button" class="btn btn-pure btn-inverse icon wb-chevron-right" aria-hidden="true"></button>
    </div>
    */ ?>
  </div>
  <h1><?= Yii::t('app', 'Order') .' '. $order_model->title(); ?> - <?= Yii::t('app', 'Change Status'); ?></h1>
</header>
<div class="slidePanel-inner order-slidepanel-wrapper">
  <section class="slidePanel-inner-section">
    <div class="row">
      <label class="col-lg-3 form-control-label"><?= Yii::t('app', 'Current status'); ?></label>
      <div class="col-lg-9 order-status-wrapper">
        <?php
          $this->renderPartial('_view_status', [
            'vec_status_types'  => $order_model->status_type_labels(),
            'status_type'       => $order_model->status_type
          ]);
        ?>
      </div>
    </div>

    <div class="row">
      <label class="col-lg-3 pt-5 form-control-label"><?= Yii::t('app', 'New status'); ?></label>
      <div class="col-lg-9">
        <?=
          Html::dropDownList('Order[status_type]', $order_model->status_type, $order_model->status_type_labels(), [
            'id'              => 'Order_status_type',
            'data-init-value' => $order_model->status_type
          ]);
        ?>
      </div>
    </div>

    <div class="row">
      <label class="col-lg-3 form-control-label"><?= Yii::t('app', 'Internal Comments'); ?></label>
      <div class="col-lg-9">
        <textarea id="Order_comments" class="maxlength-textarea form-control mb-sm" rows="3"></textarea>
        <p class="help-block"><?= Yii::t('app', 'Private comments'); ?><?php /*Comments visible to the custommer.*/ ?></p>
      </div>
    </div>

    <div id="send-mail-row" class="form-group row hide">
      <label class="col-lg-3 form-control-label"><?= Yii::t('app', 'Send Email?'); ?></label>
      <div class="col-lg-9">
        <div id="order-is-sending-mail" class="form-group form-radio-group">
          <div class="radio-custom radio-default radio-inline pl-0 pt-5">
            <input type="radio" id="is_sending_mail-1" name="Order[is_sending_mail]" value="1"<?php if ( $order_model->is_sending_mail ) : ?> checked<?php endif; ?>>
            <label for="Order[is_sending_mail]"><?= Yii::t('app', 'Yes'); ?></label>
          </div>
          <div class="radio-custom radio-default radio-inline">
            <input type="radio" id="is_sending_mail-0" name="Order[is_sending_mail]" value="0"<?php if ( ! $order_model->is_sending_mail ) : ?> checked<?php endif; ?>>
            <label for="Order[is_sending_mail]"><?= Yii::t('app', 'No'); ?></label>
          </div>
        </div>
        <p id="help-mail-block" class="help-block hide"><u>Order Shipped</u> notification will be sent to the customer's email <strong><?= $order_model->email; ?></strong>.</p>
      </div>
    </div>

    <div class="row">
      <label class="col-lg-3 form-control-label"></label>
      <div class="col-lg-9">
        <button id="order-status-save-btn" class="btn btn-primary" data-dismiss="modal" type="button" disabled data-order="<?= $order_model->order_id; ?>"><?= Yii::t('app', 'Change Status'); ?></button>
      </div>
    </div>

  </section>
</div>