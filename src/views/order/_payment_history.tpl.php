<?php
/*
|--------------------------------------------------------------------------
| Order payment history table
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|
*/
  use dz\helpers\StringHelper;
?>
<div class="table-responsive">
  <table id="payments-table" class="table table-striped table-hover">
    <thead>
      <tr>
        <?php if ( Yii::app()->user->id == 1 ) : ?><th>#</th><?php endif; ?>
        <th><?= Yii::t('app', 'Gateway'); ?></th>
        <th><?= Yii::t('app', 'Date'); ?></th>
        <th class="center"><?= Yii::t('app', 'Reference'); ?></th>
        <th class="center"><?= Yii::t('app', 'Status'); ?></th>
        <th class="center"><?= Yii::t('app', 'Amount'); ?></th>
        <th class="center"><?= Yii::t('app', 'Response'); ?></th>
        <th class="center"><?= Yii::t('app', 'Details'); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php if ( $order_model->transactions ) : ?>
        <?php foreach( $order_model->transactions as $transaction_model ) : ?>
          <?php
            // Get status type label
            $status_type = StringHelper::strtoupper($transaction_model->status_type_label($transaction_model->status_type));
          ?>
          <tr>
            <?php if ( Yii::app()->user->id == 1 ) : ?><td><?= '#'. $transaction_model->transaction_id; ?></td><?php endif; ?>
            <td><?= $transaction_model->gateway->name; ?></td>
            <td><?= $transaction_model->updated_date; ?></td>
            <td class="center">
              <?php if ( $status_type === 'ACCEPTED' ) : ?>
                <strong class="text-success"><?= $transaction_model->get_gateway_reference(); ?></strong>
              <?php else : ?>
                <?= $transaction_model->get_gateway_reference(); ?>
              <?php endif; ?>
            </td>
            <td class="center">
              <?php if ( $status_type === 'ACCEPTED' ) : ?>
                <span class="text-success"><?= $status_type; ?></span>
              <?php elseif ( $status_type === 'REJECTED' ) : ?>
                <span class="text-danger"><?= $status_type; ?></span>
              <?php else : ?>
                <?= $status_type; ?>
              <?php endif; ?>
            </td>
            <td class="center"><?= $transaction_model->amount .' &euro;'; ?></td>
            <td class="center">
              <?php
                // RESPONSE CODE
                if ( $transaction_model->response_code !== null && $transaction_model->response_code !== '' ) :
              ?>
                <?php
                  // Response code
                  echo (int)$transaction_model->response_code;

                  // Response label
                  $response_label = Yii::app()->redsys->get_response_label($transaction_model->response_code);
                  echo !empty($response_label) ? ' - '. $response_label : '';

                  // Error code
                  echo !empty($error_code) ? '<br>'. Yii::t('app', 'ERROR CODE') .': '. $error_code : '';
                ?>
              <?php endif; ?>
            </td>
            <td>
              <a href="#" class="transaction-details-btn" data-transaction="<?= $transaction_model->transaction_id; ?>"><?= Yii::t('app', 'View details'); ?></a>
              <table id="transaction-details-<?= $transaction_model->transaction_id; ?>" class="table table-bordered hide">
                <thead>
                  <tr>
                    <th><?= Yii::t('app', 'SEND DATA'); ?></th>
                    <th><?= Yii::t('app', 'RECEIVED DATA'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <?php
                        // DETAILS REQUEST
                        $vec_request = $transaction_model->get_request_data();
                        if ( ! empty($vec_request) ) :
                      ?>
                        <div>
                          <ul>
                            <?php foreach ( $vec_request as $param_key => $param_value ) : ?>
                              <li><?= $param_key; ?>: <?= $param_value; ?></li>
                            <?php endforeach; ?>
                          </ul>
                        </div>
                      <?php endif; ?>
                    </td>
                    <td>
                      <?php
                        // DETAILS RESPONSE
                        $vec_response = $transaction_model->get_response_data();
                        if ( ! empty($vec_response) ) :
                      ?>
                        <div>
                          <ul>
                            <?php foreach ( $vec_response as $param_key => $param_value ) : ?>
                              <li><?= $param_key; ?>: <?= $param_value; ?></li>
                            <?php endforeach; ?>
                          </ul>
                        </div>
                      <?php endif; ?>
                    </td>
                </tbody>
              </table>
            </td>
          </tr>
        <?php endforeach; ?>
      <?php else : ?>
        <?php if ( $order_model->source_type === 'admin' ) : ?>
          <tr>
          <td colspan="<?php if ( Yii::app()->user->id == 1 ) : ?>8<?php else : ?>7<?php endif; ?>"><h5 class="center pt-5 font-size-16 text-success"><?= Yii::t('app', 'Order created manually by an administrator'); ?></h5></td>
        </tr>
        <?php else : ?>
          <tr>
            <td colspan="<?php if ( Yii::app()->user->id == 1 ) : ?>8<?php else : ?>7<?php endif; ?>"><?= Yii::t('app', 'No payments have been registered'); ?></td>
          </tr>
        <?php endif; ?>
      <?php endif; ?>
    </tbody>
  </table>
</div>