<?php
/*
|--------------------------------------------------------------------------
| Sidebar - Order model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|  - $vec_totals: Array with total orders grouped by status
|
*/

  use dz\helpers\Url;

  // Get status labels & colors
  $vec_status_labels = Yii::app()->orderManager->status_labels();
  $vec_status_colors = Yii::app()->orderManager->status_colors();

?>
<div class="page-aside">
  <div class="page-aside-switch">
    <i class="icon wb-chevron-left" aria-hidden="true"></i>
    <i class="icon wb-chevron-right" aria-hidden="true"></i>
  </div>
  <div class="page-aside-inner page-aside-scroll">
    <div data-role="container">
      <div data-role="content">
        <div class="page-aside-section">
          <div class="list-group mb-10">
            <a class="list-group-item justify-content-between<?php if ( $order_model->disable_filter == 0 && $order_model->status_type === 'all' ) : ?> active<?php endif; ?>" href="<?= Url::to('/commerce/order', ['Order[status_type]' => 'all']); ?>">
              <span><i class="icon wb-order" aria-hidden="true"></i> <?= Yii::t('app', 'All Orders'); ?></span>
            </a>
          </div>
        </div>
        <section class="page-aside-section">
          <div class="list-group mb-20">
            <a class="list-group-item<?php if ( $order_model->status_type === 'paid' || empty($order_model->status_type) ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/commerce/order', ['Order[status_type]' => 'paid']); ?>">
              <span><?= $vec_status_labels['paid']; ?></span>
              <span class="item-right"><?= $vec_totals['paid']; ?></span>
            </a>
            <?php foreach ( $vec_status_labels as $status_type => $status_label ) : ?>
              <?php if ( $status_type !== 'paid' &&  $status_type !== 'unpaid' && $status_type !== 'cart' && $status_type !== 'payment_failed' ) : ?>
                <a class="list-group-item<?php if ( $order_model->status_type === $status_type ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/commerce/order', ['Order[status_type]' => $status_type]); ?>">
                  <span><i class="wb-medium-point <?= $vec_status_colors[$status_type]; ?>" aria-hidden="true"></i><?= $vec_status_labels[$status_type]; ?></span>
                  <span class="item-right"><?= $vec_totals[$status_type]; ?></span>
                </a>
              <?php endif; ?>
            <?php endforeach; ?>
          </div>
          <div class="list-group mb-20">
            <a class="list-group-item<?php if ( $order_model->status_type === 'unpaid' ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/commerce/order', ['Order[status_type]' => 'unpaid']); ?>">
              <span><?= $vec_status_labels['unpaid']; ?></span>
              <span class="item-right"><?= $vec_totals['unpaid']; ?></span>
            </a>
            <a class="list-group-item<?php if ( $order_model->status_type === 'cart' ) : ?> active<?php endif; ?>" href="<?= Url::to('/commerce/order', ['Order[status_type]' => 'cart']); ?>">
              <span><i class="wb-medium-point <?= $vec_status_colors['cart']; ?>" aria-hidden="true"></i><?= $vec_status_labels['cart']; ?></span>
              <span class="item-right"><?= $vec_totals['cart']; ?></span>
            </a>
            <a class="list-group-item<?php if ( $order_model->status_type === 'payment_failed' ) : ?> active<?php endif; ?>" href="<?= Url::to('/commerce/order', ['Order[status_type]' => 'payment_failed']); ?>">
              <span><i class="wb-medium-point <?= $vec_status_colors['payment_failed']; ?>" aria-hidden="true"></i><?= $vec_status_labels['payment_failed']; ?></span>
              <span class="item-right"><?= $vec_totals['payment_failed']; ?></span>
            </a>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>