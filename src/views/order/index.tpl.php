<?php
/*
|--------------------------------------------------------------------------
| Admin list page for Order model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model class
|  - $vec_totals: Array with totals orders grouped by status
|
*/  
  use dz\helpers\Html;
  use dz\helpers\Url;
  
  // Page title
  $this->pageTitle = Yii::t('app', 'Manage Orders');
?>
<?php
  // Sidebar menu with totals
  $this->renderPartial('//commerce/order/_sidebar', [
    'order_model' => $order_model,
    'vec_totals'  => $vec_totals
  ]);
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <div class="page-header-actions">
      <?php if ( Yii::app()->user->isAdmin() ) : ?>
        <a href="<?= Url::to('/commerce/orderJson'); ?>" class="btn btn-dark"><i class="wb-wrench"></i> <?= Yii::t('app', 'Order Logs'); ?></a>
      <?php endif; ?>
    </div>
  </div>
  <div class="page-content">
    <div class="row row-lg">
      <div class="col-lg-12">
        <div class="panel">
          <div class="panel-body container-fluid">
            <?php
              /*
              |--------------------------------------------------------------------------
              | HEADER GRIDVIEW - Search & buttons
              |--------------------------------------------------------------------------
              */
              $this->renderPartial('_header_grid', [
                'order_model' => $order_model,
              ]);
            ?>
            <?php
              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                $this->widget('dz.grid.GridView', [
                  'id'                => 'order-grid',
                  'dataProvider'      => $order_model->search(),
                  'filter'            => $order_model,
                  'emptyText'         => Yii::t('app', 'No orders have been found'),
                  'enableHistory'     => false,
                  'loadModal'         => true,
                  'enableSorting'     => false,
                  'enableSelect2'     => false,
                  // 'selectableRows'    => 2,
                  'type'              => ['striped', 'hover'],
                  'beforeAjaxUpdate'  => 'js:function(id, options){ $.dzGridView.beforeAjaxUpdate(id, options); }',
                  'afterAjaxUpdate'   => 'js:function() { $.orderGridUpdate(); }',
                  'columns'           => [
                    // [
                    //   'class' => '\dz\grid\CheckBoxColumn',
                    // ],
                    [
                      'header' => Yii::t('app', 'Order'),
                      'name' => 'order_number',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/order/_grid_column", ["column" => "order_number", "model" => $data]))',
                    ],
                    [
                      'header' => Yii::t('app', 'Customer'),
                      'name' => 'user_id',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/order/_grid_column", ["column" => "customer", "model" => $data]))',
                    ],
                    [
                      'header' => Yii::t('app', 'Price'),
                      'name' => 'total_price',
                      'filter' => false,
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/order/_grid_column", ["column" => "total_price", "model" => $data]))',
                    ],
                    [
                      'header' => Yii::t('app', 'Date'),
                      'name' => 'ordered_date',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/order/_grid_column", ["column" => "ordered_date", "model" => $data]))',
                      'filter_type' => 'date_range',
                      'filter_type_options' => [
                        'date_from'  => 'ordered_from_date',
                        'date_to'    => 'ordered_to_date'
                      ]
                    ],
                    [
                      'header' => Yii::t('app', 'Status'),
                      'name' => 'status_type',
                      'filter' => Yii::app()->orderManager->status_labels(),
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/order/_grid_column", ["column" => "status_type", "model" => $data]))',
                    ],
                    [
                      'class' => 'dz.grid.ButtonColumn',
                      'header' => Yii::t('app', 'ACTION'),
                      'template' => '{view}{products}',
                      'deleteButton' => false,
                      'clearButton' => true,
                      'viewButton' => true,
                      'updateButton' => false,
                      'buttons' => [
                        'view' => [
                          'label' => Yii::t('app', 'Order'),
                          'icon' => 'edit',
                          'url' => 'Yii::app()->createAbsoluteUrl("/commerce/order/view", ["id" => $data->order_id])',
                        ],
                        'products' => [
                          'label' => Yii::t('app', 'Products'),
                          'icon' => 'shopping-cart',
                          'url' => 'Yii::app()->createAbsoluteUrl("/commerce/lineItem", ["id" => $data->order_id])',
                        ],
                      ]
                    ],
                  ],
                ]);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- .page-content -->
</div><!-- .page-main -->
