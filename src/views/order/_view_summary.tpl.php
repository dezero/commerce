<?php
/*
|--------------------------------------------------------------------------
| Order Summary Information (partial)
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|
*/
  use dz\helpers\Url;

// Controller and action names in lowercase
  $current_action = Yii::currentAction(true);
?>
<div id="order-panel" class="panel">
  <header class="panel-heading">
    <h3 class="panel-title"><?= Yii::t('app', 'Order'); ?> <?= $order_model->title(); ?> (#<?= $order_model->order_id; ?>)</h3>
  </header>
  <div class="panel-body panel-view-content">
    <div class="row">
      <div class="col-sm-3">
        <h5><?= $order_model->getAttributeLabel('order_number'); ?></h5>
        <div class="item-content"><?= !empty($order_model->order_number) ? $order_model->order_number : '#'. $order_model->order_id; ?></div>
      </div>

      <div class="col-sm-3">
        <h5><?= $order_model->getAttributeLabel('order_reference'); ?></h5>
        <div class="item-content"><?= $order_model->short_reference(); ?></div>
      </div>

      <div class="col-sm-3">
        <h5><?= $order_model->getAttributeLabel('ordered_date'); ?></h5>
        <div class="item-content"><?= $order_model->ordered_date; ?></div>
      </div>

      <div class="col-sm-3">
        <h5><?= $order_model->getAttributeLabel('status_type'); ?></h5>
        <div class="item-content status-type-content">
          <?php
            $this->renderPartial('//commerce/order/_view_status', [
              'vec_status_types'  => $order_model->status_type_labels(),
              'status_type'       => $order_model->status_type
            ]);
          ?>
          <?php if ( $current_action == 'view') : ?><a id="order-status-btn" class="btn btn-primary btn-xs" href="<?= Url::to('/commerce/order/status', ['id' => $order_model->order_id]); ?>"><?= Yii::t('app', 'Change'); ?></a><?php endif; ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3">
        <h5><?= $order_model->getAttributeLabel('customer'); ?></h5>
        <?php if ( $order_model->user_id > 0 ) : ?>
            <a href="<?= Url::to('/commerce/customer/view', ['id' => $order_model->user_id]); ?>" target="_blank"><?= $order_model->user->fullname(); ?></a>
          <?php else : ?>
            <?= Yii::t('app', 'Anonymous'); ?>
          <?php endif; ?>
      </div>
      <div class="col-sm-3">
        <h5><?= $order_model->getAttributeLabel('email'); ?></h5>
        <?php if ( !empty($order_model->email) ) : ?>
          <a href="mailto:<?= $order_model->email; ?>" target="_blank"><?= $order_model->email; ?></a>
        <?php elseif ( $order_model->user_id > 0 && $order_model->user ) : ?>
          <a href="mailto:<?= $order_model->user->get_email(); ?>" target="_blank"><?= $order_model->user->get_email(); ?></a>
        <?php endif; ?>
      </div>
      <div class="col-sm-3">
        <h5><?= $order_model->getAttributeLabel('paid_price'); ?></h5>
        <div class="item-content paid-price-content">
          <?= $order_model->paid_price; ?> &euro;
        </div>
      </div>
      <div class="col-sm-3">
        <h5><?= $order_model->getAttributeLabel('total_price'); ?></h5>
        <div class="item-content total-price-content">
          <?= $order_model->total_price; ?> &euro;
        </div>
      </div>
    </div>
  </div>
</div>