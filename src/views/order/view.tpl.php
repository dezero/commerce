<?php
/*
|--------------------------------------------------------------------------
| Order VIEW page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  // Header title
  $this->renderPartial('//commerce/order/_header_title', [
    'order_model' => $order_model,
  ]);
?>
<div class="page-content container-fluid">
  <?php
    // Header menu
    $this->renderPartial('//commerce/order/_header_menu', [
      'model' => $order_model
    ]);
  ?>

  <?php
    /*
    |--------------------------------------------------------------------------
    | ORDER INFORMATION
    |--------------------------------------------------------------------------
    */
  ?>
  <?php
    $this->renderPartial('//commerce/order/_view_summary', [
      'order_model'   => $order_model
    ]);
  ?>
  

  <?php
    /*
    |--------------------------------------------------------------------------
    | CUSTOMER INFORMATION
    |--------------------------------------------------------------------------
    */
  ?>
  <?php
    // SHIPPING ADDRESS
    if ( $order_model->shippingAddress ) :
  ?> 
    <div id="shipping-address-panel" class="panel address-panel">
      <header class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('app', 'Shipping Address') ;?> <a class="btn btn-xs btn-primary" href="<?= Url::to('/commerce/order/update', ['id' => $order_model->order_id]); ?>#shipping"><?= Yii::t('app', 'Change'); ?></a></h3>
      </header>
      <div class="panel-body panel-view-content">
        <?php
          $this->renderPartial('//commerce/address/_view', [
            'order_model'   => $order_model,
            'address_model' => $order_model->shippingAddress,
            'address_type'  => 'shipping',
          ]);
        ?>
      </div>
    </div>
  <?php endif; ?>
  <?php
    // BILLING ADDRESS
    if ( $order_model->billingAddress ) :
  ?> 
    <div id="billing-address-panel" class="panel address-panel">
      <header class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('app', 'Billing Address') ;?> <a class="btn btn-xs btn-primary" href="<?= Url::to('/commerce/order/update', ['id' => $order_model->order_id]); ?>"><?= Yii::t('app', 'Change'); ?></a></h3>
      </header>
      <div class="panel-body panel-view-content">
        <?php
          $this->renderPartial('//commerce/address/_view', [
            'order_model'   => $order_model,
            'address_model' => $order_model->billingAddress,
            'address_type'  => 'billing',
          ]);
        ?>
      </div>
    </div>
  <?php endif; ?>

  <?php
    /*
    |--------------------------------------------------------------------------
    | PRODUCT LIST - LINE ITEMS
    |--------------------------------------------------------------------------
    */
  ?>
  <?php if ( $order_model->lineItems ) : ?> 
    <div id="products-panel" class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('app', 'Products') ;?> <a class="btn btn-xs btn-primary" href="<?= Url::to('/commerce/lineItem', ['id' => $order_model->order_id]); ?>"><?= Yii::t('app', 'Change') ;?></a></h3>
      </header>
      <div class="panel-body panel-view-content">
        <?php
          $this->renderPartial('//commerce/lineItem/_table_products', [
            'order_model'       => $order_model,
            'is_view_mode'      => true
          ]);
        ?>
      </div>
    </div>
  <?php endif; ?>

  <?php
    /*
    |--------------------------------------------------------------------------
    | PAYMENT INFORMATION
    |--------------------------------------------------------------------------
    */
  ?>
  <a name="payment"></a>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'Payment Information'); ?></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-sm-12">
          <?php
            $this->renderPartial('//commerce/order/_payment_history', [
              'order_model'   => $order_model
            ]);
          ?>
        </div>
      </div>
    </div>
  </div>

  <?php
    /*
    |--------------------------------------------------------------------------
    | STATUS HISTORY
    |--------------------------------------------------------------------------
    */
  ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'Status History'); ?></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-sm-12">
          <?php
            $this->renderPartial('//commerce/order/_status_history', [
              'order_model'   => $order_model
            ]);
          ?>
          <a id="order-history-btn" class="btn btn-primary mr-10" href="<?= Url::to('/commerce/order/status', ['id' => $order_model->order_id]); ?>"><?= Yii::t('app', 'Change Status'); ?></a>
        </div>
      </div>
    </div>
  </div>


  <?php
    /*
    |--------------------------------------------------------------------------
    | MAIL HISTORY
    |--------------------------------------------------------------------------
    */
  ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'Mail History'); ?></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-sm-12">
          <?php
            $this->renderPartial('//commerce/order/_mail_history', [
              'order_model'   => $order_model
            ]);
          ?>
        </div>
      </div>
    </div>
  </div>
</div><!-- .page-content -->