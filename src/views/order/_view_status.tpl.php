<?php
/*
|--------------------------------------------------------------------------
| Status type (fragment HTML) for an Order
|--------------------------------------------------------------------------
|
| Available variables:
|  - $vec_status_types: Array with all the status types
|  - $status_type: Current status type
|
*/
  // Get status labels & colors
  $vec_status_labels = Yii::app()->orderManager->status_labels();
  $vec_status_colors = Yii::app()->orderManager->status_colors();
?>
<div class="status-type-wrapper order-status">
  <?php if ( isset($vec_status_labels[$status_type]) ) : ?>
    <?php if ( isset($vec_status_colors[$status_type]) ) : ?>
      <span class="<?= $vec_status_colors[$status_type]; ?>"><i class="wb-medium-point <?= $vec_status_colors[$status_type]; ?>" aria-hidden="true"></i> <?= $vec_status_labels[$status_type]; ?></span>
    <?php else : ?>
      <span><i class="wb-medium-point"></i> <?= $vec_status_labels[$status_type]; ?></span>
    <?php endif; ?>
  <?php else : ?>
    <span><?= $status_type; ?></span>
  <?php endif; ?>
</div>