<?php
/*
|--------------------------------------------------------------------------
| Order UPDATE page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|  - $billing_address_model: CustomerAddress model for billing address
|  - $shipping_address_model: CustomerAddress model for shipping address
|  - $vec_country_list: Array with full country list
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', 'Order') .' '. $order_model->title();
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php if ( ! $order_model->is_total_paid() ) : ?>
      <span class="badge badge-danger"><?= Yii::t('app', 'UNPAID'); ?></span>
    <?php endif; ?>
  </h1>
  <div class="page-header-actions">
    <?=
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ['/commerce/order'], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Manage Orders'),
        'url' => ['/commerce/order'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Header menu
    $this->renderPartial('_header_menu', [
      'model' => $order_model
    ]);

    // Render update form
    $this->renderPartial('_form_update', [
      'order_model'             => $order_model,
      'billing_address_model'   => $billing_address_model,
      'shipping_address_model'  => $shipping_address_model,
      'vec_country_list'        => $vec_country_list,
      'form_id'                 => $form_id
    ]);
  ?>
</div>