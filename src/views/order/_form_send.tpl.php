<?php
/*
|--------------------------------------------------------------------------
| Form partial page for sending emails manually related with an Order model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Show this FORM only for SUPERADMIN user
  if ( Yii::app()->user->id == 1 && $order_model->is_total_paid() ) :
?>
  <?php
    $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
      'id' => 'order-email-form',
      'action' => Url::to('/commerce/order/sendEmail', ['id' => $order_model->order_id]),
      'enableAjaxValidation' => false,
      'htmlOptions' => [
        'class' => 'form-horizontal inline-block',
        'enctype' => 'multipart/form-data',
        'autocomplete' => 'off'
      ]
    ]);
  ?>
    <input type="hidden" id="order-mail-alias" name="MailTemplate[alias]" value="new_order">
    <div class="btn-group ml-5" role="group">
      <a class="btn btn-warning dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><i class="wb-envelope"></i> <?= Yii::t('app', 'Send Email'); ?></a>
      <div id="order-mail-menu" class="dropdown-menu dropdown-menu-right" data-confirm="<?= Yii::t('app', 'Are you sure you want to send this email?'); ?>">
        <a class="dropdown-item" href="javascript:void(0)" role="menuitem" data-alias="new_order"><?= Yii::t('app', 'New Order'); ?></a>
      </div>
    </div>
  <?php
    // End model form
    $this->endWidget();
  ?>        
<?php endif; ?>