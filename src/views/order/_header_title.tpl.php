<?php
/*
|--------------------------------------------------------------------------
| Header title zone for a Product model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

// Page title
  $this->pageTitle = Yii::t('app', 'Order') .' '. $order_model->title();
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php if ( ! $order_model->is_total_paid() ) : ?>
      <span class="badge badge-danger"><?= Yii::t('app', 'UNPAID'); ?></span>
    <?php endif; ?>
  </h1>
  <div class="page-header-actions">
    <?=
      // Back button
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ['/commerce/order'], [
        'class' => 'btn btn-dark'
      ]);
    ?>
    <?php
      // Send emails manually
      $this->renderPartial('//commerce/order/_form_send', [
        'order_model' => $order_model
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Manage Orders'),
        'url' => ['/commerce/order'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>