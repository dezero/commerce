<?php
/*
|--------------------------------------------------------------------------
| Header zone for order Gridview
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|
*/
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

  $current_controller = StringHelper::strtolower($this->currentControllerName());
  $current_action = StringHelper::strtolower($this->currentActionName());
?>
<div class="page-content-actions order-grid-actions">
  <div class="float-right">
    <div id="product-grid-search" class="grid-action-col grid-search-wrapper form-icons">
      <?php
        // Search form
        $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
          'id' => 'order-grid-search-form',
          'method' => 'get',
          'action' => Url::to($this->route),
          'enableAjaxValidation' => false,
          'htmlOptions' => [
            'autocomplete' => 'off',
            'data-grid' => 'order-grid'
          ]
        ]);
      ?>
        <div class="input-group input-search-success">
          <i class="form-control-icon wb-search"></i>
          <?=
            $form->textField($order_model, 'global_search_filter', [
              'class'         => 'form-control form-control-search',
              'placeholder'   => Yii::t('app', 'Search'),
              'autocomplete'  => 'false',
            ]);
          ?>
        </div>
      <?php
        // End model form
        $this->endWidget();
      ?>
    </div>
  </div>
  <div class="grid-action-col">
    <?php
      // Export Excel / CSV actions
      $this->renderPartial('//commerce/export/_actions', [
        'export_url'  => '/commerce/export',
        'model'       => $order_model,
        'export_id'   => 'orders',
        'grid_id'     => 'order-grid',
        'action_id'   => $current_action
      ]);
    ?>
  </div>
</div>