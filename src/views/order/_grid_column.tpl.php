<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for Order model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Order model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "order_number"
    |--------------------------------------------------------------------------
    */
    case 'order_number':
  ?>
    <a href="<?=  Url::to('/commerce/order/view', ['id' => $model->order_id]); ?>"><?= $model->title(); ?></a>
    <br>#<?= $model->order_id; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "customer"
    |--------------------------------------------------------------------------
    */
    case 'customer':
  ?>
    <?php if ( $model->user_id > 0 && $model->user ) : ?>
      <?php
        // Get customer email
        $customer_email = Yii::app()->customerManager->get_customer_email($model->user);
        if ( !empty($customer_email) ) :
      ?>
        <a href="mailto:<?= $customer_email; ?>" target="_blank"><?= $customer_email; ?></a><br>
      <?php endif; ?>
      <?= $model->user->fullname(); ?>
    <?php else : ?>
      <p><span class="badge badge-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Customer NOT registered">GUEST</span></p>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "total_price"
    |--------------------------------------------------------------------------
    */
    case 'total_price':
  ?>
    <?= $model->total_price; ?> &euro;
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "ordered_date"
    |--------------------------------------------------------------------------
    */
    case 'ordered_date':
  ?>
    <?= $model->ordered_date; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "status_type"
    |--------------------------------------------------------------------------
    */
    case 'status_type':
  ?>
    <?php
      $this->renderPartial('//commerce/order/_view_status', [
        'vec_status_types'  => $model->status_type_labels(),
        'status_type'       => $model->status_type,
      ]);
    ?>
  <?php break; ?>
<?php endswitch; ?>