<?php
/*
|--------------------------------------------------------------------------
| Header zone for Order page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Order model
|
*/
  use dz\helpers\Url;

  // Controller and action names in lowercase
  $current_controller = Yii::currentController(true);
  $current_action = Yii::currentAction(true);
?>
<ul class="nav-quick-header nav-quick nav-quick-bordered row">
  <li class="nav-item col-md-4<?php if ( $current_action == 'view' ) : ?> active<?php endif; ?>">
    <a class="nav-link" href="<?= Url::to('/commerce/order/view', ['id' => $model->order_id]); ?>">
      <i class="icon wb-order" aria-hidden="true"></i>
      <span><?= Yii::t('app', 'Order Information'); ?></span>
    </a>
  </li>
  <li class="nav-item col-md-4<?php if ( $current_action == 'update' ) : ?> active<?php endif; ?>">
    <a class="nav-link" href="<?= Url::to('/commerce/order/update', ['id' => $model->order_id]); ?>">
      <i class="icon wb-user" aria-hidden="true"></i>
      <span><?= Yii::t('app', 'Customer Addresses'); ?></span>
    </a>
  </li>
  <li class="nav-item col-md-4<?php if ( $current_controller == 'lineitem' ) : ?> active<?php endif; ?>">
    <a class="nav-link" href="<?= Url::to('/commerce/lineItem', ['id' => $model->order_id]); ?>">
      <i class="icon wb-shopping-cart" aria-hidden="true"></i>
      <span><?= Yii::t('app', 'Products'); ?></span>
    </a>
  </li>
</ul>