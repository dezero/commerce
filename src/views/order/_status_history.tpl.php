<?php
/*
|--------------------------------------------------------------------------
| Order status history table
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|
*/
?>
<div class="table-responsive">
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th><?= Yii::t('app', 'Date'); ?></th>
        <th><?= Yii::t('app', 'User'); ?></th>
        <th><?= Yii::t('app', 'Status'); ?></th>
        <th><?= Yii::t('app', 'Comments'); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php if ( $order_model->statusHistory ) : ?>
        <?php foreach( $order_model->statusHistory as $order_history_model ) : ?>
          <tr>
            <td><?= $order_history_model->created_date; ?></td>
            <td><?= $order_history_model->createdUser->fullname(); ?></td>
            <td class="col-status">
              <?php
                $this->renderPartial('//commerce/order/_view_status', [
                  'vec_status_types' => $order_model->status_type_labels(),
                  'status_type' => $order_history_model->status_type
                ]);
              ?>
            </td>
            <td><?= $order_history_model->comments; ?></td>
          </tr>
        <?php endforeach; ?>
      <?php else : ?>
        <tr>
          <td colspan="4"><?= Yii::t('app', 'No status history has been registered'); ?></td>
        </tr>
      <?php endif; ?>
    </tbody>
  </table>
</div>