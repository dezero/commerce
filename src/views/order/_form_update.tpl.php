<?php
/*
|--------------------------------------------------------------------------
| Form partial page for Order model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_model: Order model
|  - $billing_address_model: CustomerAddress model for billing address
|  - $shipping_address_model: CustomerAddress model for shipping address
|  - $vec_country_list: Array with full country list
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal order-form-wrapper',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  $vec_form_models = [$order_model];
  if ( $billing_address_model )
  {
    $vec_form_models[] = $billing_address_model;
  }
  if ( $shipping_address_model )
  {
    $vec_form_models[] = $shipping_address_model;
  }
  $errors = $form->errorSummary($vec_form_models);
  if ( $errors )
  {
    echo $errors;
  }
?>
<?php
  /*
  |--------------------------------------------------------------------------
  | ORDER INFORMATION
  |--------------------------------------------------------------------------
  */
?>
<?php
  $this->renderPartial('//commerce/order/_view_summary', [
    'order_model'   => $order_model
  ]);
?>
<?php
  /*
  |--------------------------------------------------------------------------
  | SHIPPING ADDRESS
  |--------------------------------------------------------------------------
  */
  if ( $shipping_address_model ) :
?>
  <?php
    $this->renderPartial('//commerce/address/_form', [
      'form'              => $form,
      'order_model'       => $order_model,
      'address_model'     => $shipping_address_model,
      'address_type'      => 'shipping',
      'vec_country_list'  => $vec_country_list,
      'panel_title'       => Yii::t('app', 'Shipping Address'),
      'is_include_email'  => true
    ]);
  ?>
<?php endif; ?>
<?php
  /*
  |--------------------------------------------------------------------------
  | BILLING ADDRESS
  |--------------------------------------------------------------------------
  */
  if ( $billing_address_model ) :
?>
  <?php
    $this->renderPartial('//commerce/address/_form', [
      'form'              => $form,
      'order_model'       => $order_model,
      'address_model'     => $billing_address_model,
      'address_type'      => 'billing',
      'vec_country_list'  => $vec_country_list,
      'panel_title'       => Yii::t('app', 'Billing Address'),
      'is_include_email'  => ! $shipping_address_model
    ]);
  ?>
<?php endif; ?>
<?php
  /*
  |--------------------------------------------------------------------------
  | ACTIONS
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'  => 'submit',
          'type'        => 'primary',
          'label'       => Yii::t('app', 'Save')
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/commerce/order/view', 'id' => $order_model->order_id], ['class' => 'btn btn-dark']);
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>