<?php
/*
|--------------------------------------------------------------------------
| Form partial page of a ProductAssociation model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $product_association_model: ProductAssociation model
|  - $current_action: Current action name
|
*/

  use dz\helpers\Html;
  use dz\helpers\Url;

?>
<?php
  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => 'product-association-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);
?>
  <div id="slidepanel-errors" class="messages-error alert alert-dismissible dark summary-errors alert-danger hide"></div>
  <div class="row">
    <label class="col-lg-3 form-control-label"><?= $product_model->getAttributeLabel('product_id'); ?></label>
    <div class="col-lg-9">
      <select id="ProductAssociation_product_associated_id" name="ProductAssociation[product_associated_id]"><option></option></select>
    </div>
  </div>

  <div class="row">
    <label class="col-lg-3 form-control-label"></label>
    <div class="col-lg-9 form-actions">
      <button id="product-association-save-btn" class="btn btn-primary" data-dismiss="modal" type="button" data-url="<?= Url::to('/commerce/productAssociation/create', ['product_id' => $product_model->product_id, 'type' => 'related']); ?>" data-product="<?= $product_model->product_id; ?>" data-type="related"><?= Yii::t('app', 'Save'); ?></button>
      <a class="btn btn-dark cancel slidePanel-close" href="#"><?= Yii::t('app', 'Cancel'); ?></a>
    </div>
  </div>
<?php
  // End model form
  $this->endWidget();
?>