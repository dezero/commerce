<?php
/*
|--------------------------------------------------------------------------
| Product ASSOCIATION index page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $vec_config: Product configuration options
|
*/
  use dz\helpers\Url;

  // Header title
  $this->renderPartial($product_model->get_view_path('_header_title'), [
    'product_model' => $product_model,
    'vec_config'    => $vec_config,
  ]);
?>

<div class="page-content container-fluid">
  <?php
    // Header menu
    $this->renderPartial($product_model->get_view_path('_header_menu'), [
      'product_model' => $product_model,
      'vec_config'    => $vec_config,
    ]);
  ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | PRODUCT ASSOCIATION
    |--------------------------------------------------------------------------
    */
  ?>
  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'Related Products'); ?></h3>
      <div class="panel-actions">
        <a id="product-association-create-btn" data-action="create" data-product="<?= $product_model->product_id; ?>" href="<?= Url::to('/commerce/productAssociation/create', ['product_id' => $product_model->product_id, 'type' => 'related']); ?>" class="btn btn-primary">
          <i class="icon wb-plus"></i> <?= Yii::t('app', 'Add related product'); ?>
        </a>
      </div>
    </header>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
          <?php
            $this->renderPartial('//commerce/productAssociation/_table_products', [
              'product_model'     => $product_model,
              'association_type'  => 'related',
              'is_view_mode'      => false
            ]);
          ?>
        </div>
      </div>
    </div>
  </div>
</div>