<?php
/*
|--------------------------------------------------------------------------
| Related products partial
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $association_type: Association type
|
*/

  use dz\helpers\Url;
?>
<div class="table-responsive">
  <table id="product-association-table" class="product-association-table table table-striped table-hover grid-view-loading head-uppercase" data-url="<?= Url::to('/commerce/productAssociation', ['product_id' => $product_model->product_id, 'type' => $association_type]); ?>">
    <?php if ( $product_model->relatedProducts ) : ?>
      <thead>
        <tr>
          <th class="image-column"><?= $product_model->getAttributeLabel('image'); ?></th>
          <th class="sku-column"><?= $product_model->getAttributeLabel('default_sku'); ?></th>
          <th class="title-column text-left"><?= $product_model->getAttributeLabel('name'); ?></th>
          <th class="weight-column text-center"><?= $product_model->getAttributeLabel('weight'); ?></th>
          <th class="actions-column text-center"><?= Yii::t('app', 'ACTIONS'); ?></th>
        </tr>
      </thead>
    <?php endif; ?>
    <tbody>
      <?php if ( $product_model->relatedProducts ) : ?>
        <?php foreach ( $product_model->relatedProducts as $product_association_model ) : ?>
          <tr>
            <td class="image-column">
              <?php
                $this->renderPartial($product_model->get_view_path("_grid_column"), [
                  'column' => 'image',
                  'model' => $product_association_model->productAssociated
                ]);
              ?>
            </td>
            <td class="sku-column">
              <?php
                $this->renderPartial($product_model->get_view_path("_grid_column"), [
                  'column' => 'sku',
                  'model' => $product_association_model->productAssociated
                ]);
              ?>
            </td>
            <td class="title-column">
              <?php
                $this->renderPartial($product_model->get_view_path("_grid_column"), [
                  'column' => 'name',
                  'model' => $product_association_model->productAssociated
                ]);
              ?>
            </td>
            <td class="weight-column text-center">
              <?= $product_association_model->productAssociated->weight; ?>
            </td>
            <td class="actions-column text-center">
              <a class="delete-action btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip" data-action="delete" href="<?= Url::to('/commerce/productAssociation/delete', ['product_id' => $product_model->product_id, 'association_id' => $product_association_model->association_id]); ?>" data-original-title="<?= Yii::t('app', 'Delete'); ?>" data-product="<?= $product_association_model->product_id; ?>" data-association="<?= $product_association_model->association_id; ?>"><i class="wb-trash"></i></a>
            </td>
          </tr>
        <?php endforeach; ?>
      <?php else : ?>
        <tr>
          <td colspan="5">
            <h5 class="mt-15"><?= Yii::t('app', 'No related products have been added'); ?></h5>
          </td>
        </tr>
      <?php endif; ?>
    </tbody>
  </table>
</div>        
