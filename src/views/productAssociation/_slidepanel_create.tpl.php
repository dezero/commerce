<?php
/*
|--------------------------------------------------------------------------
| Create partial page of a ProductAssociation model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $product_association_model: ProductAssociation model
|
*/

  use dz\helpers\Html;
?>
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-pure btn-inverse slidePanel-close actions-top icon wb-close" aria-hidden="true"></button>
  </div>
  <h1><?= Yii::t('app', 'Add related product'); ?></h1>
</header>
<div id="product-association-slidepanel-action" class="slidePanel-inner product-association-slidepanel-wrapper" data-action="create">
  <section class="slidePanel-inner-section">
    <?php
      // Render form
      $this->renderPartial('//commerce/productAssociation/_form', [
        'product_model'             => $product_model,
        'product_association_model' => $product_association_model,
        'current_action'            => 'create'
      ]);
    ?>
  </section>
</div>