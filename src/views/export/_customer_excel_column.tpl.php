<?php
/*
|----------------------------------------------------------------------------------
| GridView column partial page for Export PRODUCTS to an Excel file
|----------------------------------------------------------------------------------
|
| Available variables:
|  - $model: Current model
|  - $column: Column name
|
*/

  use dz\helpers\Html;
  use dz\helpers\Url;

  // Get CustomerAddress model
  $address_model = null;
  if ( isset($address_type) ) 
  {
    if ( $address_type === 'shipping' )
    {
      $address_model = $model->shippingAddress;
    }
    else if ( $address_type === 'billing' )
    {
      $address_model = $model->billingAddress;
    }
  }
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "fullname"
    |--------------------------------------------------------------------------
    */
    case 'fullname':
  ?>
    <?= $model->user_id > 0 ? $model->user->fullname() : Yii::t('app', 'Anonymous'); ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "address__<attribute>"
    |--------------------------------------------------------------------------
    */
    case 'address__firstname':
    case 'address__lastname':
    case 'address__vat_code':
    case 'address__address_line':
    case 'address__city':
    case 'address__province':
    case 'address__postal_code':
    case 'address__phone':
  ?>
    <?php if ( $address_model ) : ?>
      <?= $address_model->getAttribute(str_replace("address__", "", $column)); ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "address_country_code"
    |--------------------------------------------------------------------------
    */
    case 'address_country_code':
  ?>
    <?php if ( $address_model && $address_model->country ) : ?>
      <?= $address_model->country->name; ?>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>