<?php
/*
|----------------------------------------------------------------------------------
| GridView column partial page for Export ORDERS / LINE ITEMS to an Excel file
|----------------------------------------------------------------------------------
|
| Available variables:
|  - $model: Current model
|  - $column: Column name
|
*/

  use dz\helpers\Html;
  use dz\helpers\Url;

  // Get CustomerAddress model
  $address_model = null;
  if ( isset($address_type) ) 
  {
    if ( $address_type === 'shipping' )
    {
      $address_model = $model->shippingAddress;
    }
    else if ( $address_type === 'billing' )
    {
      $address_model = $model->billingAddress;
    }
  }
?>
<?php switch ( $column ) :
   /*
    |--------------------------------------------------------------------------
    | COLUMN "order_reference"
    |--------------------------------------------------------------------------
    */
    case 'order_reference':
  ?>
    <?= $model->short_reference(); ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "customer_fullname"
    |--------------------------------------------------------------------------
    */
    case 'customer_fullname':
  ?>
    <?= $model->user_id > 0 ? $model->user->fullname() : Yii::t('app', 'Anonymous'); ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "items_price"
    |--------------------------------------------------------------------------
    */
    case 'items_price':
  ?>
    <?php
      // Recalculate "item_price" with all the discounts and "reduced_price" applied
      echo $model->recalculate_items_price();
    ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "items_notax_price"
    |--------------------------------------------------------------------------
    */
    case 'items_no_tax_price':
  ?>
    <?php
      // Recalculate "item_price" with all the discounts and "reduced_price" applied
      $items_price_raw = $model->recalculate_items_price();

      // Tax price
      $tax_price_raw = Yii::app()->orderManager->get_tax_amount($items_price_raw, 21);

      // Item price with NO taxe
      echo $items_price_raw - $tax_price_raw;
    ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "tax_price"
    |--------------------------------------------------------------------------
    */
    case 'tax_price':
  ?>
    <?php
      // Recalculate "item_price" with all the discounts and "reduced_price" applied
      $items_price_raw = $model->recalculate_items_price();

      // Tax price
      echo Yii::app()->orderManager->get_tax_amount($items_price_raw, 21);
    ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "discount_price"
    |--------------------------------------------------------------------------
    */
    case 'discount_price':
  ?>
    <?php
      /* if ( $model->raw_attributes['discount_price'] > 0 ) : ?>-<?= $model->raw_attributes['discount_price']; ?><?php endif; ?> */
      // Recalculate "discount_price" with all the discounts and "reduced_price" applied
      $discount_price_raw = $model->recalculate_discount_price();
      if ( $discount_price_raw > 0 ) :
    ?>
      -<?= $discount_price_raw; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "address__<attribute>"
    |--------------------------------------------------------------------------
    */
    case 'address__firstname':
    case 'address__lastname':
    case 'address__vat_code':
    case 'address__address_line':
    case 'address__city':
    case 'address__province':
    case 'address__postal_code':
    case 'address__phone':
  ?>
    <?php if ( $address_model ) : ?>
      <?= $address_model->getAttribute(str_replace("address__", "", $column)); ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "address_country_code"
    |--------------------------------------------------------------------------
    */
    case 'address_country_code':
  ?>
    <?php if ( $address_model && $address_model->country ) : ?>
      <?= $address_model->country->name; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "gateway_reference"
    |--------------------------------------------------------------------------
    */
    case 'gateway_reference':
  ?>
    <?php if ( $model->transaction && $model->transaction->status_type === 'accepted' ) : ?>
      <?= $model->transaction->get_gateway_reference(); ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "line_item_unit_price"
    |--------------------------------------------------------------------------
    */
    case 'line_item_unit_price':
  ?>
    <?php if ( $model->price_alias === 'reduced_price') : ?>
      <?= $model->raw_attributes['product_base_price']; ?>
    <?php else : ?>
      <?= $model->raw_attributes['unit_price']; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "line_item_discount_unit_price"
    |--------------------------------------------------------------------------
    */
    case 'line_item_discount_unit_price':
  ?>
    <?php
      // Show price with APPLIED discount (only percentage discounts)
      if ( $model->discount && $model->discount->is_fixed_amount == 0 ) :
    ?>
      <?= $model->get_unit_price_with_discount(true); ?>
    <?php
      // Show price without discount or reduced price
      else :
    ?>
      <?= $model->raw_attributes['unit_price']; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "line_item_total_price"
    |--------------------------------------------------------------------------
    */
    case 'line_item_total_price':
  ?>
    <?php if ( $model->price_alias === 'reduced_price') : ?>
      <?= $model->quantity * $model->raw_attributes['product_base_price']; ?>
    <?php else : ?>
      <?= $model->raw_attributes['total_price']; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "line_item_discount_total_price"
    |--------------------------------------------------------------------------
    */
    case 'line_item_discount_total_price':
  ?>
    <?php
      // Show price with APPLIED discount (only percentage discounts)
      if ( $model->discount && $model->discount->is_fixed_amount == 0 ) :
    ?>
      <?= $model->get_total_price_with_discount(true); ?>
    <?php
      // Show price without discount or reduced price
      else :
    ?>
      <?= $model->raw_attributes['total_price']; ?>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>