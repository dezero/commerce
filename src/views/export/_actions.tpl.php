<?php
/*
|--------------------------------------------------------------------------
| Partial with export actions: button + dropdown
|--------------------------------------------------------------------------
|
| Available variables:
|  - $export_url: Export URL
|  - $model: Model class
|  - $export_id: Export name
|  - $grid_id: Grid name
|  - $action_id: Current action
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;
  use dz\widgets\Modal;

  $vec_export_url_params = [
    'export_id' => $export_id
  ];
  // $vec_dropdown_menu_attributes = ['class' => 'dropdown-menu dropdown-menu-right'];
  $vec_dropdown_menu_attributes = ['class' => 'dropdown-menu'];

  // Action defined?
  if ( isset($action_id) )
  {
    $vec_export_url_params['action_id'] = $action_id;
  }

  // Grid defined?
  if ( isset($grid_id) )
  {
    $vec_export_url_params['grid_id'] = $grid_id;
  }

  // Model class
  $model_class = get_class($model);
  if ( isset($action_id) )
  {
    $vec_export_url_params['model_class'] = $model_class;
  }

  // Modal for export
  Modal::createModal("dz-modal-export-options", [
    'submitHtmlOptions' => [
      'class'           => 'dz-modal-submit-button dz-modal-button btn-primary',
      'id'              => 'export-submit-btn',
      'data-export-url' => Url::to($export_url),
      'data-create-url' => Url::to('/commerce/export/createExcel', ['name' => $export_id, 'model_class' => StringHelper::basename($model_class), 'model_scenario' => 'export']),
      'data-check-url'  => Url::to('/commerce/export/checkExcel'),
      'data-grid'       => $grid_id,
      'onclick'         => '$.dzExportSubmit(this);',
    ]
  ]);
?>
<div id="export-commerce-btn-group" class="btn-group" role="group">
  <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><?= Yii::t('app', 'EXPORT EXCEL'); ?></a>
  <?php
    // --------------------------------------------------------------------------
    // Export PRODUCTS to Excel
    // --------------------------------------------------------------------------
    if ( $export_id === 'products' ) :
  ?>
    <?=
      Html::dropdown_menu(
        [
          [
            'label' => Yii::t('app', 'Export PRODUCTS'),
            'icon' => 'table',
            'url' => '#',
            'htmlOptions' => [
              'id' => 'export-products-btn',
              'class' => 'dz-modal-form-link export-excel-btn',
              'data-url' => Url::to($export_url, $vec_export_url_params),
              'data-modal' => 'dz-modal-export-options',
              'data-submit-button' => Yii::t('app', 'Export'),
              'data-width' => 500,
              'data-batch' => 0
            ],
          ],
        ], $vec_dropdown_menu_attributes
      );
    ?>
  <?php
    // --------------------------------------------------------------------------
    // Export CUSTOMERS to Excel
    // --------------------------------------------------------------------------
    elseif ( $export_id === 'customers' ) :
  ?>
    <?=
      Html::dropdown_menu(
        [
          [
            'label' => Yii::t('app', 'Export CUSTOMERS'),
            'icon' => 'table',
            'url' => '#',
            'htmlOptions' => [
              'id' => 'export-customers-btn',
              'class' => 'dz-modal-form-link export-excel-btn',
              'data-url' => Url::to($export_url, $vec_export_url_params),
              'data-modal' => 'dz-modal-export-options',
              'data-submit-button' => Yii::t('app', 'Export'),
              'data-width' => 500,
              'data-batch' => 0
            ],
          ],
        ], $vec_dropdown_menu_attributes
      );
    ?>
  <?php
    // --------------------------------------------------------------------------
    // Export ORDERS to Excel
    // --------------------------------------------------------------------------
    else :
  ?>
    <?=
      Html::dropdown_menu(
        [
          // Export ORDERS to Excel
          [
            'label' => Yii::t('app', 'Export ORDERS'),
            'icon' => 'table',
            'url' => '#',
            'htmlOptions' => [
              'id' => 'export-orders-btn',
              'class' => 'dz-modal-form-link export-excel-btn',
              'data-url' => Url::to($export_url, $vec_export_url_params),
              'data-modal' => 'dz-modal-export-options',
              'data-submit-button' => Yii::t('app', 'Export'),
              'data-width' => 500,
              'data-batch' => 0
            ],
          ],

          // Export LINE ITEMS to Excel
          /*
          [
            'label' => Yii::t('app', 'Export LINE ITEMS to Excel'),
            'icon' => 'table',
            'url' => '#',
            'htmlOptions' => [
              'id' => 'export-line-items-btn',
              'class' => 'dz-modal-form-link export-excel-btn',
              'data-url' => Url::to($export_url, $vec_export_url_params),
              'data-modal' => 'dz-modal-export-options',
              'data-submit-button' => Yii::t('app', 'Export'),
              'data-width' => 500,
              'data-batch' => 0
            ],
          ],
          */
        ], $vec_dropdown_menu_attributes
      );
    ?>
  <?php endif; ?>
</div>
<?php
  // Export actions
  Yii::app()->clientScript->registerScript('js_export_actions',
    "$('#export-commerce-btn-group').find('.export-excel-btn').dzAjaxGridModalFormLink();"
    , CClientScript::POS_READY);
?>