<?php
/*
|----------------------------------------------------------------------------------
| GridView column partial page for Export PRODUCTS to an Excel file
|----------------------------------------------------------------------------------
|
| Available variables:
|  - $model: Current model
|  - $column: Column name
|
*/

  use dz\helpers\Html;
  use dz\helpers\Url;

  switch ( $column ) :
   /*
    |--------------------------------------------------------------------------
    | COLUMN "categories"
    |--------------------------------------------------------------------------
    */
    case 'categories':
  ?>
    <?php if ( $model->productCategories ) : ?>
      <?php
        // Uset "getRelated" to refresh the relationship data
        foreach ( $model->getRelated('productCategories', true) as $product_category_model ) :
      ?>
- <?= $product_category_model->category->title() . "\n"; ?>
      <?php endforeach; ?>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>