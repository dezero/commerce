<?php
/*
|--------------------------------------------------------------------------
| Export options form partial page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $export_id: Export name
|  - $export_form_options: Export field list
|  - $export_type: Export type [ excel | csv | pdf ]
|  - $export_url: Export URL
|  - $model_class: Model class name
|
*/
  
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Selected checkboxes
  $vec_export_data = ['info'];
?>
<div class="form-wrapper export-options-form-wrapper">
  <?php
    $export_form_options = [
      'id' => 'export-options-form',
      'action' => Url::to($export_url, ['type' => $export_type, 'action_id' => $action_id]),
      'enableAjaxValidation' => false,
      'enableClientValidation' => true,

      // Yii AJAX options
      'clientOptions' => [
        'validateOnSubmit' => true,
        'validateOnType' => false,
        'validateOnChange' => false,
        'afterValidate' => 'js:function(form,data,hasError){ $.dzAjaxAfterValidate(form,data,hasError); return false; }',
      ],

      'htmlOptions' => [
        'enctype' => 'multipart/form-data',
        // 'data-grid_id' => $grid_id
      ],

      'dzAjaxOptions' => [
        'modal_id' => 'dz-modal-export-options'
      ]
    ];

    // Start model form (http://yii-booster.clevertech.biz/components.html#forms)
    $export_form = $this->beginWidget('@ext.DzAjaxForm.DzAjaxForm', $export_form_options);
  ?>

  <div id="export-options-field-wrapper">
    <div id="export-total-loading"><h4><?= Yii::t('app', 'Loading items to export...'); ?></h4></div>
    <div id="export-total-error" class="hide">
      <h4 class="text-danger"><strong><span class="export-total-items"></span> items</strong> to export have been detected.</h4>
      <p><?= Yii::t('app', 'It is not allowed to export so many items. Please, reduce the number of items to export. For example, you can filter by type.'); ?>.</p>
    </div>
    <div id="export-total-success" class="hide">
      <h4 class="text-success"><strong><span class="export-total-items"></span> items</strong> to export have been detected.</h4>
      <p><?= Yii::t('app', 'Please, click the button below to continue'); ?></p>
    </div>
    <input type="hidden" name="export_id" value="<?= $export_id; ?>">
    <input type="hidden" name="export_options" value="1">
    <input type="hidden" name="export_action_id" value="<?= $action_id; ?>" class="extra-export-param">
    <?php /*
    <h5>Select data to export:</h5>
    <div class="field-checkboxlist">
      <?= BsHtml::checkBoxListControlGroup('export_options', $vec_export_data, $vec_export_options); ?>
      <?php
        // echo DzHtml::checkBoxList('export_options', [], $vec_export_options, [
        //   'template' => '{input}{label}',
        //   'container' => '',
        // ]);
      ?>
    </div>
    */ ?>
  </div>
  <div id="export-loading" class="hide">
    <h4><img src="<?php echo Yii::app()->theme->baseUrl;?>/images/loading.gif" width="20" height="20" class="batch-loading" /> <span class="batch-description"><?= Yii::t('app', 'Exporting data to an Excel file...'); ?></span></h4>
    <p><?= Yii::t('app', 'This action could take up to several minutes'); ?> <span class="text-danger"><?= Yii::t('app', 'Please, do not refresh the page!'); ?></span></p>
  </div>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACTIONS
  |--------------------------------------------------------------------------
  */
?>
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'  => 'submit',
          'type'        => 'primary',
          'label'       => Yii::t('app', 'Export'),
        ]);
      
        // Cancel
        echo Html::link(Yii::t('app', 'Close'), '#', ['class' => 'btn btn-dark btn-close'] );
      ?>
    </div><!-- form-actions -->
  </div>

  <?php
    // End model form
    $this->endWidget();
  ?>
</div><!-- .form-wrapper -->

<?php
  // Export Excel via JS
  Yii::app()->clientscript
    ->registerScriptFile(Yii::app()->theme->baseUrl. '/js/dz.export.js', CClientScript::POS_END)
    ->registerScript('js_export_options',
      "$.dzExporTotalItems('". Url::to('/commerce/export/exportTotalItems', ['class' => $model_class]) ."');"
    , CClientScript::POS_READY);
?>