<?php
/*
|--------------------------------------------------------------------------
| Admin list page for Discount models
|--------------------------------------------------------------------------
|
| Available variables:
|  - $discount_model: Discount model
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
  
  // Page title
  $page_title = Yii::t('app', 'Discounts');
  if ( $discount_model->disable_filter == 1 )
  {
    $page_title .= ' - Inactive';
  }
  else if ( $discount_model->disable_filter == 2 )
  {
    $page_title .= ' - Active';
  }
  $this->pageTitle = Yii::t('app', $page_title);
?>
<?php
  // Sidebar menu with totals
  $this->renderPartial('_sidebar', array(
    'model'       => $discount_model,
    'vec_totals'  => $vec_totals
  ));
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <div class="page-header-actions">
      <?=
        Html::link('<i class="icon wb-plus"></i> Add new discount', ['create'], [
          'class' => 'btn btn-primary'
        ]);
      ?>
    </div>
  </div>
  <div class="page-content">
    <div class="row row-lg">
      <div class="col-lg-12">
        <div class="panel panel-top-summary">
          <div class="panel-body container-fluid">
            <?php
              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                $this->widget('dz.grid.GridView', [
                  'id'                => 'discount-grid',
                  'dataProvider'      => $discount_model->search(),
                  'filter'            => $discount_model,
                  'emptyText'         => Yii::t('app', 'No discounts found'),
                  'enableHistory'     => true,
                  'loadModal'         => true,
                  'enableSorting'     => false,
                  'enableSelect2'     => true,
                  'type'              => ['striped', 'hover'],
                  'beforeAjaxUpdate'  => 'js:function(id, options){ options.url += "&Discount[disable_filter]='. $discount_model->disable_filter .'"; $.dzGridView.beforeAjaxUpdate(id, options); }',
                  'columns'           => [
                    [
                      'name' => 'name',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "name", "model" => $data]))',
                    ],
                    [
                      'name' => 'discount_type',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "discount_type", "model" => $data]))',
                      'filter' => $discount_model->discount_type_labels()
                    ],
                    [
                      'name' => 'code',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("_grid_column", ["column" => "code", "model" => $data]))',
                    ],
                    [
                      'class' => 'dz.grid.ButtonColumn',
                      'header' => 'ACTION',
                      'template' => '{update}',
                      'deleteButton' => false,
                      'clearButton' => true,
                      'viewButton' => false,
                      'updateButton' => true,
                      'menuAction' => function($data, $row) {
                        return [
                          [
                            'label' => Yii::t('app', 'View uses'),
                            'icon' => 'shopping-cart',
                            'url' => ['/commerce/discount/uses', 'discount_id' => $data->discount_id],
                            'htmlOptions' => [
                              'target' => '_blank'
                            ]
                          ],
                          [
                            'label' => Yii::t('app', 'Enable'),
                            'icon' => 'arrow-up',
                            'url' => ['/commerce/discount/enable', 'id' => $data->discount_id],
                            'visible' => '!$data->is_enabled()',
                            'confirm' => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this discount?</h3>',
                            'htmlOptions' => [
                              'id' => 'enable-discount-'. $data->discount_id,
                              'data-gridview' => 'discount-grid'
                            ]
                          ],
                          [
                            'label' => Yii::t('app', 'Disable'),
                            'icon' => 'arrow-down',
                            'url' => ['/commerce/discount/disable', 'id' => $data->discount_id],
                            'visible' => '$data->is_enabled()',
                            'confirm' => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this discount?</h3><p>A disabled discount cannot be used by customers.</p>',
                            'htmlOptions' => [
                              'id' => 'disable-discount-'. $data->discount_id,
                              'data-gridview' => 'discount-grid'
                            ],
                          ],
                          '---',
                          [
                            'label' => Yii::t('app', 'Delete'),
                            'icon' => 'trash',
                            'url' => ['/commerce/discount/delete', 'id' => $data->discount_id],
                            'confirm' => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this discount?</h3><p>This discount will be deleted immediately. You can\'t undo this action.</p>',
                            'htmlOptions' => [
                              'id' => 'disable-discount-'. $data->discount_id,
                              'data-gridview' => 'discount-grid'
                            ],
                          ]
                        ];
                      },
                    ],
                  ],
                ]);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- .page-main -->
