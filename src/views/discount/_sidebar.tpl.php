<?php
/*
|--------------------------------------------------------------------------
| Sidebar - Discount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Discount model
|  - $vec_totals: Array with total numbers of enabled/disabled models
|
*/
  use dz\helpers\Url;
?>
<div class="page-aside">
  <div class="page-aside-switch">
    <i class="icon wb-chevron-left" aria-hidden="true"></i>
    <i class="icon wb-chevron-right" aria-hidden="true"></i>
  </div>
  <div class="page-aside-inner page-aside-scroll">
    <div data-role="container">
      <div data-role="content">
        <div class="page-aside-section">
          <div class="list-group mb-10">
            <a class="list-group-item justify-content-between<?php if ( empty($model->disable_filter) ) : ?> active<?php endif; ?>" href="<?= Url::to('/commerce/discount'); ?>">
              <span><i class="icon wb-order" aria-hidden="true"></i> All Discounts</span>
            </a>
          </div>
        </div>
        <section class="page-aside-section">
          <div class="list-group mb-5">
            <a class="list-group-item<?php if ( $model->disable_filter == 1 && $model->is_active != 0 ) : ?> active<?php endif; ?> ( " href="<?= Url::to('/commerce/discount', ['Discount[disable_filter]' => 1, 'Discount[is_active]' => 1]); ?>">
              <span>Active</span>
              <span class="item-right"><?= $vec_totals['active']; ?></span>
            </a>
            <a class="list-group-item<?php if ( $model->disable_filter == 1 && $model->is_active == 0 ) : ?> active<?php endif; ?>" href="<?= Url::to('/commerce/discount', ['Discount[disable_filter]' => 1, 'Discount[is_active]' => 0]); ?>">
              <span>Inactive</span>
              <span class="item-right"><?= $vec_totals['inactive']; ?></span>
            </a>
          </div>
          <div class="list-group mb-20">
            <a class="list-group-item<?php if ( $model->disable_filter == 2 ) : ?> active<?php endif; ?>" href="<?= Url::to('/commerce/discount', ['Discount[disable_filter]' => 2]); ?>">
              <span>Disabled manually</span>
              <span class="item-right"><?= $vec_totals['disabled']; ?></span>
            </a>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>