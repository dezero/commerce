<?php
/*
|--------------------------------------------------------------------------
| Create form page for Discount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $discount_model: Discount model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  
  // Page title
  $this->pageTitle = Yii::t('app', 'Add new discount');
?>

<div class="page-header">
  <h1 class="page-title"><?= $this->pageTitle; ?></h1>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => 'Manage Discounts',
        'url' => ['/commerce/discount/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">
  <?php
    // Render form
    $this->renderPartial('_form', [
      'discount_model' => $discount_model,
      'form_id'       => $form_id,
      'button'        => Yii::t('app', 'Create')
    ]);
  ?>
</div>