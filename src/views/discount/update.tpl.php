<?php
/*
|--------------------------------------------------------------------------
| Update form page for Discount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $discount_model: Discount model
|  - $form_id: Form identifier
|
*/
  // Header title
  $this->renderPartial('_header_title', [
    'discount_model' => $discount_model,
  ]);
?>
<div class="page-content container-fluid">
  <?php
    // Header menu
    $this->renderPartial('_header_menu', [
      'discount_model' => $discount_model,
    ]);

    // Render form
    $this->renderPartial('_form', [
      'discount_model'  => $discount_model,
      'form_id'         => $form_id,
      'button'          => Yii::t('app', 'Save')
    ]);
  ?>
</div>