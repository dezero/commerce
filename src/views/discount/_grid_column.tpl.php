<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for Discount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: Discount model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "name"
    |--------------------------------------------------------------------------
    */
    case 'name':
  ?>
    <a href="<?= Url::to('/commerce/discount/update', ['id' => $model->discount_id]); ?>"><?= $model->name; ?></a>
    <?php
      // Available dates
      if ( !empty($model->start_date) ) :
    ?>
      <p><em>From</em> <?= $model->start_date; ?><?php if ( !empty($model->expiration_date) ) : ?> <em>to</em> <?= $model->expiration_date; ?><?php endif; ?></p>
    <?php endif; ?>
    <?php
      // Number of uses
      if ( $model->totalUses > 0 ) :
    ?>
      <p><a href="<?php echo Url::to('/commerce/discount/uses', ['discount_id' => $model->discount_id]); ?>" class="used-link">Used <?= $model->totalUses .' '. Yii::t('app','time|times', $model->totalUses); ?></a></p>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "discount_type"
    |--------------------------------------------------------------------------
    */
    case 'discount_type':
  ?>
    <?= $model->discount_type_label($model->discount_type); ?>
    <p>
      <?php
        // Disabled discount
        if ( $model->is_disabled() ) :
      ?>
        <span class="badge badge-danger mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= $model->disable_date; ?>"><?= Yii::t('app', 'DISABLED'); ?></span>
      <?php
        // Enabled discount but it's inactive
        elseif ( $model->is_active == 0 ) :
      ?>
        <?php if ( ! $model->is_valid() && !empty($model->error_message) ) : ?>
          <span class="badge badge-warning mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= Yii::t('app', $model->error_message); ?>"><?= Yii::t('app', 'INACTIVE'); ?></span>
        <?php else : ?>
          <span class="badge badge-warning mr-5"><?= Yii::t('app', 'INACTIVE'); ?></span>
        <?php endif; ?>
      <?php
        // Enabled & active discount (customers can use it)
        else :
      ?>
        <span class="badge badge-success mr-5"><?= Yii::t('app', 'ACTIVE'); ?></span>
      <?php endif; ?>
    </p>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "code"
    |--------------------------------------------------------------------------
    */
    case 'code':
  ?>
    <?php
      $vec_information = [];

      // Coupon code
      if ( $model->discount_type === 'code' && !empty($model->code) )
      {
        $vec_information[] = '<em>Coupon Code:</em> <strong> '. $model->code .'</strong>';  
      }

      // Amount
      $vec_information[] = '<em>Discount Amount:</em> <strong>'. $model->amount_label() .'</strong>';

      // Application type
      $vec_information[] = '<em>Apply to:</em> <strong>'. ($model->application_type == 'total' ? 'ORDER total price' : 'PRODUCTS total price') .'</strong>';

      // Usage limit
      if ( !empty($model->limit_uses) )
      {
        $vec_information[] = '<em>Limit:</em> <strong>'. $model->limit_uses .' '. Yii::t('app','use|uses', $model->limit_uses) .'</strong>';
      }

      // Per user limit?
      if ( $model->is_per_user_limit == 1 )
      {
        $vec_information[] = '<em>Limit per user:</em> <strong>1 maximum use per customer</strong>';
      }

      // Minimum amount
      if ( !empty($model->minimum_amount) )
      {
        $vec_information[] = '<em>Minimum amount:</em> <strong>'. $model->minimum_amount .' &euro;</strong>';
      }

      // After order restriction?
      if ( $model->raw_attributes['is_after_order_restriction'] == 1 )
      {
        $vec_information[] = '<em>Valid:</em> <strong>'. $model->after_order_num_days .' days after order</strong>';
      }

      // Product restriction
      if ( $model->is_product_restriction == 1 )
      {
        $vec_information[] = $model->product_restriction_type === 'exclude' ? '<em>Restriction:</em> <strong>EXCLUDED products</strong>' : '<em>Restriction:</em> <strong>Only INCLUDED products</strong>';
      }

      echo Html::ul($vec_information);
    ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "customer"
    |--------------------------------------------------------------------------
    */
    case 'customer':
  ?>
    <?php if ( $model->user_id > 0 && $model->user ) : ?>
      <?php
        // Get customer email
        $customer_email = Yii::app()->customerManager->get_customer_email($model->user);
        if ( !empty($customer_email) ) :
      ?>
        <a href="mailto:<?= $customer_email; ?>" target="_blank"><?= $customer_email; ?></a><br>
      <?php endif; ?>
      <?= $model->user->fullname(); ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "order"
    |--------------------------------------------------------------------------
    */
    case 'order':
  ?>
    <?php if ( $model->order ) : ?>
      <a href="<?=  Url::to('/commerce/order/view', ['id' => $model->order_id]); ?>"><?= $model->order->title(); ?></a>
      <br>#<?= $model->order_id; ?>
    <?php endif; ?>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "final_price"
    |--------------------------------------------------------------------------
    */
    case 'final_price':
  ?>
    <?php if ( $model->order ) : ?>
      <?= $model->order->get_total_price(); ?>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>
