<?php
/*
|--------------------------------------------------------------------------
| Header menu zone for a Discount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $discount_model: Product model
|
*/
  use dz\helpers\Url;

  // Controller and action names in lowercase
  $current_controller = Yii::currentController(true);
  $current_action = Yii::currentAction(true);

  // Tab number
  $tabs_number = 1;
  if ( $discount_model->is_product_restriction == 1 )
  {
    $tabs_number++;
  }
  if ( $discount_model->is_category_restriction == 1 )
  {
    $tabs_number++;
  }

  if ( $tabs_number > 1 ) :
?>
  <ul class="nav-quick-header nav-quick nav-quick-bordered row">
    <li class="nav-item col-md-<?= (12 / $tabs_number); ?><?php if ( $current_action == 'update' ) : ?> active<?php endif; ?>">
      <a class="nav-link" href="<?= Url::to('/commerce/discount/update', ['id' => $discount_model->discount_id]); ?>">
        <i class="icon wb-flag" aria-hidden="true"></i>
        <span><?= Yii::t('app', 'Discount Information'); ?></span>
      </a>
    </li>
    <?php
      //----------------------------------------------------------------
      // PRODUCTS RESTRICTION
      //----------------------------------------------------------------
      if ( $discount_model->is_product_restriction == 1 ) :
    ?>
      <li class="nav-item col-md-<?= (12 / $tabs_number); ?><?php if ( $current_controller == 'discountproduct' ) : ?> active<?php endif; ?>">
        <a class="nav-link" href="<?= Url::to('/commerce/discountProduct', ['discount_id' => $discount_model->discount_id]); ?>">
          <i class="icon wb-shopping-cart" aria-hidden="true"></i>
          <span><?= $discount_model->product_restriction_type === 'exclude' ? Yii::t('app', 'Excluded Products') : Yii::t('app', 'Included Products'); ?></span>
        </a>
      </li>
    <?php endif; ?>
  </ul>
<?php endif; ?>