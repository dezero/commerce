<?php
/*
|--------------------------------------------------------------------------
| Form partial page for Discount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $discount_model: Discount model
|  - $form_id: Form identifier
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  
  $que_action = Yii::app()->controller->action->id;

  $form = $this->beginWidget('@bootstrap.widgets.BsActiveForm', [
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => [
      'class' => 'form-horizontal discount-form',
      'enctype' => 'multipart/form-data',
      'autocomplete' => 'off'
    ]
  ]);

  // Error summary
  $errors = $form->errorSummary($discount_model);
  if ( $errors )
  {
    echo $errors;
  }
?>
<input name="StatusChange" id="status-change" class="form-control" type="hidden" value="">

<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">Discount Information</h3>
  </header>

  <div class="panel-body">

    <div class="form-group row<?php if ( $discount_model->hasErrors('name') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($discount_model, 'name', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($discount_model, 'name', [
            'maxlength' => 255,
            'placeholder' => ''
          ]);
        ?>
        <p class="help-block">
          Internal name (not visible for customers).
          <?php
            /*
            <u>Public name</u> (visible for customers) if code is empty. It means, the discount is applied permanently.<br>
            <u>Internal name</u> (not visible for customers), if you enter a coupon code. In this case, the code will be showed to the customer.
            */
          ?>
        </p>
        <?= $form->error($discount_model, 'name'); ?>
      </div>
    </div>

    <div id="discount-type" class="form-group row<?php if ( $discount_model->hasErrors('discount_type') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($discount_model, 'discount_type', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?php if ( $que_action !== 'create' ) : ?>
          <?php if ( $discount_model->discount_type === 'auto' ) : ?>
            <div class="form-control view-field" style="max-width: 350px;">
              <?= Html::encode($discount_model->discount_type_label('auto')); ?>
            </div>
            <p class="help-block pl-10">The discount is applied automatically. The customer doesn't need to enter any coupon code.</p>
          <?php else : ?>
            <?= Html::encode($discount_model->discount_type_label('code')); ?>
            <div class="form-control view-field" style="max-width: 350px;">
              <p class="help-block pl-10">The customer must enter a coupon code to apply this discount.</p>
            </div>
          <?php endif; ?>
        <?php else : ?>
          <div class="form-group form-radio-group">
            <div class="radio-custom radio-default pt-20">
              <input type="radio" id="discount_type-auto" name="Discount[discount_type]" value="auto"<?php if ( $discount_model->discount_type == 'auto' ) : ?> checked<?php endif; ?>>
              <label for="discount_type-auto"><?= $discount_model->discount_type_label('auto'); ?></label>
              <p class="help-block pl-10">The discount is applied automatically. The customer doesn't need to enter any coupon code.</p>
            </div>
            <div class="radio-custom radio-default">
              <input type="radio" id="discount_type-code" name="Discount[discount_type]" value="code"<?php if ( $discount_model->discount_type == 'code' ) : ?> checked<?php endif; ?>>
              <label for="discount_type-code"><?= $discount_model->discount_type_label('code'); ?></label>
              <p class="help-block pl-10">The customer must enter a coupon code to apply this discount.</p>
            </div>
          </div>
          <?= $form->error($discount_model, 'discount_type'); ?>
        <?php endif; ?>
      </div>
    </div>

    <div id="discount-code-wrapper" class="form-group row<?php if ( $discount_model->hasErrors('code') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($discount_model, 'code', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?php if ( $que_action != 'create' AND !empty($discount_model->code) ) : ?>
          <div class="form-control view-field" style="max-width: 220px;">
            <?= Html::encode($discount_model->code); ?>
          </div>
        <?php else : ?>
          <?=
            $form->textField($discount_model, 'code', [
              'maxlength' => 128,
              'placeholder' => '',
              'style' => 'max-width: 220px;'
            ]);
          ?>
          <div class="checkbox-custom checkbox-primary">
            <input id="discount-auto-code" type="checkbox">
            <label for="discount-auto-code">Auto Generate?</label>
          </div>
          <?= $form->error($discount_model, 'code'); ?>
          <p class="help-block">
            <?php /*Leave it empty if you want to apply this discount permanently.<br> */ ?>
            This discount code must be entered on the checkout process by the customers to be validated and applied.
          </p>
        <?php endif; ?>
      </div>
    </div>

    <hr class="mb-30 mt-10">

    <div id="amount-type" class="form-group row<?php if ( $discount_model->hasErrors('is_fixed_amount') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($discount_model, 'is_fixed_amount', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <div class="form-group form-radio-group">
          <div class="radio-custom radio-default radio-inline">
              <input type="radio" id="is_fixed_amount-1" name="Discount[is_fixed_amount]" value="1"<?php if ( $discount_model->is_fixed_amount == 1 ) : ?> checked<?php endif; ?>>
              <label for="is_fixed_amount-1">Fixed</label>
            </div>
            <div class="radio-custom radio-default radio-inline">
              <input type="radio" id="is_fixed_amount-0" name="Discount[is_fixed_amount]" value="0"<?php if ( $discount_model->is_fixed_amount == 0 ) : ?> checked<?php endif; ?>>
              <label for="is_fixed_amount-0">Percentage</label>
            </div>
          </div>
        <?= $form->error($discount_model,'is_fixed_amount'); ?>
        <p class="help-block">Fixed amount value (-6,00 €) or percentage amount value (-10%)</p> 
      </div>
    </div>

    <div id="fixed-amount-wrapper" class="form-group form-currency-group row<?php if ( $discount_model->hasErrors('amount') ) : ?> has-danger<?php endif; ?><?php if ( $discount_model->is_fixed_amount == 0 ) : ?> hide<?php endif; ?>">
      <label class="col-lg-2 col-sm-2 form-control-label" for="Discount_amount">Fixed Amount</label>
      <div class="col-lg-10">
        <?=
          $form->textField($discount_model, 'amount', array(
            'data-step' => '0.10',
            'placeholder' => ''
          ));
        ?>
        <?= $form->error($discount_model, 'amount'); ?>
        <p class="help-block">Value in <strong>euros</strong>. Please, not enter the negative symbol at the beginning.<br>A fixed discount will be applied to application's total price</p>
      </div>
    </div>

    <div id="perc-amount-wrapper" class="form-group form-currency-group row<?php if ( $discount_model->hasErrors('amount') ) : ?> has-danger<?php endif; ?><?php if ( $discount_model->is_fixed_amount == 1 ) : ?> hide<?php endif; ?>">
      <?= $form->label($discount_model, 'perc_amount', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
       <?=
          $form->textField($discount_model, 'perc_amount', array(
            'data-plugin'         => 'TouchSpin',
            'data-min'            => '0',
            'data-max'            => '100',
            'data-step'           => '1',
            'data-decimals'       => '0',
            'data-boostat'        => '1',
            'data-maxboostedstep' => '1',
            'data-postfix'        => '%',
            'placeholder'         => ''
          ));
        ?>
        <?= $form->error($discount_model, 'amount'); ?>
        <p class="help-block">Value in <strong>% (percentage)</strong><br>A discount percentage will be applied to application's total price</p>
      </div>
    </div>

    <div id="application-type" class="form-group row<?php if ( $discount_model->hasErrors('application_type') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($discount_model, 'application_type', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <div class="form-group form-radio-group">
          <div class="radio-custom radio-default">
            <input type="radio" id="application_type-total" name="Discount[application_type]" value="total"<?php if ( $discount_model->application_type == 'total' ) : ?> checked<?php endif; ?>>
            <label for="application_type-total">ORDER total price</label>
            <p class="help-block pl-10">Discount is applied to the TOTAL order price (including shipping costs)</p>
          </div>
          <div class="radio-custom radio-default pt-20">
            <input type="radio" id="application_type-items" name="Discount[application_type]" value="items"<?php if ( $discount_model->application_type == 'items' ) : ?> checked<?php endif; ?>>
            <label for="application_type-items">PRODUCTS total price</label>
            <p class="help-block pl-10">Discount is applied to the total PRODUCTS price (shipping costs NOT included)</p>
          </div>
        </div>
        <?= $form->error($discount_model,'application_type'); ?>
      </div>
    </div>

    <hr class="mb-30 mt-10">

    <div class="form-group row<?php if ( $discount_model->hasErrors('internal_comments') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($discount_model, 'internal_comments', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?php
          $this->widget('@lib.redactor.ERedactorWidget', [
            'model'     => $discount_model,
            'attribute' => 'internal_comments',
            'options'   => [
              'minHeight' => 100,
            ]
          ]);
        ?>
        <p class="help-block">Internal, not visible to customers.</p>
      </div>
    </div>
  </div><!-- .panel-body -->
</div><!-- .panel -->


<div class="panel">
  <header class="panel-heading">
    <h3 class="panel-title">Discount Conditions (Optional)</h3>
  </header>

  <div class="panel-body">
    <div class="form-group row<?php if ( $discount_model->hasErrors('start_date') || $discount_model->hasErrors('expiration_date') ) : ?> has-danger<?php endif; ?>">
      <label class="col-lg-2 col-sm-2 form-control-label" for="Discount_start_date">Available Dates</label>
      <div class="col-lg-10">
        <div id="discount-date-range" class="input-daterange">
          <div class="input-group">
            <span class="input-group-addon">
              <i class="icon wb-calendar" aria-hidden="true"></i>
            </span>
            <?=
              $form->textField($discount_model, 'start_date', [
                'placeholder' => '',
                'maxlength' => 10
              ]);
            ?>
          </div>
          <div class="input-group">
            <span class="input-group-addon">to</span>
            <?=
              $form->textField($discount_model, 'expiration_date', [
                'placeholder' => '',
                'maxlength' => 10
              ]);
            ?>
          </div>
        </div>
        <p class="help-block">If no dates, the discount is always available<br>You can only set a start date (without expiration date)</p>
        <?= $form->error($discount_model, 'start_date'); ?>
        <?= $form->error($discount_model, 'expiration_date'); ?>
      </div>
    </div>

    <div class="form-group form-currency-group row<?php if ( $discount_model->hasErrors('limit_uses') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($discount_model, 'limit_uses', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-6" style="max-width: 175px;">
       <?=
          $form->textField($discount_model, 'limit_uses', array(
            'data-plugin'         => 'TouchSpin',
            'data-min'            => '0',
            'data-max'            => '99999',
            'data-step'           => '1',
            'data-decimals'       => '0',
            'data-boostat'        => '1',
            'data-maxboostedstep' => '1',
            'data-postfix'        => '',
            'placeholder'         => ''
          ));
        ?>
        <?= $form->error($discount_model, 'limit_uses'); ?>
        <p class="help-block">0 = unlimited uses</p>
      </div>
    </div>

    <div class="form-group row<?php if ( $discount_model->hasErrors('is_per_user_limit') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($discount_model, 'is_per_user_limit', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <div class="form-group form-radio-group">
          <div class="radio-custom radio-default radio-inline">
              <input type="radio" id="is_per_user_limit-1" name="Discount[is_per_user_limit]" value="1"<?php if ( $discount_model->is_per_user_limit == 1 ) : ?> checked<?php endif; ?>>
              <label for="is_per_user_limit-1">Yes</label>
            </div>
            <div class="radio-custom radio-default radio-inline">
              <input type="radio" id="is_per_user_limit-0" name="Discount[is_per_user_limit]" value="0"<?php if ( $discount_model->is_per_user_limit == 0 ) : ?> checked<?php endif; ?>>
              <label for="is_per_user_limit-0">No</label>
            </div>
          </div>
        <?= $form->error($discount_model,'is_per_user_limit'); ?>
        <p class="help-block">If YES, 1 maximum use per customer will be applied.<br>If NO, a customer could use this discount multiple times.</p> 
      </div>
    </div>

    <div class="form-group form-currency-group row<?php if ( $discount_model->hasErrors('minimum_amount') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($discount_model, 'minimum_amount', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <?=
          $form->textField($discount_model, 'minimum_amount', array(
            'data-step' => '0.10',
            'placeholder' => ''
          ));
        ?>
        <?= $form->error($discount_model, 'minimum_amount'); ?>
        <p class="help-block">Value in <strong>euros</strong>. Restrict the discount to only those orders where the customer has purchased a minimum total amount.</p>
      </div>
    </div>

    <div id="after-order-restriction" class="form-group row<?php if ( $discount_model->hasErrors('is_after_order_restriction') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($discount_model, 'is_after_order_restriction', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <div class="form-group form-radio-group">
          <div class="radio-custom radio-default radio-inline">
              <input type="radio" id="is_after_order_restriction-1" name="Discount[is_after_order_restriction]" value="1"<?php if ( $discount_model->is_after_order_restriction == 1 ) : ?> checked<?php endif; ?>>
              <label for="is_after_order_restriction-1">Yes</label>
            </div>
            <div class="radio-custom radio-default radio-inline">
              <input type="radio" id="is_after_order_restriction-0" name="Discount[is_after_order_restriction]" value="0"<?php if ( $discount_model->is_after_order_restriction == 0 ) : ?> checked<?php endif; ?>>
              <label for="is_after_order_restriction-0">No</label>
            </div>
          </div>
        <?= $form->error($discount_model,'is_after_order_restriction'); ?>
        <p class="help-block">If YES, the discount is only valid X days after the last order of the customer.</p>
      </div>
    </div>

    <div id="after-order-num-days" class="form-group form-currency-group row<?php if ( $discount_model->hasErrors('after_order_num_days') ) : ?> has-danger<?php endif; ?><?php if ( $discount_model->is_after_order_restriction == 0 ) : ?> hide<?php endif; ?>">
      <div class="col-lg-2 col-sm-2 form-control-label"></div>
      <div class="col-lg-9">
        <?= $form->label($discount_model, 'after_order_num_days', ['class' => 'mb-10']); ?>
        <div style="max-width: 175px;">
          <?=
            $form->textField($discount_model, 'after_order_num_days', array(
              'data-plugin'         => 'TouchSpin',
              'data-min'            => '1',
              'data-max'            => '99999',
              'data-step'           => '1',
              'data-decimals'       => '0',
              'data-boostat'        => '1',
              'data-maxboostedstep' => '1',
              'data-postfix'        => '',
              'placeholder'         => '',
              'autocomplete'        => 'off',
              'role'                => 'presentation'
            ));
          ?>
        </div>
        <?= $form->error($discount_model, 'after_order_num_days'); ?>
        <p class="help-block">Number of days after the order is made.<br>Example, if you put 10 days and the customer made an order on 10th April, the discount will be valid until 20th April (included).</p>
      </div>
    </div>

    <hr class="mb-30 mt-10">

    <div id="product-restriction-type" class="form-group row<?php if ( $discount_model->hasErrors('product_restriction_type') ) : ?> has-danger<?php endif; ?>">
      <?= $form->label($discount_model, 'product_restriction_type', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
      <div class="col-lg-10">
        <div class="form-group form-radio-group">
          <div class="radio-custom radio-default">
            <input type="radio" id="product_restriction_type-all" name="Discount[product_restriction_type]" value="all"<?php if ( $discount_model->is_product_restriction == 0 ) : ?> checked<?php endif; ?>>
            <label for="product_restriction_type-all">All products</label>
            <p class="help-block pl-10">The discount is applied to ALL the products.</p>
          </div>
          <div class="radio-custom radio-default pt-20">
            <input type="radio" id="product_restriction_type-exclude" name="Discount[product_restriction_type]" value="exclude"<?php if ( $discount_model->is_product_restriction == 1 && $discount_model->product_restriction_type == 'exclude' ) : ?> checked<?php endif; ?>>
            <label for="product_restriction_type-exclude">EXCLUDE products</label>
            <p class="help-block pl-10">The discount is applied to all the products EXCLUDING the selected in the list*.<br><i>* Choose this option and save the changes so that you can select the products.</i></p>
          </div>
          <div class="radio-custom radio-default pt-20">
            <input type="radio" id="product_restriction_type-include" name="Discount[product_restriction_type]" value="include"<?php if ( $discount_model->is_product_restriction == 1 && $discount_model->product_restriction_type == 'include' ) : ?> checked<?php endif; ?>>
            <label for="product_restriction_type-include">INCLUDE only a products</label>
            <p class="help-block pl-10">The discount is only applied to the selected products*.<br><i>* Choose this option and save the changes so that you can select the products.</i></p>
          </div>
        </div>
        <?= $form->error($discount_model,'product_restriction_type'); ?>
      </div>
    </div>

    <?php /*

      <hr class="mb-30 mt-10">

      <div id="user-restriction-type" class="form-group row<?php if ( $discount_model->hasErrors('user_restriction_type') ) : ?> has-danger<?php endif; ?>">
        <?= $form->label($discount_model, 'user_restriction_type', ['class' => 'col-lg-2 col-sm-2 form-control-label']); ?>
        <div class="col-lg-10">
          <div class="form-group form-radio-group">
            <div class="radio-custom radio-default">
              <input type="radio" id="user_restriction_type-all" name="Discount[user_restriction_type]" value="all"<?php if ( $discount_model->is_user_restriction == 0 ) : ?> checked<?php endif; ?>>
              <label for="user_restriction_type-all">All users</label>
              <p class="help-block pl-10">The discount is applied to ALL the users.</p>
            </div>
            <div class="radio-custom radio-default pt-20">
              <input type="radio" id="user_restriction_type-exclude" name="Discount[user_restriction_type]" value="users"<?php if ( $discount_model->is_user_restriction == 1 && $discount_model->user_restriction_type == 'users' ) : ?> checked<?php endif; ?>>
              <label for="user_restriction_type-exclude">Only for selected USERS</label>
              <p class="help-block pl-10">The discount is only applied to the selected users*.<br><i>* Choose this option and save the changes so that you can select the users.</i></p>
            </div>
            <div class="radio-custom radio-default pt-20">
              <input type="radio" id="user_restriction_type-include" name="Discount[user_restriction_type]" value="roles"<?php if ( $discount_model->is_user_restriction == 1 && $discount_model->product_restriction_type == 'roles' ) : ?> checked<?php endif; ?>>
              <label for="user_restriction_type-include">Only for selected ROLES</label>
              <p class="help-block pl-10">The discount is only applied to the selected roles*.<br><i>* Choose this option and save the changes so that you can select the roles.</i></p>
            </div>
          </div>
          <?= $form->error($discount_model,'user_restriction_type'); ?>
        </div>
      </div>
    */ ?>

  </div>

</div>

<?php
  /*
  |--------------------------------------------------------------------------
  | ACCIONES
  |--------------------------------------------------------------------------
  */
?>  
  <div class="form-group row">
    <div class="col-lg-12 form-actions buttons">
      <?php
        // Buttons (http://yii-booster.clevertech.biz/components.html#buttons)
        $this->widget('@bootstrap.widgets.TbButton', [
          'buttonType'  => 'submit',
          'type'        => 'primary',
          'label'       => $button,
        ]);
        
        // Cancel
        echo Html::link(Yii::t('app', 'Cancel'), ['/commerce/discount'], ['class' => 'btn btn-dark']);

        if ( $que_action !== 'create' )
        {
          // Disable discount button
          if ( $discount_model->is_enabled() )
          {
            echo Html::link(Yii::t('app', 'Disable'), ['#'], [
              'id'            => 'disable-discount-btn',
              'class'         => 'btn btn-danger right',
              'data-confirm'  => '<h3>Are you sure you want to <span class=\'text-danger\'>DISABLE</span> this discount?</h3><p>A disabled discount cannot be applied to any application.</p>',
              'data-form'     => $form_id,
              'data-value'    => 'disable'
            ]);
          }

          // Enable discount button
          else
          {
            echo Html::link(Yii::t('app', 'Enable'), ['#'], [
              'id'            => 'enable-discount-btn',
              'class'         => 'btn btn-success right',
              'data-confirm'  => '<h3>Are you sure you want to <span class=\'text-success\'>ENABLE</span> this discount?</h3><p>An enabled discount can be applied to applications.</p>',
              'data-form'     => $form_id,
              'data-value'    => 'enable'
            ]);
          }

          // Delete discount button
          echo Html::link(Yii::t('app', 'Delete'), ['#'], [
            'id'            => 'delete-discount-btn',
            'class'         => 'btn btn-delete right',
            'data-confirm'  => '<h3><i class="icon wb-alert-circle text-danger"></i> Are you sure you want to <span class=\'text-danger\'>DELETE</span> this discount?</h3><p>This discount will be deleted immediately. You can\'t undo this action.</p>',
            'data-form'     => $form_id,
            'data-value'    => 'delete'
          ]);
        }
      ?>
    </div><!-- form-actions -->
  </div>

<?php
  // End model form
  $this->endWidget();
?>
