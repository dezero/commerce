<?php
/*
|--------------------------------------------------------------------------
| Header title zone for a Discount model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $discount_model: Discount model
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = $discount_model->title();

  // Controller and action names in lowercase
  $current_controller = Yii::currentController(true);
  $current_action = Yii::currentAction(true);
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
    <?php
      // Disabled discount
      if ( $discount_model->is_disabled() ) :
    ?>
      <span class="badge badge-danger mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= $discount_model->disable_date; ?>"><?= Yii::t('app', 'DISABLED'); ?></span>
    <?php
      // Enabled discount but it's inactive
      elseif ( $discount_model->is_active == 0 ) :
    ?>
      <?php if ( ! $discount_model->is_valid() && !empty($discount_model->error_message) ) : ?>
        <span class="badge badge-warning mr-5" data-toggle="tooltip" data-placement="right" data-original-title="<?= Yii::t('app', $discount_model->error_message); ?>"><?= Yii::t('app', 'INACTIVE'); ?></span>
      <?php else : ?>
        <span class="badge badge-warning mr-5"><?= Yii::t('app', 'INACTIVE'); ?></span>
      <?php endif; ?>
    <?php
      // Enabled & active discount (customers can use it)
      else :
    ?>
      <span class="badge badge-success mr-5"><?= Yii::t('app', 'ACTIVE'); ?></span>
    <?php endif; ?>
  </h1>
  <div class="page-header-actions">
    <?php
      echo Html::link('<i class="wb-chevron-left"></i> Back', ["/commerce/discount"], [
        'class' => 'btn btn-dark'
      ]);

      echo Html::link('View uses', ["/commerce/discount/uses", "discount_id" => $discount_model->discount_id], [
        'class' => 'btn btn-info'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => 'Manage Discounts',
        'url' => ['/commerce/discount/'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>