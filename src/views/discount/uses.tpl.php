<?php
/*
|--------------------------------------------------------------------------
| Admin list page for DiscountUse models
|--------------------------------------------------------------------------
|
| Available variables:
|  - $discount_model: Discount model
|  - $discount_use_model: DiscountUse model
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
  
  $this->pageTitle = $discount_model->title() .' - '. Yii::t('app', 'View uses');
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title">
      <?= $this->pageTitle; ?>
    </h1>
    <div class="page-header-actions">
      <?php
        echo Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back to discounts'), ["/commerce/discount"], [
          'class' => 'btn btn-dark'
        ]);

        echo Html::link(Yii::t('app','Update discount'), ["/commerce/discount/update", "id" => $discount_model->discount_id], [
          'class' => 'btn btn-info'
        ]);
      ?>
    </div>
    <?=
      // Breadcrumbs
      Html::breadcrumbs([
        [
          'label' => Yii::t('app', 'Manage Discounts'),
          'url' => ['/commerce/discount/'],
        ],
        [
          'label' => $discount_model->title(),
          'url' => ['/commerce/discount/update', 'id' => $discount_model->discount_id],
        ],
        Yii::t('app', 'View uses')
      ]);
    ?>
  </div>

  <div class="page-content">
    <div class="row row-lg">
      <div class="col-lg-12">
        <div class="panel panel-top-summary">
          <div class="panel-body container-fluid">
            <?php
              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                $this->widget('dz.grid.GridView', [
                  'id'                => 'discount-use-grid',
                  'dataProvider'      => $discount_use_model->search(),
                  // 'filter'            => $discount_use_model,
                  'emptyText'         => Yii::t('app', 'This discount has not yet been used'),
                  'enableHistory'     => true,
                  'loadModal'         => true,
                  'enableSorting'     => false,
                  'enableSelect2'     => true,
                  'type'              => ['striped', 'hover'],
                  'beforeAjaxUpdate'  => 'js:function(id, options){ $("html, body").animate({scrollTop: 0}, 100); }',
                  'columns'           => [
                    [
                      'name' => 'created_date',
                      'header' => Yii::t('app', 'Date'),
                    ],
                    [
                      'name' => 'customer',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/discount/_grid_column", ["column" => "customer", "model" => $data]))',
                    ],
                    [
                      'name' => 'order_id',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/discount/_grid_column", ["column" => "order", "model" => $data]))',
                    ],
                    [
                      'name' => 'final_price',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/discount/_grid_column", ["column" => "final_price", "model" => $data]))',
                    ],
                  ],
                ]);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- .page-main -->