<?php
/*
|--------------------------------------------------------------------------
| OrderJSON VIEW page
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_json_model: OrderJson model
|  - $vec_data: Array with data
|
*/
  use dz\helpers\Html;
  use dz\helpers\StringHelper;
  use dz\helpers\Url;

// Page title
  $this->pageTitle = Yii::t('app', 'Log for Order') .' '. $order_json_model->order_id;
?>
<div class="page-header">
  <h1 class="page-title">
    <?= $this->pageTitle; ?>
  </h1>
  <div class="page-header-actions">
    <?=
      // Back button
      Html::link('<i class="wb-chevron-left"></i> '. Yii::t('app', 'Back'), ['/commerce/orderJson'], [
        'class' => 'btn btn-dark'
      ]);
    ?>
  </div>
  <?=
    // Breadcrumbs
    Html::breadcrumbs([
      [
        'label' => Yii::t('app', 'Orders Log'),
        'url' => ['/commerce/orderJson'],
      ],
      $this->pageTitle
    ]);
  ?>
</div>

<div class="page-content container-fluid">

  <div class="panel">
    <header class="panel-heading">
      <h3 class="panel-title"><?= Yii::t('app', 'Order'); ?> <?= $order_json_model->order_id; ?></h3>
    </header>
    <div class="panel-body panel-view-content">
      <div class="row">
        <div class="col-xl-12">
          <p><?= Yii::t('app', 'WARNING: This data has been copied directly from database') ;?></p>
          <div class="nav-tabs-horizontal pt-10" data-plugin="tabs">
            <ul class="nav nav-tabs nav-tabs-line tabs-line-top" role="tablist">
              <li class="nav-item" role="presentation">
                <a class="nav-link active" data-toggle="tab" href="#order-order" aria-controls="order-order" role="tab" aria-expanded="false"><?= Yii::t('app', 'ORDER'); ?></a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" data-toggle="tab" href="#order-shipping" aria-controls="order-shipping" role="tab" aria-expanded="false"><?= Yii::t('app', 'SHIPPING'); ?></a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" data-toggle="tab" href="#order-billing" aria-controls="order-billing" role="tab" aria-expanded="false"><?= Yii::t('app', 'BILLING'); ?></a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" data-toggle="tab" href="#order-line-items" aria-controls="order-line-items" role="tab" aria-expanded="false"><?= Yii::t('app', 'LINE ITEMS'); ?><?php if ( isset($vec_data['line_items']) && !empty($vec_data['line_items']) ) : ?> (<?= count($vec_data['line_items']); ?>)<?php endif; ?></a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" data-toggle="tab" href="#order-customer" aria-controls="order-customer" role="tab" aria-expanded="false"><?= Yii::t('app', 'CUSTOMER'); ?></a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" data-toggle="tab" href="#order-user" aria-controls="order-user" role="tab" aria-expanded="false"><?= Yii::t('app', 'USER'); ?></a>
              </li>
            </ul>
          </div>
          <div class="tab-content pt-20">

            <?php
              /*
              |--------------------------------------------------------------------------
              | ORDER INFORMATION
              |--------------------------------------------------------------------------
              */
            ?>
            <div class="tab-pane active" id="order-order" role="tabpanel" aria-expanded="false">
              <table class="table table-bordered table-sm">
                <?php if ( isset($vec_data['order']) && !empty($vec_data['order']) ) : ?>
                  <?php foreach ( $vec_data['order'] as $attribute_name => $attribute_value ) : ?>
                    <tr>
                      <td class="column-active"><?= $attribute_name; ?></td>
                      <td><?= $attribute_value; ?></td>
                    </tr>
                  <?php endforeach; ?>
                <?php endif; ?>
              </table>
            </div>

            <?php
              /*
              |--------------------------------------------------------------------------
              | SHIPPING ADDRESS
              |--------------------------------------------------------------------------
              */
            ?>
            <div class="tab-pane" id="order-shipping" role="tabpanel" aria-expanded="false">
              <table class="table table-bordered table-sm">
                <?php if ( isset($vec_data['shipping_address']) && !empty($vec_data['shipping_address']) ) : ?>
                  <?php foreach ( $vec_data['shipping_address'] as $attribute_name => $attribute_value ) : ?>
                    <tr>
                      <td class="column-active"><?= $attribute_name; ?></td>
                      <td><?= $attribute_value; ?></td>
                    </tr>
                  <?php endforeach; ?>
                <?php endif; ?>
              </table>
            </div>

            <?php
              /*
              |--------------------------------------------------------------------------
              | BILLING ADDRESS
              |--------------------------------------------------------------------------
              */
            ?>
            <div class="tab-pane" id="order-billing" role="tabpanel" aria-expanded="false">
              <table class="table table-bordered table-sm">
                <?php if ( isset($vec_data['billing_address']) && !empty($vec_data['billing_address']) ) : ?>
                  <?php foreach ( $vec_data['billing_address'] as $attribute_name => $attribute_value ) : ?>
                    <tr>
                      <td class="column-active"><?= $attribute_name; ?></td>
                      <td><?= $attribute_value; ?></td>
                    </tr>
                  <?php endforeach; ?>
                <?php endif; ?>
              </table>
            </div>

            <?php
              /*
              |--------------------------------------------------------------------------
              | LINE ITEMS
              |--------------------------------------------------------------------------
              */
            ?>
            <div class="tab-pane" id="order-line-items" role="tabpanel" aria-expanded="false">
              <?php if ( isset($vec_data['line_items']) && !empty($vec_data['line_items']) ) : ?>
                <?php foreach ( $vec_data['line_items'] as $num_line_item => $que_line_item ) : ?>
                  <?php if ( !empty($que_line_item) ) : ?>
                    <table class="table table-bordered table-sm mb-30">
                      <tr>
                        <th colspan="2"><h3 class=mb-0><?= Yii::t('app', 'LINE ITEM'); ?> #<?= $num_line_item + 1; ?></h3></th>
                      </tr>
                      <?php foreach ( $que_line_item as $attribute_name => $attribute_value ) : ?>
                        <tr>
                          <td class="column-active"><?= $attribute_name; ?></td>
                          <td><?= $attribute_value; ?></td>
                        </tr>
                      <?php endforeach; ?>
                    </table>
                  <?php endif; ?>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>

            <?php
              /*
              |--------------------------------------------------------------------------
              | CUSTOMER
              |--------------------------------------------------------------------------
              */
            ?>
            <div class="tab-pane" id="order-customer" role="tabpanel" aria-expanded="false">
              <table class="table table-bordered table-sm">
                <?php if ( isset($vec_data['customer']) && !empty($vec_data['customer']) ) : ?>
                  <?php foreach ( $vec_data['customer'] as $attribute_name => $attribute_value ) : ?>
                    <tr>
                      <td class="column-active"><?= $attribute_name; ?></td>
                      <td><?= $attribute_value; ?></td>
                    </tr>
                  <?php endforeach; ?>
                <?php endif; ?>
              </table>
            </div>

            <?php
              /*
              |--------------------------------------------------------------------------
              | USER
              |--------------------------------------------------------------------------
              */
            ?>
            <div class="tab-pane" id="order-user" role="tabpanel" aria-expanded="false">
              <table class="table table-bordered table-sm">
                <?php if ( isset($vec_data['user']) && !empty($vec_data['user']) ) : ?>
                  <?php foreach ( $vec_data['user'] as $attribute_name => $attribute_value ) : ?>
                    <tr>
                      <td class="column-active"><?= $attribute_name; ?></td>
                      <td><?= $attribute_value; ?></td>
                    </tr>
                  <?php endforeach; ?>
                <?php endif; ?>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
