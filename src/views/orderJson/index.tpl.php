<?php
/*
|--------------------------------------------------------------------------
| Admin list page for OrderJson model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $order_json_model: OrderJson model class
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;

  // Page title
  $this->pageTitle = Yii::t('app', 'Orders Log (internal use)');
?>
<div class="page-main">
  <div class="page-header">
    <h1 class="page-title"><?= $this->pageTitle; ?></h1>
    <div class="page-header-actions">
      <a href="<?= Url::to('/commerce/order'); ?>" class="btn btn-dark"><i class="wb-order"></i> <?= Yii::t('app', 'Order Management'); ?></a>
    </div>
  </div>
  <div class="page-content">
    <div class="row row-lg">
      <div class="col-lg-12">
        <div class="panel">
          <div class="panel-body container-fluid">
            <?php
              /*
              |----------------------------------------------------------------------------------------
              | GridView widget
              |----------------------------------------------------------------------------------------
              */
                $this->widget('dz.grid.GridView', [
                  'id'                => 'order-json-grid',
                  'dataProvider'      => $order_json_model->search(),
                  'filter'            => $order_json_model,
                  'emptyText'         => Yii::t('app', 'No orders have been found'),
                  'enableHistory'     => false,
                  'loadModal'         => true,
                  'enableSorting'     => false,
                  'enableSelect2'     => false,
                  // 'selectableRows'    => 2,
                  'type'              => ['striped', 'hover'],
                  'beforeAjaxUpdate'  => 'js:function(id, options){ $.dzGridView.beforeAjaxUpdate(id, options); }',
                  // 'afterAjaxUpdate'   => 'js:function() { $.orderGridUpdate(); }',
                  'columns'           => [
                    // [
                    //   'class' => '\dz\grid\CheckBoxColumn',
                    // ],
                    [
                      'header' => Yii::t('app', 'Order'),
                      'name' => 'order_id',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/orderJson/_grid_column", ["column" => "order_id", "model" => $data]))',
                    ],
                    [
                      'header' => Yii::t('app', 'Customer'),
                      'name' => 'user_id',
                      'value' => 'trim($this->grid->getOwner()->renderPartial("//commerce/orderJson/_grid_column", ["column" => "customer", "model" => $data]))',
                    ],
                    [
                      'header' => Yii::t('app', 'Last Log Update'),
                      'name' => 'updated_date',
                      'filter' => false
                    ],
                    [
                      'class' => 'dz.grid.ButtonColumn',
                      'header' => Yii::t('app', 'ACTION'),
                      'template' => '{view}',
                      'deleteButton' => false,
                      'clearButton' => true,
                      'viewButton' => true,
                      'updateButton' => false,
                      'buttons' => [
                        'view' => [
                          'label' => Yii::t('app', 'Order'),
                          'icon' => 'eye',
                          'url' => 'Yii::app()->createAbsoluteUrl("/commerce/orderJson/view", ["id" => $data->order_id])',
                        ],
                      ]
                    ],
                  ],
                ]);
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- .page-content -->
</div><!-- .page-main -->
