<?php
/*
|--------------------------------------------------------------------------
| CGridView column partial page for OrderJson model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $model: OrderJson model
|  - $column: Column name
|
*/
  use dz\helpers\Html;
  use dz\helpers\Url;
?>
<?php switch ( $column ) :
    /*
    |--------------------------------------------------------------------------
    | COLUMN "order_id"
    |--------------------------------------------------------------------------
    */
    case 'order_id':
  ?>
    <a href="<?=  Url::to('/commerce/orderJson/view', ['id' => $model->order_id]); ?>"><?= $model->order_id; ?></a>
  <?php break; ?>
  <?php
    /*
    |--------------------------------------------------------------------------
    | COLUMN "customer"
    |--------------------------------------------------------------------------
    */
    case 'customer':
  ?>
    <?php if ( $model->user_id > 0 && $model->user ) : ?>
      <?php
        // Get customer email
        $customer_email = Yii::app()->customerManager->get_customer_email($model->user);
        if ( !empty($customer_email) ) :
      ?>
        <a href="mailto:<?= $customer_email; ?>" target="_blank"><?= $customer_email; ?></a><br>
      <?php elseif ( !empty($model->email) ) : ?>
        <a href="mailto:<?= $model->email; ?>" target="_blank"><?= $model->email; ?></a><br>
      <?php endif; ?>
      <?= $model->user->fullname(); ?>
    <?php else : ?>
      <p><span class="badge badge-default" data-toggle="tooltip" data-placement="bottom" data-original-title="Customer NOT registered">GUEST</span></p>
      <?php if ( !empty($model->email) ) : ?>
        <a href="mailto:<?= $model->email; ?>" target="_blank"><?= $model->email; ?></a>
      <?php endif; ?>
    <?php endif; ?>
  <?php break; ?>
<?php endswitch; ?>
