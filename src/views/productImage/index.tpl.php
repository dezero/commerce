  <?php
/*
|--------------------------------------------------------------------------
| Images & Videos update page for a Product model
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|  - $vec_config: Product configuration options
|
*/
  use dz\helpers\Url;

  // Header title
  $this->renderPartial($product_model->get_view_path('_header_title'), [
    'product_model' => $product_model,
    'vec_config'    => $vec_config,
  ]);
?> 
<div class="page-content container-fluid">
  <?php
    // Header menu
    $this->renderPartial($product_model->get_view_path('_header_menu'), [
      'product_model' => $product_model,
      'vec_config'    => $vec_config,
    ]);
  ?>

  <?php
    //----------------------------------------------------------------
    // IMAGE
    //----------------------------------------------------------------
    if ( isset($vec_config['is_image']) && $vec_config['is_image'] ) :
  ?>
    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('app', 'Images'); ?></h3>
        <div class="panel-actions">
          <a id="sort-product-images-btn" class="btn btn-info" data-product="<?= $product_model->product_id; ?>" href="<?= Url::to('/commerce/productImage/sort', ['product_id' => $product_model->product_id]); ?>">
            <i class="wb wb-list-numbered mr-5"></i> <?= Yii::t('app', 'Sort Images'); ?>
          </a>
        </div>
      </header>

      <div class="panel-body">
        <div id="product-images-container" class="row product-images-wrapper">
          <div class="product-image-table-wrapper col-sm-12">
            <div class="table-responsive">
              <?php
                $this->renderPartial('//commerce/productImage/_table_images', [
                  'product_model'     => $product_model,
                ]);
              ?>
            </div>
            <div class="center">
              <a id="upload-product-images-btn" class="btn btn-primary btn-add mb-20" href="javascript:void(0)"><?= Yii::t('app', 'Upload Images'); ?> <i class="wb wb-chevron-down"></i></a>
              <div id="product-image-upload-container" class="upload-product-image-form-wrapper col-sm-12" style="display: none;">
                <div class="form-group">
                  <div id='image-file-input-wrapper' class="kv-wrapper">
                    <input id="product-image-file-input" name="ImageFiles[]" type="file" class="file-loading" multiple data-product="<?= $product_model->product_id; ?>">
                  </div>
                  <div id="product-image-error-message" class="mt-20" style="display:none"></div>
                  <div id="product-image-success-message" class="alert alert-success dark in" style="margin-top:20px; display:none"></div>
                </div>
              </div>  
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php
    //----------------------------------------------------------------
    // VIDEO
    //----------------------------------------------------------------
    if ( isset($vec_config['is_video']) && $vec_config['is_video'] ) :
  ?>
    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('app', 'Videos'); ?></h3>
        <div class="panel-actions">
          <a id="product-video-create-btn" data-action="create" data-product="<?= $product_model->product_id; ?>" href="<?= Url::to('/commerce/productVideo/create', ['product_id' => $product_model->product_id]); ?>" class="btn btn-primary"><i class="icon wb-plus"></i> <?= Yii::t('app', 'Add video'); ?></a>
        </div>
      </header>

      <div class="panel-body">
        <div class="row">
          <div class="col-sm-12">
            <?php
              $this->renderPartial('//commerce/productVideo/_table_videos', [
                'product_model'     => $product_model
              ]);
            ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php
    //----------------------------------------------------------------
    // LINKS
    //----------------------------------------------------------------
    if ( isset($vec_config['is_link']) && $vec_config['is_link'] ) :
  ?>
    <div class="panel">
      <header class="panel-heading">
        <h3 class="panel-title"><?= Yii::t('app', 'Links'); ?></h3>
        <div class="panel-actions">
          <a id="product-link-create-btn" data-action="create" data-product="<?= $product_model->product_id; ?>" href="<?= Url::to('/commerce/productLink/create', ['product_id' => $product_model->product_id]); ?>" class="btn btn-primary"><i class="icon wb-plus"></i> <?= Yii::t('app', 'Add link'); ?></a>
        </div>
      </header>

      <div class="panel-body">
        <div class="row">
          <div class="col-sm-12">
            <?php
              $this->renderPartial('//commerce/productLink/_table_links', [
                'product_model'     => $product_model
              ]);
            ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>
</div><!-- .page-content -->

<?php
  // --------------------------------------------------------------------
  // Custom JAVASCRIPT libraries for this page:
  //  - Dropzone - Krajee Bootstrap File Input
  //  - Nestable - Tree widget
  // --------------------------------------------------------------------
  if ( isset($vec_config['is_image']) && $vec_config['is_image'] )
  {
    Yii::app()->clientscript
      ->registerCssTopFile(Yii::app()->theme->baseUrl .'/js/krajee-fileinput/css/fileinput.min.css')
      ->registerScriptFile(Yii::app()->theme->baseUrl ."/js/krajee-fileinput/js/fileinput.min.js", CClientScript::POS_END)

      ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/jquery-nestable/jquery.nestable.js', CClientScript::POS_END)
      ->registerScriptFile(Yii::app()->theme->baseUrl. '/js/dz.nestable.js', CClientScript::POS_END);
  }
?>
