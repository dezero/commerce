<?php
/*
|--------------------------------------------------------------------------
| Tree partial page of a ProductImage model - Slide Panel
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|
*/
?>
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-pure btn-inverse slidePanel-close actions-top icon wb-close" aria-hidden="true"></button>
  </div>
  <h1><?= Yii::t('app', 'Sort images'); ?></h1>
</header>
<div id="product-image-slidepanel-action" class="slidePanel-inner product-image-slidepanel-wrapper" data-action="sort">
  <section class="slidePanel-inner-section">
    <h4><?= Yii::t('app', 'Use drag&drop to sort images'); ?></h4>
    <?php
      // Render form
      $this->renderPartial('//commerce/productImage/_tree', [
        'product_model' => $product_model,
        'is_ajax' => false
      ]);
    ?>
    <div class="row">
      <div class="col-lg-12 form-actions center mt-20">
        <a class="btn btn-dark cancel slidePanel-close" href="#"><?= Yii::t('app', 'Close'); ?></a>
      </div>
    </div>
  </section>
</div>