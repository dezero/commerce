<?php
/*
|--------------------------------------------------------------------------
| Product images partial
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|
*/

  use dz\helpers\Url;
?>
<div id="product-images-container" class="row product-images-wrapper">
  <div class="product-image-tree-wrapper col-sm-10 offset-md-1 hide">
    <h4 class="center mt-10 mb-30"><?= Yii::t('app', 'Use drag&drop to sort images'); ?></h4>
    <?php
      $this->renderPartial('//commerce/productImage/_tree', [
        'product_model' => $product_model,
        'is_ajax' => false
      ]);
    ?>
    <div class="center">
      <a id="back-product-images-btn" class="btn btn-dark" href="javascript:void(0)"><i class="wb wb-chevron-left mr-5"></i> <?= Yii::t('app', 'Back'); ?></a>
    </div>
  </div>
  <div class="product-image-table-wrapper col-sm-10 offset-md-1">
    <div class="table-responsive">
      <?php
        $this->renderPartial('//commerce/productImage/_table_images', [
          'product_model'     => $product_model,
        ]);
      ?>
    </div>
    <div class="center">
      <a id="order-product-images-btn" class="btn btn-info mr-10" href="javascript:void(0)"><i class="wb wb-list-numbered mr-5"></i> <?= Yii::t('app', 'Sort images'); ?></a>
      <a id="upload-product-images-btn" class="btn btn-primary btn-add" href="javascript:void(0)"><?= Yii::t('app', 'Upload images'); ?> <i class="wb wb-chevron-down"></i></a>
      <div id="product-image-upload-container" class="upload-product-image-form-wrapper col-sm-12" style="display: none;">
        <div class="form-group">
          <div id='image-file-input-wrapper' class="kv-wrapper">
            <input id="product-image-file-input" name="ImageFiles[]" type="file" class="file-loading" multiple data-product="<?= $product_model->product_id; ?>">
          </div>
          <div id="product-image-error-message mt-20" style="display:none"></div>
          <div id="product-image-success-message" class="alert alert-success dark in" style="margin-top:20px; display:none"></div>
        </div>
      </div>  
    </div>
  </div>
</div>
<?php
  // --------------------------------------------------------------------
  // Custom JAVASCRIPT libraries for this page:
  //  - Dropzone - Krajee Bootstrap File Input
  //  - Nestable - Tree widget
  // --------------------------------------------------------------------
  Yii::app()->clientscript
    ->registerCssTopFile(Yii::app()->theme->baseUrl .'/js/krajee-fileinput/css/fileinput.min.css')
    ->registerScriptFile(Yii::app()->theme->baseUrl ."/js/krajee-fileinput/js/fileinput.min.js", CClientScript::POS_END)

    ->registerScriptFile(Yii::app()->theme->baseUrl. '/libraries/jquery-nestable/jquery.nestable.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->theme->baseUrl. '/js/dz.nestable.js', CClientScript::POS_END);
?>