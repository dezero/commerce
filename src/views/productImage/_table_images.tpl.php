<?php
/*
|--------------------------------------------------------------------------
| Product images table
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model
|
*/

  use dz\helpers\Url;
?>
<table id="product-image-table" class="product-images-table table table-striped table-hover head-uppercase grid-view-loading" data-url="<?= Url::to('/commerce/productImage', ['product_id' => $product_model->product_id]); ?>">
  <?php if ( $product_model->images ) : ?>
    <thead>
      <tr>
        <th class="image-column"><?= Yii::t('app', 'Image'); ?></th>
        <th class="file-column"><?= Yii::t('app', 'File'); ?></th>
        <th class="text-left"><?= Yii::t('app', 'Size'); ?></th>
        <th class="text-left"><?= Yii::t('app', 'Upload Date'); ?></th>
        <th class="actions-column text-center"><?= Yii::t('app', 'ACTIONS'); ?></th>
      </tr>
    </thead>
  <?php endif; ?>
  <tbody>
    <?php if ( $product_model->images ) : ?>
      <?php foreach ( $product_model->images as $product_image_model ) : ?>
        <?php if ( $product_image_model->image ) : ?>
          <tr id="image-file-<?= $product_image_model->image_id; ?>">
            <td class="image-column">
              <img src="<?= $product_image_model->image->image_url('small'); ?>">
            </td>
            <td class="file-column">
              <?= $product_image_model->image->title(); ?>
              <?php if ( $product_image_model->is_default_image() ) : ?>
                <p><span class="badge badge-dark badge-sm"><?= Yii::t('app', 'DEFAULT'); ?></span></p>
              <?php endif; ?>    
            </td>
            <td class="size-column"><?= $product_image_model->image->get_file_size(); ?></td>
            <td class="upload-column">
              <?= $product_image_model->image->updated_date; ?>
              <br>by <?= $product_image_model->image->updatedUser->fullname(); ?>
            </td>
            <td class="actions-column text-center">
              <a class="delete-action btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip" data-action="delete" href="<?= Url::to('/commerce/productImage/delete', ['product_id' => $product_model->product_id, 'image_id' => $product_image_model->image_id]); ?>" data-original-title="Delete" data-product="<?= $product_model->product_id; ?>" data-image="<?= $product_image_model->image_id; ?>"><i class="wb-trash"></i></a>
            </td>
          </tr>
        <?php endif; ?>
      <?php endforeach; ?>
    <?php else : ?>
      <tr>
        <td>
          <h5 class="mt-15"><?= Yii::t('app', 'No images have been uploaded'); ?></h5>
        </td>
      </tr>
    <?php endif; ?>
  </tbody>
</table>