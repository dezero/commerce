<?php
/*
|--------------------------------------------------------------------------
| Form partial page for Product tree widget
|--------------------------------------------------------------------------
|
| Available variables:
|  - $product_model: Product model class
|  - $is_ajax: Is this partial loaded via AJAX?
*/
  use dz\helpers\Url;
?>
<?php if ( !$is_ajax ) : ?>
  <div id="product-image-loading-tree" class='dz-loading center hide'>
    <img src="<?= Url::theme(); ?>/images/loading.gif" height="100">
    <h5 class="mt-30"><?= Yii::t('app', 'Saving...'); ?></h5>
  </div>
  <div class="dd dd-product-image-group" id="product-image-nestable-wrapper" data-name="product-image" data-url="<?= Url::to('/commerce/productImage/updateWeight', ['product_id' => $product_model->product_id]); ?>&id=0" data-product="<?= $product_model->product_id; ?>">
<?php endif; ?>
<?php if ( $product_model->images ) : ?>
  <ol class="dd-list">
    <?php foreach ( $product_model->images as $product_image_model ) : ?>
      <?php if ( $product_image_model->image ) : ?>
        <li class="dd-item dd3-item dd-item-group dd-level1" data-rel="level1" data-id="<?= $product_image_model->image_id; ?>" id="dd-item-<?= $product_image_model->image_id; ?>">
          <div class="dd-handle dd3-handle"></div>
          <div class="dd3-content">
            <div class="image-wrapper">
              <img src="<?= $product_image_model->image->image_url('small'); ?>">
            </div>
            <div class="image-file-name">
              <?= $product_image_model->image->title(); ?><br>
              <?= $product_image_model->image->get_file_size(); ?>
            </div>
          </div>
        </li>
      <?php endif; ?>
    <?php endforeach; ?>
  </ol>
<?php else : ?>
  <p><?= Yii::t('app', 'No images found'); ?></p>
<?php endif; ?>
<?php if ( ! $is_ajax ) : ?>
  </div>
<?php endif; ?>