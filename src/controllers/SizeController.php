<?php
/*
|--------------------------------------------------------------------------
| Controller class for ProductOption model (option_type = "size")
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dzlab\commerce\controllers\_base\BaseOptionController;
use Yii;

class SizeController extends BaseOptionController
{
    /**
     * Option type = SIZE
     */
    protected $option_type = 'size';
}