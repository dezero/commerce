<?php
/*
|--------------------------------------------------------------------------
| Controller class for Customer model
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dz\helpers\Json;
use dzlab\commerce\models\Customer;
use dz\modules\settings\models\Country;
use dz\web\Controller;
use Yii;

class CustomerController extends Controller
{
    /**
     * List action for customers
     */
    public function actionIndex()
    {
        $customer_model = Yii::createObject(Customer::class, 'search');
        $customer_model->unsetAttributes();
        // $customer_model->is_cart_completed = 1;
        
        if ( isset($_GET['Customer']) )
        {
            $customer_model->setAttributes($_GET['Customer']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Render INDEX view
        $this->render('//commerce/customer/index', [
            'customer_model'    => $customer_model,
            'vec_totals'        => Yii::app()->customerManager->total_by_status()
        ]);
    }


    /**
     * View action for Customer model
     */
    public function actionView($id)
    {
        // Customer model
        $customer_model = $this->loadModel($id, Customer::class);
        $user_model = $customer_model->user;
        if ( ! $user_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'Customer user does not exist.'));
        }

        $this->render('//commerce/customer/view', [
            'customer_model'    => $customer_model,
            'user_model'        => $user_model,
        ]);
    }


    /**
     * Update action for Customer model
     */
    public function actionUpdate($id)
    {
        // Customer model
        $customer_model = $this->loadModel($id, Customer::class);
        $user_model = $customer_model->user;
        if ( ! $user_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'Customer user does not exist.'));
        }

        // CustomerAddress models
        $billing_address_model = $customer_model->billingAddress;
        $shipping_address_model = $customer_model->shippingAddress;

        // Update model?
        if ( isset($_POST['Customer']) && isset($_POST['User']) )
        {
            $customer_model->setAttributes($_POST['Customer']);
            $is_customer_validated = $customer_model->validate();

            $user_model->setAttributes($_POST['User']);
            $is_user_validated = $user_model->validate();
            if ( $is_user_validated )
            {
                $customer_model->email = $user_model->email;
            }

            // Billing address
            $is_billing_validated = true;
            if ( $billing_address_model && isset($_POST['CustomerAddress']['billing']) )
            {
                $billing_address_model->setAttributes($_POST['CustomerAddress']['billing']);
                $is_billing_validated = $billing_address_model->validate();
            }

            // Shipping address
            $is_shipping_validated = true;
            if ( $shipping_address_model && isset($_POST['CustomerAddress']['shipping']) )
            {
                $shipping_address_model->setAttributes($_POST['CustomerAddress']['shipping']);
                $is_shipping_validated = $shipping_address_model->validate();
            }

            // Save all models. They have been validated before
            if ( $is_customer_validated && $is_user_validated && $is_billing_validated && $is_shipping_validated && $customer_model->save(false) && $user_model->save(false) )
            {
                // Save billing address. It has been validated before
                if ( $billing_address_model )
                {
                    $billing_address_model->save(false);
                }

                // Save shipping address. It has been validated before
                if ( $shipping_address_model )
                {
                    $shipping_address_model->save(false);
                }

                // Flash message
                Yii::app()->user->addFlash('success', 'Customer updated successfully');

                // Redirect to same page
                $this->redirect(['update', 'id' => $customer_model->user_id]);
            }
        }

        $this->render('//commerce/customer/update', [
            'customer_model'            => $customer_model,
            'user_model'                => $user_model,
            'billing_address_model'     => $billing_address_model,
            'shipping_address_model'    => $shipping_address_model,
            'vec_country_list'          => Country::model()->country_list(),
            'form_id'                   => 'customer-form'
        ]);
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'     => 'commerce.customer.view',
        ];
    }
}