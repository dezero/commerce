<?php
/*
|--------------------------------------------------------------------------
| Controller class used for exporting models to an Excel file
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\batch\ExportExcel;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dzlab\commerce\models\Customer;
use dzlab\commerce\models\CustomerAddress;
use dzlab\commerce\models\LineItem;
use dzlab\commerce\models\Order;
use dzlab\commerce\models\Product;
use dz\utils\excel\ExcelTrait;
use dz\web\Controller;
use user\models\User;
use Yii;

class ExportController extends Controller
{
    /**
     * Excel exclusive actions
     */
    use ExcelTrait;


    /**
     * Export action
     */
    public function actionIndex($export_id, $type = 'excel')
    {
        // Render form with Export options
        if ( ! isset($_GET['export_options']) )
        {
            // This action is only allowed by AJAX requests
            if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
            {
                throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
            }

            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();

            $vec_ajax_output = [
                'result'    => 'init',
                'title'     => Yii::t('app', 'EXPORT TO EXCEL')
            ];

            $vec_ajax_output['content'] = $this->renderPartial('//commerce/export/_form', array(
                'export_type'   => $type,
                'export_url'    => '/commerce/export',
                'export_id'     => isset($_GET['export_id']) ? $_GET['export_id'] : '',
                'grid_id'       => isset($_GET['grid_id']) ? $_GET['grid_id'] : '',
                'action_id'     => isset($_GET['action_id']) ? $_GET['action_id'] : '',
                'model_class'   => isset($_GET['model_class']) ? $_GET['model_class'] : '',
            ), true, true);

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_ajax_output));
        }

        // Export callback
        else
        {
            // Time limit to 300 (5 minutes)
            set_time_limit(300);

            $this->export($export_id, $type);
        }
    }


    /**
     * Export to Excel (internal action)
     */
    public function export($export_id, $type = 'excel')
    {
        // Export file type
        $export_type = 'Excel5';    // 'Excel2007'
        if ( $type == 'csv' )
        {
            $export_type = 'CSV';
        }

        switch ( $export_id )
        {
            // Export orders
            case 'orders':
                $this->export_orders($export_id, $export_type);
            break;

            // Export products
            case 'products':
                $this->export_products($export_id, $export_type);
            break;

            // Export customers
            case 'customers':
                $this->export_customers($export_id, $export_type);
            break;
        }
    }


    /**
     * Export orders to Excel
     */
    public function export_orders($export_id, $export_type)
    {
        // Order model
        $order_model = Yii::createObject(Order::class, 'search');
        $order_model->unsetAttributes();
        if ( isset($_GET['Order']) )
        {
            $order_model->setAttributes($_GET['Order']);              
        }

        // LineItem model
        $line_item_model = Yii::createObject(LineItem::class, 'search');
        $line_item_model->unsetAttributes();
        if ( isset($_GET['LineItem']) )
        {
            $line_item_model->setAttributes($_GET['LineItem']);              
        }

        // Get export options (export fields)
        $vec_sheets = [];

        // SHEET 1 - Order details
        $order_data_provider = $order_model->search('excel');
        $vec_sheets[] = [
            'title'     => 'Orders',
            'data'      => $order_data_provider,
            'columns'   => $this->get_order_columns($order_model)
        ];

        // SHEET 2 - Line Items Details
        $vec_order_ids = [];
        if ( $order_data_provider )
        {
            $vec_order_models = $order_data_provider->getData();
            if ( !empty($vec_order_models) )
            {
                foreach ( $vec_order_models as $que_order_model )
                {
                    $vec_order_ids[$que_order_model->order_id] = $que_order_model->order_id;
                }
            }
        }

        // Search LineItem models for selected Orders
        if ( !empty($vec_order_ids) )
        {
            $line_item_model->order_id = array_values($vec_order_ids);
            $vec_sheets[] = [
                'title'     => 'Line Items',
                'data'      => $line_item_model->search('excel'),
                'columns'   => $this->get_line_item_columns($line_item_model)
            ];
        }

        // Export to excel action
        $export_filename = 'export_orders_'. time();
        $vec_document_options = [
            'creator'   => Yii::app()->name,
            'subject'   => Yii::app()->name,
            // 'autoWidth'  =>  false,
        ];

        // ExcelBehavior::toExcel() calls to commerce widget CommerceExcelView
        $this->toExcel($vec_sheets, $export_filename, $vec_document_options, $export_type);
    }


    /**
     * Export products to Excel
     */
    public function export_products($export_id, $export_type)
    {
        // Product model
        $product_model = Yii::createObject(Product::class, 'search');
        $product_model->unsetAttributes();
        if ( isset($_GET['Product']) )
        {
            $product_model->setAttributes($_GET['Product']);              
        }

        // Get export options (export fields)
        $vec_sheets = [];

        // SHEET 1 - Product details
        $product_data_provider = $product_model->search('excel');
        $vec_sheets[] = [
            'title'     => 'Products',
            'data'      => $product_data_provider,
            'columns'   => $this->get_product_columns($product_model)
        ];

        // Export to excel action
        $export_filename = 'export_products_'. time();
        $vec_document_options = [
            'creator'   => Yii::app()->name,
            'subject'   => Yii::app()->name,
            // 'autoWidth'  =>  false,
        ];

        // ExcelBehavior::toExcel() calls to commerce widget CommerceExcelView
        $this->toExcel($vec_sheets, $export_filename, $vec_document_options, $export_type);
    }


    /**
     * Export customers to Excel
     */
    public function export_customers($export_id, $export_type)
    {
        // Customer model
        $customer_model = Yii::createObject(Customer::class, 'search');
        $customer_model->unsetAttributes();
        if ( isset($_GET['Customer']) )
        {
            $customer_model->setAttributes($_GET['Customer']);              
        }

        // Get export options (export fields)
        $vec_sheets = [];

        // SHEET 1 - Customer details
        $customer_data_provider = $customer_model->search('excel');
        $vec_sheets[] = [
            'title'     => 'Customers',
            'data'      => $customer_data_provider,
            'columns'   => $this->get_customer_columns($customer_model)
        ];

        // Export to excel action
        $export_filename = 'export_customers_'. time();
        $vec_document_options = [
            'creator'   => Yii::app()->name,
            'subject'   => Yii::app()->name,
            // 'autoWidth'  =>  false,
        ];

        // ExcelBehavior::toExcel() calls to commerce widget CommerceExcelView
        $this->toExcel($vec_sheets, $export_filename, $vec_document_options, $export_type);
    }


    /**
     * Get Order columns for Excel exported file
     */
    public function get_order_columns($order_model)
    {
        // Get Labels
        $vec_labels = $order_model->attributeLabels();

        // Excel columns
        $vec_order_columns = [
            [
                'header' => 'Order Reference',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "order_reference", "model" => $data], true))',
            ],
            [
                'header' => 'Order Number',
                'name' => 'order_id',
                'htmlOptions' => [
                    'format' => 'integer'
                ]
            ],
            [
                'header' => 'Order Date',
                'name' => 'ordered_date'
            ],
            [
                'header' => 'Customer Name',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "customer_fullname", "model" => $data], true))'
            ],
            [
                'header' => 'Customer Email',
                'name' => 'email'
            ],
            [
                'header' => 'Bank Reference',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "gateway_reference", "model" => $data], true))',
            ],
            [
                'header' => 'Total Price',
                'name' => 'total_price',
                'value' => '$data->raw_attributes["total_price"]',
                'htmlOptions' => [
                    'format' => 'currency'
                ]
            ],
            [
                'header' => 'Paid Price',
                'name' => 'paid_price',
                'value' => '$data->raw_attributes["paid_price"]',
                'htmlOptions' => [
                    'format' => 'currency'
                ]
            ],
            [
                'header' => 'Subtotal Price',
                'name' => 'items_price',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "items_price", "model" => $data], true))',
                'htmlOptions' => [
                    'format' => 'currency'
                ]
            ],
            [
                'header' => 'Shipping Price',
                'name' => 'shipping_price',
                'value' => '$data->raw_attributes["shipping_price"]',
                'htmlOptions' => [
                    'format' => 'currency'
                ]
            ],
            [
                'header' => 'Discount Price',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "discount_price", "model" => $data], true))',
                'htmlOptions' => [
                    'format' => 'currency'
                ]
            ],
            [
                'header' => 'Shipping Method',
                'name' => 'shipping_method_alias'
            ],
            [
                'header' => 'Discount Coupon',
                'name' => 'coupon_code'
            ],
        ];

        // Add shipping and billing address information
        $vec_addresses_types = ['shipping', 'billing'];
        foreach ( $vec_addresses_types as $address_type )
        {
            $address_label = ucfirst($address_type);
            $vec_addresses_attributes = ['firstname', 'lastname', 'vat_code', 'address_line', 'city', 'province', 'postal_code', 'country_code', 'phone'];

            foreach ( $vec_addresses_attributes as $address_attribute )
            {
                $vec_order_columns[] = [
                    'header' => $address_label .' '. CustomerAddress::model()->getAttributeLabel($address_attribute),
                    'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "address__'. $address_attribute .'", "model" => $data, "address_type" => "'. $address_type .'"], true))'
                ];    
            }
        }

        return $vec_order_columns;
    }


    /**
     * Get LineItem columns for Excel exported file
     */
    public function get_line_item_columns($line_item_model)
    {
        // Get Labels
        $vec_labels = $line_item_model->attributeLabels();

        // Excel columns
        return [
            [
                'header' => 'Order Reference',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "order_reference", "model" => $data], true))',
            ],
            [
                'header' => 'Order Number',
                'name' => 'order_id',
                'htmlOptions' => [
                    'format' => 'integer'
                ]
            ],
            [
                'header' => 'Order Date',
                'value' => '$data->order->ordered_date'
            ],
            [
                'header' => 'Customer Name',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "customer_fullname", "model" => $data->order], true))'
            ],
            [
                'header' => 'Customer Email',
                'value' => '$data->order->email'
            ],
            [
                'header' => 'Line Position',
                'value' => '$data->get_line_position()'
            ],
            [
                'header' => 'Product Number',
                'name' => 'product_id',
                'htmlOptions' => [
                    'format' => 'integer'
                ]
            ],
            [
                'header' => 'Product SKU',
                'name' => 'sku'
            ],
            [
                'header' => 'Product Title',
                'name' => 'title'
            ],
            [
                'header' => 'Product Price (with discount)',
                // 'value' => '$data->raw_attributes["unit_price"]',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "line_item_discount_unit_price", "model" => $data], true))',
                'htmlOptions' => [
                    'format' => 'currency'
                ]
            ],
            [
                'header' => 'Quantity',
                'name' => 'quantity',
                'htmlOptions' => [
                    'format' => 'integer'
                ]
            ],
            [
                'header' => 'Total Price (with discount)',
                // 'value' => '$data->raw_attributes["total_price"]',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "line_item_discount_total_price", "model" => $data], true))',
                'htmlOptions' => [
                    'format' => 'currency'
                ]
            ],
            [
                'header' => 'Product Price (no discount)',
                'name' => 'unit_price',
                // 'value' => '$data->raw_attributes["unit_price"]',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "line_item_unit_price", "model" => $data], true))',
                'htmlOptions' => [
                    'format' => 'currency'
                ]
            ],
            [
                'header' => 'Total Price (no discount)',
                'name' => 'total_price',
                // 'value' => '$data->raw_attributes["total_price"]',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_order_excel_column", ["column" => "line_item_total_price", "model" => $data], true))',
                'htmlOptions' => [
                    'format' => 'currency'
                ]
            ]
        ];
    }


    /**
     * Get Product columns for Excel exported file
     */
    public function get_product_columns($product_model)
    {
        // Get Labels
        $vec_labels = $product_model->attributeLabels();

        // Excel columns
        $vec_product_columns = [
            [
                'header' => 'SKU',
                'name' => 'default_sku'
            ],
            [
                'header' => $vec_labels['default_ean_code'],
                'name' => 'default_ean_code'
            ],
            [
                'header' => 'ID',
                'name' => 'product_id'
            ],
            [
                'header' => $vec_labels['name'],
                'name' => 'name'
            ],
            [
                'header' => $vec_labels['description'],
                'name' => 'description'
            ],
            [
                'header' => $vec_labels['category_filter'],
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_product_excel_column", ["column" => "categories", "model" => $data], true))',
            ],
            [
                'header' => $vec_labels['default_base_price'],
                'name' => 'default_base_price',
                'value' => '$data->get_price(true)',
                'htmlOptions' => [
                    'format' => 'currency'
                ]
            ],
            [
                'header' => 'URL',
                'name' => 'seo_url',
                'value' => '$data->url()'
            ],
        ];


        return $vec_product_columns;
    }


    /**
     * Get Customer columns for Excel exported file
     */
    public function get_customer_columns($customer_model)
    {
        // Get Labels
        $vec_labels = $customer_model->attributeLabels();

        // Excel columns
        $vec_customer_columns = [
            [
                'header' => 'ID',
                'name' => 'user_id'
            ],
            [
                'header' => 'Full name',
                'name' => 'name_filter',
                'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_customer_excel_column", ["column" => "fullname", "model" => $data], true))',
            ],
            [
                'header' => $vec_labels['email'],
                'name' => 'email'
            ],
            [
                'header' => $vec_labels['created_date'],
                'name' => 'created_date'
            ]
        ];

        // Add shipping and billing address information
        $vec_addresses_types = ['shipping', 'billing'];
        foreach ( $vec_addresses_types as $address_type )
        {
            $address_label = ucfirst($address_type);
            $vec_addresses_attributes = ['firstname', 'lastname', 'vat_code', 'address_line', 'city', 'province', 'postal_code', 'country_code', 'phone'];

            foreach ( $vec_addresses_attributes as $address_attribute )
            {
                $vec_customer_columns[] = [
                    'header' => $address_label .' '. CustomerAddress::model()->getAttributeLabel($address_attribute),
                    'value' => 'trim($this->getOwner()->renderPartial("//commerce/export/_customer_excel_column", ["column" => "address__'. $address_attribute .'", "model" => $data, "address_type" => "'. $address_type .'"], true))'
                ];    
            }
        }

        // Newsletter
        $vec_customer_columns[] = [
            'header' => $vec_labels['is_newsletter'],
            'name' => 'is_newsletter',
            'value' => '$data->is_newsletter == 1 ? "'. Yii::t('app', 'Yes') .'" : "'. Yii::t('app', 'No') .'"'
        ];

        return $vec_customer_columns;
    }


    /**
     * Behaviors for Controller
     */
    public function behaviors()
    {
        return array(
            'eexcelview' => [
                'class'     => '\dz\behaviors\ExcelBehavior',
                'excelView' => '@commerce.components.CommerceExcelView'
            ],
        );
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'             => 'commerce.order.update',
            'exportTotalItems'  => 'commerce.order.update',
            'createExcel'       => 'commerce.order.update'
        ];
    }
}
