<?php
/*
|--------------------------------------------------------------------------
| Special controller to receive data from Redsys TPV
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dzlab\commerce\models\Order;
use dz\web\Controller;
use Yii;

class RedsysController extends Controller
{   
    /**
     * Main action to receive TPV response
     */
    public function actionIndex()
    {
        // Log recevied data
        $message   = "\nNEW DATA RECEIVED FROM TPV:";
        $message  .= "\n - REMOTE IP: ". isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "-";
        $message  .= "\n - POST: ". print_r($_POST, true);
        // $message  .= " - GET: ". print_r($_GET, TRUE);
        // $message  .= " - SERVER: ". print_r($_SERVER, TRUE);

        // Check if response is correct
        if ( !empty($_POST) && isset($_POST['Ds_SignatureVersion']) && isset($_POST['Ds_MerchantParameters']) && isset($_POST['Ds_Signature']) )
        {
            // TEST
            if ( $_POST['Ds_Signature'] == 'dz_test' )
            {
                $this->_run_test();
                Yii::app()->end();
            }

            $vec_response = Yii::app()->redsys->process_response($_POST);
            if ( $vec_response && is_array($vec_response) && isset($vec_response['order_id']) )
            {
                $message .= "\n - ORDER_ID: ". $vec_response['order_id'];
                $message .= "\n - MERCHANT_DATA: ". print_r($vec_response['vec_merchant_data'], true);
            }
            else
            {
                $message .= "\n --> ERROR IN GETTING ORDER & MERCHAND DATA";
            }
        }

        // Log data received from 
        Log::redsys($message);

        // Finish Yii execution
        Yii::app()->end();
    }


    /**
     * Some testing
     */
    protected function _run_test()
    {
        // Testing input params
        $order_id = 8805;
        $transaction_num = 0;
        $payment_price = 550;

        $order_model = Order::findOne($order_id);
        if ( ! $order_model )
        {
            Log::redsys("\n#". $order_id ." --> ERROR - Order ". $order_id ." cannot be found in database.");
        }
        else
        {
            // Get Merchant Parameters for Redsys TPV
            $redsys_merchant_data = Yii::app()->redsys
                                            ->set_order($order_model, $transaction_num)
                                            ->set_amount($payment_price)
                                            ->get_merchant_parameters();

            // Simulate POST params
            $vec_merchant_data = [
                'Ds_Response'           => 0,
                'Ds_Amount'             => Yii::app()->redsys->get_amount(),
                'Ds_Order'              => Yii::app()->redsys->encode_order_id($order_model->order_id, $transaction_num),
                'Ds_MerchantParameters' => $redsys_merchant_data,
                'Ds_Signature'          => Yii::app()->redsys->get_signature(),
                'Ds_SignatureVersion'   => Yii::app()->redsys->get_signature_version()
            ];

            // Log recevied data
            $message   = "\nNEW DATA RECEIVED FROM TPV:";
            $message  .= "\n - REMOTE IP: ". isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "-";
            $message  .= "\n - POST: ". print_r($vec_merchant_data, true);

            $vec_response = Yii::app()->redsys->process_response($vec_merchant_data);
            if ( $vec_response && is_array($vec_response) && isset($vec_response['order_id']) )
            {
                $message .= "\n - ORDER_ID: ". $vec_response['order_id'];
                $message .= "\n - MERCHANT_DATA: ". print_r($vec_response['vec_merchant_data'], true);
            }
            else
            {
                $message .= "\n --> ERROR IN GETTING ORDER & MERCHANT DATA";
            }

            // Log data received from 
            Log::dev($message);

            // Finish Yii execution
            Yii::app()->end();
        }
    }
}