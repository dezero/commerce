<?php
/*
|--------------------------------------------------------------------------
| Controller class for Order model
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dz\helpers\Json;
use dzlab\commerce\models\Order;
use dz\modules\settings\models\Country;
use dz\modules\settings\models\MailTemplate;
use dz\web\Controller;
use Yii;

class OrderController extends Controller
{
    /**
     * List action for orders
     */
    public function actionIndex()
    {
        $order_model = Yii::createObject(Order::class, 'search');
        $order_model->unsetAttributes();
        // $order_model->is_cart_completed = 1;
        
        if ( isset($_GET['Order']) )
        {
            $order_model->setAttributes($_GET['Order']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Render INDEX view
        $this->render('//commerce/order/index', [
            'order_model'   => $order_model,
            'vec_totals'    => Yii::app()->orderManager->total_by_status()
        ]);
    }


    /**
     * View action for Order model
     */
    public function actionView($id)
    {
        // Order model
        $order_model = $this->loadModel($id, Order::class);

        $this->render('//commerce/order/view', [
            'order_model'   => $order_model,
        ]);
    }



    /**
     * Update action for Order model
     */
    public function actionUpdate($id)
    {
        // Order model
        $order_model = $this->loadModel($id, Order::class);

        // CustomerAddress models
        $billing_address_model = $order_model->billingAddress;
        $shipping_address_model = $order_model->shippingAddress;

        // Update model?
        if ( isset($_POST['Order']) )
        {
            $order_model->setAttributes($_POST['Order']);
            $is_order_validated = $order_model->validate();

            // Billing address
            $is_billing_validated = true;
            if ( $billing_address_model && isset($_POST['CustomerAddress']['billing']) )
            {
                $billing_address_model->setAttributes($_POST['CustomerAddress']['billing']);
                $is_billing_validated = $billing_address_model->validate();
            }

            // Shipping address
            $is_shipping_validated = true;
            if ( $shipping_address_model && isset($_POST['CustomerAddress']['shipping']) )
            {
                $shipping_address_model->setAttributes($_POST['CustomerAddress']['shipping']);
                $is_shipping_validated = $shipping_address_model->validate();
            }

            // Save all models. They have been validated before
            if ( $is_order_validated && $is_billing_validated && $is_shipping_validated && $order_model->save(false) )
            {
                // Save billing address. It has been validated before
                if ( $billing_address_model )
                {
                    $billing_address_model->save(false);
                }

                // Save shipping address. It has been validated before
                if ( $shipping_address_model )
                {
                    $shipping_address_model->save(false);
                }

                // Flash message
                Yii::app()->user->addFlash('success', 'Order updated successfully');

                // Redirect to same page
                $this->redirect(['update', 'id' => $order_model->order_id]);
            }
        }

        $this->render('//commerce/order/update', [
            'order_model'               => $order_model,
            'billing_address_model'     => $billing_address_model,
            'shipping_address_model'    => $shipping_address_model,
            'vec_country_list'          => Country::model()->country_list(),
            'form_id'                   => 'order-form'
        ]);
    }


    /**
     * Change Status action for the SlidePanel widget
     */
    public function actionStatus($id)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // First of all, check if Order model exists
        $order_model = $this->loadModel($id, Order::class);

        // Submitted form?
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_ajax_output = [
                'error_msg'     => '',
                'error_code'    => 0,
            ];

            $vec_input = $this->jsonInput();

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['order_id']) && isset($vec_input['new_status']) )
            {
                // #2 - Order exists?
                $order_model = Order::findOne($vec_input['order_id']);
                if ( ! $order_model )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - Order '. $vec_input['order_id'] .' does not exist in database';
                }
                else
                {
                    // #3 - Check "is_sending_mail" value
                    $is_sending_mail = false;
                    if ( isset($vec_input['is_sending_mail']) && ($vec_input['is_sending_mail'] == "1" || $vec_input['is_sending_mail'] == 1) )
                    {
                        $is_sending_mail = true;
                    }

                    // #4 - Save new status for each Order model
                    $comments = isset($vec_input['new_comments']) ? $vec_input['new_comments'] : null;
                    if ( ! $order_model->change_status($vec_input['new_status'], $comments, $is_sending_mail) )
                    {
                        $vec_ajax_output['error_code'] = 102;
                        $vec_ajax_output['error_msg'] = 'Error - Status change cannot be saved for order '. $order_model->title();
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_ajax_output));
        }
        else
        {
            // By default, we'll send an email to the customer
            $order_model->is_sending_mail = 1;
        }

        // If we arrive here (not POST params), render partial view
        $this->renderPartial('//commerce/order/_slidepanel_status', [
            'order_model'     => $order_model,
        ]);
    }


    /**
     * Send an email
     */
    public function actionSendEmail($id)
    {
        // First of all, check if Order model exists
        $order_model = $this->loadModel($id, Order::class);

        // Send the email
        if ( isset($_POST['MailTemplate']) && isset($_POST['MailTemplate']['alias']) )
        {
            // Check MailTemplate model exists
            $mail_template_model = MailTemplate::model()->findByAlias($_POST['MailTemplate']['alias']);
            if ( ! $mail_template_model )
            {
                throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
            }

            $result_sending = $order_model->send_email($mail_template_model->alias);
            if ( $result_sending )
            {
                Yii::app()->user->addFlash('success', Yii::t('app', "Email <em>{$mail_template_model->alias}</em> has been sent successfully ") );
            }
            else
            {
                Yii::app()->user->addFlash('error', Yii::t('app', "Email <em>{$mail_template_model->alias}</em> could not be sent") );
            }
        }

        // Redirect to view order page
        $this->redirect(['view', 'id' => $id]);
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'     => 'commerce.order.view',
            'status'    => 'commerce.order.update',
            'products'  => 'commerce.order.view',
            'sendEmail' => 'commerce.order.update',
        ];
    }
}