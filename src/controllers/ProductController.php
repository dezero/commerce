<?php
/*
|--------------------------------------------------------------------------
| Controller class for Product model (product_type = "product")
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dzlab\commerce\controllers\_base\BaseProductController;
use Yii;

class ProductController extends BaseProductController
{
    /**
     * Product type = PRODUCT
     */
    protected $product_type = 'product';
}