<?php
/*
|--------------------------------------------------------------------------
| Controller class for ProductAssociation model
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dz\helpers\Html;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\ProductAssociation;
use dz\web\Controller;
use Yii;

class ProductAssociationController extends Controller
{
    /**
     * List action for associated product
     */
    public function actionIndex($product_id, $type = 'related')
    {
        // Product model
        $product_model = $this->loadModel($product_id, Product::class);

        // Data for rendering
        $vec_data = [
            'product_model'     => $product_model,
            'vec_config'        => $product_model->get_config(),
            'association_type'  => $type
        ];

        // Reload via AJAX?
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();

            // View mode?
            $vec_data['is_view_mode'] = false;

            echo $this->renderPartial('//commerce/productAssociation/_table_products', $vec_data, true, true);
            Yii::app()->end();
        }

        // Render INDEX view
        $this->render('//commerce/productAssociation/index', $vec_data);
    }


    /**
     * Create action for the SlidePanel widget
     */
    public function actionCreate($product_id, $type)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // First of all, check if Product and ProductAssociation models exist
        $product_model = $this->loadModel($product_id, Product::class);

        // Init models
        $product_association_model = Yii::createObject(ProductAssociation::class);
        $product_association_model->product_id = $product_id;

        // Submitted form?
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_ajax_output = [
                'error_msg'     => '',
                'error_code'    => 0,
                'association_id'  => 0
            ];

            // Get input data and process $_POST into $vec_post_data
            $vec_input = $this->jsonInput();
            $vec_post_data = StringHelper::parse_str($vec_input['form_data']);

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['product_id']) )
            {
                // #2 - Product matches?
                if ( $vec_input['product_id'] != $product_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - Product '. $vec_input['product_id'] .' is invalid';
                }
                else if ( !isset($vec_post_data['ProductAssociation']) || !isset($vec_post_data['ProductAssociation']['product_associated_id']) )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - Product is empty';
                }
                else
                {
                    $product_association_model = $product_model->add_product_association($vec_post_data['ProductAssociation']['product_associated_id'], $vec_input['type']);
                    if ( ! $product_association_model )
                    {
                        $vec_ajax_output['error_code'] = 104;
                        $vec_ajax_output['error_msg'] = 'Error - Product could not be added.';
                    }
                    else
                    {
                        $vec_ajax_output['association_id'] = $product_association_model->association_id;
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_ajax_output));
        }

        // If we arrive here (not POST params), render partial view
        $this->renderPartial('//commerce/productAssociation/_slidepanel_create', [
            'product_model'                 => $product_model,
            'product_association_model'     => $product_association_model
        ]);
    }


    /**
     * Delete action for ProductAssociation model
     */
    public function actionDelete($product_id, $association_id)
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // First of all, check if Product and ProductAssociation models exist
        $product_model = $this->loadModel($product_id, Product::class);
        $product_association_model = $this->loadModel($association_id, ProductAssociation::class);
        
        // Second, association belongs to this product
        if ( $product_association_model->product_id !== $product_model->product_id )
        {
            throw new \CHttpException(400, Yii::t('app', 'This association does not belong to this product.'));
        }

        $vec_ajax_output = [
            'error_msg'     => '',
            'error_code'    => 0,
        ];

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_input = $this->jsonInput();

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['product_id']) && isset($vec_input['association_id']) )
            {
                // #2 - Product matches?
                if ( $vec_input['product_id'] != $product_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - Product #'. $vec_input['product_id'] .' is invalid';
                }

                // #3 - ProductAssociation matches?
                else if ( $vec_input['association_id'] != $association_id )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - Product association #'. $vec_input['association_id'] .' is invalid';
                }

                // #4 - Delete the model
                else
                {   
                    if ( ! $product_model->remove_product_association($association_id) )
                    {
                        $vec_ajax_output['error_code'] = 103;
                        $vec_ajax_output['error_msg'] = 'Error - Product association #'. $vec_input['association_id'] .' could not be deleted';
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }
        }
        else
        {
            $vec_ajax_output['error_code'] = 202;
            $vec_ajax_output['error_msg'] = 'Access denied - Your request is invalid. Request is not POST type';
        }

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_ajax_output));
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'     => 'commerce.product.view',
            'create'    => 'commerce.product.update',
            'delete'    => 'commerce.product.delete'
        ];
    }
}