<?php
/*
|--------------------------------------------------------------------------
| Controller class for ProductImage model
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dz\helpers\Html;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dz\modules\asset\models\AssetImage;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\ProductImage;
use dz\web\Controller;
use Yii;

class ProductImageController extends Controller
{
    /**
     * List action for ProductImage product
     */
    public function actionIndex($product_id)
    {
        // Product model
        $product_model = $this->loadModel($product_id, Product::class);

        // Data for rendering
        $vec_data = [
            'product_model' => $product_model,
            'vec_config'    => $product_model->get_config()
        ];

        // Reload via AJAX?
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();

            echo $this->renderPartial('//commerce/productImage/_table_images', $vec_data, true, true);
            Yii::app()->end();
        }

        // Render INDEX view
        $this->render('//commerce/productImage/index', $vec_data);
    }


    /**
     * Sort action for the SlidePanel widget
     */
    public function actionSort($product_id)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // First of all, check if Product and ProductLink models exist
        $product_model = $this->loadModel($product_id, Product::class);

        // If we arrive here (not POST params), render partial view
        $this->renderPartial('//commerce/productImage/_slidepanel_tree', [
            'product_model'         => $product_model
        ]);
    }


    /**
     * Get product image TREE structure for AJAX
     */
    public function actionTree($product_id)
    {
        // Create action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        $vec_ajax_output = [
            'result' => 'init'
        ];

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // Product model
        $product_model = $this->loadModel($product_id, Product::class);
        $vec_ajax_output = [
            'result' => 'success',
            'tree' => $this->renderPartial('//commerce/productImage/_tree', [
                'product_model' => $product_model,
                'is_ajax'       => true
            ], true, true)
        ];

        // Output in JSON format
        echo Json::encode($vec_ajax_output);
        Yii::app()->end();
    }


    /**
     * Update product image weights
     */
    public function actionUpdateWeight($product_id)
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        $product_model = $this->loadModel($product_id, Product::class);
        if ( isset($_POST['nestable']) AND !empty($_POST['nestable']) AND is_array($_POST['nestable']) )
        {
            $vec_nestable = $_POST['nestable'];
            if ( !empty($vec_nestable) )
            {
                foreach ( $vec_nestable as $que_weight => $que_product_image )
                {
                    $new_weight = $que_weight + 1;
                    if ( isset($que_product_image['id']) )
                    {
                        // $image_id = substr($que_product_image['id'], 2);
                        $image_id = $que_product_image['id'];

                        // UPDATE slideshow weight
                        $product_image_table = ProductImage::model()->tableName();
                        Yii::app()->db->createCommand()->update($product_image_table,
                            ['weight' => $new_weight],
                            'product_id = :product_id AND image_id = :image_id',
                            [
                                ':product_id' => $product_model->product_id,
                                ':image_id' => $image_id,
                            ]
                        );

                        // Default image?
                        if ( $new_weight == 1 )
                        {
                            $product_model->save_default_image($image_id);
                        }
                    }
                }
            }

            echo Json::encode(['result' => 1]);
        }
        else
        {
            echo Json::encode(['result' => -1]);
        }

        // End application
        Yii::app()->end();
    }


    /**
     * Upload action - Multiple image files
     */
    public function actionUpload($product_id)
    {
        // Update action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        $vec_ajax_output = [
            'error_msg'     => '',
            'error_code'    => 0,
        ];

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // #1 - Product exists?
        $product_model = Product::findOne($product_id);
        if ( ! $product_model )
        {
            $vec_ajax_output['error_code'] = 101;
            $vec_ajax_output['error_msg'] = 'Access denied - Product '. $product_id .' does not exist in database';
        }

        // New uploaded files
        if ( $vec_ajax_output['error_code'] === 0 )
        {
            if ( isset($_FILES['ImageFiles']['name'][0]) )
            {
                $vec_ajax_output['files'] = $_FILES['ImageFiles'];
                foreach ( $_FILES['ImageFiles']['name'] as $num_file => $file_name )
                {
                    $que_image = Yii::app()->file->set('ImageFiles['. $num_file .']');
                    if ( $que_image->isFile )
                    {
                        if ( ! $product_model->upload_image_file($que_image) )
                        {
                            // \DzLog::warning('File cannot be loaded');
                        }
                    }
                    else
                    {
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 103;
                $vec_ajax_output['error_msg'] = 'Image files are not found';
            }
        }

        // \DzLog::dev("Upload files via AJAX with params ". print_r(['$_FILES'=> $_FILES, '$_POST' => $_POST], true));

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_ajax_output));
    }


    /**
     * Delete action for ProductImage model
     */
    public function actionDelete($product_id, $image_id)
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // First of all, check if Product and ProductImage models exist
        $product_model = $this->loadModel($product_id, Product::class);
        $product_image_model = ProductImage::get()->where([
            'product_id'    => $product_id,
            'image_id'      => $image_id
        ])->one();
        if ( ! $product_image_model )
        {
            throw new \CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }
        
        $vec_ajax_output = [
            'error_msg'     => '',
            'error_code'    => 0,
        ];

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_input = $this->jsonInput();

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['product_id']) && isset($vec_input['image_id']) )
            {
                // #2 - Product matches?
                if ( $vec_input['product_id'] != $product_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - Product #'. $vec_input['product_id'] .' is invalid';
                }

                // #3 - ProductAssociation matches?
                else if ( $vec_input['image_id'] != $image_id )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - Image association #'. $vec_input['image_id'] .' is invalid';
                }

                // #4 - Delete the model
                else
                {   
                    if ( ! $product_model->remove_product_image($image_id) )
                    {
                        $vec_ajax_output['error_code'] = 103;
                        $vec_ajax_output['error_msg'] = 'Error - Product image #'. $vec_input['image_id'] .' could not be deleted';
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }
        }
        else
        {
            $vec_ajax_output['error_code'] = 202;
            $vec_ajax_output['error_msg'] = 'Access denied - Your request is invalid. Request is not POST type';
        }

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_ajax_output));
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'         => 'commerce.product.view',
            'sort'          => 'commerce.product.update',
            'tree'          => 'commerce.product.update',
            'updateWeight'  => 'commerce.product.update',
            'upload'        => 'commerce.product.update',
            'delete'        => 'commerce.product.update'
        ];
    }
}