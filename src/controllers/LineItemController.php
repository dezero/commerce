<?php
/*
|--------------------------------------------------------------------------
| Controller class for LineItem model
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dz\helpers\Html;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dzlab\commerce\models\LineItem;
use dzlab\commerce\models\Order;
use dzlab\commerce\models\Product;
use dz\modules\settings\models\Country;
use dz\web\Controller;
use Yii;

class LineItemController extends Controller
{
    /**
     * List action for line items
     */
    public function actionIndex($id)
    {
        // Order model
        $order_model = $this->loadModel($id, Order::class);

        // Data for rendering
        $vec_data = [
            'order_model' => $order_model
        ];

        // Reload via AJAX?
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();

            // View mode?
            $vec_data['is_view_mode'] = false;

            echo $this->renderPartial('//commerce/lineItem/_table_products', $vec_data, true, true);
            Yii::app()->end();
        }

        // Render INDEX view
        $this->render('//commerce/lineItem/index', $vec_data);
    }


    /**
     * Create action for the SlidePanel widget
     */
    public function actionCreate($order_id)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // First of all, check if Order and LineItem models exist
        $order_model = $this->loadModel($order_id, Order::class);

        // Init models
        $product_model = Yii::createObject(Product::class);
        $line_item_model = Yii::createObject(LineItem::class);
        $line_item_model->order_id = $order_id;

        // Submitted form?
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_ajax_output = [
                'error_msg'     => '',
                'error_code'    => 0,
                'line_item_id'  => 0
            ];

            // Get input data and process $_POST into $vec_post_data
            $vec_input = $this->jsonInput();
            $vec_post_data = StringHelper::parse_str($vec_input['form_data']);

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['order_id']) )
            {
                // #2 - Order matches?
                if ( $vec_input['order_id'] != $order_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - Order '. $vec_input['order_id'] .' is invalid';
                }
                else if ( !isset($vec_post_data['LineItem']) || !isset($vec_post_data['LineItem']['product_id']) )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - Product is empty';
                }
                else
                {
                    $product_model = Product::findOne($vec_post_data['LineItem']['product_id']);
                    if ( ! $product_model || !$product_model->defaultVariant )
                    {
                        $vec_ajax_output['error_code'] = 103;
                        $vec_ajax_output['error_msg'] = 'Access denied - Product does not exist';
                    }
                    else
                    {
                        $line_item_model = $order_model->add_line_item($product_model->default_variant_id, 1);
                        if ( ! $line_item_model )
                        {
                            $vec_ajax_output['error_code'] = 104;
                            $vec_ajax_output['error_msg'] = 'Error - Product could not be added.';
                        }
                        else
                        {
                            $vec_ajax_output['line_item_id'] = $line_item_model->line_item_id;
                        }
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_ajax_output));
        }

        // If we arrive here (not POST params), render partial view
        $this->renderPartial('//commerce/lineItem/_slidepanel_create', [
            'order_model'       => $order_model,
            'line_item_model'   => $line_item_model,
            'product_model'     => $line_item_model->product,
        ]);
    }


    /**
     * Update action for the SlidePanel widget
     */
    public function actionUpdate($order_id, $line_item_id)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // First of all, check if Order and LineItem models exist
        $order_model = $this->loadModel($order_id, Order::class);
        $line_item_model = $this->loadModel($line_item_id, LineItem::class);
        
        // Second, line item belongs to this order
        if ( $line_item_model->order_id !== $order_model->order_id )
        {
            throw new \CHttpException(400, Yii::t('app', 'Line Item does not belong to this order.'));
        }

        // Get SELECTED ProductOption
        $vec_selected_extras = [];
        if ( $line_item_model->options )
        {
            foreach ( $line_item_model->options as $line_item_option_model )
            {
                if ( $line_item_option_model->option )
                {
                    $vec_selected_extras[$line_item_option_model->option->category_id] = $line_item_option_model->option_id;
                }
            }
        }

        // Submitted form?
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_ajax_output = [
                'error_msg'     => '',
                'error_code'    => 0,
            ];

            // Get input data and process $_POST into $vec_post_data
            $vec_input = $this->jsonInput();
            $vec_post_data = StringHelper::parse_str($vec_input['form_data']);

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['order_id']) && isset($vec_input['line_item_id']) )
            {
                // #2 - Order matches?
                if ( $vec_input['order_id'] != $order_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - Order '. $vec_input['order_id'] .' is invalid';
                }

                // #3 - LineItem matches?
                else if ( $vec_input['line_item_id'] != $line_item_id )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - LineItem '. $vec_input['line_item_id'] .' is invalid';
                }
                else
                {
                    if ( isset($vec_post_data['LineItem']) )
                    {
                        $line_item_model->setAttributes($vec_post_data['LineItem']);
                    }

                    // Save line item
                    if ( ! $line_item_model->save() )
                    {
                        $vec_ajax_output['error_code'] = 103;
                        // $vec_ajax_output['error_msg'] = 'Error - Order line could not be saved';
                        $vec_ajax_output['error_msg'] = Html::ul($this->showErrors($line_item_model->getErrors(), true));
                    }
                    else
                    {
                        // ProductOptions
                        $vec_product_options = isset($vec_post_data['ProductOption']) ? $vec_post_data['ProductOption'] : [];
                        if ( ! $order_model->update_line_item($line_item_model->line_item_id, $line_item_model->quantity, $line_item_model->price_alias, $vec_product_options) )
                        {
                            $vec_ajax_output['error_code'] = 104;
                            $vec_ajax_output['error_msg'] = 'Error - Order extra prices could not be saved';
                        }
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_ajax_output));
        }

        // If we arrive here (not POST params), render partial view
        $this->renderPartial('//commerce/lineItem/_slidepanel_update', [
            'order_model'               => $order_model,
            'line_item_model'           => $line_item_model,
            'product_model'             => $line_item_model->product,

            // Extras (options)
            'vec_extra_category_models' => Yii::app()->productOptionManager->extras_list(['is_return_model' => true]),
            'vec_selected_extras'       => $vec_selected_extras,
        ]);
    }


    /**
     * Delete action for LineItem model
     */
    public function actionDelete($order_id, $line_item_id)
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // First of all, check if Order and LineItem models exist
        $order_model = $this->loadModel($order_id, Order::class);
        $line_item_model = $this->loadModel($line_item_id, LineItem::class);
        
        // Second, line item belongs to this order
        if ( $line_item_model->order_id !== $order_model->order_id )
        {
            throw new \CHttpException(400, Yii::t('app', 'Line Item does not belong to this order.'));
        }

        $vec_ajax_output = [
            'error_msg'     => '',
            'error_code'    => 0,
        ];

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_input = $this->jsonInput();

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['order_id']) && isset($vec_input['line_item_id']) )
            {
                // #2 - Order matches?
                if ( $vec_input['order_id'] != $order_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - Order #'. $vec_input['order_id'] .' is invalid';
                }

                // #3 - LineItem matches?
                else if ( $vec_input['line_item_id'] != $line_item_id )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - LineItem #'. $vec_input['line_item_id'] .' is invalid';
                }

                // #4 - Delete the model
                else
                {   
                    if ( ! $order_model->remove_line_item($line_item_id) )
                    {
                        $vec_ajax_output['error_code'] = 103;
                        $vec_ajax_output['error_msg'] = 'Error - Line item #'. $vec_input['line_item_id'] .' could not be deleted';
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }
        }
        else
        {
            $vec_ajax_output['error_code'] = 202;
            $vec_ajax_output['error_msg'] = 'Access denied - Your request is invalid. Request is not POST type';
        }

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_ajax_output));
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'     => 'commerce.order.view',
            'create'    => 'commerce.order.update',
            'update'    => 'commerce.order.update',
            'delete'    => 'commerce.order.delete'
        ];
    }
}