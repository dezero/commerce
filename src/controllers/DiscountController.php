<?php
/*
|--------------------------------------------------------------------------
| Controller class for Discount model
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dz\helpers\Json;
use dzlab\commerce\models\Discount;
use dzlab\commerce\models\DiscountUse;
use dzlab\commerce\models\Order;
use dz\web\Controller;
use Yii;

class DiscountController extends Controller
{
    /**
     * List action for discounts
     */
    public function actionIndex()
    {
        // Check all ENABLED discounts. If there's some change. Reload this page
        if ( ! Yii::app()->discountManager->check_all_enabled_discounts() )
        {
            $this->refresh();
        }

        $discount_model = Yii::createObject(Discount::class, 'search');
        $discount_model->unsetAttributes();
        
        if ( isset($_GET['Discount']) )
        {
            $discount_model->setAttributes($_GET['Discount']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Render INDEX view
        $this->render('//commerce/discount/index', [
            'discount_model'    => $discount_model,
            'vec_totals'        => Yii::app()->discountManager->total_by_status()
        ]);
    }


    /**
     * Create action for Discount model
     */
    public function actionCreate()
    {
        // Discount model
        $discount_model = Yii::createObject(Discount::class);

        // Insert new model?
        if ( isset($_POST['Discount']) )
        {
            $discount_model->setAttributes($_POST['Discount']);
            $discount_model->is_active = 1;
            if ( $discount_model->save() )
            {
                Yii::app()->discountManager->after_save_discount($discount_model, 'create', $_POST);
            }

            // Some error(s) detected
            else
            {
                $discount_model->afterFind(new \CEvent($discount_model));
            }
        }

        $this->render('create', [
            'discount_model'    => $discount_model,
            'form_id'           => 'discount-form'
        ]);
    }


    /**
     * Update action for Discount model
     */
    public function actionUpdate($id)
    {
        // Discount model
        $discount_model = $this->loadModel($id, Discount::class);
        $discount_model->perc_amount = $discount_model->amount;

        // Update model?
        if ( isset($_POST['Discount']) )
        {
            $discount_model->setAttributes($_POST['Discount']);
            if ( $discount_model->save() )
            {
                Yii::app()->discountManager->after_save_discount($discount_model, 'update', $_POST);
            }
        }

        $this->render('//commerce/discount/update', [
            'discount_model'    => $discount_model,
            'form_id'           => 'discount-form'
        ]);
    }


    /**
     * Disable a Discount
     */
    public function actionDisable($id)
    {
        // Baja action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $discount_model = $this->loadModel($id, Discount::class);
            if ( $discount_model->disable() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', 'Discount DISABLED successfully') );
                }
                else
                {
                    echo Yii::t('app', 'Discount DISABLED successfully');
                }
            }
            else
            {
                $this->showErrors($discount_model->getErrors());
            }           
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }
    
    
    
    /**
     * Enable a Discount
     */
    public function actionEnable($id)
    {
        // Alta action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $discount_model = $this->loadModel($id, Discount::class);
            if ( $discount_model->enable() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', 'Discount ENABLED successfully') );
                }
                else
                {
                    echo Yii::t('app', 'Discount ENABLED successfully');
                }
            }
            else
            {
                $this->showErrors($discount_model->getErrors());
            }           
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }



    /**
     * Delete action for Discount model
     */
    public function actionDelete($id)
    {
        // Delete action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $discount_model = $this->loadModel($id, Discount::class);
            if ( $discount_model->delete() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app', 'Discount DELETED successfully') );
                }
                else
                {
                    echo Yii::t('app', 'Discount DELETED successfully');
                }
            }
            else
            {
                $this->showErrors($discount_model->getErrors());
            }
            
            if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
            {
                $this->redirect(['index']);
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }


    /**
     * Returns a new discount code generated automatically
     */
    public function actionGenerateCode()
    {
        // Create action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        $vec_ajax_output = [
            'code' => Yii::app()->discountManager->generate_coupon_code()
        ];

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_ajax_output));
    }


    /**
     * List action for DiscountUse models
     */
    public function actionUses($discount_id)
    {
        // Check if Discount model exists
        $discount_model = $this->loadModel($discount_id, Discount::class);

        // DiscountUse model
        $discount_use_model = Yii::createObject(DiscountUse::class, 'search');
        $discount_use_model->unsetAttributes();
        $discount_use_model->discount_id = $discount_id;

        if ( isset($_GET['DiscountUse']) )
        {
            $discount_model->setAttributes($_GET['DiscountUse']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }
        
        $this->render('//commerce/discount/uses', [
            'discount_model'        => $discount_model,
            'discount_use_model'    => $discount_use_model
        ]);
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'         => 'commerce.discount.view',
            'enable'        => 'commerce.discount.update',
            'disable'       => 'commerce.discount.update',
            'generateCode'  => 'commerce.discount.update',
            'uses'          => 'commerce.discount.view',
        ];
    }
}