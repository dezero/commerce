<?php
/*
|--------------------------------------------------------------------------
| Controller class for DiscountProduct model
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dz\helpers\Html;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dzlab\commerce\models\Discount;
use dzlab\commerce\models\DiscountProduct;
use dz\web\Controller;
use Yii;

class DiscountProductController extends Controller
{
    /**
     * List action for associated product
     */
    public function actionIndex($discount_id)
    {
        // Discount model
        $discount_model = $this->loadModel($discount_id, Discount::class);

        // Data for rendering
        $vec_data = [
            'discount_model'    => $discount_model,
        ];

        // Reload via AJAX?
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();

            // View mode?
            $vec_data['is_view_mode'] = false;

            echo $this->renderPartial('//commerce/discountProduct/_table_products', $vec_data, true, true);
            Yii::app()->end();
        }

        // Render INDEX view
        $this->render('//commerce/discountProduct/index', $vec_data);
    }


    /**
     * Create action for the SlidePanel widget
     */
    public function actionCreate($discount_id)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // First of all, check if Discount and DiscountProduct models exist
        $discount_model = $this->loadModel($discount_id, Discount::class);

        // Init models
        $discount_product_model = Yii::createObject(DiscountProduct::class);
        $discount_product_model->discount_id = $discount_id;

        // Submitted form?
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_ajax_output = [
                'error_msg'     => '',
                'error_code'    => 0
            ];

            // Get input data and process $_POST into $vec_post_data
            $vec_input = $this->jsonInput();
            $vec_post_data = StringHelper::parse_str($vec_input['form_data']);

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['discount_id']) )
            {
                // #2 - Discount matches?
                if ( $vec_input['discount_id'] != $discount_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - Discount '. $vec_input['discount_id'] .' is invalid';
                }
                else if ( !isset($vec_post_data['DiscountProduct']) || !isset($vec_post_data['DiscountProduct']['product_id']) )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - Product is empty';
                }
                else
                {
                    $discount_product_model = $discount_model->add_product($vec_post_data['DiscountProduct']['product_id']);
                    if ( ! $discount_product_model )
                    {
                        $vec_ajax_output['error_code'] = 104;
                        $vec_ajax_output['error_msg'] = 'Error - Product could not be added.';
                    }
                    else
                    {
                        $vec_ajax_output['product_id'] = $discount_product_model->product_id;
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_ajax_output));
        }

        // If we arrive here (not POST params), render partial view
        $this->renderPartial('//commerce/discountProduct/_slidepanel_create', [
            'discount_model'            => $discount_model,
            'discount_product_model'    => $discount_product_model
        ]);
    }


    /**
     * Delete action for DiscountProduct model
     */
    public function actionDelete($discount_id, $product_id)
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // First of all, check if Discount and DiscountProduct models exist
        $discount_model = $this->loadModel($discount_id, Discount::class);

        $discount_product_model = DiscountProduct::get()
            ->where([
                'discount_id'   => $discount_id,
                'product_id'    => $product_id
            ])
            ->one();
        if ( ! $discount_product_model )
        {
            throw new CHttpException(404, Yii::t('giix', 'The requested page does not exist.'));
        }
        
        // Second, association belongs to this product
        if ( $discount_product_model->discount_id !== $discount_model->discount_id )
        {
            throw new \CHttpException(400, Yii::t('app', 'This product does not belong to this discount.'));
        }

        $vec_ajax_output = [
            'error_msg'     => '',
            'error_code'    => 0,
        ];

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_input = $this->jsonInput();

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['discount_id']) && isset($vec_input['product_id']) )
            {
                // #2 - Discount matches?
                if ( $vec_input['discount_id'] != $discount_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - Discount #'. $vec_input['discount_id'] .' is invalid';
                }

                // #3 - Product matches?
                else if ( $vec_input['product_id'] != $product_id )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - Product #'. $vec_input['product_id'] .' is invalid';
                }

                // #4 - Delete the model
                else
                {   
                    if ( ! $discount_model->remove_product($product_id) )
                    {
                        $vec_ajax_output['error_code'] = 103;
                        $vec_ajax_output['error_msg'] = 'Error - Product #'. $vec_input['product_id'] .' could not be deleted';
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }
        }
        else
        {
            $vec_ajax_output['error_code'] = 202;
            $vec_ajax_output['error_msg'] = 'Access denied - Your request is invalid. Request is not POST type';
        }

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_ajax_output));
    }


    /**
     * Delete all the products
     *
     * @since 20/04/2023
     */
    public function actionDeleteAll($discount_id)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // First of all, check if Discount and DiscountProduct models exist
        $discount_model = $this->loadModel($discount_id, Discount::class);

        // Submitted form?
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        $vec_ajax_output = [
            'error_msg'     => '',
            'error_code'    => 0,
        ];

        $vec_input = $this->jsonInput();

        // #1 - JSON input params are correct?
        if ( !empty($vec_input) && isset($vec_input['discount_id']) )
        {
            // #2 - Discount matches?
            if ( $vec_input['discount_id'] != $discount_id )
            {
                $vec_ajax_output['error_code'] = 101;
                $vec_ajax_output['error_msg'] = 'Access denied - Discount '. $vec_input['discount_id'] .' is invalid';
            }
            else
            {
                $discount_model->remove_all_products();
            }
        }
        else
        {
            $vec_ajax_output['error_code'] = 201;
            $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
        }

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_ajax_output));
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'     => 'commerce.discount.view',
            'create'    => 'commerce.discount.update',
            'delete'    => 'commerce.discount.delete',
            'deleteAll' => 'commerce.discount.delete'
        ];
    }
}
