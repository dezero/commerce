<?php
/*
|--------------------------------------------------------------------------
| Controller class for ProductOption model (option_type = "extra")
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dz\modules\category\models\Category;
use dzlab\commerce\controllers\_base\BaseOptionController;
use dzlab\commerce\models\ProductOption;
use Yii;

class ExtraController extends BaseOptionController
{
    /**
     * Option type = EXTRA
     */
    protected $option_type = 'extra';


    /**
     * List action for extras
     */
    public function actionIndex()
    {
        $category_model = Yii::createObject(Category::class, 'search');
        $category_model->unsetAttributes();
        $category_model->category_type = 'product_option_extra';
        
        if ( isset($_GET['Category']) )
        {
            $category_model->setAttributes($_GET['Category']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Option type configuration
        $product_option_model = Yii::createObject(ProductOption::class, 'search');
        $product_option_model->option_type = $this->option_type;
        $vec_config = $product_option_model->get_config();

        // Render INDEX view
        $this->render($product_option_model->get_view_path('index'), [
            'category_model'        => $category_model,
            'product_option_model'  => $product_option_model,
            'vec_config'            => $vec_config
        ]);
    }


    /**
     * Create action for extra
     */
    public function actionCreate($id = '')
    {
        // Create Category model
        if ( empty($id) )
        {
            $product_option_model = Yii::createObject(ProductOption::class);
            $product_option_model->option_type = $this->option_type;

            $category_model = Yii::createObject(Category::class);
            $category_model->category_type = 'product_option_extra';
        }

        // Create ProductOption model
        else
        {
            $category_model = $this->loadModel($id, Category::class);

            $product_option_model = Yii::createObject(ProductOption::class);
            $product_option_model->option_type = $this->option_type;
            $product_option_model->category_id = $category_model->category_id;
        }

        // Create action
        $vec_data = Yii::app()->productOptionManager->create_extra($product_option_model, $category_model, $_POST);

        // Render create template
        $this->render($product_option_model->get_view_path('create'), $vec_data);
    }


    /**
     * Update action for extra
     */
    public function actionUpdate($id, $option_id = '')
    {
        // ProductOption model
        $category_model = $this->loadModel($id, Category::class);

        // Update Category model
        if ( empty($option_id) )
        {
            // Option type configuration
            $product_option_model = Yii::createObject(ProductOption::class);
            $product_option_model->option_type = $this->option_type;
            $product_option_model->category_id = $id;
        }

        // Update ProductOption model
        else
        {
            $product_option_model = $this->loadModel($option_id, ProductOption::class);
        }
        
        // Update action
        $vec_data = Yii::app()->productOptionManager->update_extra($product_option_model, $category_model, $_POST);

        // Render update template with params
        $this->render($product_option_model->get_view_path('update'), $vec_data);
    }
}