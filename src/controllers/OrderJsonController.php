<?php
/*
|--------------------------------------------------------------------------
| Controller class for OrderJson model
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dz\helpers\Json;
use dzlab\commerce\models\OrderJson;
use dz\web\Controller;
use Yii;

class OrderJsonController extends Controller
{
    /**
     * List action for orders
     */
    public function actionIndex()
    {
        $order_json_model = Yii::createObject(OrderJson::class, 'search');
        $order_json_model->unsetAttributes();

        if ( isset($_GET['OrderJson']) )
        {
            $order_json_model->setAttributes($_GET['OrderJson']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Render INDEX view
        $this->render('//commerce/orderJson/index', [
            'order_json_model'   => $order_json_model
        ]);
    }



    /**
     * View action for OrderJson model
     */
    public function actionView($id)
    {
        // OrderJson model
        $order_json_model = $this->loadModel($id, OrderJson::class);

        $this->render('//commerce/orderJson/view', [
            'order_json_model'  => $order_json_model,
            'vec_data'          => Json::decode($order_json_model->json_data)
        ]);
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index' => 'commerce.order.update',
            'view'  => 'commerce.order.update',
        ];
    }
}
