<?php
/*
|--------------------------------------------------------------------------
| Controller class for ProductLink model (is_video = 1)
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers;

use dz\helpers\Log;
use dz\helpers\Html;
use dz\helpers\Json;
use dz\helpers\StringHelper;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\ProductLink;
use dz\web\Controller;
use Yii;

class ProductVideoController extends Controller
{
    /**
     * List action for product videos
     */
    public function actionIndex($product_id)
    {
        // Product model
        $product_model = $this->loadModel($product_id, Product::class);

        // Data for rendering
        $vec_data = [
            'product_model' => $product_model
        ];

        // Reload via AJAX?
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();

            // View mode?
            $vec_data['is_view_mode'] = false;

            echo $this->renderPartial('//commerce/productVideo/_table_videos', $vec_data, true, true);
            Yii::app()->end();
        }

        // Render INDEX view
        $this->render('//commerce/productVideo/index', $vec_data);
    }


    /**
     * Create action for the SlidePanel widget
     */
    public function actionCreate($product_id)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // First of all, check if Product and ProductLink models exist
        $product_model = $this->loadModel($product_id, Product::class);

        // Init models
        $product_link_model = Yii::createObject(ProductLink::class);
        $product_link_model->product_id = $product_id;
        $product_link_model->is_video = 1;

        // Submitted form?
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_ajax_output = [
                'error_msg'         => '',
                'error_code'        => 0,
                'link_id'   => 0
            ];

            // Get input data and process $_POST into $vec_post_data
            $vec_input = $this->jsonInput();
            $vec_post_data = StringHelper::parse_str($vec_input['form_data']);

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['product_id']) )
            {
                // #2 - Product matches?
                if ( $vec_input['product_id'] != $product_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - Product '. $vec_input['product_id'] .' is invalid';
                }
                else if ( !isset($vec_post_data['ProductLink']) || !isset($vec_post_data['ProductLink']['url']) )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - URL is empty';
                }
                else
                {
                    $product_link_model = $product_model->add_product_video($vec_post_data['ProductLink']['url'], $vec_post_data['ProductLink']['title']);
                    if ( ! $product_link_model )
                    {
                        $vec_ajax_output['error_code'] = 104;
                        $vec_ajax_output['error_msg'] = 'Error - Video could not be saved.';
                    }
                    else
                    {
                        $vec_ajax_output['link_id'] = $product_link_model->link_id;
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_ajax_output));
        }

        // If we arrive here (not POST params), render partial view
        $this->renderPartial('//commerce/productVideo/_slidepanel_create', [
            'product_model'         => $product_model,
            'product_link_model'    => $product_link_model
        ]);
    }


    /**
     * Update action for ProductLink model
     * 
     * Action loaded via SLIDEPANEL
     */
    public function actionUpdate($product_id, $link_id)
    {
        // Action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // First of all, check if Variant models exists
        $product_model = $this->loadModel($product_id, Product::class);
        $product_link_model = $this->loadModel($link_id, ProductLink::class);

        // Second, video belongs to this product
        if ( $product_link_model->product_id !== $product_model->product_id )
        {
            throw new \CHttpException(400, Yii::t('app', 'This video does not belong to this product.'));
        }

        // Submitted form?
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_ajax_output = [
                'error_msg'     => '',
                'error_code'    => 0,
                'link_id'       => '',
                'product_id'    => '',
            ];

            $vec_input = $this->jsonInput();
            $vec_post_data = StringHelper::parse_str($vec_input['form_data']);

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['form_data']) && isset($vec_input['link_id']) && isset($vec_input['product_id']) && $vec_input['product_id'] == $product_link_model->product_id )
            {
                $vec_post_data = StringHelper::parse_str($vec_input['form_data']);
                if ( isset($vec_post_data['ProductLink']) && isset($vec_post_data['ProductLink']['url']) && isset($vec_post_data['ProductLink']['title']) )
                {
                    // Return "link_id" and "product_id"
                    $vec_ajax_output['link_id'] = $product_link_model->link_id;
                    $vec_ajax_output['product_id'] = $product_link_model->product_id;

                    // #2 - Save ProductLink model
                    $product_link_model->setAttributes($vec_post_data['ProductLink']);
                    if ( ! $product_link_model->save() )
                    {
                        $vec_ajax_output['error_code'] = 101;
                        $vec_ajax_output['error_msg'] = 'Error - Data cannot be saved: '. print_r($product_link_model->getErrors(), true);
                    }
                }
                else
                {
                    $vec_ajax_output['error_code'] = 202;
                    $vec_ajax_output['error_msg'] = 'Access denied - POST input params are incorrect';
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }

            // Return JSON and end application
            $this->jsonOutput(200, Json::encode($vec_ajax_output));
        }


        // If we arrive here (not POST params), render partial view
        $this->renderPartial('//commerce/productVideo/_slidepanel_update', [
            'product_model'         => $product_model,
            'product_link_model'    => $product_link_model
        ]);
    }


    /**
     * Delete action for ProductLink model
     */
    public function actionDelete($product_id, $link_id)
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        // First of all, check if Product and ProductLink models exist
        $product_model = $this->loadModel($product_id, Product::class);
        $product_link_model = $this->loadModel($link_id, ProductLink::class);
        
        // Second, video belongs to this product
        if ( $product_link_model->product_id !== $product_model->product_id )
        {
            throw new \CHttpException(400, Yii::t('app', 'This video does not belong to this product.'));
        }

        $vec_ajax_output = [
            'error_msg'     => '',
            'error_code'    => 0,
        ];

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $vec_input = $this->jsonInput();

            // #1 - JSON input params are correct?
            if ( !empty($vec_input) && isset($vec_input['product_id']) && isset($vec_input['link_id']) )
            {
                // #2 - Product matches?
                if ( $vec_input['product_id'] != $product_id )
                {
                    $vec_ajax_output['error_code'] = 101;
                    $vec_ajax_output['error_msg'] = 'Access denied - Product #'. $vec_input['product_id'] .' is invalid';
                }

                // #3 - ProductLink matches?
                else if ( $vec_input['link_id'] != $link_id )
                {
                    $vec_ajax_output['error_code'] = 102;
                    $vec_ajax_output['error_msg'] = 'Access denied - Video #'. $vec_input['link_id'] .' is invalid';
                }

                // #4 - Delete the model
                else
                {   
                    if ( ! $product_model->remove_product_video($link_id) )
                    {
                        $vec_ajax_output['error_code'] = 103;
                        $vec_ajax_output['error_msg'] = 'Error - Video #'. $vec_input['link_id'] .' could not be deleted';
                    }
                }
            }
            else
            {
                $vec_ajax_output['error_code'] = 201;
                $vec_ajax_output['error_msg'] = 'Access denied - JSON input params are incorrect';
            }
        }
        else
        {
            $vec_ajax_output['error_code'] = 202;
            $vec_ajax_output['error_msg'] = 'Access denied - Your request is invalid. Request is not POST type';
        }

        // Return JSON and end application
        $this->jsonOutput(200, Json::encode($vec_ajax_output));
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'     => 'commerce.product.view',
            'create'    => 'commerce.product.update',
            'update'    => 'commerce.product.update',
            'delete'    => 'commerce.product.delete'
        ];
    }
}