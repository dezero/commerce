<?php
/*
|--------------------------------------------------------------------------
| Base controller class for ProductOption model
|
| Common class methods used in different controllers of commerce module
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers\_base;

use dz\helpers\Html;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\Transliteration;
use dzlab\commerce\models\ProductOption;
use dz\web\Controller;
use Yii;

abstract class BaseOptionController extends Controller
{   
    /**
     * Option type
     */
    protected $option_type;


    /**
     * List action for ProductOption models
     */
    public function actionIndex()
    {
        $model = Yii::createObject(ProductOption::class, 'search');
        $model->unsetAttributes();
        $model->option_type = $this->option_type;
        
        if ( isset($_GET['ProductOption']) )
        {
            $model->setAttributes($_GET['ProductOption']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Option type configuration
        $vec_config = $model->get_config();

        // Render INDEX view
        $this->render($model->get_view_path('index'), [
            'model'         => $model,
            'vec_config'    => $vec_config
        ]);
    }


    /**
     * Create action for ProductOption model
     */
    public function actionCreate()
    {
        // ProductOption model
        $product_option_model = Yii::createObject(ProductOption::class);
        $product_option_model->option_type = $this->option_type;

        // Create action
        $vec_data = Yii::app()->productOptionManager->create_option($product_option_model, $_POST);

        // Render create template
        $this->render($product_option_model->get_view_path('create'), $vec_data);
    }


    /**
     * Update action for ProductOption model
     */
    public function actionUpdate($id)
    {
        // ProductOption model
        $product_option_model = $this->loadModel($id, ProductOption::class);
        
        // Update action
        $vec_data = Yii::app()->productOptionManager->update_option($product_option_model, $_POST);

        // Render update template with params
        $this->render($product_option_model->get_view_path('update'), $vec_data);
    }


    /**
     * Update ProductOption weights
     */
    public function actionUpdateWeight($option_type)
    {
        // Delete action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsPostRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        if ( isset($_POST['nestable']) AND !empty($_POST['nestable']) AND is_array($_POST['nestable']) )
        {
            $vec_nestable = $_POST['nestable'];
            if ( !empty($vec_nestable) )
            {
                foreach ( $vec_nestable as $new_weight => $que_product_option )
                {
                    if ( isset($que_product_option['id']) )
                    {
                        $option_id = $que_product_option['id'];

                        // UPDATE ExamDocument weight
                        Yii::app()->db->createCommand()->update('commerce_product_option',
                            ['weight' => $new_weight+1],
                            'option_type=:option_type AND option_id=:option_id',
                            [
                                ':option_type'  => $option_type,
                                ':option_id'    => $option_id
                            ]
                        );
                    }
                }
            }

            echo Json::encode(['result' => 1]);
        }
        else
        {
            echo Json::encode(['result' => -1]);
        }

        // End application
        Yii::app()->end();
    }


    /**
     * Get document TREE structure for AJAX
     */
    public function actionTree($option_type)
    {
        // Create action only allowed by AJAX requests
        if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }

        $vec_ajax_output = [
            'result' => 'init'
        ];

        // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
        Yii::app()->clientScript->reset();

        // ProductOption
        $product_option_model = Yii::createObject(ProductOption::class);
        $product_option_model->option_type = $option_type;
        $vec_ajax_output = [
            'result' => 'success',
            'tree' => $this->renderPartial($product_option_model->get_view_path('_tree'), [
                'model' => $product_option_model,
                'is_ajax' => true
            ], true, true)
        ];

        // Output in JSON format
        echo Json::encode($vec_ajax_output);
        Yii::app()->end();
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'         => 'commerce.option.view',
            'create'        => 'commerce.option.create',
            'update'        => 'commerce.option.update',
            'updateWeight'  => 'commerce.option.update',
            'tree'          => 'commerce.option.view',
            'enable'        => 'commerce.option.update',
            'disable'       => 'commerce.option.update',
            'delete'        => 'commerce.option.update',
        ];
    }
}