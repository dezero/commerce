<?php
/*
|--------------------------------------------------------------------------
| Base controller class for Product model
|
| Common class methods used in different controllers of commerce module
|--------------------------------------------------------------------------
*/

namespace dzlab\commerce\controllers\_base;

use dz\helpers\Log;
use dz\modules\asset\models\AssetImage;
use dz\modules\category\models\Category;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\ProductCategory;
use dzlab\commerce\models\ProductPrice;
use dzlab\commerce\models\TranslatedProduct;
use dzlab\commerce\models\Variant;
use dz\modules\web\models\Seo;
use dz\web\Controller;
use Yii;

abstract class BaseProductController extends Controller
{   
    /**
     * Product type
     */
    protected $product_type;


    /**
     * List action for Product models
     */
    public function actionIndex()
    {
        $model = Yii::createObject(Product::class, 'search');
        $model->unsetAttributes();
        $model->product_type = $this->product_type;
        
        if ( isset($_GET['Product']) )
        {
            // Enabled?
            if ( isset($_GET['Product']['disable_filter']) )
            {
                $model->disable_filter = $_GET['Product']['disable_filter'];
                unset($_GET['Product']['disable_filter']);
            }

            $model->setAttributes($_GET['Product']);
        }

        // Update GRID with AJAX
        if ( Yii::app()->getRequest()->getIsAjaxRequest() )
        {
            // Remove every single script --> http://hungred.com/how-to/yii-cclientscript-disable-registerscript/
            Yii::app()->clientScript->reset();
        }

        // Product type configuration
        $vec_config = $model->get_config($this->product_type);

        // Category list
        $vec_category_list = Category::model()->category_list($vec_config['main_category_type'], ['max_depth' => 1]);
        
        $this->render($model->get_view_path('index'), [
            'model'             => $model,
            'vec_config'        => $vec_config,
            'vec_category_list' => $vec_category_list,
        ]);
    }



    /**
     * Create action for Product model
     */
    public function actionCreate()
    {
        // Product model
        $product_model = Yii::createObject(Product::class);
        $product_model->product_type = $this->product_type;

        // Create action
        $vec_data = Yii::app()->productManager->create_product($product_model, $_POST);

        // Render create template
        $this->render($product_model->get_view_path('create'), $vec_data);
    }


    /**
     * Update action for Product model
     */
    public function actionUpdate($id)
    {
        // Product model
        $product_model = $this->loadModel($id, Product::class);
        
        // Update action
        $vec_data = Yii::app()->productManager->update_product($product_model, $_POST);

        // Render update template with params
        $this->render($product_model->get_view_path('update'), $vec_data);
    }


    /**
     * Delete action for a Product model
     */
    public function actionDelete($id)
    {
        // Delete action only allowed by POST requests
        if ( Yii::app()->getRequest()->getIsPostRequest() )
        {
            $product_model = $this->loadModel($id, Product::class);
            
            // It is not allowed to DELETE a product if it has been registered on an order
            if ( $this->lineItems )
            {
                $this->showErrors(Yii::t('app', 'Product cannot be deleted because it belongs to at least one purchased orded. Try to DISABLE it.'));
            }
            
            else if ( $product_model->delete() )
            {
                if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
                {
                    Yii::app()->user->addFlash('success', Yii::t('app',  $product_model->text('delete_success')) );
                }
                else
                {
                    echo Yii::t('app', $product_model->text('delete_success'));
                }
            }

            else
            {
                $this->showErrors($product_model->getErrors());
            }
            
            if ( ! Yii::app()->getRequest()->getIsAjaxRequest() )
            {
                $this->redirect(['index']);
            }
        }
        else
        {
            throw new \CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }
    }


    /**
     * Returns an actions list of current controller related to its auth operation to check access in "AuthFilter"
     *
     * array('<defined_action>' => '<operation_name_to_check>')
     *
     * @return array
     */
    static public function checkAliasActions()
    {
        return [
            'index'         => 'commerce.product.view',
            'create'        => 'commerce.product.create',
            'update'        => 'commerce.product.update',
            'enable'        => 'commerce.product.update',
            'disable'       => 'commerce.product.update',
            'delete'        => 'commerce.product.update',
        ];
    }
}
