(function(document, window, $) {

  // Product Association SlidePanel global object
  // -------------------------------------------------------------------------------------------  
  $.dzProductAssociation = {

    // Properties
    // -------------------------------
    _slidePanel: null,
    association_id: null,
    action: null,

    // Load SlidePanel actions
    // -------------------------------
    slidePanel: function() {
      var $actions = $("#product-association-table").children("tbody").children("tr").children('td.actions-column');

      // DELETE action
      $actions.children('.delete-action').on('click', function(e){
        e.preventDefault();
        $.dzProductAssociation.action = 'delete';
        var self = this;
        $.dzSlideTable.delete(self, {
          confirmMessage: '<h3>Are your sure you want to <span class="text-danger">DELETE</span> this product association?</h3>',
          successMessage: 'Product association removed successfully',
          
          // Params sent as INPUT params via AJAX delete action
          ajaxData: JSON.stringify({
            product_id: $(self).data('product'),
            association_id: $(self).data('association')
          }),

          // After delete event -> Reload product association table
          afterDelete: function(data) {
            $.dzProductAssociation.refreshTable();
          }
        });
      });

      // CREATE action
      $('#product-association-create-btn').on('click', function(e){
        e.preventDefault();
        $.dzProductAssociation.action = 'create';
        $(this).tooltip('hide');

        // Show SLIDE PANEL clicking on an action button from the association product list table
        $(this).dzSlidePanel({
          afterLoad: $.dzProductAssociation.afterLoad
        });
      });
    },

    // Refresh/reload Product Association table
    // ---------------------------------------------------
    refreshTable: function(params) {
      // Default options
      var options = {
        afterAjaxUpdate: function(id, data) {
          // $('html, body').animate({scrollTop: 0}, 100);
          $.dzProductAssociation.slidePanel();
        }
      };

      // Add input params
      if ( ! $.isEmptyObject(params) ) {
        $.extend(options, params);
      }

      // Reload table
      $.dzSlideTable.reload('product-association-table', options);
    },

    // Load Product Association form
    // ---------------------------------------------------
    afterLoad: function(){
      var self = this;
      $.dzProductAssociation._slidePanel = this;

      // Start scroll
      $.dzSlidePanel.startScroll();

      // Get current action from data-action attribute on <div class="product-association-slidepanel-wrapper">
      $.dzProductAssociation.action = self.$panel.find('#product-association-slidepanel-action').eq(0).data('action');
      var que_action = $.dzProductAssociation.action;

      // CREATE - After slidePanel loaded
      if ( que_action == 'create' )
      {
        self.$panel.find('#ProductAssociation_product_associated_id').select2({
          width: '100%',
          dropdownCssClass: 'modal-dropdown',
          placeholder: 'Search a product by title...',
          minimumInputLength: 1,
          language: {
            inputTooShort: function () { return 'Search a product by title...'; }
          },
          ajax: {
            url: js_globals.base_url +'/search/product',
            dataType: 'json',
            delay: 250,
            data: function(params) {
              return {
                q: params.term,
                page_limit: 10
              };
            },
            processResults: function (data, params) {
              return {results: data.results};
            },
            cache: true
          }
        }).width('100%');

        // Save button
        $('#product-association-save-btn').on('click', $.dzProductAssociation.submitCreateForm);
        
      }
    },

    // Submit create Product Association form
    // -------------------------------
    submitCreateForm: function(e){
      e.preventDefault();
      var $this = $(this);
      var self = $.dzProductAssociation._slidePanel;

      // Disable button to avoid click twice
      $this.prop('disabled', true);

      // Hide errors
      self.$panel.find('#slidepanel-errors').removeClass('hide').addClass('hide');

      // Save via AJAX
      $.ajax({
        url: $this.data('url'),
        type: 'POST',
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
          product_id: $this.data('product'),
          type: $this.data('type'),
          form_data: self.$panel.find('#product-association-form').serialize()
        }),
        success: function(data) {
          if ( data.error_code == 0 && data.association_id != 0) {
            // Hide slidePanel
            $.slidePanel.hide();

            // Show success message
            $.pnotify({
              sticker: false,
              text: 'Product associated successfully',
              type: 'success'
            });

            // Save new association_id
            $.dzProductAssociation.association_id = data.association_id;

            // Reload product association table
            $.dzProductAssociation.refreshTable({
              afterAjaxUpdate: function(id, data) {
                // $('html, body').animate({scrollTop: 0}, 100);
                $.dzProductAssociation.slidePanel();
              }
            });
          }
          else {
            // Show errors
            $.dzSlidePanel.showErrors(data.error_msg);

            // Enable button again
            $this.prop('disabled', false);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          // Show errors
          $.dzSlidePanel.showErrors('ERROR - Unable to add product');

          // Enable button again
          $this.prop('disabled', false);
        }
      });
    },
  };

})(document, window, jQuery);