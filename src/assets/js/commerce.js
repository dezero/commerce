/**
 * Scripts for COMMERCE module
 */
 (function(document, window, $) {

  // Order grid view - Refresh filters
  // -------------------------------------------------------------------------------------------
  $.orderGridUpdate = function() {
    $('#Order_status_type').select2({
      templateResult: $.dzOrderStatus.select2_format,
      templateSelection: $.dzOrderStatus.select2_format,
      width: '100%'
    });

    var date_options = {
      autoclose: true,
      clearBtn: true,
      format: 'dd/mm/yyyy',
      language: js_globals.language
    };
    jQuery('#Order_ordered_from_date').datepicker(date_options);
    jQuery('#Order_ordered_to_date').datepicker(date_options);
  };

  // Commerce objet
  // -------------------------------------------------------------------------------------------
  $.dzCommerce = {

    // Caculate price for a line item
    calculateLinePrice: function($panel) {
      var extra_price = 0;
      var $extra_select = $panel.find('.product-option-select');
      var $product_price = $panel.find('#line-item-product-price');
      var $total_price = $panel.find('#line-item-total-price');
              
      // Sum all selected extras
      $extra_select.each(function(){
        var que_price = $(this).children("option:selected").attr('data-price');
        if (typeof que_price !== typeof undefined && que_price !== false) {
          extra_price += parseFloat(que_price);
        }
      });

      // Get total price
      $total_price.html($.number($panel.find('#LineItem_quantity').val() * ($product_price.data('price') + extra_price), 2, ',', '.'));
    }
  };


  // Discount global object
  // -------------------------------------------------------------------------------------------
  $.dzDiscount = {
    
    // Init discount form
    init: function() {
      // Datepicker (default options)
      var date_options = {
        autoclose: true,
        clearBtn: true,
        format: 'dd/mm/yyyy',
        weekStart: 1,
        language: 'en'
      };
      $('#discount-date-range').datepicker(date_options);

      // Disable/enable buttons -> Discounts
      $('#delete-discount-btn').dzStatusButton();
      $('#disable-discount-btn').dzStatusButton();
      $('#enable-discount-btn').dzStatusButton();

      // Generate coupon code
      $('#discount-auto-code').on('click', $.dzDiscount.generateCouponCode);

      // Discount amounts
      $('#Discount_amount').dzCurrencyTouchspin();
      $('#Discount_minimum_amount').dzCurrencyTouchspin();

      // Discount type
      $('#discount-type').find('input[name="Discount[discount_type]"]').on('change', function(e){
        if ( $(this).val() == 'code' ) {
          $("#discount-code-wrapper").removeClass("hide");
        } else {
          $("#discount-code-wrapper").removeClass("hide").addClass("hide");
          $("#Discount_code").val("");
        }
      });

      // Amount type
      $('#amount-type').find('input[name="Discount[is_fixed_amount]"]').on('change', function(e){
        if ( $(this).val() == 1 ) {
          $("#fixed-amount-wrapper").removeClass("hide");
          $("#perc-amount-wrapper").removeClass("hide").addClass("hide");
        } else {
          $("#fixed-amount-wrapper").removeClass("hide").addClass("hide");
          $("#perc-amount-wrapper").removeClass("hide");
        }
      });

      // After order restriction
      $('#after-order-restriction').find('input[name="Discount[is_after_order_restriction]"]').on('change', function(e){
        if ( $(this).val() == 1 ) {
          $("#after-order-num-days").removeClass("hide");
        } else {
          $("#after-order-num-days").removeClass("hide").addClass("hide");
        }
      });
    },

    // Coupon code auto generation
    generateCouponCode: function(e) {
      var $discount_code = $('#Discount_code');
      if ( $(this).is(':checked') ) {
        $discount_code.val('Generating code...');
        $discount_code.prop('readonly', true);
        
        $.ajax({
          url: js_globals.base_url +'/commerce/discount/generateCode',
          type: 'post',
          dataType: 'json',
          success: function(data) {
            $discount_code.val(data.code);
          },
          error: function(request, status, error) {
            alert('ERROR: '+request.responseText);
            $discount_code.val('');
            $discount_code.prop('readonly', false);
          },
          cache: true
        });
      } else {
        $discount_code.val('');
        $discount_code.prop('readonly', false);
      }
    }
  };

  // Order mail global object
  $.dzOrderMail = {
    // Init order mail form
    init: function() {
      var $order_mail_menu = $('#order-mail-menu');
      $order_mail_menu.children('a').on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        bootbox.confirm(
          '<h3>'+ $order_mail_menu.data('confirm') +'</h3><h4>Email template: <i>'+ $this.data('alias') +'</i></h4>',
          function(confirmed){
            if ( confirmed ) {
              $('#order-mail-alias').val($this.data('alias'));
              $('#order-email-form').submit();
            }
          }
        );
      });
      
    },
  };


  // DOCUMENT READY
  // -------------------------------------------------------------------------------------------
  $(document).ready(function() {
    // Order grid - Select2 custom style
    if ( $('#order-grid').size() > 0 ) {
      $.orderGridUpdate();

      // Order grid search form
      $('#order-grid-search-form').dzGridSearch();
    }

    // Customer grid
    $('#customer-grid-search-form').dzGridSearch();

    // Product grid
    $('#product-grid-search').children('form').dzGridSearch();

    // Order change status via panelSlider
    $.dzOrderStatus.slidePanel();

    // Product images widget
    $.dzProductImage.init();

    // Product videos actions via panelSlider
    $.dzProductVideo.slidePanel();

    // Product links actions via panelSlider
    $.dzProductLink.slidePanel();

    // Line Items actions via panelSlider
    $.dzLineItem.slidePanel();

    // Product assciation actions via panelSlider
    $.dzProductAssociation.slidePanel();

    // Discount product actions via panelSlider
    $.dzDiscountProduct.slidePanel();

    // Order payment history table
    $('#payments-table').find('.transaction-details-btn').on('click', function(e){
      e.preventDefault();
      $('#transaction-details-'+ $(this).data('transaction')).removeClass('hide');
      $(this).addClass('hide');
    });

    // Init discount
    if ( $('#discount-form').size() > 0 ) {
      $.dzDiscount.init();
    }

    // Init order mail
    if ( $('#order-email-form').size() > 0 ) {
      $.dzOrderMail.init();
    }
  });

})(document, window, jQuery);
