(function(document, window, $) {

  // Order SlidePanel global object
  // -------------------------------------------------------------------------------------------  
  $.dzOrderStatus = {

    // Properties
    // -------------------------------
    _slidePanel: null,
    action: null,

    // ORDER STATUS TYPE - Special function to render format for SELECT2 widget
    // -------------------------------------------------------------------------------------------
    select2_format: function(item) {
      if (!item.id) {
        return $('<span>- All -</span>');
      }

      var que_color = '';
      switch ( item.id ) {
        case 'shipped':
          que_color = 'purple-800';
        break;

        case 'in_progress':
          que_color = 'blue-800';
        break;

        case 'payment_received':
          que_color = 'green-800';
        break;

        case 'payment_failed':
          que_color = 'red-500';
        break;
              
        case 'canceled':
          que_color = 'red-800';
        break;

        case 'on_hold':
          que_color = 'yellow-800';
        break;

        case 'completed':
          que_color = 'grey-800';
        break;

        case 'cart':
          que_color = 'grey-500';
        break;
      }

      if ( que_color !== '' ) {
        return $('<span><i class=\'wb-medium-point '+ que_color +'\' aria-hidden=\'true\'></i> '+ item.text +'</span>');
      }

      return $('<span>'+ item.text +' (all)</span>');
    },

    // Load SlidePanel
    // -------------------------------
    slidePanel: function() {
      var self = this;
      $('#order-history-btn').on('click', self.show);      
      $('#order-status-btn').on('click', self.show);      
    },

    // Show SlidePanel
    // ------------------------------
    show: function(e) {
      e.preventDefault();

      // Show SLIDE PANEL clicking on an action button from the line item list table
      $(this).dzSlidePanel({
        afterLoad: $.dzOrderStatus.afterLoad
      });
    },

    // Load Order status form
    // -------------------------------
    afterLoad: function(){
      var self = this;
      $.dzOrderStatus._slidePanel = this;

      // Start scroll
      $.dzSlidePanel.startScroll();

      var $order_btn = $('#order-status-save-btn');
      var $order_status = $('#Order_status_type');

      // Order change status
      $order_status.select2({
        templateResult: $.dzOrderStatus.select2_format,
        templateSelection: $.dzOrderStatus.select2_format,
        width: '100%',
        dropdownCssClass: 'modal-dropdown'
      });

      // Select2 change event
      $order_status.on('change', function(e){
        $('#send-mail-row').removeClass('hide').addClass('hide');
        $('#help-mail-block').removeClass('hide').addClass('hide');

        if ( $(this).val() == $(this).data('init-value') ) {
          $order_btn.prop('disabled', true);
        } else {
          $order_btn.prop('disabled', false);

          // Show "send email"?
          if ( $(this).val() == "shipped" ) {
            $('#send-mail-row').removeClass('hide');
            $('#help-mail-block').removeClass('hide');
          }
        }
      });

      // Save order status button
      $order_btn.on('click', $.dzOrderStatus.submitForm);
    },

    // Submit Order Status form
    // -------------------------------
    submitForm: function(e){
      e.preventDefault();

      var $this = $(this);
      var self = $.dzOrderStatus._slidePanel;
      var $order_status = $('#Order_status_type');
      var $order_btn = $('#order-status-save-btn');

      var msg_alert = '<h3>Are your sure you want to change the status to '+ $order_status.val().toUpperCase().replace(/\_/g, ' ') +'?</h3>';
      if ( $order_status.val() === 'confirmed' ) {
        msg_alert += '<p><u>WARNING</u>: This action could take several minutes. <span class="text-danger">Please, do not refresh the page!</span><ul><li>A <strong>NEW INVOICE</strong> is going to be created.</li></ul></p>';
      } else {
        msg_alert += '<p></p>';
      }

      bootbox.confirm(
        msg_alert,
        function(confirmed){
          if ( confirmed ) {
            // Disable button to avoid click twice
            $order_btn.prop('disabled', true);

            // Change status via AJAX
            $.ajax({
              url: js_globals.base_url +'/commerce/order/status?id='+  $this.data('order'),
              type: 'POST',
              cache: false,
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              data: JSON.stringify({
                order_id: $this.data('order'),
                new_status: $order_status.val(),
                new_comments: $('#Order_comments').val(),
                is_sending_mail: $('input[name="Order[is_sending_mail]"]:checked').val()
              }),
              success: function(data) {
                if ( data.error_code == 0 ) {
                  // Show success message
                  $.pnotify({
                    sticker: false,
                    text: 'Status changed successfully',
                    type: 'success'
                  });

                  // Hide slidePanel
                  $.slidePanel.hide();

                  // Reload page
                  window.location.href = js_globals.base_url +'/commerce/order/view?id='+ $this.data('order') +'&new_status='+ $order_status.val();
                }
                else {
                  alert('ERROR '+ data.error_code +': '+ data.error_msg);

                  // Enable button again
                  $order_btn.prop('disabled', false);
                }
              },
              error: function(xhr, ajaxOptions, thrownError) {
                console.log('Unable to change the status');

                // Enable button again
                $order_btn.prop('disabled', false);
              }
            });
          }
        }
      );
    }
  };
})(document, window, jQuery);