(function(document, window, $) {

  // Discount Product SlidePanel global object
  // -------------------------------------------------------------------------------------------  
  $.dzDiscountProduct = {

    // Properties
    // -------------------------------
    _slidePanel: null,
    product_id: null,
    action: null,

    // Load SlidePanel actions
    // -------------------------------
    slidePanel: function() {
      var $actions = $("#discount-product-table").children("tbody").children("tr").children('td.actions-column');

      // DELETE action
      $actions.children('.delete-action').on('click', function(e){
        e.preventDefault();
        $.dzDiscountProduct.action = 'delete';
        var self = this;
        $.dzSlideTable.delete(self, {
          confirmMessage: '<h3>Are your sure you want to <span class="text-danger">DELETE</span> this product?</h3>',
          successMessage: 'Product removed successfully',
          
          // Params sent as INPUT params via AJAX delete action
          ajaxData: JSON.stringify({
            discount_id: $(self).data('discount'),
            product_id: $(self).data('product')
          }),

          // After delete event -> Reload discount product table
          afterDelete: function(data) {
            $.dzDiscountProduct.refreshTable();
          }
        });
      });

      // CREATE action
      $('#discount-product-create-btn').on('click', function(e){
        e.preventDefault();
        $.dzDiscountProduct.action = 'create';
        $(this).tooltip('hide');

        // Show SLIDE PANEL clicking on an action button from the discount products list table
        $(this).dzSlidePanel({
          afterLoad: $.dzDiscountProduct.afterLoad
        });
      });

      // DELETE ALL action
      $('#discount-product-delete-all-btn').on('click', $.dzDiscountProduct.deleteAll);
    },


    // Refresh/reload Discount Products table
    // ---------------------------------------------------
    refreshTable: function(params) {
      // Default options
      var options = {
        afterAjaxUpdate: function(id, data) {
          // $('html, body').animate({scrollTop: 0}, 100);
          $.dzDiscountProduct.slidePanel();
        }
      };

      // Add input params
      if ( ! $.isEmptyObject(params) ) {
        $.extend(options, params);
      }

      // Reload table
      $.dzSlideTable.reload('discount-product-table', options);
    },


    // Load Discount Product form
    // ---------------------------------------------------
    afterLoad: function(){
      var self = this;
      $.dzDiscountProduct._slidePanel = this;

      // Start scroll
      $.dzSlidePanel.startScroll();

      // Get current action from data-action attribute on <div class="discount-product-slidepanel-wrapper">
      $.dzDiscountProduct.action = self.$panel.find('#discount-product-slidepanel-action').eq(0).data('action');
      var que_action = $.dzDiscountProduct.action;

      // CREATE - After slidePanel loaded
      if ( que_action == 'create' )
      {
        self.$panel.find('#DiscountProduct_product_id').select2({
          width: '100%',
          dropdownCssClass: 'modal-dropdown',
          placeholder: 'Search a product by title...',
          minimumInputLength: 1,
          language: {
            inputTooShort: function () { return 'Search a product by title...'; }
          },
          ajax: {
            url: js_globals.base_url +'/search/product',
            dataType: 'json',
            delay: 250,
            data: function(params) {
              return {
                q: params.term,
                page_limit: 10
              };
            },
            processResults: function (data, params) {
              return {results: data.results};
            },
            cache: true
          }
        }).width('100%');

        // Save button
        $('#discount-product-save-btn').on('click', $.dzDiscountProduct.submitCreateForm);
        
      }
    },


    // Submit create Discount Product form
    // ---------------------------------------
    submitCreateForm: function(e){
      e.preventDefault();
      var $this = $(this);
      var self = $.dzDiscountProduct._slidePanel;

      // Disable button to avoid click twice
      $this.prop('disabled', true);

      // Hide errors
      self.$panel.find('#slidepanel-errors').removeClass('hide').addClass('hide');

      // Save via AJAX
      $.ajax({
        url: $this.data('url'),
        type: 'POST',
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
          discount_id: $this.data('discount'),
          form_data: self.$panel.find('#discount-product-form').serialize()
        }),
        success: function(data) {
          if ( data.error_code == 0 && data.product_id != 0) {
            // Hide slidePanel
            $.slidePanel.hide();

            // Show success message
            $.pnotify({
              sticker: false,
              text: 'Product added successfully',
              type: 'success'
            });

            // Save new product_id
            $.dzDiscountProduct.product_id = data.product_id;

            // Reload discount product table
            $.dzDiscountProduct.refreshTable({
              afterAjaxUpdate: function(id, data) {
                // $('html, body').animate({scrollTop: 0}, 100);
                $.dzDiscountProduct.slidePanel();
              }
            });
          }
          else {
            // Show errors
            $.dzSlidePanel.showErrors(data.error_msg);

            // Enable button again
            $this.prop('disabled', false);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          // Show errors
          $.dzSlidePanel.showErrors('ERROR - Unable to add product');

          // Enable button again
          $this.prop('disabled', false);
        }
      });
    },

    // Delete ALL products
    deleteAll: function(e) {
      e.preventDefault();
      var $this = $(this);
      bootbox.confirm(
        $this.data('confirm'),
        function(confirmed){
          if ( confirmed ) {
            // Disable button to avoid click twice
            $this.prop('disabled', true);

            $.ajax({
              url: $this.data('url'),
              type: 'POST',
              cache: false,
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              data: JSON.stringify({
                discount_id: $this.data('discount'),
              }),
              success: function(data) {
                if ( data.error_code == 0 ) {
                  // Show success message
                  $.pnotify({
                    sticker: false,
                    text: 'Products deleted successfully',
                    type: 'success'
                  });

                  // Reload page
                  window.location.href = js_globals.base_url +'/commerce/discountProduct/?discount_id='+ $this.data('discount');
                }
                else {
                  alert('ERROR '+ data.error_code +': '+ data.error_msg);

                  // Enable button again
                  $this.prop('disabled', false);
                }
              },
              error: function(xhr, ajaxOptions, thrownError) {
                alert('ERROR - Products could not be deleted');

                // Enable button again
                $this.prop('disabled', false);
              }
            });
          }
        }
      );
    }
  };

})(document, window, jQuery);
