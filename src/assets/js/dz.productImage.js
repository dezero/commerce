(function(document, window, $) {

  // Product Image global object
  // -------------------------------------------------------------------------------------------  
  $.dzProductImage = {

    // Properties
    // -------------------------------
    _slidePanel: null,
    action: null,
    
    // Init product image
    // -------------------------------
    init: function() {
      if ( $('#product-image-file-input').size() > 0 ) {
        // Delete action
        $.dzProductImage.tableActions();

        // Dropzone upload image
        $.dzProductImage.dropzone();

        // Show nestable
        $('#sort-product-images-btn').on('click', function(e){
          e.preventDefault();
          $.dzProductImage.action = 'sort';
          $(this).tooltip('hide');

          // Show SLIDE PANEL clicking on an action button from the link list table
          $(this).dzSlidePanel({
            afterLoad: $.dzProductImage.afterLoad
          });
        });
      }
    },

    // AfterLoad callback for dzProductImage, dzProductVideo and dzProductLink
    // ------------------------------------------------------------------------------------
    afterLoad: function(){
      var self = this;
      var que_action = '';

      // Start scroll
      $.dzSlidePanel.startScroll();

      // AfterLoad callback for dzProductImage
      if ( self.$panel.find('#product-image-slidepanel-action').size() > 0 ) {
        $.dzProductImage._slidePanel = this;

        // Get current action from data-action attribute on <div class="product-image-slidepanel-wrapper">
        $.dzProductImage.action = self.$panel.find('#product-image-slidepanel-action').eq(0).data('action');
        que_action = $.dzProductImage.action;

        // Product image nestable tree
        if ( que_action == 'sort' ) {
          $.dzProductImage.nestable({
            maxDepth: 1
          });
        }
      }

      // AfterLoad callback for dzProductVideo
      else if ( self.$panel.find('#product-video-slidepanel-action').size() > 0 ) {
        $.dzProductVideo._slidePanel = this;

        // Get current action from data-action attribute on <div class="product-video-slidepanel-wrapper">
        $.dzProductVideo.action = self.$panel.find('#product-video-slidepanel-action').eq(0).data('action');
        que_action = $.dzProductVideo.action;

        // CREATE or UPDATE - After slidePanel loaded
        if ( que_action == 'create' || que_action == 'update' )
        {
          // Save button
          $('#product-video-save-btn').on('click', $.dzProductVideo.submitForm);
        }
      }

      // AfterLoad callback for dzProductLink
      else if ( self.$panel.find('#product-link-slidepanel-action').size() > 0 ) {
        $.dzProductLink._slidePanel = this;

        // Get current action from data-action attribute on <div class="product-link-slidepanel-wrapper">
        $.dzProductLink.action = self.$panel.find('#product-link-slidepanel-action').eq(0).data('action');
        que_action = $.dzProductLink.action;

        // CREATE or UPDATE - After slidePanel loaded
        if ( que_action == 'create' || que_action == 'update' )
        {
          // Save button
          $('#product-link-save-btn').on('click', $.dzProductLink.submitForm);
        }
      }
    },

    // Nestable tree widget
    // ---------------------------------------------------
    nestable: function(settings) {
      $('#product-image-nestable-wrapper').nestable(settings).on('change', function(){
        var que_nestable = $(this).nestable('serialize');
        var $this = $(this);
        $('#'+ $this.data('name') +'-loading-tree').height($this.height()+'px').removeClass('hide');
        $this.addClass('hide');

        $.ajax({
          url: $this.data('url'),
          type: 'post',
          dataType: 'json',
          data: { nestable: que_nestable },
          success: function(data) {
            $.dzProductImage.refreshTable();
            $('#'+ $this.data('name') +'-loading-tree').addClass('hide');
            $this.removeClass('hide');
          },
          error: function(request, status, error) {
            alert('ERROR: '+request.responseText);
          },
          cache: false
        });
      });
    },

    // Load Krajee Dropzone File Upload
    // -------------------------------
    dropzone: function() {
      var $container = $('#product-image-upload-container');

      // Click on "UPLOAD IMAGES" button
      $('#upload-product-images-btn').on('click', function(e){
        // Reset Dropzone messages
        $('#product-image-success-message').hide();
        $('#product-image-success-message').html('');
        $('#product-image-error-message').hide();
        $('#product-image-error-message').html('');
        var $icon = $(this).children('.wb');

        if ( $icon.hasClass('wb-chevron-down') ) {
          $icon.removeClass('wb-chevron-down').addClass('wb-chevron-up');
          $container.slideDown(200);
        } else {
          $icon.removeClass('wb-chevron-up').addClass('wb-chevron-down');
          $container.slideUp(200);
        }
        $(this).blur();
      });

      // ---------------------------------------------------------
      // KRAJEE DROPZONE
      // ---------------------------------------------------------
      var $product_image_file_input = $('#product-image-file-input');

      $product_image_file_input.fileinput({
        language: window.js_globals.language,
        uploadUrl: window.js_globals.baseUrl + "/commerce/productImage/upload?product_id="+ $product_image_file_input.data('product'),
        uploadAsync: false,
        showPreview: true,  // needed to show the dropzone
        showUpload: false,
        showRemove: false,
        showCaption: false,
        showZoom: false,

        // allowedFileExtensions: ['pdf','doc','docx'],
        // allowedFileTypes: ['video'],
        maxFileSize: 102400, // 100 MB (104857600 / 1024)
        maxFileCount: 10,
        elErrorContainer: '#product-image-error-message',
        dropZoneEnabled: true,

        browseIcon: '<i class=\"wb wb-folder\"></i>&nbsp;',
        browseClass: 'btn btn-primary btn-block',
        removeIcon: '<i class=\"wb wb-trash text-danger\"></i>',
        removeClass: 'btn btn-default',
        cancelIcon: '<i class=\"wb wb-close\"></i>',
        cancelClass: 'btn btn-default btn-block',
        uploadIcon: '<i class=\"wb wb-upload\"></i>',
        uploadClass: 'btn btn-default',
        zoomIcon: '<i class=\"wb wb-eye\"></i>',
        zoomClass: 'btn btn-xs btn-default',
        uploadRetry: '<i class=\"wb wb-refresh\"></i>',

        // Dropzone / preview window
        fileActionSettings: {
          removeIcon: '<i class=\"wb wb-trash text-danger\"></i>',
          removeClass: 'btn btn-xs btn-default',
          removeTitle: 'Delete file',
          uploadIcon: '<i class=\"wb wb-upload text-success\"></i>',
          uploadClass: 'btn btn-xs btn-default',
          uploadTitle: 'Upload file',
          zoomIcon: '<i class=\"wb wb-eye\"></i>',
          zoomClass: 'btn btn-xs btn-default',
          uploadRetry: '<i class=\"wb wb-refresh\"></i>',
          indicatorNew: '<i class=\"wb wb-arrow-down text-warning\"></i>',
          indicatorSuccess: '<i class=\"wb wb-check text-success\"></i>',
          indicatorError: '<i class=\"wb wb-warning text-danger\"></i>',
          indicatorLoading: '<i class=\"wb wb-arrow-up text-muted\"></i>',
          indicatorNewTitle: 'Not uploaded yet',
          indicatorSuccessTitle: 'Uploaded',
          indicatorErrorTitle: 'Upload Error',
          indicatorLoadingTitle: 'Uploading ...'
        },

        // function a callback to convert the filename as a slug string eliminating special characters
        slugCallback: function(text) {
          return text;
          // return isEmpty(text) ? '' : String(text).replace(/[\[\]\/\{}:;#%=\(\)\*\+\?\\\^\$\|<>&\"']/g, '_');
        }
      })
      
      // trigger upload method immediately after files are selected
      .on('filebatchselected', function(event, files) {
        $product_image_file_input.fileinput('upload');
      })

      // before batch upload event
      // .on('filebatchpreupload', function(event, data, id, index) {
      //  $('#product-image-success-message').html('<h4>Document upload results</h4><ul></ul>').hide();
      // })

      // after batch upload event (success)
      .on('filebatchuploadsuccess', function(event, data) {
        var out = '';
        $.each(data.files, function(key, file) {
          var fname = file.name;
          // out = out + '<li>' + 'Document #' + (key + 1) + ' - "'  +  fname + '" uploaded successfully.' + '</li>';
          out = out + '<li>"'+  fname +'" uploaded successfully.</li>';
        });
        /*
        $('#product-image-success-message').children('ul').append(out);
        $('#product-image-success-message').append('<a href="" class="btn btn-dark btn-block btn-close"><i class="wb wb-close"></i> Close</a>');
        $('#product-image-success-message').fadeIn('slow');
        $('#product-image-file-input-wrapper').slideUp(200);
        */

        // Show success message
        $.pnotify({
          sticker: false,
          text: '<ul>'+ out + '</ul>',
          type: 'success'
        });

        // Reload product images file list
        setTimeout(function(){
          $product_image_file_input.fileinput('clear');
          $product_image_file_input.blur();

          // Reload product images list
          $.dzProductImage.refreshTable();
          // $.dzProductImage.reloadNestable();
        }, 1000);

        // Hide success message
        // $('#product-image-success-message').find('.btn-close').on('click', function(e){
        //   e.preventDefault();
        //   $('#toggle-document-upload').click();
        //   $('#toggle-document-upload').data('reload', 1);
        // });
      });
    },

    // Load table actions
    // ---------------------------------------------------
    tableActions: function() {
      var $actions = $("#product-image-table").children("tbody").children("tr").children('td.actions-column');

      // DELETE action
      $actions.children('.delete-action').on('click', function(e){
        e.preventDefault();
        $.dzProductAssociation.action = 'delete';
        var self = this;
        $.dzSlideTable.delete(self, {
          confirmMessage: '<h3>Are your sure you want to <span class="text-danger">DELETE</span> this product image?</h3>',
          successMessage: 'Product image removed successfully',
          
          // Params sent as INPUT params via AJAX delete action
          ajaxData: JSON.stringify({
            product_id: $(self).data('product'),
            image_id: $(self).data('image')
          }),

          // After delete event -> Reload product image table
          afterDelete: function(data) {
            $.dzProductImage.refreshTable();
            // $.dzProductImage.reloadNestable();
          }
        });
      });
    },

    // Refresh/reload Product Image table
    // ---------------------------------------------------
    refreshTable: function(params) {
      // Default options
      var options = {
        afterAjaxUpdate: function(id, data) {
          // $('html, body').animate({scrollTop: 0}, 100);
          $.dzProductImage.tableActions();
        }
      };

      // Add input params
      if ( ! $.isEmptyObject(params) ) {
        $.extend(options, params);
      }

      // Reload table
      $.dzSlideTable.reload('product-image-table', options);
    },


    // Reload tree widget
    // ---------------------------------------------------
    reloadNestable: function() {
      var que_url = window.js_globals.baseUrl + '/commerce/productImage/tree?product_id='+ $('#product-image-nestable-wrapper').data('product');
      $.dzNestableReload(que_url, 'product-image');
    }
  };

})(document, window, jQuery);
