(function(document, window, $) {

  // Product Link SlidePanel global object
  // -------------------------------------------------------------------------------------------  
  $.dzProductLink = {

    // Properties
    // -------------------------------
    _slidePanel: null,
    link_id: null,
    action: null,

    // Load SlidePanel actions for links
    // --------------------------------------
    slidePanel: function() {
      var $actions = $("#product-link-table").children("tbody").children("tr").children('td.actions-column');

      // DELETE action
      $actions.children('.delete-action').on('click', function(e){
        e.preventDefault();
        $.dzProductLink.action = 'delete';
        var self = this;
        $.dzSlideTable.delete(self, {
          confirmMessage: '<h3>Are your sure you want to <span class="text-danger">DELETE</span> this link?</h3>',
          successMessage: 'Link removed successfully',
          
          // Params sent as INPUT params via AJAX delete action
          ajaxData: JSON.stringify({
            product_id: $(self).data('product'),
            link_id: $(self).data('link')
          }),

          // After delete event -> Reload link table
          afterDelete: function(data) {
            $.dzProductLink.refreshTable();
          }
        });
      });

      // UPDATE action
      $actions.children('.update-action').on('click', function(e){      
        e.preventDefault();
        $.dzProductLink.action = 'update';
        $(this).tooltip('hide');

        // Show SLIDE PANEL clicking on an action button from the link list table
        $(this).dzSlidePanel({
          // afterLoad: $.dzProductLink.afterLoad
          afterLoad: $.dzProductImage.afterLoad
        });
      });

      // CREATE action
      $('#product-link-create-btn').on('click', function(e){
        e.preventDefault();
        $.dzProductLink.action = 'create';
        $(this).tooltip('hide');

        // Show SLIDE PANEL clicking on an action button from the link list table
        $(this).dzSlidePanel({
          // afterLoad: $.dzProductLink.afterLoad
          afterLoad: $.dzProductImage.afterLoad
        });
      });
    },

    // Refresh/reload product link table
    // ---------------------------------------------------
    refreshTable: function(params) {
      // Default options
      var options = {
        afterAjaxUpdate: function(id, data) {
          // $('html, body').animate({scrollTop: 0}, 100);
          $.dzProductLink.slidePanel();
        }
      };

      // Add input params
      if ( ! $.isEmptyObject(params) ) {
        $.extend(options, params);
      }

      // Reload table
      $.dzSlideTable.reload('product-link-table', options);
    },


    /*
    // Load Product Link form (LOADED FROM dz.productImage.js)
    // ---------------------------------------------------
    afterLoad: function(){
      var self = this;
      $.dzProductLink._slidePanel = this;

      // Start scroll
      $.dzSlidePanel.startScroll();

      // Get current action from data-action attribute on <div class="product-link-slidepanel-wrapper">
      $.dzProductLink.action = self.$panel.find('#product-link-slidepanel-action').eq(0).data('action');
      var que_action = $.dzProductLink.action;

      // CREATE or UPDATE - After slidePanel loaded
      if ( que_action == 'create' || que_action == 'update' )
      {
        // Save button
        $('#product-link-save-btn').on('click', $.dzProductLink.submitForm);
      }
    },
    */

    // Submit Product Link form
    // -------------------------------
    submitForm: function(e){
      e.preventDefault();
      var $this = $(this);
      var self = $.dzProductLink._slidePanel;

      // Disable button to avoid click twice
      $this.prop('disabled', true);

      // Hide errors
      self.$panel.find('#slidepanel-errors').removeClass('hide').addClass('hide');

      // Save via AJAX
      $.ajax({
        url: $this.data('url'),
        type: 'POST',
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
          product_id: $this.data('product'),
          link_id: $this.data('link'),
          is_video: 0,
          form_data: self.$panel.find('#product-link-form').serialize()
        }),
        success: function(data) {
          if ( data.error_code == 0 && data.link_id != 0) {
            // Hide slidePanel
            $.slidePanel.hide();

            // Show success message
            $.pnotify({
              sticker: false,
              text: 'Link saved successfully',
              type: 'success'
            });

            // Save new link_id
            $.dzProductLink.link_id = data.link_id;

            // Reload link table
            $.dzProductLink.refreshTable({
              afterAjaxUpdate: function(id, data) {
                // $('html, body').animate({scrollTop: 0}, 100);
                $.dzProductLink.slidePanel();
              }
            });
          }
          else {
            // Show errors
            $.dzSlidePanel.showErrors(data.error_msg);

            // Enable button again
            $this.prop('disabled', false);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          // Show errors
          $.dzSlidePanel.showErrors('ERROR - Unable to save link');

          // Enable button again
          $this.prop('disabled', false);
        }
      });
    },
  };

})(document, window, jQuery);