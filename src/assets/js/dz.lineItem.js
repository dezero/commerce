(function(document, window, $) {

  // Line Item SlidePanel global object
  // -------------------------------------------------------------------------------------------  
  $.dzLineItem = {

    // Properties
    // -------------------------------
    _slidePanel: null,
    line_item_id: null,
    action: null,

    // Load SlidePanel actions
    // -------------------------------
    slidePanel: function() {
      var $actions = $("#line-item-table").children("tbody").children("tr").children('td.actions-column');

      // DELETE action
      $actions.children('.delete-action').on('click', function(e){
        e.preventDefault();
        $.dzLineItem.action = 'delete';
        var self = this;
        $.dzSlideTable.delete(self, {
          confirmMessage: '<h3>Are your sure you want to <span class="text-danger">DELETE</span> this product?</h3><p><strong>WARNING:</strong> It will be removed permanently on the platform.</p>',
          successMessage: 'Product removed successfully',
          
          // Params sent as INPUT params via AJAX delete action
          ajaxData: JSON.stringify({
            order_id: $(self).data('order'),
            line_item_id: $(self).data('line-item')
          }),

          // After delete event -> Reload line item table
          afterDelete: function(data) {
            $.dzLineItem.refreshTable();
          }
        });
      });

      // UPDATE action
      $actions.children('.update-action').on('click', function(e){      
        e.preventDefault();
        $.dzLineItem.action = 'update'; // $(this).data('action');
        $(this).tooltip('hide');

        // Show SLIDE PANEL clicking on an action button from the line item list table
        $(this).dzSlidePanel({
          afterLoad: $.dzLineItem.afterLoad
        });
      });

      // CREATE action
      $('#line-item-create-btn').on('click', function(e){
        e.preventDefault();
        $.dzLineItem.action = 'create';
        $(this).tooltip('hide');

        // Show SLIDE PANEL clicking on an action button from the line item list table
        $(this).dzSlidePanel({
          afterLoad: $.dzLineItem.afterLoad
        });
      });
    },

    // Refresh/reload Line Item table
    // -------------------------------
    refreshTable: function(params) {
      // Default options
      var options = {
        afterAjaxUpdate: function(id, data) {
          $('html, body').animate({scrollTop: 0}, 100);
          $.dzLineItem.slidePanel();
        }
      };

      // Add input params
      if ( ! $.isEmptyObject(params) ) {
        $.extend(options, params);
      }

      // Reload table
      $.dzSlideTable.reload('line-item-table', options);
    },

    // Load Line Item form
    // -------------------------------
    afterLoad: function(){
      var self = this;
      $.dzLineItem._slidePanel = this;

      // Start scroll
      $.dzSlidePanel.startScroll();

      // Get current action from data-action attribute on <div class="line-item-slidepanel-wrapper">
      $.dzLineItem.action = self.$panel.find('#line-item-slidepanel-action').eq(0).data('action');
      var que_action = $.dzLineItem.action;

      // CREATE - After slidePanel loaded
      if ( que_action == 'create' )
      {
        self.$panel.find('#LineItem_product_id').select2({
          width: '100%',
          dropdownCssClass: 'modal-dropdown',
          placeholder: 'Search a product by title...',
          minimumInputLength: 1,
          language: {
            inputTooShort: function () { return 'Search a product by title...'; }
          },
          ajax: {
            url: js_globals.base_url +'/search/product',
            dataType: 'json',
            delay: 250,
            data: function(params) {
              return {
                q: params.term,
                page_limit: 10
              };
            },
            processResults: function (data, params) {
              return {results: data.results};
            },
            cache: true
          }
        }).width('100%');

        // Save line item button
        $('#line-item-continue-btn').on('click', $.dzLineItem.submitCreateForm);
        
      }

      // UPDATE - After slidePanel loaded
      else if ( que_action == 'update' ) {
        // Currency input
        self.$panel.find('input[data-plugin="TouchSpin"]').TouchSpin({
          verticalupclass: 'wb-plus',
          verticaldownclass: 'wb-minus',
          buttondown_class: 'btn btn-outline btn-default',
          buttonup_class: 'btn btn-outline btn-default'
        }).on('change', function(e){
          $.dzCommerce.calculateLinePrice(self.$panel);
        });

        // Dropdown for variant
        $('#LineItem_variant_id').select2({
          width: '100%',
          dropdownCssClass: 'modal-dropdown',
          placeholder: 'Select a variant'
        }).width('100%');

        // Dropdown for ProductOptions
        self.$panel.find('.product-option-select').select2({
          width: '100%',
          dropdownCssClass: 'modal-dropdown',
          placeholder: 'Choose an option',
          allowClear: true,
        })
        .on('change', function(e) {
          $.dzCommerce.calculateLinePrice(self.$panel);
        })
        .width('100%');

        // Dropdown for product prices
        $('#LineItem_price_alias').select2({
          width: '100%',
          dropdownCssClass: 'modal-dropdown',
          placeholder: 'Choose a product price'
        }).on('change', function(e){
          var $current_selected = $(e.currentTarget).find("option:selected");
          var product_price = $current_selected.data('price');
          if ( typeof product_price !== "undefined" )
          {
            self.$panel.find('#line-item-product-price').data('price', product_price);
            self.$panel.find('#line-item-product-price').html($current_selected.data('format-price'));
            $.dzCommerce.calculateLinePrice(self.$panel);
          }          
        }).width('100%');

        // Save line item button
        $('#line-item-save-btn').on('click', $.dzLineItem.submitUpdateForm);
      }
    },

    // Submit create Line Item form
    // -------------------------------
    submitCreateForm: function(e){
      e.preventDefault();
      var $this = $(this);
      var self = $.dzLineItem._slidePanel;

      // Disable button to avoid click twice
      $this.prop('disabled', true);

      // Hide errors
      self.$panel.find('#slidepanel-errors').removeClass('hide').addClass('hide');

      // Save via AJAX
      $.ajax({
        url: $this.data('url'),
        type: 'POST',
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
          order_id: $this.data('order'),
          form_data: self.$panel.find('#line-item-form').serialize()
        }),
        success: function(data) {
          if ( data.error_code == 0 && data.line_item_id != 0) {
            // Hide slidePanel
            $.slidePanel.hide();

            // Show success message
            $.pnotify({
              sticker: false,
              text: 'Product added successfully',
              type: 'success'
            });

            // Save new line item id
            $.dzLineItem.line_item_id = data.line_item_id;

            // Reload line item table
            $.dzLineItem.refreshTable({
              afterAjaxUpdate: function(id, data) {
                $('html, body').animate({scrollTop: 0}, 100);
                $.dzLineItem.slidePanel();

                // Click on UPDATE newly created line item
                $("#line-item-row-"+ $.dzLineItem.line_item_id).children('td.actions-column').children('.update-action').click();
              }
            });
          }
          else {
            // Show errors
            $.dzSlidePanel.showErrors(data.error_msg);

            // Enable button again
            $this.prop('disabled', false);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          // Show errors
          $.dzSlidePanel.showErrors('ERROR - Unable to add product');

          // Enable button again
          $this.prop('disabled', false);
        }
      });
    },

    // Submit update Line Item form
    // -------------------------------
    submitUpdateForm: function(e){
      e.preventDefault();
      var $this = $(this);
      var self = $.dzLineItem._slidePanel;

      // Disable button to avoid click twice
      $this.prop('disabled', true);

      // Hide errors
      self.$panel.find('#slidepanel-errors').removeClass('hide').addClass('hide');

      // Save via AJAX
      $.ajax({
        url: $this.data('url'),
        type: 'POST',
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
          order_id: $this.data('order'),
          line_item_id: $this.data('line-item'),
          form_data: self.$panel.find('#line-item-form').serialize()
        }),
        success: function(data) {
          if ( data.error_code == 0 ) {
            // Show success message
            $.pnotify({
              sticker: false,
              text: 'Product saved successfully',
              type: 'success'
            });

            // Hide slidePanel
            $.slidePanel.hide();

            // Reload line item table
            $.dzLineItem.refreshTable();
          }
          else {
            // Show errors
            $.dzSlidePanel.showErrors(data.error_msg);

            // Enable button again
            $this.prop('disabled', false);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          // Show errors
          $.dzSlidePanel.showErrors('ERROR - Unable to save line item');

          // Enable button again
          $this.prop('disabled', false);
        }
      });
    },
  };

})(document, window, jQuery);