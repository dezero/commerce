(function(document, window, $) {

  // Product Video SlidePanel global object
  // -------------------------------------------------------------------------------------------  
  $.dzProductVideo = {

    // Properties
    // -------------------------------
    _slidePanel: null,
    link_id: null,
    action: null,

    // Load SlidePanel actions for videos
    // --------------------------------------
    slidePanel: function() {
      var $actions = $("#product-video-table").children("tbody").children("tr").children('td.actions-column');

      // DELETE action
      $actions.children('.delete-action').on('click', function(e){
        e.preventDefault();
        $.dzProductVideo.action = 'delete';
        
        var self = this;
        $.dzSlideTable.delete(self, {
          confirmMessage: '<h3>Are your sure you want to <span class="text-danger">DELETE</span> this video?</h3>',
          successMessage: 'Video removed successfully',
          
          // Params sent as INPUT params via AJAX delete action
          ajaxData: JSON.stringify({
            product_id: $(self).data('product'),
            link_id: $(self).data('link')
          }),

          // After delete event -> Reload video table
          afterDelete: function(data) {
            $.dzProductVideo.refreshTable();
          }
        });
      });

      // UPDATE action
      $actions.children('.update-action').on('click', function(e){      
        e.preventDefault();
        $.dzProductVideo.action = 'update';
        $(this).tooltip('hide');

        // Show SLIDE PANEL clicking on an action button from the video list table
        $(this).dzSlidePanel({
          // afterLoad: $.dzProductVideo.afterLoad
          afterLoad: $.dzProductImage.afterLoad
        });
      });

      // CREATE action
      $('#product-video-create-btn').on('click', function(e){
        e.preventDefault();
        $.dzProductVideo.action = 'create';
        $(this).tooltip('hide');

        // Show SLIDE PANEL clicking on an action button from the video list table
        $(this).dzSlidePanel({
          // afterLoad: $.dzProductVideo.afterLoad
          afterLoad: $.dzProductImage.afterLoad
        });
      });
    },

    // Refresh/reload product video table
    // ---------------------------------------------------
    refreshTable: function(params) {
      // Default options
      var options = {
        afterAjaxUpdate: function(id, data) {
          // $('html, body').animate({scrollTop: 0}, 100);
          $.dzProductVideo.slidePanel();
        }
      };

      // Add input params
      if ( ! $.isEmptyObject(params) ) {
        $.extend(options, params);
      }

      // Reload table
      $.dzSlideTable.reload('product-video-table', options);
    },

    /*
    // Load Product Video form (LOADED FROM dz.productImage.js)
    // ---------------------------------------------------
    afterLoad: function(){
      var self = this;
      $.dzProductVideo._slidePanel = this;

      // Start scroll
      $.dzSlidePanel.startScroll();

      // Get current action from data-action attribute on <div class="product-video-slidepanel-wrapper">
      $.dzProductVideo.action = self.$panel.find('#product-video-slidepanel-action').eq(0).data('action');
      var que_action = $.dzProductVideo.action;

      // CREATE or UPDATE - After slidePanel loaded
      if ( que_action == 'create' || que_action == 'update' )
      {
        // Save button
        $('#product-video-save-btn').on('click', $.dzProductVideo.submitForm);
      }
    },
    */

    // Submit Product Video form
    // -------------------------------
    submitForm: function(e){
      e.preventDefault();
      var $this = $(this);
      var self = $.dzProductVideo._slidePanel;

      // Disable button to avoid click twice
      $this.prop('disabled', true);

      // Hide errors
      self.$panel.find('#slidepanel-errors').removeClass('hide').addClass('hide');

      // Save via AJAX
      $.ajax({
        url: $this.data('url'),
        type: 'POST',
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({
          product_id: $this.data('product'),
          link_id: $this.data('link'),
          is_video: 1,
          form_data: self.$panel.find('#product-video-form').serialize()
        }),
        success: function(data) {
          if ( data.error_code == 0 && data.link_id != 0) {
            // Hide slidePanel
            $.slidePanel.hide();

            // Show success message
            $.pnotify({
              sticker: false,
              text: 'Video saved successfully',
              type: 'success'
            });

            // Save new link_id
            $.dzProductVideo.link_id = data.link_id;

            // Reload video table
            $.dzProductVideo.refreshTable({
              afterAjaxUpdate: function(id, data) {
                // $('html, body').animate({scrollTop: 0}, 100);
                $.dzProductVideo.slidePanel();
              }
            });
          }
          else {
            // Show errors
            $.dzSlidePanel.showErrors(data.error_msg);

            // Enable button again
            $this.prop('disabled', false);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          // Show errors
          $.dzSlidePanel.showErrors('ERROR - Unable to save video');

          // Enable button again
          $this.prop('disabled', false);
        }
      });
    },
  };

})(document, window, jQuery);