<?php
/**
 * RedsysGateway
 * 
 * Payment via Redsys TPV
 */

namespace dzlab\commerce\gateways;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\Gateway;
use dzlab\commerce\models\Transaction;
use Yii;

class RedsysGateway extends Gateway
{
    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Create a new "purchase" Transaction
     */
    public function purchase($order_model, $payment_amount, $vec_input_data = [])
    {
        $vec_output = [
            'error_msg'         => '',
            'error_code'        => 0,
            'transaction_id'    => 0,
            'gateway'           => 'redsys'
        ];

        if ( $order_model )
        {
            // Current transaction number
            $transaction_num = $order_model->transactionsTotal;
            
            // Merchant Parameters for Redsys TPV
            $vec_input_data['Ds_MerchantParameters'] = Yii::app()->redsys
                ->set_order($order_model, $transaction_num)
                ->set_amount($payment_amount)
                ->get_merchant_parameters();

            if ( !empty($vec_input_data['Ds_MerchantParameters']) )
            {
                // Create new transaction model
                $transaction_model = Yii::createObject(Transaction::class);
                $transaction_model->setAttributes([
                    'order_id'              => $order_model->order_id,
                    'order_transaction_num' => $transaction_num,
                    'gateway_id'            => $this->gateway_id,
                    'gateway_alias'         => $this->alias,            // "redsys"
                    'status_type'           => 'pending',
                    'transaction_type'      => 'purchase',
                    'amount'                => !empty($payment_amount) ? $payment_amount : null,
                    'request_json'          => !empty($vec_input_data) ? Json::encode($vec_input_data) : null,
                    'gateway_reference'     => $this->get_order_reference($order_model->order_id, $transaction_num)
                ]);

                if ( ! $transaction_model->save() )
                {
                    Log::save_model_error($transaction_model);
                    
                    $vec_output['error_code'] = 603;
                    $vec_output['error_msg'] = 'No se puede realizar el pago. Inténtelo de nuevo';            
                }
                else
                {
                    // Add output data
                    $vec_output['transaction_id'] = $transaction_model->transaction_id;
                    $vec_output['merchant_parameters'] = $vec_input_data['Ds_MerchantParameters'];
                    $vec_output['signature'] = Yii::app()->redsys->get_signature();
                    $vec_output['total_price'] = Yii::app()->number->formatNumber($payment_amount);
                }
            }
            else
            {
                $vec_output['error_code'] = 604;
                $vec_output['error_msg'] = 'No se puede conectar con el banco. Inténtelo de nuevo';
            }
        }

        return $vec_output;
    }


    /**
     * Process the response from the Gateway
     */
    public function response($order_model, $transaction_type = 'purchase', $payment_amount = null, $vec_input_data = [])
    {
        // Only process new payments if the order hasn't been yet paid
        if ( $order_model && ! $order_model->is_paid() && !empty($vec_input_data) )
        {
            // Get last pending transaction
            $transaction_model = $order_model->get_pending_transaction();

            // Strange situation: Payment data received from Gateway but Transaction model does not exist
            if ( ! $transaction_model )
            {
                Log::redsys("\n#". $order_model->order_id ." --> WARNING - Payment data received from Gateway TPV but Transaction model does not exist");

                // Create new transaction
                $vec_output = Yii::app()->checkoutManager->create_transaction($order_model, $transaction_type, $payment_amount, $vec_input_data);
                if ( !empty($vec_output) && isset($vec_output['transaction_id']) )
                {
                    $transaction_model = Transaction::findOne($vec_output['transaction_id']);
                }
            }

            if ( $transaction_model && $transaction_model->status_type === 'pending' && !empty($vec_input_data) && is_array($vec_input_data) && isset($vec_input_data['Ds_Response'])  )
            {
                // Get order reference from TPV
                $encoded_order_id = null;
                if ( isset($vec_input_data['Ds_Order']) )
                {
                    $encoded_order_id = $vec_input_data['Ds_Order'];
                }
                else if ( isset($vec_input_data['DS_ORDER']) )
                {
                    $encoded_order_id = $vec_input_data['DS_ORDER'];
                }
                else if ( isset($vec_input_data['DS_MERCHANT_ORDER']) )
                {
                    $encoded_order_id = $vec_input_data['DS_MERCHANT_ORDER'];
                }

                $transaction_model->setAttributes([
                    'response_code'     => $vec_input_data['Ds_Response'],
                    'response_json'     => Json::encode($vec_input_data),
                    'error_code'        => isset($vec_input_data['Ds_ErrorCode']) ? $vec_input_data['Ds_ErrorCode'] : null,
                    'gateway_reference' => $encoded_order_id
                ]);

                if ( isset($vec_input_data['Ds_Amount']) && $vec_input_data['Ds_Amount'] != Yii::app()->redsys->encode_amount($transaction_model->raw_attributes['amount']) )
                {
                    Log::redsys("\n#". $order_model->order_id ." --> ERROR - Amounts are different. Registered: ". $transaction_model->raw_attributes['amount'] ." vs Received: ". $vec_input_data['Ds_Amount']);
                }

                // ACCEPT or REJECT the Transaction model
                $responde_code = (int)$vec_input_data['Ds_Response'];
                if ( $responde_code == 0 )
                {
                    return $transaction_model->accepted();
                }
                else
                {
                    return $transaction_model->rejected();
                }
            }
        }

        return false;
    }


    /**
     * Get order reference for this Gateway
     */
    public function get_order_reference($order_id, $transaction_num = 0)
    {
        return Yii::app()->redsys->get_encoded_order($order_id, $transaction_num);
    }


    /**
     * Process an array from the request data of a Transaction model
     */
    public function view_request_data($vec_data, $transaction_model = null)
    {
        // Remove "Ds_MerchantParameters" for all the users except SUPERADMIN
        if ( Yii::app()->user->id != 1 && isset($vec_data['Ds_MerchantParameters']) )
        {
            unset($vec_data['Ds_MerchantParameters']);
        }

        // Add all the parameters into the request
        if ( $transaction_model && $transaction_model->order )
        {
            Yii::app()->redsys
                ->set_order($transaction_model->order, $transaction_model->order_transaction_num)
                ->set_amount($transaction_model->order->get_total_price());

            if ( !empty(Yii::app()->redsys->vec_merchant_params) )
            {
                $vec_data = \CMap::mergeArray($vec_data, Yii::app()->redsys->vec_merchant_params);
            }
        }

        return $vec_data;
    }
}