<?php
/**
 * DummyGateway
 * 
 * Dummy payment gateway (for testing purposes)
 */

namespace dzlab\commerce\gateways;

use dz\base\ApplicationComponent;
use dz\db\DbCriteria;
use dz\helpers\Json;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\commerce\models\Gateway;
use dzlab\commerce\models\Transaction;
use Yii;

class DummyGateway extends Gateway
{
    /**
     * Constructor
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Create a new "purchase" Transaction
     */
    public function purchase($order_model, $payment_amount, $vec_input_data = [])
    {
        $vec_output = [
            'error_msg'         => '',
            'error_code'        => 0,
            'transaction_id'    => 0,
            'gateway'           => 'dummy'
        ];

        if ( $order_model )
        {
            // Current transaction number
            $transaction_num = $order_model->transactionsTotal;

            // Create new transaction model
            $transaction_model = Yii::createObject(Transaction::class);
            $transaction_model->setAttributes([
                'order_id'              => $order_model->order_id,
                'order_transaction_num' => $transaction_num,
                'gateway_id'            => $this->gateway_id,
                'gateway_alias'         => $this->alias,            // "dummy"
                'status_type'           => 'pending',
                'transaction_type'      => 'purchase',
                'amount'                => !empty($payment_amount) ? $payment_amount : null,
                'request_json'          => !empty($vec_input_data) ? Json::encode($vec_input_data) : null,
            ]);

            if ( ! $transaction_model->save() )
            {
                Log::save_model_error($transaction_model);
                
                $vec_output['error_code'] = 603;
                $vec_output['error_msg'] = 'No se puede realizar el pago. Inténtelo de nuevo';            
            }
            else
            {
                // Add output data
                $vec_output['total_price'] = $order_model->total_price;
            }
        }

        return $vec_output;
    }


    /**
     * Process the response from the Gateway
     */
    public function response($order_model, $transaction_type = 'purchase', $payment_amount = null, $vec_input_data = [])
    {
        // Only process new payments if the order hasn't been yet paid
        if ( $order_model && ! $order_model->is_paid() )
        {
            // Get last pending transaction
            $transaction_model = $order_model->get_pending_transaction();

            // Strange situation: Payment data received from Gateway but Transaction model does not exist
            if ( ! $transaction_model )
            {
                Log::warning("\n#". $order_model->order_id ." --> WARNING - Payment data received from Gateway but Transaction model does not exist");

                // Create new transaction
                $vec_output = Yii::app()->checkoutManager->create_transaction($order_model, $transaction_type, $payment_amount, $vec_input_data);
                if ( !empty($vec_output) && isset($vec_output['transaction_id']) )
                {
                    $transaction_model = Transaction::findOne($vec_output['transaction_id']);
                }
            }

            if ( $transaction_model && $transaction_model->status_type === 'pending' )
            {
                $transaction_model->setAttributes([
                    'response_json' => Json::encode($vec_input_data),
                ]);

                // Simulate an ACCEPTED transaction
                return $transaction_model->accepted();
            }
        }

        return false;
    }
}