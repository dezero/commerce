<?php
/**
 * ProductSearchCommand class file
 *
 * This commmand is used to manage ProductSearch models
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link https://www.dezero.es
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package commands
 */

namespace dzlab\commerce\commands;

use dz\console\CronCommand;
use dzlab\commerce\models\Product;
use dzlab\commerce\models\SearchProduct;
use Yii;

class ProductSearchCommand extends CronCommand
{
    /**
     * Reload product search data
     *
     * ./yiic productSearch reload
     */
    public function actionReload()
    {
         // Memory limit
        ini_set('memory_limit', '1024M');

        // Get database table names from the models
        $search_table = SearchProduct::model()->tableName();

        // Remove current search resutls => TRUNCATE TABLE
        Yii::app()->db->createCommand()->truncateTable($search_table);

        echo "Truncanted table ". $search_table ."\n";

        // Reload ALL search results
        $this->_reload_products();
    }


    /**
     * Reload data from product
     */
    private function _reload_products()
    {
        echo "Reloading product data search...\n";
        
        $vec_product_models = Product::model()->findAll();
        if ( $vec_product_models )
        {
            foreach ( $vec_product_models as $product_model )
            {
                $product_model->update_search_product();
            }
            echo "Products: ". count($vec_product_models) ." inserted\n";
        }
    }
}