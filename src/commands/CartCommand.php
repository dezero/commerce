<?php
/**
 * CartCommand class file
 *
 * This commmand is used to manage carts (Order models)
 *
 * @author Fabián Ruiz <fabian.ruiz@gmail.com>
 * @link https://www.dezero.es
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package commands
 */

namespace dzlab\commerce\commands;

use dz\console\CronCommand;
use dz\helpers\Log;
use dzlab\commerce\models\Order;
use Yii;

class CartCommand extends CronCommand
{
    /**
     * Remove abandoned carts (30 days with no activity)
     *
     * ./yiic cart clean
     */
    public function actionClean()
    {
        $one_month_ago = time() - (31 * 86400);
        $vec_order_models = Order::get()
            ->where(['status_type' => 'cart'])
            ->andWhere('created_date < '. $one_month_ago)
            ->order('created_date ASC')
            ->all();

        if ( !empty($vec_order_models) )
        {
            $log_message = "Detected ". count($vec_order_models) ." abondoned carts...\n";

            foreach ( $vec_order_models as $order_model )
            {
                if ( $order_model->delete() )
                {
                    $log_message .= " - Deleted cart #{$order_model->order_id} created at {$order_model->created_date}\n";
                }
                else
                {
                    $log_message .= " - ERROR deleting cart #{$order_model->order_id}}\n";
                }
            }

            // Save results to log
            Log::commerce_abandoned_carts($log_message);
        }
    }
}
