# Commerce module for Dz Framework

## Installation

Add these lines to composer.json file:

```shell
"require": {
    ...
    "dezero/commerce": "dev-master"
    ...
},
"repositories":[
    ...
    {
        "type": "vcs",
        "url" : "git@bitbucket.org:dezero/commerce.git"
    }
    ...
]
```
